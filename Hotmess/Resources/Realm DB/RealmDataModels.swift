//
//  RealmDataModels.swift
//  Hotmess
//
//  Created by Flaxon on 11/06/21.
//

import RealmSwift

class UserRealm: Object {
    @objc dynamic var userFirstName: String = ""
    @objc dynamic var userLastName: String = ""
    @objc dynamic var userEmail: String = ""
    @objc dynamic var userMobileNumber: String = ""
    @objc dynamic var userImageURL: String = ""
    @objc dynamic var userId: String = ""
    @objc dynamic var verifiedEmailID: Bool = false
    @objc dynamic var verifiedMobileNumeber: Bool = false
    @objc dynamic var countryCode: String = ""
    @objc dynamic var userSession: String = ""
    @objc dynamic var accessToken: String = ""
    @objc dynamic var idToken: String = ""
    @objc dynamic var refreshToken: String = ""
}
class WhatIsNewRealmModel: Object {
    @objc dynamic var whatIsNewTitle: String = ""
    @objc dynamic var whatIsNewSubTitle: String = ""
    @objc dynamic var buttonText: String = ""
    @objc dynamic var totalCollections: String = ""
    var whatIsNewCollection = List<WhatIsNewCollection>()
}
class WhatIsNewCollection: Object {
    @objc dynamic var whatIsNewProductName: String = ""
    @objc dynamic var whatIsNewImageUrl: String = ""
    @objc dynamic var skuID: String = ""
    @objc dynamic var productID: String = ""
}
class FeaturedProductRealmModel: Object {
    @objc dynamic var featuredProductTitle: String = ""
    @objc dynamic var featuredProductSubTitle: String = ""
    @objc dynamic var featuredProductDescription: String = ""
    @objc dynamic var featuredProductImageUrl: String = ""
    @objc dynamic var buttonText: String = ""
    @objc dynamic var filterList: String = ""
}
class DressRefreshRealmModel: Object {
    @objc dynamic var dressRefreshTitle: String = ""
    @objc dynamic var dressRefreshSubTitle: String = ""
    @objc dynamic var dressRefreshDecsription: String = ""
    var dressRefreshItems = List<DressRefreshItems>()
}
class DressRefreshItems: Object {
    @objc dynamic var dressRefreshMoodTitle: String = ""
    @objc dynamic var dressRefreshImageUrl: String = ""
    @objc dynamic var filterList: String = ""
}
class MoodCollectionsRealm: Object {
    @objc dynamic var moodCollectionsTitle: String = ""
    @objc dynamic var moodCollectionsSubTitle: String = ""
    @objc dynamic var moodCollectionsDescription: String = ""
    var placeHolderImageOne: PlaceHolderImageOneRealm!
    var placeHolderImageTwo: PlaceHolderImageTwoRealm!
    var placeHolderImageThree: PlaceHolderImageThreeRealm!
    @objc dynamic var buttonText: String = ""
    var curatedList = [String]()
}
class PlaceHolderImageOneRealm: Object {
    @objc dynamic var imageUrl: String = ""
    @objc dynamic var searchKey: String = ""
    @objc dynamic var searchValue: String = ""
}
class PlaceHolderImageTwoRealm: Object {
    @objc dynamic var imageUrl: String = ""
    @objc dynamic var searchKey: String = ""
    @objc dynamic var searchValue: String = ""
}
class PlaceHolderImageThreeRealm: Object {
    @objc dynamic var imageUrl: String = ""
    @objc dynamic var searchKey: String = ""
    @objc dynamic var searchValue: String = ""
}
class CelebrityPicksRealm: Object {
    @objc dynamic var celebrityPicksTitle: String = ""
    @objc dynamic var celebrityPicksSubTitle: String = ""
    @objc dynamic var celebrityPicksDescription: String = ""
    var placeHolderImage: PlaceHolderImageRealm!
    @objc dynamic var searchKey: String = ""
    @objc dynamic var searchValue: String = ""
    @objc dynamic var buttonText: String = ""
    @objc dynamic var filterList: String = ""
}
class PlaceHolderImageRealm: Object {
    @objc dynamic var image: String = ""
    @objc dynamic var searchKey: String = ""
    @objc dynamic var searchValue: String = ""
}
class TrendingProductMetaDataRealm: Object {
    @objc dynamic var trendingProductTitle: String = ""
    @objc dynamic var trendingProductSubTitle: String = ""
    @objc dynamic var trendingProductDescription: String = ""
    var productListItem = List<ProductListRealm>()
}
class HotmessCollectionMetaDataRealm: Object {
    @objc dynamic var hotmessCollectionTitle: String = ""
    @objc dynamic var hotmessCollectionSubTitle: String = ""
    @objc dynamic var hotmessCollectionDescription: String = ""
    var hotmessProductListItem = List<HotmessProductListRealm>()
}
class HotmessProductListRealm: Object {
    @objc dynamic var productImageURL: String = ""
    @objc dynamic var productName: String = ""
    @objc dynamic var productDescription: String = ""
    @objc dynamic var productPrice: Int64 = 0
    @objc dynamic var productID: String = ""
    @objc dynamic var skuID: String = ""
}

class ProductListRealm: Object {
    @objc dynamic var productID: String = ""
    @objc dynamic var productName: String = ""
    @objc dynamic var designer: String = ""
    @objc dynamic var createdAt: Int64 = 0
    @objc dynamic var productImageURL: String = ""
    @objc dynamic var updatedAt: Int64 = 0
    @objc dynamic var productSize: String = ""
    @objc dynamic var categoryName: String = ""
    @objc dynamic var editorNote: String = ""
    @objc dynamic var productDescription: String = ""
    @objc dynamic var productStatus: String = ""
    @objc dynamic var productSizeFit: String = ""
    @objc dynamic var isRecommended: Bool = false
    @objc dynamic var productOfferPrice: Int64 = 0
    @objc dynamic var productPrice: Int64 = 0
    @objc dynamic var productDesignerName: String = ""
    @objc dynamic var productCategoryID: String = ""
    @objc dynamic var productPriceGuideID: String = ""
    @objc dynamic var productDesignerURL: String = ""
    @objc dynamic var isnew: Bool = false
    @objc dynamic var productCategoryURL: String = ""
    @objc dynamic var productAltTag: String = ""
    @objc dynamic var productDetailsCare: String = ""
    @objc dynamic var stockKeepUnitID: String = ""
    @objc dynamic var productColour: String = ""
    var curatedList = List<String>()
    var variants = List<ProductVariantsRealm>()
}
class ProductDetailRealm: Object {
    @objc dynamic var productID: String = ""
    @objc dynamic var productName: String = ""
    @objc dynamic var designer: String = ""
    @objc dynamic var createdAt: Int64 = 0
    @objc dynamic var productImageURL: String = ""
    @objc dynamic var updatedAt: Int64 = 0
    @objc dynamic var productSize: String = ""
    @objc dynamic var categoryName: String = ""
    @objc dynamic var editorNote: String = ""
    @objc dynamic var productDescription: String = ""
    @objc dynamic var productStatus: String = ""
    @objc dynamic var productSizeFit: String = ""
    @objc dynamic var isRecommended: Bool = false
    @objc dynamic var productOfferPrice: Int64 = 0
    @objc dynamic var productInStock: Bool = false
    @objc dynamic var productPrice: Int64 = 0
    @objc dynamic var productDesignerName: String = ""
    @objc dynamic var productCategoryID: String = ""
    @objc dynamic var productPriceGuideID: String = ""
    @objc dynamic var productDesignerURL: String = ""
    @objc dynamic var isnew: Bool = false
    @objc dynamic var productSizeGuide: Bool = false
    @objc dynamic var productCategoryURL: String = ""
    @objc dynamic var productAltTag: String = ""
    @objc dynamic var productDetailsCare: String = ""
    @objc dynamic var stockKeepUnitID: String = ""
    @objc dynamic var productColour: String = ""
    @objc dynamic var productColourCode: String = ""
    var assets = List<ProductAssestsRealm>()
    var sizes = List<ProductSizeRealm>()
    var variants = List<ProductVariantsRealm>()
}
class ProductSizeRealm: Object {
    @objc dynamic var size: String = ""
    @objc dynamic var stockKeepUnitID: String = ""
    @objc dynamic var productID: String = ""
    var sizeAttributes = List<SizeAttributesRealm>()
}
class SizeAttributesRealm: Object {
    @objc dynamic var key: String = ""
    @objc dynamic var value: Int64 = 0
}
class ProductVariantsRealm: Object {
    @objc dynamic var productID: String = ""
    @objc dynamic var colourIcon: String = ""
    @objc dynamic var colour: String = ""
    @objc dynamic var sizeID: String = ""
    @objc dynamic var offerPrice: Int64 = 0
    @objc dynamic var productPrice: Int64 = 0
    @objc dynamic var productSize: String = ""
    @objc dynamic var colourID: String = ""
    @objc dynamic var stockKeepUnitID: String = ""
    @objc dynamic var colourCode: String = ""
    var assets = List<ProductAssestsRealm>()
}
class ProductAssestsRealm: Object {
    @objc dynamic var altText: String = ""
    @objc dynamic var type: String = ""
    @objc dynamic var imageURL: String = ""
    @objc dynamic var thumbnailImageURL: String = ""
}
class ShopCategoryRealm: Object {
    @objc dynamic var parentCategoryID: String = ""
    @objc dynamic var updatedAt: Int64 = 0
    @objc dynamic var createdAt: Int64 = 0
    @objc dynamic var accountID: String = ""
    @objc dynamic var productCategoryImageURL: String = ""
//    var productCategory = List<ProductCategoryRealm>()
}
class ProductCategoryRealm: Object {
    @objc dynamic var redirectURL: String = ""
    @objc dynamic var searchValue: String = ""
    @objc dynamic var searchKey: String = ""
    @objc dynamic var productCategoryImageURL: String = ""
}
class ShoppingBagRealm: Object {
    @objc dynamic var productID: String = ""
    @objc dynamic var stockKeepUnitID: String = ""
    @objc dynamic var productImageURL: String = ""
    @objc dynamic var productName: String = ""
    @objc dynamic var productDescription: String = ""
    @objc dynamic var productOfferPrice: Int64 = 0
    @objc dynamic var productPrice: Int64 = 0
    @objc dynamic var productDesignerName: String = ""
    @objc dynamic var productColour: String = ""
    @objc dynamic var productSize: String = ""
    @objc dynamic var productQuantity: Int64 = 0
    @objc dynamic var productColorCode: String = ""
    @objc dynamic var productInStock: Bool = false
}
class WishlishRealm: Object {
    @objc dynamic var productID: String = ""
    @objc dynamic var stockKeepUnitID: String = ""
    @objc dynamic var productImageURL: String = ""
    @objc dynamic var productName: String = ""
    @objc dynamic var productDescription: String = ""
    @objc dynamic var productOfferPrice: Int64 = 0
    @objc dynamic var productPrice: Int64 = 0
    @objc dynamic var productDesignerName: String = ""
    @objc dynamic var productColour: String = ""
    @objc dynamic var productSize: String = ""
    @objc dynamic var productColourCode: String = ""
    @objc dynamic var productInStock: Bool = false
    @objc dynamic var productStock: Int64 = 0
}
class ShippingAddressRealm: Object {
    @objc dynamic var addressID: String = ""
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var mobileNumber: String = ""
    @objc dynamic var addressLine1: String = ""
    @objc dynamic var addressLine2: String = ""
    @objc dynamic var areaPinCode: String = ""
    @objc dynamic var landMark: String = ""
    @objc dynamic var city: String = ""
    @objc dynamic var state: String = ""
    @objc dynamic var country: String = ""
    @objc dynamic var countryCode: String = ""
    @objc dynamic var isDefaultAddress: Bool = false
    @objc dynamic var isSelected: Bool = false
}
class CalculateOrderPriceRealm: Object {
    @objc dynamic var total: Double = 0.0
}
class SearchTextRealm: Object {
    @objc dynamic var searchText: String = ""
}
