//
//  RealmDataManager.swift
//  Hotmess
//
//  Created by Flaxon on 11/06/21.
//

import RealmSwift

class RealmDataManager {
    private var hotmessRealm: Realm!
    static let sharedInstance = RealmDataManager()
    private init() {
        do {
            hotmessRealm = try Realm()
        } catch {
            print("Error initializing realm")
        }
    }
    // MARK: - To Delete the UserData from DB
    func deleteUserDetails() {
        let userDetails = hotmessRealm.objects(UserRealm.self)
        do {
            try hotmessRealm.write {
                hotmessRealm.delete(userDetails)
            }
        } catch {
            print("Failed To Delete the UserData from DB")
        }
    }
    // MARK: - To Save All the UserData
    func saveUserDetails(details: UserRealm) {
        do {
            try hotmessRealm.write {
                hotmessRealm.add(details)
            }
        } catch {
            print("Failed To Save All the UserData")
        }
    }
    // MARK: - Update User Details After OTP
    func updateUserDetails(accessToken: String, refreshToken: String, idToken: String) {
        let userDetails = hotmessRealm.objects(UserRealm.self).first
        do {
            try hotmessRealm.write {
                userDetails?.accessToken = accessToken
                userDetails?.refreshToken = refreshToken
                userDetails?.idToken = idToken
            }
        } catch {
            print("Failed To Update the UserData to DB")
        }
    }
    // MARK: - Get User Details
    func getUserDetails() -> UserRealm {
        guard let userDetails = hotmessRealm.objects(UserRealm.self).first else { return UserRealm() }
        return userDetails
    }
    // MARK: - Update Session for Resend OTP
    func updateUserSession(session: String, email: String) {
        let userDetails = hotmessRealm.objects(UserRealm.self).first
        do {
            try hotmessRealm.write {
                userDetails?.userSession = session
                userDetails?.userEmail = email
            }
        } catch {
            print("Failed To Update the UserData to DB")
        }
    }
    func getAllProductList() -> [ProductListRealm] {
        var products = [ProductListRealm]()
        let allProducts = hotmessRealm.objects(ProductListRealm.self)
        for product in allProducts {
            products.append(product)
        }
        return products
    }
    func isProductInBag(productID: String, skuID: String) -> Bool {
        var isFound: Bool = false
        let allProducts = hotmessRealm.objects(ShoppingBagRealm.self)
        for product in allProducts where product.productID == productID && product.stockKeepUnitID == skuID {
            isFound = true
        }
        return isFound
    }
    func getAllProductsFromBag() -> [ShoppingBagRealm] {
        var arrayOfItems = [ShoppingBagRealm]()
        let allProducts = hotmessRealm.objects(ShoppingBagRealm.self)
        for product in allProducts {
            arrayOfItems.append(product)
        }
        return arrayOfItems
    }
    func saveProductToBag(productInfo: ShoppingBagRealm) {
        do {
            try hotmessRealm.write {
                hotmessRealm.add(productInfo)
            }
        } catch {
            print("Failed To Save Product In DBagB")
        }
    }
    func deleteAllFromBag() {
        let shoppingBag = hotmessRealm.objects(ShoppingBagRealm.self)
        do {
            try hotmessRealm.write {
                hotmessRealm.delete(shoppingBag)
            }
        } catch {
            print("Failed To Delete the Shopping Bag Items")
        }
    }
    func increaseProductQuantity(productId: String, skuId: String) {
        guard let shoppingBagItem = hotmessRealm.objects(ShoppingBagRealm.self).filter(
                "productID = %@ AND stockKeepUnitID = %@", productId, skuId).first else { return }
        do {
            try hotmessRealm.write {
                shoppingBagItem.productQuantity += 1
            }
        } catch {
            print("Error updating quantity")
        }
    }
    func decreaseProductQuantity(productId: String, skuId: String) {
        guard let shoppingBagItem = hotmessRealm.objects(ShoppingBagRealm.self).filter(
                "productID = %@ AND stockKeepUnitID = %@", productId, skuId).first else { return }
        do {
            try hotmessRealm.write {
                shoppingBagItem.productQuantity -= 1
            }
        } catch {
            print("Error updating quantity")
        }
    }
    func deleteFromBag(productId: String, skuID: String) {
        guard let shoppingBag = hotmessRealm.objects(ShoppingBagRealm.self).filter(
                "productID = %@ AND stockKeepUnitID = %@",
                productId, skuID).first else { return }
        do {
            try hotmessRealm.write {
                hotmessRealm.delete(shoppingBag)
            }
        } catch {
            print("Failed To Delete the Shopping Bag Items")
        }
    }
    // MARK: - Get Product Details
    func getProductDetails() -> ProductDetailRealm {
        guard let productDetails = hotmessRealm.objects(ProductDetailRealm.self).first
        else { return ProductDetailRealm() }
        return productDetails
    }
    // MARK: - Wishlist
    func getAllProductsFromWishlist() -> [WishlishRealm] {
        var arrayOfItems = [WishlishRealm]()
        let allProducts = hotmessRealm.objects(WishlishRealm.self)
        for product in allProducts {
            arrayOfItems.append(product)
        }
        return arrayOfItems
    }
    func saveProductToWishlist(productInfo: WishlishRealm) {
        do {
            try hotmessRealm.write {
                hotmessRealm.add(productInfo)
            }
        } catch {
            print("Failed To Save Product In Wishlist")
        }
    }
    func deleteAllFromWishlist() {
        let wishList = hotmessRealm.objects(WishlishRealm.self)
        do {
            try hotmessRealm.write {
                hotmessRealm.delete(wishList)
            }
        } catch {
            print("Failed To Delete the Wishlist Items")
        }
    }
    func isProductInWishlist(productID: String, skuID: String) -> Bool {
        var isFound: Bool = false
        let allProducts = hotmessRealm.objects(WishlishRealm.self)
        for product in allProducts where product.productID == productID && product.stockKeepUnitID == skuID {
            isFound = true
        }
        return isFound
    }
    func deleteFromWishlist(productId: String, skuID: String) {
        guard let wishList = hotmessRealm.objects(
                WishlishRealm.self).filter("productID = %@ AND stockKeepUnitID = %@",
                                           productId, skuID).first else { return }
        do {
            try hotmessRealm.write {
                hotmessRealm.delete(wishList)
            }
        } catch {
            print("Failed To Delete the Wishlist Items.")
        }
    }
    func saveMultipleToWishList(items: [WishlishRealm]) {
        for item in items {
            do {
                try hotmessRealm.write {
                    hotmessRealm.add(item)
                }
            } catch {
                print("Failed To Save Multiple Products into Wishlist.")
            }
        }
    }
    func updateUserTokensOnRefresh(accessToken: String, idToken: String) {
        let userDetails = hotmessRealm.objects(UserRealm.self).first
        do {
            try hotmessRealm.write {
                userDetails?.accessToken = accessToken
                userDetails?.idToken = idToken
            }
        } catch {
            print("Failed To Update the UserData to DB")
        }
    }
    func getRefreshToken() -> String {
        let userDetails = hotmessRealm.objects(UserRealm.self).first
        return userDetails?.refreshToken ?? ""
    }
    func updateUserProfileDetails(firstName: String, lastName: String, imageUrl: String, mobileNumber: String) {
        let userDetails = hotmessRealm.objects(UserRealm.self).first
        do {
            try hotmessRealm.write {
                userDetails?.userFirstName = firstName
                userDetails?.userLastName = lastName
                userDetails?.userImageURL = imageUrl
                userDetails?.userMobileNumber = mobileNumber
            }
        } catch {
            print("Failed To Update the UserData to DB")
        }
    }
    func saveSearchText(searchText: SearchTextRealm) {
        let allTextSearch = hotmessRealm.objects(SearchTextRealm.self)
        var shouldAdd: Bool = true
        for text in allTextSearch where text.searchText == searchText.searchText {
            shouldAdd = false
        }
        if shouldAdd {
            do {
                try hotmessRealm.write {
                    hotmessRealm.add(searchText)
                }
            } catch {
                print("Failed To Svae in Search text DB")
            }
        }
    }
    func getAllProductsFromTextSearch() -> [SearchTextRealm] {
        var arrayOfItems = [SearchTextRealm]()
        let allTextSearch = hotmessRealm.objects(SearchTextRealm.self)
        for searchItems in allTextSearch {
            arrayOfItems.append(searchItems)
        }
        return arrayOfItems
    }
}
extension RealmDataManager {
    // MARK: - To Delete All the UserData from DB
    func deleteAll() {
        let userDetails = hotmessRealm.objects(UserRealm.self)
        let whatIsNew = hotmessRealm.objects(WhatIsNewRealmModel.self)
        let featuredProducts = hotmessRealm.objects(FeaturedProductRealmModel.self)
        let dressRefresh = hotmessRealm.objects(DressRefreshRealmModel.self)
        let moodCollection = hotmessRealm.objects(MoodCollectionsRealm.self)
        let celebrityPicks = hotmessRealm.objects(CelebrityPicksRealm.self)
        let trendingProducts = hotmessRealm.objects(TrendingProductMetaDataRealm.self)
        let hotmessCollection = hotmessRealm.objects(HotmessCollectionMetaDataRealm.self)
        let wishList = hotmessRealm.objects(WishlishRealm.self)
        let searchText = hotmessRealm.objects(SearchTextRealm.self)
        let shoppingBag = hotmessRealm.objects(ShoppingBagRealm.self)
        let productList = hotmessRealm.objects(ProductListRealm.self)
        let productDetail = hotmessRealm.objects(ProductDetailRealm.self)
        let shopCategory = hotmessRealm.objects(ShopCategoryRealm.self)
        let shippingAddress = hotmessRealm.objects(ShippingAddressRealm.self)
        let orderList = hotmessRealm.objects(OrderListRealm.self)
        let orderView = hotmessRealm.objects(OrderRealm.self)
        do {
            try hotmessRealm.write {
                hotmessRealm.delete(userDetails)
                hotmessRealm.delete(wishList)
                hotmessRealm.delete(searchText)
                hotmessRealm.delete(shoppingBag)
                hotmessRealm.delete(whatIsNew)
                hotmessRealm.delete(featuredProducts)
                hotmessRealm.delete(dressRefresh)
                hotmessRealm.delete(moodCollection)
                hotmessRealm.delete(celebrityPicks)
                hotmessRealm.delete(trendingProducts)
                hotmessRealm.delete(hotmessCollection)
                hotmessRealm.delete(productList)
                hotmessRealm.delete(productDetail)
                hotmessRealm.delete(shopCategory)
                hotmessRealm.delete(shippingAddress)
                hotmessRealm.delete(orderList)
                hotmessRealm.delete(orderView)
            }
        } catch {
            print("Failed To Delete All the UserData from DB")
        }
    }
}
