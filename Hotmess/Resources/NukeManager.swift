//
//  NukeManager.swift
//  Hotmess
//
//  Created by Cedan Misquith on 14/07/21.
//

import UIKit
import Nuke

class NukeManager: NSObject {
    static let sharedInstance = NukeManager()
    let placeHolderImage = UIColor.lightGray.solidColorImage()
    func setImage(url: ImageRequestConvertible?, imageView: ImageDisplayingView, withPlaceholder: Bool) {
        let options = ImageLoadingOptions(
            placeholder: placeHolderImage,
            transition: .fadeIn(duration: 0.33)
        )
        if withPlaceholder {
            Nuke.loadImage(with: url, options: options, into: imageView)
        } else {
            Nuke.loadImage(with: url, into: imageView)
        }
    }
}
