//
//  DataModels.swift
//  Hotmess
//
//  Created by Flaxon on 14/06/21.
//

import UIKit

// MARK: - SignUp Data Model
class SignUpDataModel {
    var firstname: String?
    var lastname: String?
    var password: String?
    var confirmPassword: String?
    var email: String?
    var code: String?
    var mobileNumber: String?
    init(firstname: String, lastname: String, password: String,
         confirmPassword: String, email: String, code: String,
         mobileNumber: String) {
        self.firstname = firstname
        self.lastname = lastname
        self.password = password
        self.confirmPassword = confirmPassword
        self.email = email
        self.code = code
        self.mobileNumber = mobileNumber
    }
}
// MARK: - Updated Password Data Model
class UpdatePasswordModel {
    var newPassword: String?
    var confirmNewPassword: String?
    init(newPassword: String, confirmNewPassword: String) {
        self.newPassword = newPassword
        self.confirmNewPassword = confirmNewPassword
    }
}
// MARK: - Select Size Model
struct SizeModel {
    var sizeValue: String?
    var skuID: String!
    var productID: String!
    init(sizeValue: String, skuID: String, productID: String) {
        self.sizeValue = sizeValue
        self.skuID = skuID
        self.productID = productID
    }
}
// MARK: - Select Colour Model
struct ColourModel {
    var colourName: String?
    var colourHexCode: String?
    var skuID: String!
    var productID: String!
    init(colourName: String, colourHexCode: String, skuID: String, productID: String) {
        self.colourName = colourName
        self.colourHexCode = colourHexCode
        self.skuID = skuID
        self.productID = productID
    }
}
// MARK: - Order Summer Data  Models
struct OrderSummarySectionModel {
    var headerName: String?
    var sectionData: [OrderSummaryItemModel]!
    init(headerName: String, sectionData: [OrderSummaryItemModel]) {
        self.headerName = headerName
        self.sectionData = sectionData
    }
}
struct OrderSummaryItemModel {
    var type: OrderSummarySectionType?
    var data: Any!
    init(type: OrderSummarySectionType, data: Any) {
        self.type = type
        self.data = data
    }
}
struct SingleSelectOptionsModel {
    var title: String?
    var subtitle: String?
    var price: String?
    var imageName: String?
    var isSelected: Bool?
    init(title: String, subtitle: String, price: String, imageName: String, isSelected: Bool) {
        self.title = title
        self.subtitle = subtitle
        self.price = price
        self.imageName = imageName
        self.isSelected = isSelected
    }
}
// MARK: - Add Address Data Model
struct UserAddress {
    var firstName: String?
    var lastName: String?
    var mobileNumber: String?
    var addressLine1: String?
    var addressLine2: String?
    var landMark: String?
    var pinCode: String?
    var city: String?
    var state: String?
    var country: String?
    init(firstName: String, lastName: String?, mobileNumber: String, addressLine1: String,
         addressLine2: String, landMark: String, pinCode: String,
         city: String, state: String, country: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.mobileNumber = mobileNumber
        self.addressLine1 = addressLine1
        self.addressLine2 = addressLine2
        self.landMark = landMark
        self.pinCode = pinCode
        self.city = city
        self.state = state
        self.country = country
    }
}
