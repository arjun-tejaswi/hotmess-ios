//
//  Loader.swift
//  Hotmess
//
//  Created by Cedan Misquith on 23/06/21.
//

import UIKit
import Lottie

class Loader: UIView {
    var animationView: AnimationView?
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        animationView = AnimationView()
        animationView?.frame = CGRect(x: frame.width/2-50, y: frame.height/2-50, width: 100, height: 100)
        animationView?.animation = Animation.named("HotMess Loader")
        animationView?.loopMode = .loop
        animationView?.contentMode = .scaleAspectFit
        addSubview(animationView!)
        animationView?.play()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// class Loader: UIView {
//    // Animation View
//    var animationView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        // Set overlay bg color
//        self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
//        // Configuring Animation.
//        animationView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
//        animationView.startAnimating()
//        addSubview(animationView)
//    }
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
// }
