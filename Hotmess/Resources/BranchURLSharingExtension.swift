//
//  BranchURLSharingExtension.swift
//  Hotmess
//
//  Created by Cedan Misquith on 13/09/21.
//

import UIKit
import Branch

extension ProductOrderDetailViewController: UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }
    func activityViewController(_ activityViewController: UIActivityViewController,
                                itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return activityType
    }
    var linkProperty: BranchLinkProperties {
        let productId = presenter.productInfo.productID
        let skuId = presenter.productInfo.stockKeepUnitID
        let property = BranchLinkProperties()
        property.channel = "facebook"
        property.feature = "sharing"
        property.campaign = "content 123 launch"
        property.stage = "new user"
        property.tags = ["one", "two", "three"]
        property.addControlParam("custom_data", withValue: "yes")
        property.addControlParam("look_at", withValue: "this")
        property.addControlParam("nav_to", withValue: "over here")
        property.addControlParam("random", withValue: UUID.init().uuidString)
        property.addControlParam("product_id", withValue: productId)
        property.addControlParam("sku_id", withValue: skuId)
        return property
    }
    var universalObject: BranchUniversalObject {
        let buo = BranchUniversalObject(canonicalIdentifier: "product_id/\(presenter.productInfo.productID)")
        buo.title = presenter.productInfo.productDesignerName
        buo.contentDescription = presenter.productInfo.productDescription
        buo.imageUrl = presenter.productInfo.productImageURL
        buo.publiclyIndex = true
        buo.locallyIndex = true
        return buo
    }
    func share() {
        let productDesignerName = presenter.productInfo.productDesignerName.withReplacedCharacters(" ", by: "-")
        let productId = presenter.productInfo.productID
        let categoryName = presenter.productInfo.categoryName.withReplacedCharacters(" ", by: "-")
        let productDescription = presenter.productInfo.productDescription.withReplacedCharacters(" ", by: "-")
        let skuId = presenter.productInfo.stockKeepUnitID
        let desktopURL = ApiBaseURL.shareBaseURL +
            "shop/product/\(productDesignerName)/\(categoryName)/\(productDescription)/\(skuId)"
        print(desktopURL)
        let deeplinkPath = "product_id/\(productId)/sku_id/\(skuId)"
        linkProperty.addControlParam(BranchKeyConstants.branchDeepLinkPathKey, withValue: deeplinkPath)
        linkProperty.addControlParam("$desktop_url", withValue: desktopURL)
//        linkProperty.addControlParam(BranchKeyConstants.branchiOSURLKey, withValue: "http://example.com/ios")
//        linkProperty.addControlParam(BranchKeyConstants.branchAndroidURLKey, withValue: "http://example.com/android")
        universalObject.getShortUrl(with: linkProperty) { [weak self] url, error in
            if error != nil {
                print(error ?? "Error creating share product short url")
                return
            }
            if let url = url {
                let productShareMessage = "Check this out!\nHi, I found something on HotMess you might like - "
                self?.showSharePopUp(
                    stringURL: "\(productShareMessage)\(productDesignerName)\n\(url)")
                print(url)
            }
        }
    }
}

extension WishlistViewController: UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }
    func activityViewController(_ activityViewController: UIActivityViewController,
                                itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return activityType
    }
    var linkProperty: BranchLinkProperties {
        let property = BranchLinkProperties()
//        property.channel = "facebook"
//        property.feature = "sharing"
//        property.campaign = "content 123 launch"
//        property.stage = "new user"
//        property.tags = ["one", "two", "three"]
        property.addControlParam("custom_data", withValue: "yes")
        // property.addControlParam("look_at", withValue: "this")
        // property.addControlParam("nav_to", withValue: "over here")
        property.addControlParam("random", withValue: UUID.init().uuidString)
        property.addControlParam("wishlist_id", withValue: presenter.wishlistId)
        return property
    }
    var universalObject: BranchUniversalObject {
        let userDetails = RealmDataManager.sharedInstance.getUserDetails()
        let buo = BranchUniversalObject(canonicalIdentifier: "wishlist/\(presenter.wishlistId)")
        buo.title = "\(userDetails.userFirstName)'s Wishlist"
        buo.contentDescription = "\(userDetails.userFirstName) would like to share their wishlist with you."
        buo.imageUrl = presenter.wishlistArray.first?.productImageURL
        buo.publiclyIndex = true
        buo.locallyIndex = true
        return buo
    }
    func share() {
//        let productDesignerName = presenter.productInfo.productDesignerName
//        let productId = presenter.productInfo.productID
//        let categoryName = presenter.productInfo.categoryName
//        let skuId = presenter.productInfo.stockKeepUnitID
        let userDetails = RealmDataManager.sharedInstance.getUserDetails()
        let desktopURL = ApiBaseURL.shareBaseURL +
            "wishlist/\(presenter.wishlistId)"
        let deeplinkPath = "wishlist/\(presenter.wishlistId)"
        linkProperty.addControlParam(BranchKeyConstants.branchDeepLinkPathKey, withValue: deeplinkPath)
        linkProperty.addControlParam("$desktop_url", withValue: desktopURL)
        // linkProperty.addControlParam(StringConstants.branchiOSURLKey, withValue: "http://example.com/ios")
        // linkProperty.addControlParam(StringConstants.branchAndroidURLKey, withValue: "http://example.com/android")
        universalObject.getShortUrl(with: linkProperty) { [weak self] url, error in
            if error != nil {
                print(error ?? "Error creating wishlist short url")
                return
            }
            if let url = url {
                let wishlistShareMessage = "HotMess Wishlist\nTake a look at this HotMess Wishlist I made! \n"
                self?.showSharePopUp(stringURL: "\(userDetails.userFirstName)'s \(wishlistShareMessage)\(url)")
                print(url)
            }
        }
    }
}
extension EditorialDetailViewController: UIActivityItemSource {
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""
    }
    func activityViewController(_ activityViewController: UIActivityViewController,
                                itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return activityType
    }
    var linkProperty: BranchLinkProperties {
        let property = BranchLinkProperties()
//        property.channel = "facebook"
//        property.feature = "sharing"
//        property.campaign = "content 123 launch"
//        property.stage = "new user"
//        property.tags = ["one", "two", "three"]
        property.addControlParam("custom_data", withValue: "yes")
        // property.addControlParam("look_at", withValue: "this")
        // property.addControlParam("nav_to", withValue: "over here")
//        property.addControlParam("random", withValue: UUID.init().uuidString)
        property.addControlParam("editorial_id", withValue: presenter.editorialID)
        property.addControlParam("category_id", withValue: presenter.categoryID)
        return property
    }
    var universalObject: BranchUniversalObject {
        let buo = BranchUniversalObject(canonicalIdentifier: "Editorial/\(presenter.editorialID)")
        buo.title = presenter.editorialDetail.coverStoryTitle
        buo.contentDescription = presenter.editorialDetail.coverStoryDescription
        buo.imageUrl = presenter.editorialDetail.coverStoryImageUrl
        buo.publiclyIndex = true
        buo.locallyIndex = true
        return buo
    }
    func share() {
        let editorialTitle = presenter.editorialDetail.coverStoryTitle
        let deeplinkPath = "editorial_id/\(presenter.editorialID)/category_id/\(presenter.categoryID)"
        linkProperty.addControlParam(BranchKeyConstants.branchDeepLinkPathKey, withValue: deeplinkPath)
//        linkProperty.addControlParam(BranchKeyConstants.branchiOSURLKey, withValue: "http://google.co.in/")
//        linkProperty.addControlParam(BranchKeyConstants.branchAndroidURLKey, withValue: "http://example.com/android")
        universalObject.getShortUrl(with: linkProperty) { [weak self] url, error in
            if error != nil {
                print(error ?? "Error creating shared editorial short url")
                return
            }
            if let url = url {
                let editorialShareMessage =
                    "Hey, check out this editorial on Hotmess - \(editorialTitle). Read, shop the look and more! "
                self?.showSharePopUp(stringURL: "\(editorialShareMessage)\n\(url)")
                print(url)
            }
        }
    }
}
