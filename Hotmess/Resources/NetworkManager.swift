//
//  NetworkManager.swift
//  Hotmess
//
//  Created by Flaxon on 14/05/21.
//

import Alamofire
import SwiftyJSON

enum AppUpdateStatus {
    case UPDATENOTNEEDED
    case UPDATEAVAILABLE
    case UPDATEREQUIRED
    var title: String {
        switch self {
        case .UPDATEAVAILABLE:
            return "Update Avaliable"
        case .UPDATEREQUIRED:
            return "Update Required"
        case .UPDATENOTNEEDED:
            return ""
        }
    }
    var message: String {
        switch self {
        case .UPDATEAVAILABLE:
            return "Please update application to get latest functionality"
        case .UPDATEREQUIRED:
            return "Application requires an update to fuction properly"
        case .UPDATENOTNEEDED:
            return ""
        }
    }
}

class NetworkManager: NSObject {
    static let sharedInstance = NetworkManager()
    let headersWithoutAuth: HTTPHeaders = [
        "Content-type": "application/json"
    ]
    var headersWithAuth: HTTPHeaders = [
        "Content-type": "application/json",
        "Authorization": "Bearer " + "\(UserDefaults.standard.string(forKey: "userIdToken") ?? "")"
    ]
    enum RequestType {
        case withoutAuth
        case withAuth
    }
    enum MethodType {
        case GET
        case POST
        case PATCH
    }
    func updateUserIdToken() {
        headersWithAuth.update(name: "Authorization",
                               value: "Bearer " + "\(UserDefaults.standard.string(forKey: "userIdToken") ?? "")")
    }
    func returnFailure() -> JSON {
        var jsonObject = JSON()
        jsonObject = [
            "error_status": true
        ]
        return jsonObject
    }
    func requestRefresh() -> JSON {
        var jsonObject = JSON()
        jsonObject = [
            "error_status": true,
            "message": "requestForRefresh"
        ]
        return jsonObject
    }
    func makeNetworkRequest(urlString: String,
                            prametres: Any,
                            type: RequestType,
                            method: MethodType,
                            completion: @escaping(JSON) -> Void) {
        let internetIsReachable = NetworkReachabilityManager()?.isReachable ?? false
        if !internetIsReachable {
            AlertManager.sharedInstance.showAlertWith(title: "Internet Error",
                                                      message: "Please check your internet connection & try again.")
        } else {
            switch type {
            case .withoutAuth:
                commonRequest(urlString: urlString,
                              parameters: prametres,
                              completion: completion,
                              headers: headersWithoutAuth,
                              methodType: method)
            case .withAuth:
                commonRequest(urlString: urlString,
                              parameters: prametres,
                              completion: completion,
                              headers: headersWithAuth,
                              methodType: method)
            }
        }
    }
    func commonRequest(urlString: String,
                       parameters: Any,
                       completion: @escaping (JSON) -> Void,
                       headers: HTTPHeaders,
                       methodType: MethodType) {
        let url = NSURL(string: urlString)
        var request = URLRequest(url: url! as URL)
        request.headers = headers
        print("URL \(urlString)")
        print("Headers \(headers)")
        print("Body: \(parameters)")
        do {
            let data = try JSONSerialization.data(withJSONObject: parameters,
                                                  options: JSONSerialization.WritingOptions.prettyPrinted)
            let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            switch methodType {
            case .GET:
                request.httpMethod = "GET"
            case .POST:
                request.httpMethod = "POST"
                request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
            case .PATCH:
                request.httpMethod = "PATCH"
                request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
            }
            let alamoRequest = AF.request(request as URLRequestConvertible)
            alamoRequest.validate(statusCode: 200..<500)
            alamoRequest.responseJSON { response in
                self.parseResponse(urlString: urlString, response: response, completion: completion)
            }
        } catch {
            completion(self.returnFailure())
        }
    }
    func parseResponse(urlString: String, response: AFDataResponse<Any>, completion: @escaping (JSON) -> Void) {
        if let status = response.response?.statusCode {
            switch status {
            case 200 :
                if let json = response.value {
                    let jsonObject = JSON(json)
                    completion(jsonObject)
                }
            case 401:
                completion(self.requestRefresh())
            default:
                completion(self.returnFailure())
            }
        } else {
            completion(self.returnFailure())
        }
    }
    func forceLogOut() {
        Utilities.sharedInstance.logOutApp()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError("Not of type 'AppDelegate'")
        }
        appDelegate.restartApp()
    }
    func checkForUpdate(completion: ((AppUpdateStatus) -> Void)?) {
        commonRequest(urlString: ApiConstants.checkForUpdate, parameters: [:], completion: { (response) in
            if let errorStatus = response["error_status"].bool {
                if !errorStatus {
                    let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                    let minVersion = response["data"]["min_version"].double ?? 0
                    let latestVersion = response["data"]["latest_version"].double ?? 0
                    // CurrentVersion = 1
                    // LatestVersion = 3
                    // MiniVersion = 3
                    guard let version = Double(currentVersion ?? "1.0") else {
                        return
                    }
                    // 3 == 1
                    if latestVersion == version {
                        completion?(.UPDATENOTNEEDED)
                        return
                    }
                    // 3 = 1
                    if minVersion > version {
                        completion?(.UPDATEREQUIRED)
                        return
                    }
                    // 3 = 1
                    if latestVersion > version {
                        completion?(.UPDATEAVAILABLE)
                        return
                    }
                }
            }
        }, headers: headersWithoutAuth, methodType: .POST)
    }
    func refreshBearerToken(completion: @escaping() -> Void) {
        let refreshHeaders: HTTPHeaders = [
            "Content-type": "application/json",
            "Authorization": "Bearer " + "\(RealmDataManager.sharedInstance.getRefreshToken())"
        ]
        commonRequest(urlString: ApiConstants.refreshToken,
                      parameters: [:],
                      completion: { (response) in
            print(response)
            if let status = response["error_status"].bool {
                if !status {
                    let accessToken = response["data"]["access_token"].string ?? ""
                    let idToken = response["data"]["id_token"].string ?? ""
                    UserDefaults.standard.setValue(idToken, forKey: "userIdToken")
                    self.updateUserIdToken()
                    RealmDataManager.sharedInstance.updateUserTokensOnRefresh(
                        accessToken: accessToken,
                        idToken: idToken)
                    completion()
                } else {
                    self.forceLogOut()
                }
            }
        }, headers: refreshHeaders, methodType: .POST)
    }
}
