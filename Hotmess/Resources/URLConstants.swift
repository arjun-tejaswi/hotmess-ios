//
//  URLConstants.swift
//  Hotmess
//
//  Created by Flaxon on 14/05/21.
//

import Foundation

enum ApiEnvironments {
    case STAGING
    case PRODCUTION
}

struct ApiBaseURL {
    static let apiEnvironment: ApiEnvironments = .STAGING
    static let apiVersion = "v1"
    static let stagingBaseURL = "https://staging-apis.hotmessfashion.com/staging/api/\(apiVersion)"
    static let productionBasrURL = ""
    static let stagingImageBaseURL = "https://development-cdn.hotmessfashion.com/"
    static let productionImageBaseURL = ""
    static let stagingHTMLBaseURL = "https://staging-cdn.hotmessfashion.com/static-pages/"
    static let productionHTMLBaseURL = ""
    static let stagingShareBaseURL = "http://staging.hotmessfashion.com/"
    static let productionShareBaseURL = "https://www.hotmessfashion.com/"
    static let baseURL = apiEnvironment == .STAGING ? stagingBaseURL : productionBasrURL
    static let imageBaseURL = apiEnvironment == .STAGING ? stagingImageBaseURL : productionImageBaseURL
    static let htmlBaseURL = apiEnvironment == .STAGING ? stagingHTMLBaseURL : productionHTMLBaseURL
    static let shareBaseURL = apiEnvironment == .STAGING ? stagingShareBaseURL : productionShareBaseURL
}

struct ApiConstants {
    static let userSignUp = "\(ApiBaseURL.baseURL)/customers/signup"
    static let userLogin = "\(ApiBaseURL.baseURL)/customers/login"
    static let verifyOTP = "\(ApiBaseURL.baseURL)/customers/verify-otp"
    static let requestOTP = "\(ApiBaseURL.baseURL)/customers/resend-otp"
    static let updatePassword = "\(ApiBaseURL.baseURL)/customers/reset-password"
    static let forgotPassword = "\(ApiBaseURL.baseURL)/customers/forgot-password"
    static let faceBookSignUp = "\(ApiBaseURL.baseURL)/customers/facebook-sign-in"
    static let googleSignUp = "\(ApiBaseURL.baseURL)/customers/google-sign-in"
    static let appleSignIn = "\(ApiBaseURL.baseURL)/customers/apple-sign-in"
    static let quickLinks = "\(ApiBaseURL.baseURL)/quick-links-app/view"
    static let whatsNew = "\(ApiBaseURL.baseURL)/whats-new-app/view"
    static let womenFeaturedProducts = "\(ApiBaseURL.baseURL)/featured-products-app/view"
    static let whatToWearCollections = "\(ApiBaseURL.baseURL)/what-to-wear-collections-app/view"
    static let moodCollections = "\(ApiBaseURL.baseURL)/mood-collections-app/view"
    static let celebrityPicks = "\(ApiBaseURL.baseURL)/celebrity-picks-app/"
    static let trendingProductsMetaData = "\(ApiBaseURL.baseURL)/trending-products/apps/women"
    static let trendingProducts = "\(ApiBaseURL.baseURL)/products/list"
    static let hotmessCollectionMetaData = "\(ApiBaseURL.baseURL)/trending-products/timeline/apps/women"
    static let hotmessCollectionProducts = "\(ApiBaseURL.baseURL)/products/list"
    static let productListing = "\(ApiBaseURL.baseURL)/products/list"
    static let shopWomenCategory = "\(ApiBaseURL.baseURL)/shop-categories/app"
    static let productDetail = "\(ApiBaseURL.baseURL)/products/view"
    static let addToBag = "\(ApiBaseURL.baseURL)/shopping-bag/add"
    static let shoppingBag = "\(ApiBaseURL.baseURL)/shopping-bag/list"
    static let updateBag = "\(ApiBaseURL.baseURL)/shopping-bag/update"
    static let removeFromBag = "\(ApiBaseURL.baseURL)/shopping-bag/remove"
    static let removeAllFromBag = "\(ApiBaseURL.baseURL)/shopping-bag/remove-all"
    static let recommendedProducts = "\(ApiBaseURL.baseURL)/products/recommended-products"
    static let addToWishlist = "\(ApiBaseURL.baseURL)/wishlist/add"
    static let removeFromWishlist = "\(ApiBaseURL.baseURL)/wishlist/remove"
    static let removeAllFromWishlist = "\(ApiBaseURL.baseURL)/wishlist/remove-all"
    static let wishlist = "\(ApiBaseURL.baseURL)/wishlist/list"
    static let wishlistStatus = "\(ApiBaseURL.baseURL)/wishlist/status"
    static let refreshToken = "\(ApiBaseURL.baseURL)/customers/refresh-auth-tokens"
    static let addAddress = "\(ApiBaseURL.baseURL)/customers/addresses/create"
    static let listAddress = "\(ApiBaseURL.baseURL)/customers/addresses/list"
    static let updateAddress = "\(ApiBaseURL.baseURL)/customers/addresses/update"
    static let removeAddress = "\(ApiBaseURL.baseURL)/customers/addresses/delete"
    static let setDefaultAddress = "\(ApiBaseURL.baseURL)/customers/addresses/set-default"
    static let getLocationByPin = "\(ApiBaseURL.baseURL)/customers/get-location-by-pincode"
    static let sharedWishlistListing = "\(ApiBaseURL.baseURL)/shareable-wishlist/list"
    static let checkForUpdate = "\(ApiBaseURL.baseURL)/force-update-apps/ios"
    static let createOrder = "\(ApiBaseURL.baseURL)/orders/create"
    static let createGuestOrder = "\(ApiBaseURL.baseURL)/orders/create-guest-order"
    static let verifyPayment = "\(ApiBaseURL.baseURL)/orders/verify-payment"
    static let verifyGuestPayment = "\(ApiBaseURL.baseURL)/orders/validate-guest-payment"
    static let calculateOrderPrice = "\(ApiBaseURL.baseURL)/orders/calculate-order-price"
    static let updateProfile = "\(ApiBaseURL.baseURL)/customers/update"
    static let passwordUpdate = "\(ApiBaseURL.baseURL)/customers/update-password"
    static let newsLetterSubscription = "\(ApiBaseURL.baseURL)/newsletter-subscriptions/create"
    static let productSubscription = "\(ApiBaseURL.baseURL)/products/subscribe"
    static let orderList = "\(ApiBaseURL.baseURL)/orders/list"
    static let orderDetails = "\(ApiBaseURL.baseURL)/orders/view"
    static let cancelOrder = "\(ApiBaseURL.baseURL)/orders/cancel"
    static let shareWishlist = "\(ApiBaseURL.baseURL)/shareable-wishlist/create"
    static let createGiftCard = "\(ApiBaseURL.baseURL)/gift-card-orders/create"
    static let verifyGiftCardPayment = "\(ApiBaseURL.baseURL)/gift-card-orders/validate_payment"
    static let redeemGiftCard = "\(ApiBaseURL.baseURL)/gift-card-orders/redeem"
    static let giftCardWallet = "\(ApiBaseURL.baseURL)/gift-card-orders/wallet_balance"
    static let returnOrder = "\(ApiBaseURL.baseURL)/orders/return"
    static let searchProduct = "\(ApiBaseURL.baseURL)/products/free-text-search"
    static let designersList = "\(ApiBaseURL.baseURL)/designer/list"
    static let calculateGuestPrice = "\(ApiBaseURL.baseURL)/orders/calculate-guest-order-price"
    static let getOrderSummaryPDF = "\(ApiBaseURL.baseURL)/pdf-converter/html-to-pdf"
    static let editorialCategory = "\(ApiBaseURL.baseURL)/editorials/category/list"
    static let editorialCategoryContent = "\(ApiBaseURL.baseURL)/editorials/list"
    static let editorialDetails = "\(ApiBaseURL.baseURL)/editorials/view"
}
struct BranchStaticURL {
    static let inviteFriend = "https://e2d8c.test-app.link/BMCL2N1znjb"
}
struct ExternalURL {
    static let slackErrorReporting = "https://slack.com/api/chat.postMessage"
    static let appStoreURL = "https://apps.apple.com/app/id1576111681"
}
struct LeagalTermsURL {
    static let termsAndConditions = "\(ApiBaseURL.htmlBaseURL)terms-services.html"
    static let privacyAndPolicies = "\(ApiBaseURL.htmlBaseURL)privacy-policy.html"
    static let cancelPolicy = "\(ApiBaseURL.htmlBaseURL)cancel-order-policy.html"
    static let returnPolicy = "\(ApiBaseURL.htmlBaseURL)return-order-policy.html"
    static let faqs = "\(ApiBaseURL.htmlBaseURL)faq.html"
}
