//
//  AppConstants.swift
//  Hotmess
//
//  Created by Flaxon on 19/05/21.
//
import UIKit

// MARK: - Colors
struct ColourConstants {
    static let hex3C3C3C = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.2352941176, alpha: 1)
    static let hex191919 = #colorLiteral(red: 0.09803921569, green: 0.09803921569, blue: 0.09803921569, alpha: 1)
    static let hexE0E0E0 = #colorLiteral(red: 0.8784313725, green: 0.8784313725, blue: 0.8784313725, alpha: 1)
    static let hex717171 = #colorLiteral(red: 0.4431372549, green: 0.4431372549, blue: 0.4431372549, alpha: 1)
    static let hex8B8B8B = #colorLiteral(red: 0.5450980392, green: 0.5450980392, blue: 0.5450980392, alpha: 1)
    static let hexD3D3D3 = #colorLiteral(red: 0.8274509804, green: 0.8274509804, blue: 0.8274509804, alpha: 1)
    static let hexDBDBDB = #colorLiteral(red: 0.8588235294, green: 0.8588235294, blue: 0.8588235294, alpha: 1)
    static let hex707070 = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
    static let hex9B9B9B = #colorLiteral(red: 0.6078431373, green: 0.6078431373, blue: 0.6078431373, alpha: 1)
    static let hexFFFFFF = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    static let hexF0F0F0 = #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
    static let hexCECECE = #colorLiteral(red: 0.8078431373, green: 0.8078431373, blue: 0.8078431373, alpha: 1)
    static let hex000000 = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    static let hexF4F4F4 = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
    static let hexE8F8FF = #colorLiteral(red: 0.9098039216, green: 0.9725490196, blue: 1, alpha: 1)
    static let hex9AB5BC = #colorLiteral(red: 0.6039215686, green: 0.7098039216, blue: 0.737254902, alpha: 1)
    static let hexF8F8F8 = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
    static let hexE4E4E4 = #colorLiteral(red: 0.8941176471, green: 0.8941176471, blue: 0.8941176471, alpha: 1)
    static let hexFFE8E8 = #colorLiteral(red: 1, green: 0.9098039216, blue: 0.9098039216, alpha: 1)
    static let hexE56A6A = #colorLiteral(red: 0.8980392157, green: 0.4156862745, blue: 0.4156862745, alpha: 1)
    static let hex1D1D1D = #colorLiteral(red: 0.1137254902, green: 0.1137254902, blue: 0.1137254902, alpha: 1)
    static let hexEF5353 = #colorLiteral(red: 0.937254902, green: 0.3254901961, blue: 0.3254901961, alpha: 1)
    static let hexEAEAEA = #colorLiteral(red: 0.9176470588, green: 0.9176470588, blue: 0.9176470588, alpha: 1)
    static let hex1A1A1A = #colorLiteral(red: 0.1019607843, green: 0.1019607843, blue: 0.1019607843, alpha: 1)
    static let hexEBF7FD = #colorLiteral(red: 0.9215686275, green: 0.968627451, blue: 0.9921568627, alpha: 1)
    static let hexEAEBEB = #colorLiteral(red: 0.9176470588, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
    static let hex1B8CD3 = #colorLiteral(red: 0.1058823529, green: 0.5490196078, blue: 0.8274509804, alpha: 1)
    static let hexDE650D = #colorLiteral(red: 0.8705882353, green: 0.3960784314, blue: 0.05098039216, alpha: 1)
    static let hexFFEBD6 = #colorLiteral(red: 1, green: 0.9215686275, blue: 0.8392156863, alpha: 1)
    static let hex399861 = #colorLiteral(red: 0.2235294118, green: 0.5960784314, blue: 0.3803921569, alpha: 1)
    static let hexFFF2F2 = #colorLiteral(red: 1, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
    static let hexECFCF2 = #colorLiteral(red: 0.9254901961, green: 0.9882352941, blue: 0.9490196078, alpha: 1)
    static let hexF8F7F6 = #colorLiteral(red: 0.9725490196, green: 0.968627451, blue: 0.9647058824, alpha: 1)
}

// MARK: - Fonts
struct FontConstants {
    static let medium = "Montserrat-Medium"
    static let bold = "Montserrat-Bold"
    static let regular = "Montserrat-Regular"
    static let semiBold = "Montserrat-Semibold"
    static let light = "Montserrat-Light"
}

// MARK: - User Defaults
struct UserDefaultsConstants {
    static let isLoggedIn = UserDefaults.standard.bool(forKey: "isLoggedIn")
    static let notFirstInApp = UserDefaults.standard.bool(forKey: "notFirstInApp")
    static let userIdToken = UserDefaults.standard.string(forKey: "userIdToken")
}

// MARK: - Razorpay Defaults
struct RazorPayConstants {
    static let testKey = "rzp_test_lEiCiZyqm7vpQG"
    static let liveKey = ""
    static let apiKey = ApiBaseURL.apiEnvironment == .STAGING ? testKey : liveKey
    static let baseUrl = "https://challengepost-s3-challengepost.netdna-ssl.com/photos/production"
    static let logoUrl = "\(baseUrl)/software_photos/001/205/265/datas/original.png"
}

// MARK: - String Constants
struct BranchKeyConstants {
    static let branchDeepLinkPathKey = "$deeplink_path"
    static let branchiOSURLKey = "$ios_url"
    static let branchAndroidURLKey = "$android_url"
}
