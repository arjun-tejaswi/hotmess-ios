//
//  AlertManager.swift
//  Hotmess
//
//  Created by Flaxon on 14/05/21.
//

import UIKit
import Actions

class AlertManagerParameters: NSObject {
    var title: String?
    var message: String?
    var leftButtonTitle: String?
    var rightButtonTitle: String?
    init(title: String, message: String, leftButtonTitle: String, rightButtonTitle: String) {
        self.title = title
        self.message = message
        self.leftButtonTitle = leftButtonTitle
        self.rightButtonTitle = rightButtonTitle
    }
}
class AlertManager: NSObject {
    static let sharedInstance = AlertManager()
    let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
    func getAlertMessage(title: String, message: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) { _ in  })
       return alert
    }
    func showAlertWith(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) { _ in  })
        keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    func showAlertWithTwoAction(parameters: AlertManagerParameters,
                                leftButtonAction: ( () -> Void)?,
                                rightButtonAction: @escaping () -> Void) {
        let alertController = UIAlertController(title: parameters.title,
                                                message: parameters.message,
                                                preferredStyle: .alert)
        let leftAction = UIAlertAction(title: parameters.leftButtonTitle, style: UIAlertAction.Style.default) { _ in
            leftButtonAction?()
        }
        let rightAction = UIAlertAction(title: parameters.rightButtonTitle, style: UIAlertAction.Style.default) { _ in
            rightButtonAction()
        }
        alertController.addAction(leftAction)
        alertController.addAction(rightAction)
        keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    func showAlertWithOneAction(parameters: AlertManagerParameters,
                                actionButton: @escaping () -> Void) {
        let alertController = UIAlertController(title: parameters.title,
                                                message: parameters.message,
                                                preferredStyle: .alert)
        let buttonAction = UIAlertAction(title: parameters.rightButtonTitle, style: UIAlertAction.Style.default) { _ in
            actionButton()
        }
        alertController.addAction(buttonAction)
        keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    func showCustomAlertPopUp(bounds: CGRect,
                              title: String, subTitle: String,
                              buttonTitle: String,
                              buttonAction: @escaping () -> Void = {
                                // Optional method to handle action for button.
                              }) -> CustomAlertPopUp {
        guard let popUpView = Bundle.main.loadNibNamed("CustomAlertPopUp",
                                                       owner: self, options: nil)?.first as? CustomAlertPopUp else {
            fatalError("Not of type 'CustomAlertPopUp'")
        }
        let originalCoordinates = popUpView.cardView.bounds
        popUpView.cardView.alpha = 0
        popUpView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        popUpView.cardView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 0)
        popUpView.titleLabel.text = title
        popUpView.subtitleLabel.text = subTitle
        popUpView.actionButton.setTitle(buttonTitle, for: .normal)
        popUpView.actionButton.add(event: .touchUpInside) {
            popUpView.dismiss()
            buttonAction()
        }
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
                        popUpView.cardView.alpha = 1
                        popUpView.cardView.frame = CGRect(x: originalCoordinates.minX,
                                                          y: originalCoordinates.minY,
                                                          width: originalCoordinates.width,
                                                          height: originalCoordinates.height)
        })
        return popUpView
    }
}
