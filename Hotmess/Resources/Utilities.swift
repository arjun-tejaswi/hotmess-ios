//
//  Utilities.swift
//  Hotmess
//
//  Created by Cedan Misquith on 12/05/21.
//

import UIKit
import AudioToolbox
// Instant Vibrate for an action
enum Vibration {
    case weak
    case pulse
    case strong
    public func vibrate() {
        switch self {
        case .weak:
            AudioServicesPlaySystemSound(1519) // Actuate "Peek" feedback (weak boom)
        case .strong:
            AudioServicesPlaySystemSound(1520) // Actuate "Pop" feedback (strong boom)
        case .pulse:
            AudioServicesPlaySystemSound(1521) // Actuate "Nope" feedback (series of three weak booms)
        }
    }
}
class Utilities: NSObject {
    static let sharedInstance = Utilities()
    func convertServerDate(value: Int64, delivered: Bool) -> String {
        let truncatedValue = value/1000
        let date = Date(timeIntervalSince1970: TimeInterval(truncatedValue))
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        if delivered {
            formatter.dateFormat = "dd MMM, yyyy"
        } else {
            formatter.dateFormat = "EEEE dd, MMM"
        }
        return formatter.string(from: date)
    }
    func convertTodayToString() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.dateFormat = "dd MMM, yyyy"
        return formatter.string(from: date)
    }
    // Clear app data on logout
    func logOutApp() {
        RealmDataManager.sharedInstance.deleteAll()
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        UserDefaults.standard.removeObject(forKey: "userIdToken")
        NetworkManager.sharedInstance.updateUserIdToken()
    }
    // Convert #Hex code to UIColor
    func hexStringToUIColor(hex: String) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        if (cString.count) != 6 {
            return UIColor.gray
        }
        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func imageFromInitials(initial01: String?, initial02: String?,
                           BGColor: UIColor, fontName: String = FontConstants.regular,
                           fontSize: Float = 40, textColor: UIColor = ColourConstants.hex191919) -> UIImage? {
        let frame = CGRect(x: 0, y: 0, width: 150, height: 150)
        let nameLabel = UILabel(frame: frame)
        nameLabel.textAlignment = .center
        nameLabel.backgroundColor = BGColor
        nameLabel.textColor = textColor
        nameLabel.font = UIFont(name: fontName, size: CGFloat(fontSize))
        nameLabel.text = "\(initial01?.prefix(1) ?? "") \(initial02?.prefix(1) ?? "")"
        nameLabel.adjustsFontSizeToFitWidth = true
        UIGraphicsBeginImageContext(frame.size)
        if let currentContext = UIGraphicsGetCurrentContext() {
            nameLabel.layer.render(in: currentContext)
            let nameImage = UIGraphicsGetImageFromCurrentImageContext()
            return nameImage
        }
        return nil
    }
}
extension Utilities {
    // MARK: - Colour PopUp Display Method
    func showColourPopUp(bounds: CGRect) -> ColourPopUp {
        guard let popUpView = Bundle.main.loadNibNamed("ColourPopUp",
                                                       owner: self, options: nil)?.first as? ColourPopUp else {
            fatalError("Not of type 'ColourPopUp'")
        }
        let originalCoordinates = popUpView.cardView.bounds
        popUpView.cardView.alpha = 0
        popUpView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        popUpView.cardView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 0)
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
                        popUpView.cardView.alpha = 1
                        popUpView.cardView.frame = CGRect(x: originalCoordinates.minX,
                                                          y: originalCoordinates.minY,
                                                          width: originalCoordinates.width,
                                                          height: originalCoordinates.height)
                       })
        return popUpView
    }
    // MARK: - Size PopUp Display Method
    func showSizePopUp(bounds: CGRect) -> SizePopUp {
        guard let popUpView = Bundle.main.loadNibNamed("SizePopUp",
                                                       owner: self, options: nil)?.first as? SizePopUp else {
            fatalError("Not of type 'SizePopUp'")
        }
        let originalCoordinates = popUpView.cardView.bounds
        popUpView.cardView.alpha = 0
        popUpView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        popUpView.cardView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 0)
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
                        popUpView.cardView.alpha = 1
                        popUpView.cardView.frame = CGRect(x: originalCoordinates.minX,
                                                          y: originalCoordinates.minY,
                                                          width: originalCoordinates.width,
                                                          height: originalCoordinates.height)
                       })
        return popUpView
    }
    // MARK: - Remaining Items PopUp Display Method
    func showRemainingItemsPopUp(bounds: CGRect) -> RemainingItemsPopUp {
        guard let popUpView = Bundle.main.loadNibNamed("RemainingItemsPopUp",
                                                       owner: self, options: nil)?.first as? RemainingItemsPopUp else {
            fatalError("Not of type 'RemainingItemsPopUp'")
        }
        let originalCoordinates = popUpView.cardView.bounds
        popUpView.cardView.alpha = 0
        popUpView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        popUpView.cardView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 0)
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
                        popUpView.cardView.alpha = 1
                        popUpView.cardView.frame = CGRect(x: originalCoordinates.minX,
                                                          y: originalCoordinates.minY,
                                                          width: originalCoordinates.width,
                                                          height: originalCoordinates.height)
                       })
        return popUpView
    }
    // MARK: - Cancel Order Confirmation PopUp Display Method
    func showCancelOrderConfirmationPopUp(bounds: CGRect) -> CancelOrderConfirmationPopUp {
        guard let popUpView = Bundle.main.loadNibNamed("CancelOrderConfirmationPopUp",
                                                       owner: self,
                                                       options: nil)?.first as? CancelOrderConfirmationPopUp else {
            fatalError("Not of type 'CancelOrderConfirmationPopUp'")
        }
        let originalCoordinates = popUpView.cardView.bounds
        popUpView.cardView.alpha = 0
        popUpView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        popUpView.cardView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 0)
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
                        popUpView.cardView.alpha = 1
                        popUpView.cardView.frame = CGRect(x: originalCoordinates.minX,
                                                          y: originalCoordinates.minY,
                                                          width: originalCoordinates.width,
                                                          height: originalCoordinates.height)
                       })
        return popUpView
    }
    // MARK: - Return Item Confirmation PopUp Display Method
    func showReturnItemConfirmationPopUp(bounds: CGRect) -> ReturnItemConfirmationPopUp {
        guard let popUpView = Bundle.main.loadNibNamed("ReturnItemConfirmationPopUp",
                                                       owner: self,
                                                       options: nil)?.first as? ReturnItemConfirmationPopUp else {
            fatalError("Not of type 'ReturnItemConfirmationPopUp'")
        }
        let originalCoordinates = popUpView.cardView.bounds
        popUpView.cardView.alpha = 0
        popUpView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        popUpView.cardView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 0)
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
                        popUpView.cardView.alpha = 1
                        popUpView.cardView.frame = CGRect(x: originalCoordinates.minX,
                                                          y: originalCoordinates.minY,
                                                          width: originalCoordinates.width,
                                                          height: originalCoordinates.height)
                       })
        return popUpView
    }
    // MARK: - Size Guide PopUp Display Method
    func showSizeGuidePopUp(bounds: CGRect) -> SizeGuidePopUp {
        guard let popUpView = Bundle.main.loadNibNamed("SizeGuidePopUp",
                                                       owner: self, options: nil)?.first as? SizeGuidePopUp else {
            fatalError("Not of type 'SizeGuidePopUp'")
        }
        let originalCoordinates = popUpView.cardView.bounds
        popUpView.cardView.alpha = 0
        popUpView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        popUpView.cardView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 0)
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseIn], animations: {
                        popUpView.cardView.alpha = 1
                        popUpView.cardView.frame = CGRect(x: originalCoordinates.minX,
                                                          y: originalCoordinates.minY,
                                                          width: originalCoordinates.width,
                                                          height: originalCoordinates.height)
                       })
        return popUpView
    }
    func showAddToBagPopUp(bounds: CGRect) -> AddToBagPopUp {
        guard let popUpView = Bundle.main.loadNibNamed("AddToBagPopUp",
                                                       owner: self, options: nil)?.first as? AddToBagPopUp else {
            fatalError("Not of type 'AddToBagPopUp'")
        }
        let originalCoordinates = popUpView.cardView.bounds
        popUpView.cardView.alpha = 0
        popUpView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        popUpView.cardView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 0)
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
                        popUpView.cardView.alpha = 1
                        popUpView.cardView.frame = CGRect(x: originalCoordinates.minX,
                                                          y: originalCoordinates.minY,
                                                          width: originalCoordinates.width,
                                                          height: originalCoordinates.height)
                       })
        return popUpView
    }
    func showSortByPopUp(bounds: CGRect) -> SortByPopUp {
        guard let popUpView = Bundle.main.loadNibNamed("SortByPopUp",
                                                       owner: self, options: nil)?.first as? SortByPopUp else {
            fatalError("Not of type 'SortByPopUp'")
        }
        let originalCoordinates = popUpView.cardView.bounds
        popUpView.cardView.alpha = 0
        popUpView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        popUpView.cardView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 0)
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
                        popUpView.cardView.alpha = 1
                        popUpView.cardView.frame = CGRect(x: originalCoordinates.minX,
                                                          y: originalCoordinates.minY,
                                                          width: originalCoordinates.width,
                                                          height: originalCoordinates.height)
                       })
        return popUpView
    }
    // MARK: - Colour PopUp Display Method
    func showNotifyProductInstockPopUp(bounds: CGRect) -> ProductInStockPopUp {
        guard let popUpView = Bundle.main.loadNibNamed("ProductInStockPopUp",
                                                       owner: self, options: nil)?.first as? ProductInStockPopUp else {
            fatalError("Not of type 'ProductInStockPopUp'")
        }
        let originalCoordinates = popUpView.cardView.bounds
        popUpView.cardView.alpha = 0
        popUpView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        popUpView.cardView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 0)
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
                        popUpView.cardView.alpha = 1
                        popUpView.cardView.frame = CGRect(x: originalCoordinates.minX,
                                                          y: originalCoordinates.minY,
                                                          width: originalCoordinates.width,
                                                          height: originalCoordinates.height)
                       })
        return popUpView
    }
    func showProductNotifySuccessPopUp(bounds: CGRect) -> InstockNotifySuccessPopup {
        guard let popUpView = Bundle.main.loadNibNamed("InstockNotifySuccessPopup",
                                                       owner: self,
                                                       options: nil)?.first as? InstockNotifySuccessPopup else {
            fatalError("Not of type 'InstockNotifySuccessPopup'")
        }
        let originalCoordinates = popUpView.cardView.bounds
        popUpView.cardView.alpha = 0
        popUpView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        popUpView.cardView.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 0)
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
                        popUpView.cardView.alpha = 1
                        popUpView.cardView.frame = CGRect(x: originalCoordinates.minX,
                                                          y: originalCoordinates.minY,
                                                          width: originalCoordinates.width,
                                                          height: originalCoordinates.height)
                       })
        return popUpView
    }
}
