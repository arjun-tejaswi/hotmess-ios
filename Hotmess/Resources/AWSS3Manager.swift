//
//  AWSS3Manager.swift
//  Hotmess
//
//  Created by Akshatha on 12/08/21.
//

import UIKit
import AWSS3

class AWSS3Manager: NSObject {
    static let sharedInstance = AWSS3Manager()
    func uploadS3(image: UIImage,
                  name: String,
                  errorCompletion: @escaping(String) -> Void,
                  successCompletion: @escaping(String) -> Void) {
        guard let data = image.jpegData(compressionQuality: 0.3) else {
            return
        }
        let fileName = "customers-profile/\(name)"
        let poolId = "ap-south-1:1b661ed3-c23a-454f-976b-579db1bb725d"
        let credentialsProvider = AWSCognitoCredentialsProvider(
            regionType: .APSouth1,
            identityPoolId: poolId
        )
        let configuration = AWSServiceConfiguration(region: .APSouth1, credentialsProvider: credentialsProvider)
        let expression = AWSS3TransferUtilityUploadExpression()
                expression.setValue("public-read", forRequestHeader: "x-amz-acl")
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        AWSS3TransferUtility.default().uploadData(
            data,
            bucket: "hotmessfashion-development",
            key: fileName,
            contentType: "image/jpg",
            expression: expression).continueWith(block: { (task: AWSTask) -> Any? in
            if let error = task.error {
                DispatchQueue.main.async {
                    errorCompletion("An error occurred while Uploading your file, try again.\(error)")
                }
            }
            if task.result != nil {
                let baseURL = "https://hotmessfashion-development.s3.ap-south-1.amazonaws.com/"
                let imageURL = "\(baseURL)\(fileName)"
                successCompletion(imageURL)
            }
            return nil
        })
    }
}
