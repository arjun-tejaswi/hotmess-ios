//
//  DeeplinkManager.swift
//  Hotmess
//
//  Created by Cedan Misquith on 03/08/21.
//

import Foundation
import SwiftyJSON

enum DeepLinkNavigationType {
    case VIEWPRODUCT
    case SHAREDWISHLIST
    case VIEWEDITORIAL
}

class DeeplinkManager: NSObject {
    static let sharedInstance = DeeplinkManager()
    func handleNavigationForBranchDeeplink(result: [AnyHashable: Any]?,
                                           completion: (DeepLinkNavigationType, [String: Any]) -> Void ) {
        guard let jsondata = result as? [String: Any], result != nil else {
            return
        }
        if let didClickBranchLink = jsondata["+clicked_branch_link"] as? Bool {
            if didClickBranchLink {
                if let productId = jsondata["product_id"] as? String, let skuId = jsondata["sku_id"] as? String {
                    completion(.VIEWPRODUCT, ["sku_id": skuId, "product_id": productId])
                } else if let wishlistId = jsondata["wishlist_id"] as? String {
                    completion(.SHAREDWISHLIST, ["wishlist_id": wishlistId])
                } else if let editorialId = jsondata["editorial_id"]
                            as? String, let categoryId = jsondata["category_id"] as? String {
                    completion(.VIEWEDITORIAL, ["category_id": categoryId, "editorial_id": editorialId])
                }
            }
        }
    }
}
