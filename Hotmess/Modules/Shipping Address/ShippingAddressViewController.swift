//
//  ShippingAddressViewController.swift
//  Hotmess
//
//  Created by Akshatha on 02/08/21.
//

import UIKit

enum AddressState {
    case SHIPPING
    case FROMSIDEMENU
    case FROMRETURNORDER
}

class ShippingAddressViewController: UIViewController {
    @IBOutlet weak var addressTitleLabel: UILabel!
    @IBOutlet weak var shippingAddressTableView: UITableView!
    @IBOutlet weak var shippingAddressTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var addAddressButton: UIButton!
    var state: AddressState = .SHIPPING
    var presenter = ShippingAddressPresenter()
    weak var changeAddressDelegate: ChangeAddressProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        addressTitleLabel.text = getTitleLabel()
        presenter.preSelectDefaultAddress()
        setUpTableView()
        applyStyling()
        applyFont()
    }
    override func viewWillAppear(_ animated: Bool) {
        if state == .FROMSIDEMENU {
            self.view.showLoader()
            presenter.requestAddressList()
        }
    }
    func getTitleLabel() -> String {
        var titleLabelText: String
        switch state {
        case .SHIPPING:
            titleLabelText = "SHIPPING ADDRESS"
        case .FROMSIDEMENU:
            titleLabelText = "MY ADDRESS"
        case .FROMRETURNORDER:
            titleLabelText = "RETURN ADDRESS"
        }
        return titleLabelText
    }
    override func viewWillLayoutSubviews() {
        self.shippingAddressTableViewHeight.constant = self.shippingAddressTableView.intrinsicContentSize.height
    }
    // MARK: - Statusbar Style Method
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        addAddressButton.applyButtonLetterSpacing(spacing: 2.52)
        addressTitleLabel.applyLetterSpacing(spacing: 0.7)
    }
    // MARK: - Font Style Method
    func applyFont() {
        addAddressButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        addressTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
    }
    func setUpTableView() {
        shippingAddressTableView.register(UINib(nibName: "ShippingAddressTableViewCell", bundle: nil),
                                          forCellReuseIdentifier: "ShippingAddressTableViewCell")
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        changeAddressDelegate?.changedAddress(address: presenter.userAddressList)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addAddressButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "AddAddress", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "AddAddressViewController")
                as? AddAddressViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @objc func editButtonAction(sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "AddAddress", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "AddAddressViewController")
                as? AddAddressViewController else { return }
        nextvc.presenter.viewMode = 1
        nextvc.presenter.addressData = presenter.userAddressList[sender.tag]
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @objc func removeAddressButtonAction(sender: UIButton) {
        self.view.showLoader()
        presenter.requestRemoveAddress(for: sender.tag)
    }
}
extension ShippingAddressViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.userAddressList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = shippingAddressTableView.dequeueReusableCell(
                withIdentifier: "ShippingAddressTableViewCell") as? ShippingAddressTableViewCell else {
            return UITableViewCell()
        }
        cell.editButton.tag = indexPath.row
        cell.editButton.addTarget(self, action: #selector(editButtonAction), for: .touchUpInside)
        cell.removeAddressButton.tag = indexPath.row
        cell.removeAddressButton.addTarget(self, action: #selector(removeAddressButtonAction), for: .touchUpInside)
        let fullName =
            "\(presenter.userAddressList[indexPath.row].firstName)" +
            " \(presenter.userAddressList[indexPath.row].lastName)"
        let address = "\(presenter.userAddressList[indexPath.row].addressLine1)," +
            "\n\(presenter.userAddressList[indexPath.row].addressLine2)," +
            "\n\(presenter.userAddressList[indexPath.row].city), " +
            "\(presenter.userAddressList[indexPath.row].state) - " +
            "\(presenter.userAddressList[indexPath.row].areaPinCode)" +
            "\n\(presenter.userAddressList[indexPath.row].country)" +
            "\nLandmark : \(presenter.userAddressList[indexPath.row].landMark)"
        cell.nameLabel.text = "\(fullName)"
        cell.addressLabel.text = address
        cell.mobileNumberCodeLabel.text = "Mobile No.: +91 \(presenter.userAddressList[indexPath.row].mobileNumber)"
        if presenter.userAddressList[indexPath.row].isSelected {
            cell.isCheckedImageView.image = UIImage(named: "accountCreatedCheck")
        } else {
            cell.isCheckedImageView.image = UIImage(named: "orderUncheck")
        }
        if state == .FROMSIDEMENU {
            cell.isCheckedImageView.isHidden = true
            if presenter.userAddressList[indexPath.row].isDefaultAddress {
                cell.defaultAddressLabel.isHidden = false
            } else {
                cell.defaultAddressLabel.isHidden = true
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var index: Int = 0
        for address in presenter.userAddressList {
            if index == indexPath.row {
                address.isSelected = true
            } else {
                address.isSelected = false
            }
            index += 1
        }
        shippingAddressTableView.reloadData()
    }
}
extension ShippingAddressViewController: ShippingAddressPresenterProtocol {
    func refresh() {
        self.view.hideLoader()
        presenter.requestAddressList()
    }
    func listAddressSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            if presenter.userAddressList.count == 0 {
                changeAddressDelegate?.clearAddresses()
                self.navigationController?.popViewController(animated: true)
            } else {
                presenter.preSelectDefaultAddress()
                shippingAddressTableView.reloadData()
            }
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)        }
    }
}
