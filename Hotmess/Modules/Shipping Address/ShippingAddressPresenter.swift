//
//  ShippingAddressPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 05/08/21.
//

import Foundation
import SwiftyJSON

protocol ShippingAddressPresenterProtocol: AnyObject {
    func listAddressSuccess(status: Bool, title: String, message: String)
    func refresh()
}

class ShippingAddressPresenter: NSObject {
    var userAddressList = [ShippingAddressRealm]()
    weak var delegate: ShippingAddressPresenterProtocol?
    func requestAddressList() {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.listAddress,
            prametres: [:],
            type: .withAuth,
            method: .POST,
            completion: self.handleAddressListResponse)
    }
    func requestRemoveAddress(for index: Int) {
        let item = userAddressList[index]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.removeAddress,
            prametres: [
                "address_id": item.addressID
            ],
            type: .withAuth,
            method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.requestRemoveAddress(for: index)
                            }
                        } else {
                            self.delegate?.listAddressSuccess(status: false, title: "Error", message: message)
                        }
                    }
                } else {
                    self.delegate?.refresh()
                }
            }
        }
    }
    func requestToSetDefaultAddress(for index: Int) {
        let item = userAddressList[index]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.setDefaultAddress,
            prametres: [
                "address_id": item.addressID],
            type: .withAuth,
            method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.requestToSetDefaultAddress(for: index)
                            }
                        }
                    }
                } else {
                    self.delegate?.refresh()
                }
            }
        }
    }
    func handleAddressListResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.requestAddressList()
                        }
                    } else {
                        delegate?.listAddressSuccess(status: false, title: "Error", message: message)
                    }
                }
            } else {
                if let data = response["data"].array {
                    userAddressList.removeAll()
                    for aAddress in data {
                        if let aAddressDictionary = aAddress.dictionary {
                            let allAddresses = AddressParser.sharedInstance.parseAddressData(data: aAddressDictionary)
                            userAddressList.append(allAddresses)
                        }
                    }
                }
                delegate?.listAddressSuccess(status: true, title: "", message: "")
            }
        }
    }
    func preSelectDefaultAddress() {
        for address in userAddressList {
            if address.isDefaultAddress {
                address.isSelected = true
            } else {
                address.isSelected = false
            }
        }
    }
}
