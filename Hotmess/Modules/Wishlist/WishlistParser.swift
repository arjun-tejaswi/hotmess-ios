//
//  WishlistParser.swift
//  Hotmess
//
//  Created by Cedan Misquith on 28/07/21.
//

import Foundation
import SwiftyJSON

class WishlistParser: NSObject {
    static let sharedInstance = WishlistParser()
    func parseWishlist(item: JSON) -> WishlishRealm {
        let wishlist = WishlishRealm()
        wishlist.productID = item["product_id"].string ?? ""
        wishlist.stockKeepUnitID = item["sku_id"].string ?? ""
        let imageUrl = ApiBaseURL.imageBaseURL + "\(item["featured_image_url"].string ?? "")"
        wishlist.productImageURL = imageUrl
        wishlist.productName = item["name"].string ?? ""
        wishlist.productDescription = item["description"].string ?? ""
        wishlist.productOfferPrice = item["offer_price"].int64 ?? 0
        wishlist.productPrice = item["cost_price"].int64 ?? 0
        wishlist.productDesignerName = item["designer_name"].string ?? ""
        wishlist.productColour = item["colour"].string ?? ""
        wishlist.productSize = item["size"].string ?? ""
        wishlist.productColourCode = item["colour_code"].string ?? ""
        wishlist.productInStock = item["in_stock"].bool ?? false
        wishlist.productStock = item["stock"].int64 ?? 0
        return wishlist
    }
}
