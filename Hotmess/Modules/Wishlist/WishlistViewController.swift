//
//  WishlistViewController.swift
//  Hotmess
//
//  Created by Cedan Misquith on 22/07/21.
//

import UIKit

class WishlistViewController: UIViewController {
    @IBOutlet weak var emptyListTitleLabel: UILabel!
    @IBOutlet weak var emptyListSubtitleLabel: UILabel!
    @IBOutlet weak var backToShoppingButton: UIButton!
    @IBOutlet weak var topBarContainerView: UIView!
    @IBOutlet weak var emptyListIndicatorView: UIView!
    @IBOutlet weak var bagCountLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var itemCountLabel: UILabel!
    @IBOutlet weak var sortByTitleLabel: UILabel!
    @IBOutlet weak var shareTitleLabel: UILabel!
    @IBOutlet weak var filterNameLabel: UILabel!
    @IBOutlet weak var selectedFilterContainerView: UIView!
    @IBOutlet weak var removeAllButton: UIButton!
    @IBOutlet weak var topBarHeightConstraint: NSLayoutConstraint!
    var presenter: WishlistPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = WishlistPresenter()
        presenter.delegate = self
        self.view.showLoader()
        presenter.getWishlist()
        applyStyling()
        applyFont()
        configureCollectionView()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func applyStyling() {
        selectedFilterContainerView.isHidden = true
        titleLabel.textColor = .white
        bagCountLabel.textColor = .white
        filterNameLabel.textColor = ColourConstants.hex717171
        itemCountLabel.textColor = ColourConstants.hex191919
        shareTitleLabel.textColor = ColourConstants.hex3C3C3C
        sortByTitleLabel.textColor = ColourConstants.hex3C3C3C
        removeAllButton.setTitleColor(ColourConstants.hex3C3C3C, for: .normal)
        emptyListTitleLabel.textColor = ColourConstants.hex191919
        emptyListSubtitleLabel.textColor = ColourConstants.hex191919
    }
    func applyFont() {
        emptyListTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        emptyListSubtitleLabel.font = UIFont(name: FontConstants.light, size: 13)
        backToShoppingButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        filterNameLabel.font = UIFont(name: FontConstants.medium, size: 13)
        bagCountLabel.font = UIFont(name: FontConstants.regular, size: 7)
        titleLabel.font = UIFont(name: FontConstants.regular, size: 14)
        itemCountLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        sortByTitleLabel.font = UIFont(name: FontConstants.regular, size: 13)
        shareTitleLabel.font = UIFont(name: FontConstants.regular, size: 13)
        removeAllButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 13)
    }
    // MARK: Configure collectionview layout
    func configureCollectionView() {
        collectionView.parallaxHeader.view = topBarContainerView
        collectionView.parallaxHeader.height = 90
        collectionView.parallaxHeader.mode = .fill
        collectionView.bounces = false
        collectionView.parallaxHeader.minimumHeight = 0
        collectionView.register(UINib(nibName: "WishlistCustomCell", bundle: nil),
                                            forCellWithReuseIdentifier: "WishlistCustomCell")
        let collectionViewLayout = CustomWishlistGridCollectionViewLayout()
        collectionView.collectionViewLayout = collectionViewLayout
    }
    @objc func removeItemButtonAction(sender: UIButton) {
        self.view.showLoader()
        presenter.remove(for: sender.tag)
    }
    @objc func addToBagButtonAction(sender: UIButton) {
        self.view.showLoader()
        presenter.addToBag(for: sender.tag)
        Vibration.pulse.vibrate()
    }
    func showRemoveConfirmationPopUp(removeAll: Bool, index: Int = 0) {
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: "REMOVE ALL",
            subTitle: "Are you sure you want to remove all items from your wishlist ?",
            buttonTitle: "REMOVE NOW") {
            self.view.showLoader()
            self.presenter.removeAllFromWishlist()
        }
        self.view.addSubview(popUp)
    }
    @objc func filterSelectButtonAction(sender: UIButton) {
        self.view.showLoader()
        presenter.selectedFilter = sender.tag
        presenter.setCurrentFilters()
        presenter.getWishlist()
        selectedFilterContainerView.isHidden = false
    }
    // MARK: - Share PopUp Method
    func showSharePopUp(stringURL: String) {
        let textToShare = [stringURL]
        let activityViewController = UIActivityViewController(
            activityItems: textToShare,
            applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.airDrop,
            UIActivity.ActivityType.postToFacebook
        ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func showFiltersButtonAction(_ sender: UIButton) {
        let filtersPopUp = Utilities.sharedInstance.showSortByPopUp(bounds: self.view.bounds)
        for button in filtersPopUp.filterButtons {
            button.addTarget(self, action: #selector(filterSelectButtonAction), for: .touchUpInside)
        }
        self.view.addSubview(filtersPopUp)
    }
    @IBAction func clearFilterButtonAction(_ sender: UIButton) {
        filterNameLabel.text = ""
        selectedFilterContainerView.isHidden = true
        self.view.showLoader()
        presenter.selectedFilter = -1
        presenter.setCurrentFilters()
        presenter.getWishlist()
    }
    @IBAction func searchButtonAction(_ sender: UIButton) {
        guard let nextvc = storyboard?.instantiateViewController(
                withIdentifier: "SharedWishlistViewController") as? SharedWishlistViewController else {
            return
        }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @IBAction func shareButtonAction(_ sender: UIButton) {
        self.view.showLoader()
        presenter.shareWishlist()
    }
    @IBAction func bagButtonAction(_ sender: UIButton) {
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func profileButtonAction(_ sender: UIButton) {
    }
    @IBAction func removeAllButtonAction(_ sender: UIButton) {
        showRemoveConfirmationPopUp(removeAll: true)
    }
}
extension WishlistViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.wishlistArray.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "WishlistCustomCell",
                for: indexPath) as? WishlistCustomCell else {
            return UICollectionViewCell()
        }
        if presenter.wishlistArray[indexPath.row].productInStock {
            cell.applyData(data: presenter.wishlistArray[indexPath.row])
            cell.grayedOutView.isHidden = true
        } else {
            cell.applyUnavailableData(data: presenter.wishlistArray[indexPath.row])
            cell.grayedOutView.isHidden = false
        }
        cell.removeButton.tag = indexPath.row
        cell.addToBagButton.tag = indexPath.row
        cell.removeButton.addTarget(self, action: #selector(removeItemButtonAction), for: .touchUpInside)
        cell.addToBagButton.addTarget(self, action: #selector(addToBagButtonAction), for: .touchUpInside)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        if indexPath.row >= presenter.wishlistArray.count - 1 {
            presenter.getMore()
        }
    }
}
extension WishlistViewController: WishlistPresenterProtocol {
    func alertMessage(title: String, message: String) {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: title,
            subTitle: message,
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)
    }
    func shareWishlist() {
        self.view.hideLoader()
        share()
    }
    func sortedBy(type: Int) {
        switch type {
        case -1:
            filterNameLabel.text = ""
            selectedFilterContainerView.isHidden = true
        case 0:
            filterNameLabel.text = "Recommended"
        case 1:
            filterNameLabel.text = "Most Recently Added"
        case 2:
            filterNameLabel.text = "Price(Low to High)"
        default:
            filterNameLabel.text = "Price(High to Low)"
        }
        refresh()
    }
    func failedToListItem() {
        self.view.hideLoader()
        self.navigationController?.popViewController(animated: true)
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: "Error",
            subTitle: "Failed to list the items form Wishlist",
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)
    }
    func refresh() {
        self.view.hideLoader()
        if presenter.wishlistArray.count == 0 {
            topBarHeightConstraint.constant = 0
            topBarContainerView.isHidden = true
            emptyListIndicatorView.isHidden = false
            collectionView.isHidden = true
            collectionView.removeFromSuperview()
        } else {
            let itemString = presenter.totalCount == 1 ? "ITEM" : "ITEMS"
            itemCountLabel.text = "\(presenter.totalCount) \(itemString)"
            topBarHeightConstraint.constant = 90
            topBarContainerView.isHidden = false
            emptyListIndicatorView.isHidden = true
            collectionView.isHidden = false
            collectionView.reloadData()
        }
    }
}
