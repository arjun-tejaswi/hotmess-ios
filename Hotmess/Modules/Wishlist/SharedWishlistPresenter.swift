//
//  SharedWishlistPresenter.swift
//  Hotmess
//
//  Created by Cedan Misquith on 03/08/21.
//

import Foundation
import SwiftyJSON

class SharedWishlistPresenter: NSObject {
    var isFromDeepLink: Bool = false
    var wishlistID: String  = ""
    var wishlistName: String = ""
    var pageNumber: Int64 = 1
    var totalCount: Int = 0
    var selectedFilter: Int = -1
    var cartTotalItems: Int64 = 0
    var filters: [String: Any] = [
        "sort": [],
        "pagination":
            [
                "page_number": 1,
                "per_page": 10
            ]
    ]
    var wishlistArray = [WishlishRealm]()
    weak var delegate: WishlistPresenterProtocol?
    func requestSharedWishlist(wishlistID: String) {
        filters.updateValue(wishlistID, forKey: "wishlist_id")
        pageNumber = 1
        filters.updateValue([
            "page_number": pageNumber,
            "per_page": 10
        ], forKey: "pagination")

        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.sharedWishlistListing,
            prametres: filters,
            type: .withoutAuth,
            method: .POST,
            completion: self.handleSharedWishlist)
    }
    func handleSharedWishlist(response: JSON) {
        print(response)
        wishlistName = response["name"].string ?? ""
        self.totalCount = response["pagination"]["total_records"].int ?? 0
        if let wishlistItems = response["data"].array {
            self.wishlistArray.removeAll()
            for item in wishlistItems {
                self.wishlistArray.append(WishlistParser.sharedInstance.parseWishlist(item: item))
            }
            self.delegate?.sortedBy(type: self.selectedFilter)
        }
    }
    func addToBag(for index: Int) {
        let item = wishlistArray[index]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.addToBag,
            prametres: [
                [
                    "product_id": item.productID,
                    "sku_id": item.stockKeepUnitID,
                    "quantity": 1
                ]
            ],
            type: .withAuth,
            method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.addToBag(for: index)
                            }
                        } else {
                            self.delegate?.alertMessage(title: "Error", message: message)
                        }
                    }
                } else {
                    self.delegate?.refresh()
                }
            }
        }
    }
    func setCurrentFilters() {
        switch selectedFilter {
        case -1:
            filters.updateValue([], forKey: "sort")
        case 0:
            filters.updateValue(["is_recommended": true], forKey: "sort")
        case 1:
            filters.updateValue([], forKey: "sort")
        case 2:
            filters.updateValue(["price": "low-to-high"], forKey: "sort")
        default:
            filters.updateValue(["price": "high-to-low"], forKey: "sort")
        }
    }
    func getMore() {
        print("Triggering GET MORE")
        print(wishlistArray.count)
        print(totalCount)
        if wishlistArray.count < totalCount {
            pageNumber += 1
            filters.updateValue([
                "page_number": pageNumber,
                "per_page": 10
            ], forKey: "pagination")
            NetworkManager.sharedInstance.makeNetworkRequest(
                urlString: ApiConstants.sharedWishlistListing,
                prametres: filters,
                type: .withAuth,
                method: .POST) { response in
                if let status = response["error_status"].bool {
                    if status {
                        if let message = response["message"].string {
                            if message == "requestForRefresh" {
                                NetworkManager.sharedInstance.refreshBearerToken {
                                    self.getMore()
                                }
                            } else {
                                self.delegate?.failedToListItem()
                            }
                        }
                    } else {
                        self.totalCount = response["pagination"]["total_records"].int ?? 0
                        if let wishlistItems = response["data"].array {
                            for item in wishlistItems {
                                self.wishlistArray.append(WishlistParser.sharedInstance.parseWishlist(item: item))
                            }
                            self.delegate?.sortedBy(type: self.selectedFilter)
                        }
                    }
                }
            }
        }
    }

    func saveProductToBag(for index: Int) {
        let item = wishlistArray[index]
        if RealmDataManager.sharedInstance.isProductInBag(
            productID: item.productID,
            skuID: item.stockKeepUnitID) {
            RealmDataManager.sharedInstance.increaseProductQuantity(
                productId: item.productID,
                skuId: item.stockKeepUnitID)
            print(RealmDataManager.sharedInstance.getAllProductsFromBag())
            delegate?.refresh()
        } else {
            let productItem = ShoppingBagRealm()
            productItem.productID = item.productID
            productItem.stockKeepUnitID = item.stockKeepUnitID
            productItem.productColour = item.productColour
            productItem.productImageURL = item.productImageURL
            productItem.productPrice = item.productPrice
            productItem.productName = item.productName
            productItem.productSize = item.productSize
            productItem.productQuantity += 1
            productItem.productDescription = item.productDescription
            productItem.productDesignerName = item.productDesignerName
            productItem.productColorCode = item.productColourCode
            productItem.productInStock = item.productInStock
            RealmDataManager.sharedInstance.saveProductToBag(productInfo: productItem)
            print(RealmDataManager.sharedInstance.getAllProductsFromBag())
            delegate?.refresh()
        }
    }
}
