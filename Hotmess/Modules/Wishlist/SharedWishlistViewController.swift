//
//  SharedWishlistViewController.swift
//  Hotmess
//
//  Created by Cedan Misquith on 03/08/21.
//

import UIKit

class SharedWishlistViewController: UIViewController {
    @IBOutlet weak var topBarContainerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var itemsCountLabel: UILabel!
    @IBOutlet weak var sortByTitleLabel: UILabel!
    @IBOutlet weak var selectedFilterNameLabel: UILabel!
    @IBOutlet weak var wishlistNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sharedTitleLabel: UILabel!
    @IBOutlet weak var selectedFilterContainerView: UIView!
    @IBOutlet weak var bagCountLabel: UILabel!
    var presenter = SharedWishlistPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        self.view.showLoader()
        presenter.requestSharedWishlist(wishlistID: presenter.wishlistID)
        applyStyling()
        applyFont()
        configureCollectionView()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func applyStyling() {
        selectedFilterContainerView.isHidden = true
        titleLabel.textColor = .white
        bagCountLabel.textColor = .white
        selectedFilterNameLabel.textColor = ColourConstants.hex717171
        itemsCountLabel.textColor = ColourConstants.hex191919
        sortByTitleLabel.textColor = ColourConstants.hex3C3C3C
    }
    func applyFont() {
        selectedFilterNameLabel.font = UIFont(name: FontConstants.medium, size: 13)
        bagCountLabel.font = UIFont(name: FontConstants.regular, size: 7)
        titleLabel.font = UIFont(name: FontConstants.regular, size: 14)
        itemsCountLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        sortByTitleLabel.font = UIFont(name: FontConstants.regular, size: 13)
        wishlistNameLabel.font = UIFont(name: FontConstants.regular, size: 16)
        sharedTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 13)
    }
    // MARK: Configure collectionview layout
    func configureCollectionView() {
        collectionView.parallaxHeader.view = topBarContainerView
        collectionView.parallaxHeader.mode = .fill
        collectionView.bounces = false
        collectionView.parallaxHeader.minimumHeight = 0
        collectionView.register(UINib(nibName: "WishlistCustomCell", bundle: nil),
                                forCellWithReuseIdentifier: "WishlistCustomCell")
        let collectionViewLayout = CustomWishlistGridCollectionViewLayout()
        collectionView.collectionViewLayout = collectionViewLayout
    }
    @IBAction func shoppingBagButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "ShoppingBag", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ShoppingBagViewController")
                as? ShoppingBagViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @IBAction func sortByButtonAction(_ sender: UIButton) {
        let filtersPopUp = Utilities.sharedInstance.showSortByPopUp(bounds: self.view.bounds)
        for button in filtersPopUp.filterButtons {
            button.addTarget(self, action: #selector(filterSelectButtonAction), for: .touchUpInside)
        }
        self.view.addSubview(filtersPopUp)
    }
    @objc func filterSelectButtonAction(sender: UIButton) {
        self.view.showLoader()
        presenter.selectedFilter = sender.tag
        presenter.setCurrentFilters()
        presenter.requestSharedWishlist(wishlistID: presenter.wishlistID)
        selectedFilterContainerView.isHidden = false
    }
    @IBAction func clearFilterButtonAction(_ sender: UIButton) {
        selectedFilterNameLabel.text = ""
        selectedFilterContainerView.isHidden = true
        self.view.showLoader()
        presenter.selectedFilter = -1
        presenter.setCurrentFilters()
        presenter.requestSharedWishlist(wishlistID: presenter.wishlistID)
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        if presenter.isFromDeepLink {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                fatalError("Not of type 'AppDelegate'")
            }
            appDelegate.restartApp()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    @objc func addToBagButtonAction(sender: UIButton) {
        self.view.showLoader()
        if UserDefaults.standard.bool(forKey: "isLoggedIn") == true {
            presenter.addToBag(for: sender.tag)
            Vibration.pulse.vibrate()
        } else {
            presenter.saveProductToBag(for: sender.tag)
        }
        Vibration.pulse.vibrate()
    }
}
extension SharedWishlistViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.wishlistArray.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "WishlistCustomCell",
                for: indexPath) as? WishlistCustomCell else {
            return UICollectionViewCell()
        }
        if presenter.wishlistArray[indexPath.row].productInStock {
            cell.applyData(data: presenter.wishlistArray[indexPath.row])
            cell.grayedOutView.isHidden = true
        } else {
            cell.applyUnavailableData(data: presenter.wishlistArray[indexPath.row])
            cell.grayedOutView.isHidden = false
        }
        cell.removeButton.tag = indexPath.row
        cell.addToBagButton.tag = indexPath.row
        cell.removeButton.isHidden = true
        cell.addToBagButton.addTarget(self, action: #selector(addToBagButtonAction), for: .touchUpInside)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        if indexPath.row >= presenter.wishlistArray.count - 1 {
            presenter.getMore()
        }
    }
}
extension SharedWishlistViewController: WishlistPresenterProtocol {
    func shareWishlist() {
        // Do Nothing
    }
    func sortedBy(type: Int) {
        switch type {
        case -1:
            selectedFilterNameLabel.text = ""
            selectedFilterContainerView.isHidden = true
        case 0:
            selectedFilterNameLabel.text = "Recommended"
        case 1:
            selectedFilterNameLabel.text = "Most Recently Added"
        case 2:
            selectedFilterNameLabel.text = "Price(Low to High)"
        default:
            selectedFilterNameLabel.text = "Price(High to Low)"
        }
        refresh()
    }
    func failedToListItem() {
        self.view.hideLoader()
        self.navigationController?.popViewController(animated: true)
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: "Error",
            subTitle: "Failed to list the items form Wishlist",
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)
    }
    func refresh() {
        self.view.hideLoader()
        wishlistNameLabel.text = "\(presenter.wishlistName)'s Wishlist"
        let itemString = presenter.wishlistArray.count == 1 ? "ITEM" : "ITEMS"
        itemsCountLabel.text = "\(presenter.wishlistArray.count) \(itemString)"
        bagCountLabel.text = "\(RealmDataManager.sharedInstance.getAllProductsFromBag().count)"
        collectionView.reloadData()
    }
    func alertMessage(title: String, message: String) {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: title,
            subTitle: message,
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)
    }
}
