//
//  WishlistPresenter.swift
//  Hotmess
//
//  Created by Cedan Misquith on 22/07/21.
//

import Foundation
import SwiftyJSON

protocol WishlistPresenterProtocol: AnyObject {
    func alertMessage(title: String, message: String)
    func refresh()
    func failedToListItem()
    func sortedBy(type: Int)
    func shareWishlist()
}

class WishlistPresenter: NSObject {
    weak var delegate: WishlistPresenterProtocol?
    var wishlistArray = [WishlishRealm]()
    var pageNumber: Int64 = 1
    var totalCount: Int = 0
    var selectedFilter: Int = -1
    var wishlistId: String = ""
    var filters: [String: Any] = [
        "sort": [],
        "pagination":
            [
                "page_number": 1,
                "per_page": 10
            ]
    ]
    func addToBag(for index: Int) {
        let item = wishlistArray[index]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.addToBag,
            prametres: [
                [
                    "product_id": item.productID,
                    "sku_id": item.stockKeepUnitID,
                    "quantity": 1
                ]
            ],
            type: .withAuth,
            method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.addToBag(for: index)
                            }
                        } else {
                            self.delegate?.alertMessage(title: "Error", message: message)
                        }
                    }
                } else {
                    self.delegate?.refresh()
                }
            }
        }
    }
    func remove(for index: Int) {
        let item = wishlistArray[index]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.removeFromWishlist,
            prametres: [
                [
                    "sku_id": item.stockKeepUnitID
                ]
            ],
            type: .withAuth,
            method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.remove(for: index)
                            }
                        } else {
                            self.selectedFilter = -1
                            self.setCurrentFilters()
                            self.getWishlist()
                        }
                    }
                } else {
                    self.wishlistArray.remove(at: index)
                    self.delegate?.refresh()
                }
            }
        }
    }
    func removeAllFromWishlist() {
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.removeAllFromWishlist,
                                                         prametres: [:],
                                                         type: .withAuth,
                                                         method: .POST,
                                                         completion: handleResponseForRemoveAll)
    }
    func handleResponseForRemoveAll(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.removeAllFromWishlist()
                        }
                    } else {
                        selectedFilter = -1
                        setCurrentFilters()
                        getWishlist()
                    }
                }
            } else {
                wishlistArray.removeAll()
                delegate?.refresh()
            }
        }
    }
    func setCurrentFilters() {
        switch selectedFilter {
        case -1:
            filters.updateValue([], forKey: "sort")
        case 0:
            filters.updateValue(["is_recommended": true], forKey: "sort")
        case 1:
            filters.updateValue([], forKey: "sort")
        case 2:
            filters.updateValue(["price": "low-to-high"], forKey: "sort")
        default:
            filters.updateValue(["price": "high-to-low"], forKey: "sort")
        }
    }
    func getMore() {
        print("Triggering GET MORE")
        print(wishlistArray.count)
        print(totalCount)
        if wishlistArray.count < totalCount {
            pageNumber += 1
            filters.updateValue([
                "page_number": pageNumber,
                "per_page": 10
            ], forKey: "pagination")
            NetworkManager.sharedInstance.makeNetworkRequest(
                urlString: ApiConstants.wishlist,
                prametres: filters,
                type: .withAuth,
                method: .POST) { response in
                if let status = response["error_status"].bool {
                    if status {
                        if let message = response["message"].string {
                            if message == "requestForRefresh" {
                                NetworkManager.sharedInstance.refreshBearerToken {
                                    self.getMore()
                                }
                            } else {
                                self.delegate?.failedToListItem()
                            }
                        }
                    } else {
                        self.totalCount = response["pagination"]["total_records"].int ?? 0
                        if let wishlistItems = response["data"].array {
                            for item in wishlistItems {
                                self.wishlistArray.append(WishlistParser.sharedInstance.parseWishlist(item: item))
                            }
                            self.delegate?.sortedBy(type: self.selectedFilter)
                        }
                    }
                }
            }
        }
    }
    func shareWishlist() {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.shareWishlist,
            prametres: [:],
            type: .withAuth,
            method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if !status {
                    self.wishlistId = response["data"]["wishlist_id"].string ?? ""
                    self.delegate?.shareWishlist()
                } else {
                    self.delegate?.refresh()
                }
            }
        }
    }
    func getWishlist() {
        pageNumber = 1
        filters.updateValue([
            "page_number": pageNumber,
            "per_page": 10
        ], forKey: "pagination")
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.wishlist,
            prametres: filters,
            type: .withAuth,
            method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.selectedFilter = -1
                                self.setCurrentFilters()
                                self.getWishlist()
                            }
                        } else {
                            self.delegate?.failedToListItem()
                        }
                    }
                } else {
                    self.totalCount = response["pagination"]["total_records"].int ?? 0
                    if let wishlistItems = response["data"].array {
                        self.wishlistArray.removeAll()
                        for item in wishlistItems {
                            self.wishlistArray.append(WishlistParser.sharedInstance.parseWishlist(item: item))
                        }
                        self.delegate?.sortedBy(type: self.selectedFilter)
                    }
                }
            }
        }
    }
}
