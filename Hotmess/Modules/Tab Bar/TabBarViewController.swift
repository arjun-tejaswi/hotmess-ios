//
//  TabBarViewController.swift
//  Hotmess
//
//  Created by Flaxon on 27/05/21.
//

import UIKit
import DeviceKit
import SideMenu

protocol TabbarProtocol: AnyObject {
    func tabbarToBack()
    func tabbarToFront()
    func slideMenu(isVisible: Bool)
}
class TabBarViewController: UIViewController {
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var categegoryOptionImageView: UIImageView!
    @IBOutlet weak var categoryOptionsContainerView: UIView!
    @IBOutlet weak var categoryTabButton: UIButton!
    @IBOutlet weak var tabbarButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tabbarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tabbarBottomConstraint: NSLayoutConstraint!
    @IBOutlet var tabBarIcons: [UIImageView]!
    @IBOutlet var tabBarTitles: [UILabel]!
    @IBOutlet var tabBarButtons: [UIButton]!
    @IBOutlet weak var tabBarButton04: UIButton!
    @IBOutlet weak var tabBarButton03: UIButton!
    @IBOutlet weak var tabBarButton02: UIButton!
    @IBOutlet weak var tabBarButton01: UIButton!
    @IBOutlet weak var tabbarView: UIView!
    @IBOutlet weak var tabbarClippingBotomView: UIView!
    @IBOutlet weak var categoryTabView: UIView!
    // MARK: - View Controllers
    var landingView: LandingViewController!
    var shopView: ShopViewController!
    var designerView: DesignerViewController!
    var editorialView: EditorialViewController!
    var selectedIndex: Int = 0
    var viewControllers: [UIViewController]!
    var originalHeight: CGRect!
    var isOpened: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyling()
        bringTabbarToFront()
        categoryOptionViewIntialConfiguration()
        configureViewControllers()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleSlideMenu),
                                               name: Notification.Name("SHOW_SLIDE_MENU"),
                                               object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("SHOW_SLIDE_MENU"), object: nil)
    }
    // MARK: - Slide In Menu Method
    @objc func handleSlideMenu() {
        let storyBoard = UIStoryboard(name: "SlideMenu", bundle: nil)
        guard let slideVC = storyBoard.instantiateViewController(
                withIdentifier: "SlideMenuViewController") as? SlideMenuViewController
        else {
            return
        }
        let sideMenu = SideMenuNavigationController(rootViewController: slideVC)
        sideMenu.presentationStyle = .menuSlideIn
        sideMenu.isNavigationBarHidden = true
        sideMenu.blurEffectStyle = .dark
        blurView.isHidden = false
        self.view.bringSubviewToFront(blurView)
        sideMenu.menuWidth = self.view.frame.width - 64
        self.present(sideMenu, animated: true, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - Category Switch Action
    @IBAction func categoryTabbuttonAction(_ sender: UIButton) {
        if isOpened {
            dismiss()
        } else {
            show()
        }
        isOpened = !isOpened
    }
    @IBAction func didPressTabBar(sender: UIButton) {
        toggleButtonMode(tagValue: sender.tag)
        var previousIndex = selectedIndex
        selectedIndex = sender.tag
        switch selectedIndex {
        case 0, 1, 2, 3:
            previousIndex = selectedIndex
            tabBarButtons[previousIndex].isSelected = false
            let previousVC = viewControllers[previousIndex]
            previousVC.willMove(toParent: nil)
            previousVC.view.removeFromSuperview()
            previousVC.removeFromParent()
            let viewController = viewControllers[selectedIndex]
            addChild(viewController)
            let device = Device.current
            if device.isTouchIDCapable {
                viewController.view.frame = CGRect(x: 0, y: 0,
                                                   width: self.view.frame.width, height: self.view.frame.height - 60)
                tabbarHeightConstraint.constant = 60
                tabbarButtonBottomConstraint.constant = 0
                tabbarBottomConstraint.constant = 0
            } else {
                viewController.view.frame = CGRect(x: 0, y: 0,
                                                   width: self.view.frame.width, height: self.view.frame.height - 80)
                tabbarHeightConstraint.constant = 80
                tabbarButtonBottomConstraint.constant = 25
                tabbarBottomConstraint.constant = 0
            }
            self.view.addSubview(viewController.view)
            viewController.didMove(toParent: self)
        case 4:
            print("Home")
        default:
            print("Kids")
        }
        bringTabbarToFront()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        for optionButtons in tabBarButtons {
            if optionButtons.tag == 4 {
                optionButtons.layer.cornerRadius = optionButtons.frame.width / 2
                optionButtons.contentMode = .center
                optionButtons.backgroundColor = ColourConstants.hexFFFFFF
                optionButtons.setTitle("HOME", for: .normal)
                optionButtons.titleLabel?.font = UIFont(name: FontConstants.semiBold, size: 14)
                optionButtons.layer.applySketchShadow(
                    color: .black,
                    alpha: 0.25,
                    size: CGSize(width: 2, height: 2),
                    blur: 6, spread: 0)
            } else if optionButtons.tag == 5 {
                optionButtons.layer.cornerRadius = optionButtons.frame.width / 2
                optionButtons.contentMode = .center
                optionButtons.backgroundColor = ColourConstants.hexFFFFFF
                optionButtons.setTitle("KIDS", for: .normal)
                optionButtons.titleLabel?.font = UIFont(name: FontConstants.semiBold, size: 14)
                optionButtons.layer.applySketchShadow(
                    color: .black,
                    alpha: 0.25,
                    size: CGSize(width: 2, height: 2),
                    blur: 6, spread: 0)
            }
        }
    }
    // MARK: - Category Option View Initial SetUp
    func categoryOptionViewIntialConfiguration() {
        originalHeight = categoryOptionsContainerView.bounds
        categoryOptionsContainerView.bounds = .zero
        categoryOptionsContainerView.alpha = 0
    }
    // MARK: - Category Buuton View Dismiss Method
    @objc func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 0,
                       initialSpringVelocity: 0, options: [], animations: {
                        self.categoryOptionsContainerView.alpha = 0
                        self.categoryOptionsContainerView.bounds = .zero
                       })
    }
    // MARK: - Category Buuton View Show Method
    func show() {
        categoryOptionsContainerView.alpha = 0
        categoryOptionsContainerView.frame = CGRect(
            x: originalHeight.minX,
            y: originalHeight.maxY,
            width: 50,
            height: 0)
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0, options: [], animations: {
                        self.categoryOptionsContainerView.alpha = 1
                        self.categoryOptionsContainerView.frame = self.originalHeight
                       })
    }
    // MARK: - TabBar SetUp
    func bringTabbarToFront() {
        self.view.bringSubviewToFront(categoryOptionsContainerView)
        self.view.bringSubviewToFront(tabbarClippingBotomView)
        self.view.bringSubviewToFront(tabbarView)
    }
    // MARK: - TabBar Toggle Method
    func toggleButtonMode(tagValue: Int) {
        for button in tabBarButtons {
            switch button.tag {
            case 0, 1, 2, 3:
                if button.tag == tagValue {
                    tabBarTitles[button.tag].textColor = ColourConstants.hex717171
                    tabBarIcons[button.tag].setImageColour(
                        color: ColourConstants.hex717171)
                } else {
                    tabBarTitles[button.tag].textColor = ColourConstants.hex9B9B9B
                    tabBarIcons[button.tag].setImageColour(
                        color: ColourConstants.hex9B9B9B)
                }
            default:
                break
            }
        }
    }
    // MARK: - View Controller SetUp
    func configureViewControllers() {
        let storyboardLanding = UIStoryboard(name: "LandingView", bundle: nil)
        let storyboardShop = UIStoryboard(name: "Shop", bundle: nil)
        let storyboardDesigner = UIStoryboard(name: "Designer", bundle: nil)
        let storyboardEditorial = UIStoryboard(name: "Editorial", bundle: nil)
        landingView = storyboardLanding.instantiateViewController(
            withIdentifier: "LandingViewController") as? LandingViewController
        shopView = storyboardShop.instantiateViewController(
            withIdentifier: "ShopViewController") as? ShopViewController
        designerView = storyboardDesigner.instantiateViewController(
            withIdentifier: "DesignerViewController") as? DesignerViewController
        editorialView = storyboardEditorial.instantiateViewController(
            withIdentifier: "EditorialViewController") as? EditorialViewController
        viewControllers = [landingView, shopView, designerView, editorialView]
        tabBarButtons[selectedIndex].isSelected = true
        didPressTabBar(sender: tabBarButtons[selectedIndex])
    }
}
// MARK: - Tab Bar Protocol Methods
extension TabBarViewController: TabbarProtocol {
    func slideMenu(isVisible: Bool) {
        if isVisible {
            blurView.isHidden = true
        } else {
            blurView.isHidden = false
        }
    }
    func tabbarToBack() {
        tabbarView.isUserInteractionEnabled = false
        tabbarClippingBotomView.isUserInteractionEnabled = false
        self.view.sendSubviewToBack(tabbarView)
        self.view.sendSubviewToBack(tabbarClippingBotomView)
    }
    func tabbarToFront() {
        tabbarView.isUserInteractionEnabled = true
        tabbarClippingBotomView.isUserInteractionEnabled = true
        bringTabbarToFront()
    }
}
extension TabBarViewController: SideMenuNavigationControllerDelegate {
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        blurView.isHidden = true
    }
}
