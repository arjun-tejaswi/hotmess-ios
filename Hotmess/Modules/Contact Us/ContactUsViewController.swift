//
//  ContactUsViewController.swift
//  Hotmess
//
//  Created by Akshatha on 30/09/21.
//

import UIKit
import MessageUI

class ContactUsViewController: UIViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var contactUsTitle: UILabel!
    @IBOutlet weak var contactUsSubtitle: UILabel!
    @IBOutlet weak var emailIdLabel: UILabel!
    @IBOutlet weak var faqLabel: UILabel!
    @IBOutlet weak var chatbotLabel: UILabel!
    @IBOutlet weak var hotmessImageview: UIImageView!
    let recipientEmail = "support@hotmessfashion.com"
    override func viewDidLoad() {
        super.viewDidLoad()
        applyFont()
        applyStyling()
        applyData()
        Freshchat.sharedInstance().resetUser(completion: nil)
        if !UserDefaults.standard.bool(forKey: "isLoggedIn") {
            let user = FreshchatUser.sharedInstance()
            user.firstName = "Hotmess"
            user.lastName = "Customer"
            Freshchat.sharedInstance().setUser(user)
        }
    }
    // MARK: - Statusbar Style Method
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func applyFont() {
        contactUsTitle.font = UIFont(name: FontConstants.semiBold, size: 18)
        contactUsSubtitle.font = UIFont(name: FontConstants.light, size: 14)
    }
    func applyStyling() {
        contactUsTitle.textColor = ColourConstants.hex191919
        contactUsSubtitle.textColor = ColourConstants.hex191919
        hotmessImageview.image = UIImage(named: "HOTMESS Logo")
    }
    @IBAction func chatButtonAction(_ sender: UIButton) {
        let options = ConversationOptions.init()
        options.filteredViewTitle()
        Freshchat.sharedInstance().showConversations(self)
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func applyData() {
        faqLabel.isUserInteractionEnabled = true
        emailIdLabel.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabel(_:)))
        tapGesture.numberOfTouchesRequired = 1
        faqLabel.addGestureRecognizer(tapGesture)
        let tapEmailGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnEmailLabel(_:)))
        tapEmailGesture.numberOfTouchesRequired = 1
        emailIdLabel.addGestureRecognizer(tapEmailGesture)
        setAttributedLables()
    }
    func setAttributedLables() {
        let lightText: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.light, size: 14) ?? UIFont.systemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex191919]
        let boldText: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.medium, size: 14) ?? UIFont.boldSystemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex191919]
        let messageOneString = NSMutableAttributedString(
            string: "Reach out to us at ",
            attributes: lightText)
        let emailIdString = NSMutableAttributedString(string: "support@hotmessfashion.com", attributes: boldText)
        let messageString = NSMutableAttributedString(
            string: " and we'll respond to your query within 2 business days.",
            attributes: lightText)
        emailIdString.append(messageString)
        messageOneString.append(emailIdString)
        emailIdLabel.attributedText = messageOneString
        let messageTwoString = NSMutableAttributedString(
            string: "For quick questions, check out our ",
            attributes: lightText)
        let faqString = NSMutableAttributedString(string: "FAQs", attributes: boldText)
        messageTwoString.append(faqString)
        faqLabel.attributedText = messageTwoString
        let messageThreeString = NSMutableAttributedString(
            string: "Or, for immediate support, you can find our ",
            attributes: lightText)
        let chatBotString = NSMutableAttributedString(string: "Chatbot ", attributes: boldText)
        let messageFourString = NSMutableAttributedString(
            string: "on the bottom right, and someone from our team will assist you.",
            attributes: lightText)
        chatBotString.append(messageFourString)
        messageThreeString.append(chatBotString)
        chatbotLabel.attributedText = messageThreeString
    }
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = faqLabel.text else { return }
        let wordRange = (text as NSString).range(of: "FAQs")
        if gesture.didTapAttributedTextInLabel(label: self.faqLabel, inRange: wordRange) {
            let storyBoard = UIStoryboard(name: "WebView", bundle: nil)
            guard let webViewVC = storyBoard.instantiateViewController(
                    withIdentifier: "WebViewController") as? WebViewController else {
                return
            }
            webViewVC.state = .FAQS
            self.navigationController?.pushViewController(webViewVC, animated: true)
        }
    }
    @objc func tappedOnEmailLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = emailIdLabel.text else { return }
        let wordRange = (text as NSString).range(of: "support@hotmessfashion.com")
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        if gesture.didTapAttributedTextInLabel(label: self.emailIdLabel, inRange: wordRange) {
            if MFMailComposeViewController.canSendMail() {
                composeVC.setToRecipients([recipientEmail])
                self.present(composeVC, animated: true, completion: nil)
            } else {
                let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                    bounds: self.view.bounds,
                    title: "Error",
                    subTitle: "Unable to send email. Try again later.",
                    buttonTitle: "OK") { return }
                self.view.addSubview(popUp)
            }
        }
    }
}
