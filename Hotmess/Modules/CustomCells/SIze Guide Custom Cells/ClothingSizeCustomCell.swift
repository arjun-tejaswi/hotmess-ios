//
//  TableViewCell.swift
//  Hotmess
//
//  Created by Flaxon on 17/05/21.
//

import UIKit

class ClothingSizeCustomCell: UITableViewCell {

    @IBOutlet weak var sizeValue: UILabel!
    @IBOutlet weak var sizeCode: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
    }
    // MARK: - Font Style Method
    func applyFont() {
        sizeValue.font = UIFont(name: FontConstants.regular, size: 10)
        sizeCode.font = UIFont(name: FontConstants.medium, size: 11)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
