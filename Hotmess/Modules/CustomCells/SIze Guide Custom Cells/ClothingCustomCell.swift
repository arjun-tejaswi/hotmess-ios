//
//  ClothingCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 17/05/21.
//

import UIKit

class ClothingCustomCell: UICollectionViewCell {
    @IBOutlet weak var sizeChartTableView: UITableView!
    @IBOutlet weak var sizeNumberLabel: UILabel!
    @IBOutlet weak var sizeNumberView: UIView!
    @IBOutlet weak var cardView: UIView!
    var countyCodes = ["AU", "DE", "DK", "FR", "IT", "JP", "RU", "SML", "UK", "US"]
    var countrySizes = ["AU 6", "DE 32", "DK 32", "FR 34", "IT 38", "JP 5", "RU 40", "XXS", "UK 6", "US 0"]
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
        sizeChartTableView.delegate = self
        sizeChartTableView.dataSource = self
        sizeChartTableView.register(UINib(nibName: "ClothingSizeCustomCell", bundle: nil),
                                    forCellReuseIdentifier: "ClothingSizeCustomCell")
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        cardView.layer.cornerRadius = 5
        sizeNumberView.layer.cornerRadius = 4
        cardView.layer.applySketchShadow(
            color: .black,
            alpha: 0.25,
            size: CGSize(width: 2, height: 2),
            blur: 6, spread: 0)
    }
    // MARK: - Font Style Method
    func applyFont() {
        sizeNumberLabel.font = UIFont(name: FontConstants.semiBold, size: 10)
    }
}
// MARK: - TableView Methods
extension ClothingCustomCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countyCodes.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ClothingSizeCustomCell",
                                                       for: indexPath) as? ClothingSizeCustomCell else {
            return UITableViewCell()
        }
        cell.sizeCode.text = countyCodes[indexPath.item]
        cell.sizeValue.text = countrySizes[indexPath.item]
        return cell
    }
}
