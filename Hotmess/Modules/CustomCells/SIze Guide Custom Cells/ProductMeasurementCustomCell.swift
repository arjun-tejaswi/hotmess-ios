//
//  ProductMeasurementCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 17/05/21.
//

import UIKit

class ProductMeasurementCustomCell: UICollectionViewCell {
    @IBOutlet weak var measurementTableView: UITableView!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    var attributes: [SizeAttributesRealm]!
    var metrics: Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
        measurementTableView.delegate = self
        measurementTableView.dataSource = self
        measurementTableView.register(UINib(nibName: "ClothingSizeCustomCell",
                                            bundle: nil),
                                      forCellReuseIdentifier: "ClothingSizeCustomCell")
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        cardView.layer.cornerRadius = 5
        cardView.layer.applySketchShadow(
            color: .black,
            alpha: 0.25,
            size: CGSize(width: 2, height: 2),
            blur: 6, spread: 0)
    }
    // MARK: - Font Style Method
    func applyFont() {
        sizeLabel.font = UIFont(name: FontConstants.medium, size: 12)
    }
}
// MARK: - TableView Methods
extension ProductMeasurementCustomCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attributes == nil ? 0 : attributes.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ClothingSizeCustomCell",
                                                       for: indexPath) as? ClothingSizeCustomCell else {
            return UITableViewCell()
        }
        cell.sizeCode.text = attributes[indexPath.row].key
        let sizeValue = attributes[indexPath.row].value
        if metrics == 0 {
            cell.sizeValue.text = "\(sizeValue)"
        } else {
            let inchValue = Double(sizeValue)/2.54
            cell.sizeValue.text = String(format: "%0.1f", inchValue)
        }
        return cell
    }
}
