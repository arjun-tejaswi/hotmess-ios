//
//  PaymentOptionTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 02/08/21.
//

import UIKit

class PaymentOptionTableViewCell: UITableViewCell {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var paymentTypeLabel: UILabel!
    @IBOutlet weak var isSelectedImageView: UIImageView!
    @IBOutlet weak var paymentImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyBordersToFirstCell()
        applyBordersToOtherCells()
        applyStyling()
        applyFont()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func applyData(paymentObject: SingleSelectOptionsModel) {
        paymentTypeLabel.text = paymentObject.title
         let image = paymentObject.isSelected ?? false ? UIImage(
        named: "accountCreatedCheck") : UIImage(named: "orderUncheck")
        isSelectedImageView.image = image
        if paymentObject.title == "Pay with Debit or Credit Card" {
            paymentImageView.image = UIImage(named: "Card")
        } else {
            paymentImageView.image = UIImage(named: "Cash")
    }
    }
    func applyBordersToFirstCell() {
        cardView.addBorders(edges: [.top, .left, .right, .bottom],
                        color: ColourConstants.hex8B8B8B)
    }
    func applyBordersToOtherCells() {
        cardView.addBorders(edges: [.left, .right, .bottom],
                        color: ColourConstants.hex8B8B8B)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        paymentTypeLabel.applyLetterSpacing(spacing: 0)
    }
    // MARK: - Font Style Method
    func applyFont() {
        paymentTypeLabel.font = UIFont(name: FontConstants.regular, size: 14)
    }
}
