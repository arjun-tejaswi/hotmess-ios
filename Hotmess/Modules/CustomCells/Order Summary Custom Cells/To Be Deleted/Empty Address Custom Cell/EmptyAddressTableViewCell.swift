//
//  EmptyAddressTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 21/07/21.
//

import UIKit

class EmptyAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var addAddressButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        addAddressButton.applyButtonLetterSpacing(spacing: 0)
        cardView.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
        cardView.layer.borderWidth = 1.0
    }
    // MARK: - Font Style Method
    func applyFont() {
        addAddressButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 16)
    }
}
