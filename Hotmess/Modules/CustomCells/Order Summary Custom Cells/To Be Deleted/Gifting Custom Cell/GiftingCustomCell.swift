//
//  GiftingCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 30/07/21.
//

import UIKit

class GiftingCustomCell: UITableViewCell {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var giftingToTextField: UITextField!
        @IBOutlet weak var phoneNumberTextField: UITextField!
        @IBOutlet weak var messageTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
    }
    func applyStyling() {
        cardView.addBorders(edges: [.left, .right, .bottom], color: ColourConstants.hex8B8B8B)
        applyTextFieldStyling(textFields: [
            giftingToTextField,
            phoneNumberTextField,
            messageTextField
        ])
        giftingToTextField.leftView = UIView(frame: CGRect(x: 0, y: 0,
                width: 10, height: giftingToTextField.frame.height))
        giftingToTextField.leftViewMode = .always
        phoneNumberTextField.leftView = UIView(frame: CGRect(x: 0, y: 0,
                            width: 10, height: phoneNumberTextField.frame.height))
        phoneNumberTextField.leftViewMode = .always
        messageTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: messageTextField.frame.height))
        messageTextField.leftViewMode = .always
    }
    func applyTextFieldStyling(textFields: [UITextField]) {
        for textField in textFields {
//            textField.layer.borderWidth = 1
            textField.layer.backgroundColor = ColourConstants.hexF8F8F8.cgColor
            textField.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
            textField.font = UIFont(name: FontConstants.regular, size: 13)
            textField.setLeftPaddingPoints(10)
            textField.setRightPaddingPoints(10)
        }
    }
}
