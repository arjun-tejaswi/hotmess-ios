//
//  PackingGiftingTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 19/07/21.
//

import UIKit

class PackingGiftingTableViewCell: UITableViewCell {
    @IBOutlet weak var addMessageButton: UIButton!
    @IBOutlet weak var optionalView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var giftTypeImageView: UIImageView!
    @IBOutlet weak var giftTypeLabel: UILabel!
    @IBOutlet weak var giftTypeCurrencyLabel: UILabel!
    @IBOutlet weak var isCheckedImageView: UIImageView!
    @IBOutlet weak var giftTypePriceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    func applyData(packingObject: SingleSelectOptionsModel) {
        giftTypeLabel.text = packingObject.title
        giftTypePriceLabel.text = packingObject.price
        giftTypeCurrencyLabel.text = packingObject.price == "FREE" ? "" : "₹"
        let image = packingObject.isSelected ?? false ? UIImage(
            named: "accountCreatedCheck") : UIImage(named: "orderUncheck")
        isCheckedImageView.image = image
        if packingObject.title == "Regular Packing" {
            giftTypeImageView.image = UIImage(named: "Normal-Packing")
            optionalView.isHidden = true
        } else {
            giftTypeImageView.image = UIImage(named: "Premium Gift Packing")
            if packingObject.isSelected == false {
                optionalView.isHidden = true
                applyBordersToOtherCells()
            } else {
                optionalView.isHidden = false
                applyTo2Cells()
            }
        }
    }
    func applyTo2Cells() {
        cardView.addBorders(edges: [.left, .right],
                        color: ColourConstants.hex8B8B8B)
    }
    func applyBordersToFirstCell() {
        cardView.addBorders(edges: [.top, .left, .right, .bottom],
                        color: ColourConstants.hex8B8B8B)
    }
    func applyBordersToOtherCells() {
        cardView.addBorders(edges: [.left, .right, .bottom],
                        color: ColourConstants.hex8B8B8B)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        giftTypeLabel.applyLetterSpacing(spacing: 0.7)
    }
    // MARK: - Font Style Method
    func applyFont() {
        giftTypeLabel.font = UIFont(name: FontConstants.regular, size: 14)
        giftTypePriceLabel.font = UIFont(name: FontConstants.medium, size: 14)
    }
}
