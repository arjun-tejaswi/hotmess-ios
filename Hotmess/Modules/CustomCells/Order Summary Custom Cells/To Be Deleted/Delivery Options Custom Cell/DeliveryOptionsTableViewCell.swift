//
//  ShippingAddressTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 19/07/21.
//

import UIKit

class DeliveryOptionsTableViewCell: UITableViewCell {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var deliveryTypeTitle: UILabel!
    @IBOutlet weak var deliveryTypeSubtitle: UILabel!
    @IBOutlet weak var isCheckedImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    func applyData(deliveryObject: SingleSelectOptionsModel) {
        deliveryTypeTitle.text = deliveryObject.title
        deliveryTypeSubtitle.text = deliveryObject.subtitle
        let image = deliveryObject.isSelected ?? false ? UIImage(
            named: "accountCreatedCheck") : UIImage(named: "orderUncheck")
        isCheckedImageView.image = image
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        deliveryTypeTitle.applyLetterSpacing(spacing: 0.7)
        deliveryTypeSubtitle.applyLetterSpacing(spacing: 0.5)
    }
    // MARK: - Font Style Method
    func applyFont() {
        deliveryTypeTitle.font = UIFont(name: FontConstants.medium, size: 13)
        deliveryTypeSubtitle.font = UIFont(name: FontConstants.light, size: 12)
    }
    func applyBordersToFirstCell() {
        cardView.addBorders(edges: [.top, .left, .right, .bottom],
                        color: ColourConstants.hex8B8B8B)
    }
    func applyBordersToOtherCells() {
        cardView.addBorders(edges: [.left, .right, .bottom],
                        color: ColourConstants.hex8B8B8B)
    }
}
