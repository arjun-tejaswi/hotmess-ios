//
//  DeliveryOptionsTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 19/07/21.
//

import UIKit

class ShippingAddressTableViewCell: UITableViewCell {
    @IBOutlet weak var defaultAddressLabel: UILabel!
    @IBOutlet weak var removeAddressButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mobileNumberCodeLabel: UILabel!
    @IBOutlet weak var isCheckedImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
        applyBordersToFirstCell()
    }
    func applyData(shippingObject: ShippingAddressRealm) {
        nameLabel.text = "\(shippingObject.firstName)" + " \(shippingObject.lastName)"
        addressLabel.text = "\(shippingObject.addressLine1), "
            + "\(shippingObject.addressLine2),"
            + "\n\(shippingObject.city), "
            + "\(shippingObject.state) "
            + "- \(shippingObject.areaPinCode)"
            + "\nLandmark: \(shippingObject.landMark)"
        if shippingObject.isSelected {
            isCheckedImageView.image = UIImage(named: "accountCreatedCheck")
        } else {
            isCheckedImageView.image = UIImage(named: "orderUncheck")
        }
    }
    func applyBordersToFirstCell() {
        cardView.addBorders(edges: [.top, .left, .right, .bottom],
                        color: ColourConstants.hex8B8B8B)
    }
    func applyBorderToLastCell() {
        cardView.addBorders(edges: [.left, .right, .bottom],
                        color: ColourConstants.hex8B8B8B)
    }
    func applyBordersToOtherCells() {
        cardView.addBorders(edges: [.left, .right, .bottom],
                        color: ColourConstants.hex8B8B8B)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        nameLabel.applyLetterSpacing(spacing: 0.7)
        addressLabel.textColor = ColourConstants.hex191919
        mobileNumberCodeLabel.textColor = ColourConstants.hex191919
        addressLabel.applyLetterSpacing(spacing: 0.5)
    }
    // MARK: - Font Style Method
    func applyFont() {
        nameLabel.font = UIFont(name: FontConstants.medium, size: 18)
        addressLabel.font = UIFont(name: FontConstants.light, size: 16)
        mobileNumberCodeLabel.font = UIFont(name: FontConstants.regular, size: 16)
        editButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        removeAddressButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        defaultAddressLabel.font = UIFont(name: FontConstants.regular, size: 13)
    }
}
