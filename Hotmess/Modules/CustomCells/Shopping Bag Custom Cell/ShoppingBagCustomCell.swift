//
//  ShoppingBagCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 02/06/21.
//

import UIKit

class ShoppingBagCustomCell: UITableViewCell {
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var unavailableLabel: UILabel!
    @IBOutlet weak var bottomSeparationView: UIView!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productCurrencyLabel: UILabel!
    @IBOutlet weak var productSubTitleLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var quantityTileLabel: UILabel!
    @IBOutlet weak var increaseQuantityButton: UIButton!
    @IBOutlet weak var sizeTitleLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var quantityCountLabel: UILabel!
    @IBOutlet weak var decreaseQuantityButton: UIButton!
    @IBOutlet weak var quantityContainerView: UIView!
    @IBOutlet weak var colorTitleLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var stockLabel: UILabel!
    @IBOutlet weak var colorIndicatorView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
    }
    // MARK: - Apply data to UI
    func applyData(item: ShoppingBagRealm) {
        let url = URL(string: item.productImageURL)
        NukeManager.sharedInstance.setImage(url: url, imageView: productImageView, withPlaceholder: true)
        productSubTitleLabel.text = item.productDescription
        productNameLabel.text = item.productDesignerName
        quantityCountLabel.text = "\(item.productQuantity)"
        sizeLabel.text = item.productSize
        productPriceLabel.text = "\(item.productPrice)"
        colorLabel.text = item.productColour
        overlayView.isHidden = true
        unavailableLabel.isHidden = true
    }
    func applyUnavailableData(item: ShoppingBagRealm) {
        let url = URL(string: item.productImageURL)
        NukeManager.sharedInstance.setImage(url: url, imageView: productImageView, withPlaceholder: true)
        productSubTitleLabel.text = item.productDescription
        productNameLabel.text = item.productDesignerName
        quantityCountLabel.text = "\(item.productQuantity)"
        sizeLabel.text = item.productSize
        productPriceLabel.text = "\(item.productPrice)"
        colorLabel.text = item.productColour
        overlayView.isHidden = false
        unavailableLabel.isHidden = false
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        colorIndicatorView.layer.cornerRadius = colorIndicatorView.frame.width / 2
        colorIndicatorView.layer.borderWidth = 0.5
        colorIndicatorView.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
        increaseQuantityButton.setTitleColor(ColourConstants.hex707070, for: .normal)
        decreaseQuantityButton.setTitleColor(ColourConstants.hex707070, for: .normal)
        quantityContainerView.layer.borderWidth = 1
        quantityContainerView.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
        productNameLabel.applyLetterSpacing(spacing: 0.7)
        quantityLabel.textColor = ColourConstants.hex3C3C3C
        quantityTileLabel.textColor = ColourConstants.hex3C3C3C
        sizeTitleLabel.textColor = ColourConstants.hex3C3C3C
        sizeLabel.textColor = ColourConstants.hex3C3C3C
        colorTitleLabel.textColor = ColourConstants.hex3C3C3C
        colorLabel.textColor = ColourConstants.hex3C3C3C
        stockLabel.textColor = ColourConstants.hex3C3C3C
    }
    // MARK: - Font Stlying Method
    func applyFont() {
        colorTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
        colorLabel.font = UIFont(name: FontConstants.medium, size: 14)
        stockLabel.font = UIFont(name: FontConstants.light, size: 10)
        sizeTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
        sizeLabel.font = UIFont.init(name: FontConstants.medium, size: 14)
        productNameLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        productSubTitleLabel.font = UIFont(name: FontConstants.light, size: 12)
        quantityLabel.font = UIFont(name: FontConstants.medium, size: 16)
        quantityTileLabel.font = UIFont(name: FontConstants.regular, size: 14)
        productCurrencyLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        productPriceLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        unavailableLabel.font = UIFont(name: FontConstants.regular, size: 13)
//        removeFromBagButton.titleLabel?.font = UIFont(name: FontConstants.medium, size: 14)
//        moveToWishlistButton.titleLabel?.font = UIFont(name: FontConstants.medium, size: 14)
    }
}
