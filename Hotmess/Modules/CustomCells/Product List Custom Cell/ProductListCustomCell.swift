//
//  ProductPromotionCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 19/05/21.
//

import UIKit

class ProductListCustomCell: UICollectionViewCell {
    @IBOutlet weak var oneLeftHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var remainingProductCountLabel: UILabel!
    @IBOutlet weak var productListSubTitleLabel: UILabel!
    @IBOutlet weak var productListTiltleLabel: UILabel!
    @IBOutlet weak var productListImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        productListImageView.clipsToBounds = true
        productListImageView.contentMode = .scaleAspectFill
    }
    // MARK: - Font Style Method
    func applyFont() {
        currencyLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        priceLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        remainingProductCountLabel.font = UIFont(name: FontConstants.light, size: 10)
        productListSubTitleLabel.font = UIFont(name: FontConstants.light, size: 10)
        productListTiltleLabel.font = UIFont(name: FontConstants.semiBold, size: 12)
    }
}
