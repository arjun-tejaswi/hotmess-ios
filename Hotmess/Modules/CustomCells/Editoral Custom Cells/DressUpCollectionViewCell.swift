//
//  DressUpCollectionViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 13/09/21.
//

import UIKit

class DressUpCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dressUpImageview: UIImageView!
    @IBOutlet weak var dressUpSubtitleLabel: UILabel!
    @IBOutlet weak var dressUpTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
        // Initialization code
    }
    // MARK: - Font Style Method
    func applyFont() {
        dressUpTitleLabel.font = UIFont(name: FontConstants.medium, size: 14)
        dressUpSubtitleLabel.font = UIFont(name: FontConstants.light, size: 14)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        dressUpTitleLabel.applyLetterSpacing(spacing: 2.52)
        dressUpTitleLabel.textColor = ColourConstants.hex191919
        dressUpSubtitleLabel.textColor = ColourConstants.hex191919
    }
}
