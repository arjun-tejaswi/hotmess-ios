//
//  imageCollectionTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 27/09/21.
//

import UIKit

protocol ImageCollectionDidSelectProtocol: AnyObject {
    func didSelectIndex(selectedIndex: Int)
}
class ImageCollectionTableViewCell: UITableViewCell {
    weak var delegate: ImageCollectionDidSelectProtocol?
    @IBOutlet weak var imageCollectionCollectionview: UICollectionView!
    var dataValue = [ItemListRealm]()
    var itemList = [String]()
    var itemArray = [String]()
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCollectionView()
    }
    // MARK: - Collectionview SetUp Method
    func setUpCollectionView() {
        configureCollectionView()
        imageCollectionCollectionview.dataSource = self
        imageCollectionCollectionview.delegate = self
        imageCollectionCollectionview.register(UINib(nibName: "ImageSetCollectionViewCell", bundle: nil),
                                               forCellWithReuseIdentifier: "ImageSetCollectionViewCell")
    }
    // MARK: - Collectionview Cell Configure Method
    func configureCollectionView() {
        let cellSize = CGSize(width: 62,
                              height: imageCollectionCollectionview.bounds.size.height)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        imageCollectionCollectionview.backgroundColor = .clear
        imageCollectionCollectionview.bounces = false
        imageCollectionCollectionview.isPagingEnabled = false
        imageCollectionCollectionview.setCollectionViewLayout(layout, animated: true)
    }
    func splitData() {
        itemList.removeAll()
        for data in dataValue {
            if data.topStoryImageUrl.count > 4 {
                for item in data.topStoryImageUrl.prefix(4) {
                    itemList.append(item)
                }
            } else {
                for item in data.topStoryImageUrl {
                    itemList.append(item) }
            }
        }
        itemArray.removeAll()
        for data in dataValue {
            for item in data.topStoryImageUrl {
                itemArray.append(item)
            }
        }
    }
}
// MARK: - CollectionView Methods
extension ImageCollectionTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemList.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell
                = collectionView.dequeueReusableCell(
                    withReuseIdentifier: "ImageSetCollectionViewCell",
                    for: indexPath) as? ImageSetCollectionViewCell else {
            return UICollectionViewCell()
        }
        if itemArray.count > 4 && indexPath.item == 3 {
            NukeManager.sharedInstance.setImage(
                url: itemList[indexPath.item],
                imageView: cell.imageView,
                withPlaceholder: true)
            cell.imageView.contentMode = .scaleAspectFill
            cell.contentLabel.text = "+\(itemArray.count - 4)"
        } else {
            NukeManager.sharedInstance.setImage(
                url: itemList[indexPath.item],
                imageView: cell.imageView,
                withPlaceholder: true)
            cell.imageView.contentMode = .scaleAspectFill
            cell.contentLabel.isHidden = true
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectIndex(selectedIndex: indexPath.item)
    }
}
