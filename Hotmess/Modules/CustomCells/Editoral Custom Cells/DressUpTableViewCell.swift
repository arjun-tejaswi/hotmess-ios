//
//  DressUpTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 13/09/21.
//

import UIKit
protocol DressUpProtocol: AnyObject {
    func dressUpContent(skuId: [String])
    func didSelectDress(skuId: String, productId: String)
}

class DressUpTableViewCell: UITableViewCell {
    weak var delegate: DressUpProtocol?
    @IBOutlet weak var dressUpTitle: UILabel!
    @IBOutlet weak var dressUpSubTitle: UILabel!
    @IBOutlet weak var dressUpCollectionView: UICollectionView!
    var dataValue = [ItemListRealm]()
    var dressUpProducts = [ProductListRealm]()
    var skuIdList = [String]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        applyFont()
        applyStyling()
        setUpCollectionView()
    }
    // MARK: - Font Style Method
    func applyFont() {
        dressUpTitle.font = UIFont(name: FontConstants.semiBold, size: 28)
        dressUpSubTitle.font = UIFont(name: FontConstants.regular, size: 12)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        dressUpTitle.applyLetterSpacing(spacing: 1.4)
        dressUpSubTitle.applyLetterSpacing(spacing: 2.16)
        dressUpTitle.textColor = ColourConstants.hex1A1A1A
        dressUpSubTitle.textColor = ColourConstants.hex1A1A1A
    }
    // MARK: - Collectionview SetUp Method
    func setUpCollectionView() {
        configureDressUpCollectionView()
        dressUpCollectionView.dataSource = self
        dressUpCollectionView.delegate = self
        dressUpCollectionView.register(UINib(nibName: "DressUpCollectionViewCell", bundle: nil),
                                                 forCellWithReuseIdentifier: "DressUpCollectionViewCell")
    }
    func applyData() {
        skuIdList.removeAll()
        for data in dataValue {
            dressUpTitle.text = data.storyTitle
            dressUpSubTitle.text = data.storySubTitle
            for product in data.products {
                skuIdList.append(product.productSkuId)
            }
        }
        delegate?.dressUpContent(skuId: skuIdList)
    }
    // MARK: - Collectionview Cell Configure Method
    func configureDressUpCollectionView() {
        let cellSize = CGSize(width: 225,
                              height: dressUpCollectionView.frame.height)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 33, bottom: 0, right: 13)
        layout.minimumLineSpacing = 19.7
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        dressUpCollectionView.backgroundColor = .clear
        dressUpCollectionView.bounces = false
        dressUpCollectionView.isPagingEnabled = false
        dressUpCollectionView.setCollectionViewLayout(layout, animated: true)
    }
}
// MARK: - CollectionView Methods
extension DressUpTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dressUpProducts.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell
                = collectionView.dequeueReusableCell(
                withReuseIdentifier: "DressUpCollectionViewCell",
        for: indexPath) as? DressUpCollectionViewCell else {
            return UICollectionViewCell()
        }
        NukeManager.sharedInstance.setImage(
            url: dressUpProducts[indexPath.item].productImageURL,
            imageView: cell.dressUpImageview,
            withPlaceholder: true)
        cell.dressUpTitleLabel.text = dressUpProducts[indexPath.item].productDesignerName
        cell.dressUpSubtitleLabel.text = dressUpProducts[indexPath.item].productDescription
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectDress(skuId: dressUpProducts[indexPath.item].stockKeepUnitID,
                                 productId: dressUpProducts[indexPath.item].productID)
    }
}
