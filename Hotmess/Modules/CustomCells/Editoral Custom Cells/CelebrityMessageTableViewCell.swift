//
//  CelebrityMessageTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 22/09/21.
//

import UIKit

class CelebrityMessageTableViewCell: UITableViewCell {
    @IBOutlet weak var celebrityMessageTitle: UILabel!
    @IBOutlet weak var celebrityMessageSubtitle: UILabel!
    var dataValue = [ItemListRealm]()
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
        // Initialization code
    }
    // MARK: - Font Style Method
    func applyFont() {
        celebrityMessageTitle.font = UIFont(name: FontConstants.semiBold, size: 24)
        celebrityMessageSubtitle.font = UIFont(name: FontConstants.light, size: 14)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        celebrityMessageTitle.textColor = ColourConstants.hex191919
        celebrityMessageSubtitle.textColor = ColourConstants.hex191919
        celebrityMessageSubtitle.adjustsFontSizeToFitWidth = true
        celebrityMessageSubtitle.minimumScaleFactor = 0.5
    }
    func applyData() {
        if let data = dataValue.first {
            celebrityMessageTitle.text = data.storyTitle
            celebrityMessageSubtitle.text = data.storyDescription
        }
    }
}
