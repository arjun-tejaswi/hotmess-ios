//
//  RecommendedTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 21/09/21.
//

import UIKit

class RecommendedTableViewCell: UITableViewCell {
    @IBOutlet weak var topLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var recommendedTopLabel: UILabel!
    @IBOutlet weak var recommendedImageview: UIImageView!
    @IBOutlet weak var recommendedTitleLabel: UILabel!
    @IBOutlet weak var recommendedSubtitleLabel: UILabel!
    @IBOutlet weak var recommendedDateLabel: UILabel!
    var dataValue = [ItemListRealm]()
    var index: Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
        // Initialization code
    }
    // MARK: - Font Style Method
    func applyFont() {
        recommendedTopLabel.font = UIFont(name: FontConstants.semiBold, size: 28)
        recommendedTitleLabel.font = UIFont(name: FontConstants.regular, size: 12)
        recommendedDateLabel.font = UIFont(name: FontConstants.regular, size: 12)
        recommendedSubtitleLabel.font = UIFont(name: FontConstants.semiBold, size: 26)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        recommendedTopLabel.applyLetterSpacing(spacing: 1.4)
        recommendedTitleLabel.applyLetterSpacing(spacing: 2.16)
        recommendedDateLabel.applyLetterSpacing(spacing: 2.16)
        recommendedSubtitleLabel.applyLetterSpacing(spacing: 1.4)
        recommendedTopLabel.textColor = ColourConstants.hex191919
        recommendedTitleLabel.textColor = ColourConstants.hex191919
        recommendedDateLabel.textColor = ColourConstants.hex8B8B8B
        recommendedSubtitleLabel.textColor = ColourConstants.hex191919
    }
    func applyData () {
        if let value = dataValue.first {
            let url = URL(string: value.storyImageUrl)
            NukeManager.sharedInstance.setImage(url: url, imageView: recommendedImageview, withPlaceholder: true)
            let blogDate = Utilities.sharedInstance.convertServerDate(value: Int64(value.storyDate),
                delivered: true)
            recommendedDateLabel.text = "\(blogDate)"
            recommendedTitleLabel.text = value.storyTitle
            recommendedSubtitleLabel.text = value.storySubTitle
            if value.indexPathCount == 0 {
                topLabelHeight.constant = 21
                recommendedTopLabel.isHidden = false
            } else {
                recommendedTopLabel.isHidden = true
                topLabelHeight.constant = 0
            }
        }
    }
}
