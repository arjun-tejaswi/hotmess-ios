//
//  ImageCollectionCollectionViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 28/09/21.
//

import UIKit

class ImageCollectionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        imageView.contentMode = .scaleAspectFill
    }
}
