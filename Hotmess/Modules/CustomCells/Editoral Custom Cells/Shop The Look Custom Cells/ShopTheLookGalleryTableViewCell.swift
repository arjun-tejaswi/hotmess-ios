//
//  ShopTheLookGalleryTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 22/09/21.
//

import UIKit

class ShopTheLookGalleryTableViewCell: UITableViewCell {
    @IBOutlet weak var shopTheLookImageview: UIImageView!
    @IBOutlet weak var shopTheLookTitle: UILabel!
    @IBOutlet weak var shopTheLookSubtitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
        // Initialization code
    }
    // MARK: - Font Style Method
    func applyFont() {
        shopTheLookTitle.font = UIFont(name: FontConstants.regular, size: 13)
        shopTheLookSubtitle.font = UIFont(name: FontConstants.light, size: 14)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        shopTheLookTitle.applyLetterSpacing(spacing: 2.34)
        shopTheLookTitle.textColor = ColourConstants.hex191919
        shopTheLookSubtitle.textColor = ColourConstants.hex191919
    }
}
