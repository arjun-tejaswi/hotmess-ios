//
//  ShopTheLookVideoTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 22/09/21.
//

import UIKit
import AVFoundation

protocol shopTheLookVideoProtocol: AnyObject {
    func selectedVideo(videoUrl: String)
}

class ShopTheLookVideoTableViewCell: UITableViewCell {
    weak var delegate: shopTheLookVideoProtocol?
    @IBOutlet weak var shopTheLookTitle: UILabel!
    @IBOutlet weak var shopTheLookImageview: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var shopTheLookSubtitle: UILabel!
    var dataValue = [ItemListRealm]()
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
        // Initialization code
    }
    // MARK: - Font Style Method
    func applyFont() {
        shopTheLookTitle.font = UIFont(name: FontConstants.regular, size: 13)
        shopTheLookSubtitle.font = UIFont(name: FontConstants.light, size: 14)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        shopTheLookTitle.applyLetterSpacing(spacing: 2.34)
        shopTheLookTitle.textColor = ColourConstants.hex191919
        shopTheLookSubtitle.textColor = ColourConstants.hex191919
    }
    func applyData() {
        if let data = dataValue.first {
            shopTheLookTitle.text = data.storyTitle
            shopTheLookSubtitle.text = data.storyDescription
            let url = URL(string: data.thumbNailImageUrl)
            NukeManager.sharedInstance.setImage(url: url, imageView: shopTheLookImageview, withPlaceholder: true)
            delegate?.selectedVideo(videoUrl: data.videoUrl)
        }
    }
}
