//
//  ShopTheLookTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 17/09/21.
//

import UIKit

class ShopTheLookTableViewCell: UITableViewCell {
    @IBOutlet weak var shopTheLookImageView: UIImageView!
    @IBOutlet weak var shopTheLookTitle: UILabel!
    @IBOutlet weak var shokTheLookTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var shokTheLookBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var shopTheLookdescription: UILabel!
    @IBOutlet weak var shopTheLookSubtitle: UILabel!
    var dataValue = [ItemListRealm]()
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
        // Initialization code
    }
    // MARK: - Font Style Method
    func applyFont() {
        shopTheLookTitle.font = UIFont(name: FontConstants.regular, size: 13)
        shopTheLookSubtitle.font = UIFont(name: FontConstants.light, size: 16)
        shopTheLookdescription.font = UIFont(name: FontConstants.light, size: 14)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        shopTheLookTitle.applyLetterSpacing(spacing: 2.34)
        shopTheLookTitle.textColor = ColourConstants.hex191919
        shopTheLookSubtitle.textColor = ColourConstants.hex191919
        shopTheLookdescription.textColor = ColourConstants.hex191919
    }
    func applyData() {
        if let data = dataValue.first {
            shopTheLookTitle.text = data.storyTitle
            shopTheLookSubtitle.text = data.storyDescription
            let url = URL(string: data.storyImageUrl)
            NukeManager.sharedInstance.setImage(url: url, imageView: shopTheLookImageView, withPlaceholder: true)
            if data.storySummary == "" {
//                shopTheLookdescription.isHidden = true
                shokTheLookTopConstraint.constant = 0
                shokTheLookBottomConstraint.constant = 20
                shopTheLookdescription.text = ""
            } else {
//                shopTheLookdescription.isHidden = false
                shokTheLookTopConstraint.constant = 27.8
                shokTheLookBottomConstraint.constant = 20
                shopTheLookdescription.text = data.storySummary
            }
        }
    }

}
