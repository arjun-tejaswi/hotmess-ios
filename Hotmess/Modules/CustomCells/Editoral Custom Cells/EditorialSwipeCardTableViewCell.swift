//
//  EditorialSwipeCardTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 15/09/21.
//

import UIKit
import Koloda

protocol KolodaDidSelectProtocol: AnyObject {
    func didSelectWith(editorialId: String, categoryId: String, topLabel: String)
}

class EditorialSwipeCardTableViewCell: UITableViewCell {
    @IBOutlet weak var latestStoriesTopLabel: UILabel!
    @IBOutlet weak var pageControlTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var pageControlBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var latestStoriesTitleLabel: UILabel!
    @IBOutlet weak var latestStoriesDateLabel: UILabel!
    @IBOutlet weak var swipeCardView: KolodaView!
    @IBOutlet weak var latestStoriesPageControl: UIPageControl!
    @IBOutlet weak var latestStoriesSubTitleLabel: UILabel!
    var cardImages = [ItemListRealm]()
    weak var delegate: KolodaDidSelectProtocol?
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
        swipeCardView.dataSource = self
        swipeCardView.delegate = self
        // Initialization code
    }
    // MARK: - Font Style Method
    func applyFont() {
        latestStoriesTopLabel.font = UIFont(name: FontConstants.semiBold, size: 28)
        latestStoriesTitleLabel.font = UIFont(name: FontConstants.regular, size: 12)
        latestStoriesDateLabel.font = UIFont(name: FontConstants.regular, size: 12)
        latestStoriesSubTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        latestStoriesTopLabel.applyLetterSpacing(spacing: 1.4)
        latestStoriesTitleLabel.applyLetterSpacing(spacing: 2.16)
        latestStoriesDateLabel.applyLetterSpacing(spacing: 2.16)
        latestStoriesTopLabel.textColor = ColourConstants.hexFFFFFF
        latestStoriesTitleLabel.textColor = ColourConstants.hexFFFFFF
        latestStoriesDateLabel.textColor = ColourConstants.hex8B8B8B
        latestStoriesSubTitleLabel.textColor = ColourConstants.hexFFFFFF
        swipeCardView.backgroundColor = .clear
    }
}
// MARK: - Koloda Delegate Methods
extension EditorialSwipeCardTableViewCell: KolodaViewDelegate, KolodaViewDataSource {
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        koloda.resetCurrentCardIndex()
    }
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return cardImages.count
    }
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .fast
    }
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        if cardImages.count > 0 {
            guard let url = URL(string: cardImages[index].storyImageUrl) else {
                return UIImageView() }
            let imageView = UIImageView()
            imageView.downloaded(from: url)
            return imageView
        } else {
            return UIImageView()
        }
    }
    func koloda(_ koloda: KolodaView, allowedDirectionsForIndex index: Int) -> [SwipeResultDirection] {
        return [.left, .right]
    }
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        delegate?.didSelectWith(editorialId: cardImages[index].storyEditorialId,
                                categoryId: cardImages[index].storyCategoryId,
                                topLabel: cardImages[index].storyTitle)
    }
    func koloda(_ koloda: KolodaView, didShowCardAt index: Int) {
        latestStoriesTitleLabel.text = cardImages[index].storyTitle
        let blogDate = Utilities.sharedInstance.convertServerDate(
            value: Int64(cardImages[index].storyDate), delivered: true)
        latestStoriesDateLabel.text = "\(blogDate)"
        latestStoriesSubTitleLabel.text = cardImages[index].storySubTitle
        if cardImages.count <= 1 {
            pageControlTopConstraint.constant = 0
            pageControlBottomConstraint.constant = 0
            latestStoriesPageControl.isHidden = true
        } else {
            latestStoriesPageControl.isHidden = false
            latestStoriesPageControl.numberOfPages = cardImages.count
            self.latestStoriesPageControl.currentPage = index
        }
    }
}
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                  let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                  let data = data, error == nil,
                  let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
