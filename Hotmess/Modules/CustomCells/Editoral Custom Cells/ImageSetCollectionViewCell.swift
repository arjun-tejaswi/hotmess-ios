//
//  ImageSetCollectionViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 08/10/21.
//

import UIKit

class ImageSetCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        contentLabel.textColor = ColourConstants.hexFFFFFF
    }
    // MARK: - Font Style Method
    func applyFont() {
        contentLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
    }
}
