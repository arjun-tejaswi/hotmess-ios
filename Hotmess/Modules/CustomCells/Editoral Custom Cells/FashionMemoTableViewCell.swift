//
//  FashionMemoTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 13/09/21.
//

import UIKit

class FashionMemoTableViewCell: UITableViewCell {
    @IBOutlet weak var fashionMemoImageview: UIImageView!
    @IBOutlet weak var fashionMemoTitleLabel: UILabel!
    @IBOutlet weak var fashionMemoDateLabel: UILabel!
    @IBOutlet weak var fashionMemoSubTitleLabel: UILabel!
    var dataValue: ItemListRealm!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        applyFont()
        applyStyling()
    }
    func applyData () {
        if dataValue != nil {
        let url = URL(string: dataValue.storyImageUrl)
            NukeManager.sharedInstance.setImage(url: url, imageView: fashionMemoImageview, withPlaceholder: true)
        let blogDate = Utilities.sharedInstance.convertServerDate(value: Int64(dataValue.storyDate),
                    delivered: true)
            fashionMemoDateLabel.text = "\(blogDate)"
        fashionMemoTitleLabel.text = dataValue.storyTitle
        fashionMemoSubTitleLabel.text = dataValue.storySubTitle
        }
    }
    // MARK: - Font Style Method
    func applyFont() {
        fashionMemoTitleLabel.font = UIFont(name: FontConstants.regular, size: 12)
        fashionMemoDateLabel.font = UIFont(name: FontConstants.regular, size: 12)
        fashionMemoSubTitleLabel.font = UIFont(name: FontConstants.light, size: 14)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        fashionMemoTitleLabel.applyLetterSpacing(spacing: 2.16)
        fashionMemoDateLabel.applyLetterSpacing(spacing: 2.16)
        fashionMemoTitleLabel.textColor = ColourConstants.hex191919
        fashionMemoDateLabel.textColor = ColourConstants.hex8B8B8B
        fashionMemoSubTitleLabel.textColor = ColourConstants.hex191919
    }
}
