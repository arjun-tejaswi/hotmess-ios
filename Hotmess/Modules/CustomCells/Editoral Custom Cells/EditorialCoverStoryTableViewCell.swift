//
//  EditorialCoverStoryTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 13/09/21.
//

import UIKit

class EditorialCoverStoryTableViewCell: UITableViewCell {
    @IBOutlet weak var coverStoryImageview: UIImageView!
    @IBOutlet weak var coverStoryView: UIView!
    @IBOutlet weak var coverStoryDateLabel: UILabel!
    @IBOutlet weak var coverStoryTitleLabel: UILabel!
    @IBOutlet weak var coverStorySubTitleLabel: UILabel!
    var dataValue: ItemListRealm!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
        // Initialization code
    }
     func applyData () {
        let url = URL(string: dataValue.storyImageUrl)
        NukeManager.sharedInstance.setImage(url: url, imageView: coverStoryImageview, withPlaceholder: true)
        let blogDate = Utilities.sharedInstance.convertServerDate(value: Int64(dataValue.storyDate), delivered: true)
        coverStoryDateLabel.text = "\(blogDate)"
        coverStoryTitleLabel.text = dataValue.storyTitle
        coverStorySubTitleLabel.text = dataValue.storySubTitle
}
    // MARK: - Font Style Method
    func applyFont() {
        coverStoryTitleLabel.font = UIFont(name: FontConstants.regular, size: 12)
        coverStoryDateLabel.font = UIFont(name: FontConstants.regular, size: 12)
        coverStorySubTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 28)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        coverStoryView.backgroundColor = ColourConstants.hexF8F7F6
        coverStoryTitleLabel.applyLetterSpacing(spacing: 2.16)
        coverStoryDateLabel.applyLetterSpacing(spacing: 2.16)
        coverStorySubTitleLabel.applyLetterSpacing(spacing: 1.4)
        coverStoryTitleLabel.textColor = ColourConstants.hex191919
        coverStorySubTitleLabel.textColor = ColourConstants.hex191919
        coverStoryDateLabel.textColor = ColourConstants.hex8B8B8B
    }
}
