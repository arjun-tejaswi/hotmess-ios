//
//  editorialCategoryCollectionViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 14/09/21.
//

import UIKit

class EditorialCategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var categoryUnderView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        categoryTitleLabel.contentMode = .center
        categoryTitleLabel.textColor = ColourConstants.hex1A1A1A
        categoryUnderView.backgroundColor = ColourConstants.hex9B9B9B
    }
    // MARK: - Font Styling Method
    func applyFont() {
        categoryTitleLabel.font = UIFont(name: FontConstants.light, size: 14)
    }
}
