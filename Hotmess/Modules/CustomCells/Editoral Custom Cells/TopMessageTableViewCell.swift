//
//  TopMessageTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 05/10/21.
//

import UIKit

class TopMessageTableViewCell: UITableViewCell {

    @IBOutlet weak var textValue: UILabel!
    var dataValue = [ItemListRealm]()
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
    }
    // MARK: - Font Style Method
    func applyFont() {
        textValue.font = UIFont(name: FontConstants.light, size: 13)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        textValue.applyLetterSpacing(spacing: 2.34)
        textValue.textColor = ColourConstants.hex191919
    }
    func applyData() {
        if let data = dataValue.first {
            textValue.text = data.storyTitle
        }
    }
}
