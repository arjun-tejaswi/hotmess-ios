//
//  EditorialDetailTopStoryTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 22/09/21.
//

import UIKit

class EditorialDetailTopStoryTableViewCell: UITableViewCell {

    @IBOutlet weak var topStoryImageview: UIImageView!
    @IBOutlet weak var topStoryTitleLabel: UILabel!
    @IBOutlet weak var topStoryDateLabel: UILabel!
    @IBOutlet weak var topStorySubtitleLabel: UILabel!
    @IBOutlet weak var topStoryDescriptionLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    var dataValue: EditorialDetailRealm!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
        // Initialization code
    }
    // MARK: - Font Style Method
    func applyFont() {
        topStoryTitleLabel.font = UIFont(name: FontConstants.regular, size: 12)
        topStoryDateLabel.font = UIFont(name: FontConstants.regular, size: 12)
        topStorySubtitleLabel.font = UIFont(name: FontConstants.semiBold, size: 28)
        topStoryDescriptionLabel.font = UIFont(name: FontConstants.light, size: 14)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        topStoryTitleLabel.applyLetterSpacing(spacing: 2.16)
        topStoryDateLabel.applyLetterSpacing(spacing: 2.16)
        topStorySubtitleLabel.applyLetterSpacing(spacing: 1.4)
        topStoryTitleLabel.textColor = ColourConstants.hex191919
        topStoryDateLabel.textColor = ColourConstants.hex8B8B8B
        topStorySubtitleLabel.textColor = ColourConstants.hex191919
        topStoryDescriptionLabel.textColor = ColourConstants.hex191919
    }
    func applyData() {
        let url = URL(string: dataValue.coverStoryImageUrl)
        NukeManager.sharedInstance.setImage(url: url, imageView: topStoryImageview, withPlaceholder: true)
        topStoryTitleLabel.text = dataValue.coverStoryTitle
        let blogDate = Utilities.sharedInstance.convertServerDate(value: Int64(dataValue.coverStoryDate),
            delivered: true)
        topStoryDateLabel.text = "\(blogDate)"
        topStorySubtitleLabel.text = dataValue.coverStoryDescription
        topStoryDescriptionLabel.text = dataValue.coverStorySummary
    }
}
