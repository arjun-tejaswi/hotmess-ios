//
//  MainFilterCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 31/05/21.
//

import UIKit

class MainFilterCustomCell: UICollectionViewCell {
    @IBOutlet weak var filterUnderLineView: UIView!
    @IBOutlet weak var filterLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        filterLabel.contentMode = .center
        filterLabel.textColor = ColourConstants.hex717171
    }
    // MARK: - Font Styling Method
    func applyFont() {
        filterLabel.font = UIFont(name: FontConstants.regular, size: 12)
    }
}
