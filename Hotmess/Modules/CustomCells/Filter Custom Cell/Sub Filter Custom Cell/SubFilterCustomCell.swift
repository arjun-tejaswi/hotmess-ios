//
//  SubFilterCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 31/05/21.
//

import UIKit

class SubFilterCustomCell: UICollectionViewCell {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var subFilterLabel: UILabel!
    @IBOutlet weak var subFilterImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    // MARK: UI Styling Method
    func applyStyling() {
        subFilterLabel.textColor = ColourConstants.hex717171
        subFilterImageView.contentMode = .scaleAspectFit
    }
    // MARK: - Font Styling Method
    func applyFont() {
        subFilterLabel.font = UIFont(name: FontConstants.regular, size: 10)
    }
}
