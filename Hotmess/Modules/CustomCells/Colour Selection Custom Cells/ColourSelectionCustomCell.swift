//
//  ColourSelectionCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 18/05/21.
//

import UIKit

class ColourSelectionCustomCell: UICollectionViewCell {
    @IBOutlet weak var selectionCircleView: UIView!
    @IBOutlet weak var colourLabel: UILabel!
    @IBOutlet weak var colourSelectionButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        colourSelectionButton.layer.cornerRadius = colourSelectionButton.frame.width / 2
        selectionCircleView.layer.cornerRadius = selectionCircleView.frame.width / 2
        selectionCircleView.layer.borderWidth = 0.5
        selectionCircleView.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
        selectionCircleView.backgroundColor = .clear
        selectionCircleView.isHidden = true
    }
    // MARK: - Font Style Method
    func applyFont() {
        colourLabel.font = UIFont(name: FontConstants.regular, size: 12)
    }
}
