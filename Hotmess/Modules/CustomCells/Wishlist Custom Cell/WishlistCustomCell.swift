//
//  WishlistCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 22/07/21.
//

import UIKit

class WishlistCustomCell: UICollectionViewCell {
    @IBOutlet weak var grayedOutView: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var addToBagButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFonts()
    }
    func applyData(data: WishlishRealm) {
        NukeManager.sharedInstance.setImage(
            url: data.productImageURL,
            imageView: productImageView,
            withPlaceholder: true)
        nameLabel.text = data.productDesignerName
        descriptionLabel.text = data.productDescription
        priceLabel.text = "₹ \(data.productPrice)"
        let attribute1: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.regular, size: 13) ?? UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
        let attribute2: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.medium, size: 13) ?? UIFont.boldSystemFont(ofSize: 13),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
        let attributedString1 = NSMutableAttributedString(string: "Colour : ", attributes: attribute1)
        let attributedString2 = NSMutableAttributedString(string: "\(data.productColour)", attributes: attribute2)
        attributedString1.append(attributedString2)
        colorLabel.attributedText = attributedString1
        addToBagButton.setTitle("ADD TO BAG", for: .normal)
        addToBagButton.setTitleColor(ColourConstants.hexE0E0E0, for: .normal)
        addToBagButton.backgroundColor = ColourConstants.hex191919
        addToBagButton.isEnabled = true
    }
    func applyUnavailableData(data: WishlishRealm) {
        NukeManager.sharedInstance.setImage(
            url: data.productImageURL,
            imageView: productImageView,
            withPlaceholder: true)
        nameLabel.text = data.productDesignerName
        descriptionLabel.text = data.productDescription
        priceLabel.text = "₹ \(data.productPrice)"
        let attribute1: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.regular, size: 13) ?? UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
        let attribute2: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.medium, size: 13) ?? UIFont.boldSystemFont(ofSize: 13),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
        let attributedString1 = NSMutableAttributedString(string: "Colour : ", attributes: attribute1)
        let attributedString2 = NSMutableAttributedString(string: "\(data.productColour)", attributes: attribute2)
        attributedString1.append(attributedString2)
        colorLabel.attributedText = attributedString1
        addToBagButton.setTitle("Product is unavailable", for: .normal)
        addToBagButton.setTitleColor(ColourConstants.hexE56A6A, for: .normal)
        addToBagButton.backgroundColor = ColourConstants.hexFFE8E8
        addToBagButton.isEnabled = false
    }
    func applyStyling() {
        nameLabel.textColor = ColourConstants.hex191919
        descriptionLabel.textColor = ColourConstants.hex3C3C3C
        priceLabel.textColor = ColourConstants.hex191919
        removeButton.setTitleColor(ColourConstants.hex3C3C3C, for: .normal)
        addToBagButton.setTitleColor(ColourConstants.hexE0E0E0, for: .normal)
        mainStackView.setCustomSpacing(1, after: nameLabel)
        mainStackView.setCustomSpacing(6, after: descriptionLabel)
        mainStackView.setCustomSpacing(3, after: priceLabel)
        mainStackView.setCustomSpacing(11, after: colorLabel)
    }
    func applyFonts() {
        nameLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        descriptionLabel.font = UIFont(name: FontConstants.light, size: 12)
        priceLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        removeButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 13)
        addToBagButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
    }
}

extension UIImage {
    var mono: UIImage {
        let context = CIContext(options: nil)
        let currentFilter = CIFilter(name: "CIPhotoEffectMono")!
        currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        let output = currentFilter.outputImage!
        let cgImage = context.createCGImage(output, from: output.extent)!
        let processedImage = UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
        return processedImage
    }
}
