//
//  NewsLetterCustomCell.swift
//  Hotmess
//
//  Created by Akshatha on 16/08/21.
//

import UIKit

class NewsLetterCustomCell: UITableViewCell {

    @IBOutlet weak var selectionSwitch: UISwitch!
    @IBOutlet weak var newsLetterTableViewTitle: UILabel!
    @IBOutlet weak var newsLetterBottonView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func applyFont() {
        newsLetterTableViewTitle.font = UIFont(name: FontConstants.light, size: 14)
        }
    func applyStyling() {
        newsLetterTableViewTitle.textColor = ColourConstants.hex191919
        self.selectionSwitch.onTintColor =  ColourConstants.hex191919
        self.selectionSwitch.tintColor =  ColourConstants.hexE4E4E4
    }
    }
