//
//  OrderDetailCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 03/09/21.
//

import UIKit

class OrderDetailCustomCell: UITableViewCell {
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var returnLabel: UILabel!
    @IBOutlet weak var returnView: UIView!
    @IBOutlet weak var separaterView: UIView!
    @IBOutlet weak var productCurrencyLabel: UILabel!
    @IBOutlet weak var productQuantityLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productSizeLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var productDesignerNameLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    func applyStyling() {
        productDesignerNameLabel.applyLetterSpacing(spacing: 0.7)
        returnView.backgroundColor = ColourConstants.hexFFEBD6
        returnLabel.textColor = ColourConstants.hexDE650D
    }
    func applyFont() {
        productDesignerNameLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        productDescriptionLabel.font = UIFont(name: FontConstants.light, size: 12)
        productSizeLabel.font = UIFont(name: FontConstants.regular, size: 14)
        productQuantityLabel.font = UIFont(name: FontConstants.regular, size: 14)
        productPriceLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        productCurrencyLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        returnLabel.font = UIFont(name: FontConstants.regular, size: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
