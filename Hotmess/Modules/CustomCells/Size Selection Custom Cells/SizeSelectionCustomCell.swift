//
//  SizeSelectionCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 18/05/21.
//

import UIKit

class SizeSelectionCustomCell: UITableViewCell {
    @IBOutlet weak var countrySizeLabel: UILabel!
    @IBOutlet weak var selectSizeButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
    }
    // MARK: - Font Style Method
    func applyFont() {
        countrySizeLabel.font = UIFont(name: FontConstants.semiBold, size: 12)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
