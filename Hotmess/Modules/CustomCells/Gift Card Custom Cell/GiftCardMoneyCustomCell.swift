//
//  GiftCardMoneyCustomCell.swift
//  Hotmess
//
//  Created by Akshatha on 01/09/21.
//

import UIKit

class GiftCardMoneyCustomCell: UICollectionViewCell {
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var rupeeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
    }
    func applyFont() {
        amountView.backgroundColor = ColourConstants.hexF8F8F8
        amountLabel.font = UIFont(name: FontConstants.medium, size: 14)
        rupeeLabel.font = UIFont(name: FontConstants.medium, size: 14)
    }
    func applyStyling() {
        amountLabel.textColor = ColourConstants.hex000000
        rupeeLabel.textColor = ColourConstants.hex000000
    }
    func select() {
        amountView.backgroundColor = ColourConstants.hex000000
        amountLabel.textColor = ColourConstants.hexFFFFFF
        rupeeLabel.textColor = ColourConstants.hexFFFFFF
    }
    func deSelect() {
        amountView.backgroundColor = ColourConstants.hexF8F8F8
        amountLabel.textColor = ColourConstants.hex000000
        rupeeLabel.textColor = ColourConstants.hex000000
    }
}
