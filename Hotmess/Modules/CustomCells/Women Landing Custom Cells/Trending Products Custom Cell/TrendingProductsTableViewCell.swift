//
//  TrendingProductsTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 02/06/21.
//

import UIKit
import CHIPageControl

protocol TrendingProductsTableViewCellProtocol: AnyObject {
    func naviagteToDetailScreen(skuID: String, productID: String)
}

class TrendingProductsTableViewCell: UITableViewCell {
    weak var delegate: TrendingProductsTableViewCellProtocol?
    @IBOutlet weak var trendingProductsLabel: UILabel!
    @IBOutlet weak var trendingProductCollectionView: UICollectionView!
    @IBOutlet weak var trendingProductSubTitleLabel: UILabel!
    @IBOutlet weak var collectionViewPageControl: CHIPageControlJaloro!
    @IBOutlet weak var trendingProductTitleLabel: UILabel!
    @IBOutlet weak var pageControlBottomConstraint: NSLayoutConstraint!
    var trendingProductMetaDataRealm: TrendingProductMetaDataRealm!
    var currentPage = 0
    var numberOfPages = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
        setUpCollectionView()
        collectionViewPageControl.tintColor = .systemGray
        collectionViewPageControl.radius = 1
        collectionViewPageControl.currentPageTintColor = .black
        collectionViewPageControl.padding = 0
        trendingProductCollectionView.reloadData()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    // MARK: - Trending product CollectionView SetUp Method
    func setUpCollectionView() {
        configureCollectionView()
        trendingProductCollectionView.dataSource = self
        trendingProductCollectionView.delegate = self
        trendingProductCollectionView.register(UINib(nibName: "TrendingProductsCollectionViewCell", bundle: nil),
                                               forCellWithReuseIdentifier: "TrendingProductsCollectionViewCell")
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        trendingProductsLabel.applyLetterSpacing(spacing: 1.98)
        trendingProductsLabel.textColor = ColourConstants.hex717171
        trendingProductTitleLabel.textColor = ColourConstants.hex191919
        trendingProductSubTitleLabel.textColor = ColourConstants.hex191919
    }
    // MARK: - Font Style Method
    func applyFont() {
        trendingProductsLabel.font = UIFont(name: FontConstants.regular, size: 11)
        trendingProductTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 28)
        trendingProductSubTitleLabel.font = UIFont(name: FontConstants.light, size: 18)
    }
    // MARK: - Trending Product CollectionView Cell SetUp
    func configureCollectionView() {
        let cellSize = CGSize(width: 125,
                              height: trendingProductCollectionView.frame.height)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 13, bottom: 0, right: 13)
        layout.minimumLineSpacing = 12.67
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        trendingProductCollectionView.backgroundColor = .clear
        trendingProductCollectionView.bounces = false
        trendingProductCollectionView.isPagingEnabled = false
        trendingProductCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    // MARK: - Assigning trending product data
    func assignTrendingProductData() {
        let imageCount = trendingProductMetaDataRealm.productListItem.count
        let pageSize: Double = Double(125 * imageCount)
        let tempOne = Double(UIScreen.main.bounds.width)
        numberOfPages = Int(round(pageSize/tempOne))
        pageControlBottomConstraint.constant = numberOfPages >= 6 ? 20 : 0
        collectionViewPageControl.numberOfPages = numberOfPages
        trendingProductsLabel.text = trendingProductMetaDataRealm.trendingProductTitle
        trendingProductTitleLabel.text = trendingProductMetaDataRealm.trendingProductSubTitle
        trendingProductSubTitleLabel.text = trendingProductMetaDataRealm.trendingProductDescription
        trendingProductCollectionView.reloadData()
    }
}
// MARK: - CollectionView Methods
extension TrendingProductsTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if trendingProductMetaDataRealm != nil {
            return trendingProductMetaDataRealm.productListItem.count
        } else {
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if trendingProductMetaDataRealm != nil {
            let collection = trendingProductMetaDataRealm.productListItem[indexPath.row]
            guard let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: "TrendingProductsCollectionViewCell",
                for: indexPath) as? TrendingProductsCollectionViewCell else {
                return UICollectionViewCell()
            }
            let url = URL(string: collection.productImageURL)
            NukeManager.sharedInstance.setImage(url: url,
                                                imageView: cell.trendingProductImageView,
                                                withPlaceholder: true)
            cell.trendingProductTitleLabel.text = collection.productDesignerName
            cell.trendingProductSubTitleLabel.text = collection.productDescription
            cell.trendingProductPriceLabel.text = String(collection.productPrice)
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let collection = trendingProductMetaDataRealm.productListItem[indexPath.row]
        delegate?.naviagteToDetailScreen(skuID: collection.stockKeepUnitID, productID: collection.productID)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let index = Int(round(offSet/width))
        collectionViewPageControl.set(progress: index, animated: true)
    }
}
