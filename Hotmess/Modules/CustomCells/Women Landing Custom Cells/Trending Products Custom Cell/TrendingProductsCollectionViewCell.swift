//
//  TrendingProductsCollectionViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 02/06/21.
//

import UIKit

class TrendingProductsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var trendingProductSubTitleLabel: UILabel!
    @IBOutlet weak var trendingProductTitleLabel: UILabel!
    @IBOutlet weak var trendingProductImageView: UIImageView!
    @IBOutlet weak var trendingProductCurrencyLabel: UILabel!
    @IBOutlet weak var trendingProductPriceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        trendingProductTitleLabel.textColor = ColourConstants.hex191919
        trendingProductSubTitleLabel.textColor = ColourConstants.hex717171
        trendingProductCurrencyLabel.textColor = ColourConstants.hex191919
        trendingProductPriceLabel.textColor = ColourConstants.hex191919
        trendingProductImageView.contentMode = .scaleAspectFit
    }
    // MARK: - Font style method
    func applyFont() {
        trendingProductTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 12)
        trendingProductSubTitleLabel.font = UIFont(name: FontConstants.light, size: 10)
        trendingProductPriceLabel.font = UIFont(name: FontConstants.semiBold, size: 12)
        trendingProductCurrencyLabel.font = UIFont(name: FontConstants.semiBold, size: 12)
    }
}
