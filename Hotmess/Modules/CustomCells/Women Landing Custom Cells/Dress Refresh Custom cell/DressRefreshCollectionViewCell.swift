//
//  DressRefreshCollectionViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 01/06/21.
//

import UIKit

class DressRefreshCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var dressRefreshImageView: UIImageView!
    @IBOutlet weak var dressRefreshSubTitleLabel: UILabel!
    @IBOutlet weak var dressRefreshTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        dressRefreshImageView.contentMode = .scaleToFill
        dressRefreshTitleLabel.applyLetterSpacing(spacing: 0.7)
        dressRefreshSubTitleLabel.applyLetterSpacing(spacing: 0.5)
    }
    // MARK: - Font Style Method
    func applyFont() {
        dressRefreshTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
        dressRefreshSubTitleLabel.font = UIFont(name: FontConstants.regular, size: 10)
    }
}
