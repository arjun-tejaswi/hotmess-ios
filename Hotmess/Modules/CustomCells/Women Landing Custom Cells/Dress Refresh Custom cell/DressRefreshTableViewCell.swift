//
//  DressRefreshTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 01/06/21.
//

import UIKit

protocol DressRefreshTableViewCellProtocol: AnyObject {
    func naviagteToProductList(filters: String)
}

class DressRefreshTableViewCell: UITableViewCell {
    weak var delegate: DressRefreshTableViewCellProtocol?
    @IBOutlet weak var dressRefreshLabel: UILabel!
    @IBOutlet weak var dressRefreshTitleLabel: UILabel!
    @IBOutlet weak var dressRefressSubTitleLabel: UILabel!
    @IBOutlet weak var dressRefreshCollectionView: CustomSelfSizingCollectionView!
    var dressRefreshRealm: DressRefreshRealmModel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        setUpCollectionView()
        applyStyling()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    // MARK: - Dress Refresh CollectionView SetUp Method
    func setUpCollectionView() {
        configureCollectionView()
        dressRefreshCollectionView.dataSource = self
        dressRefreshCollectionView.delegate = self
        dressRefreshCollectionView.register(UINib(nibName: "DressRefreshCollectionViewCell", bundle: nil),
                                            forCellWithReuseIdentifier: "DressRefreshCollectionViewCell")
    }
    // MARK: - Font Spacing Method
    func applyStyling() {
        dressRefreshLabel.textColor = ColourConstants.hex717171
        dressRefreshTitleLabel.textColor = ColourConstants.hex191919
        dressRefreshLabel.textColor = ColourConstants.hex191919
        dressRefreshLabel.applyLetterSpacing(spacing: 1.98)
    }
    // MARK: - Font Style Method
    func applyFont() {
        dressRefreshLabel.font = UIFont(name: FontConstants.regular, size: 11)
        dressRefreshTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 28)
        dressRefressSubTitleLabel.font = UIFont(name: FontConstants.light, size: 18)
    }
    // MARK: - Dress Refresh CollectionView Cell SetUp
    func configureCollectionView() {
        dressRefreshCollectionView.bounces = false
        dressRefreshCollectionView.contentSize = CGSize(width: dressRefreshCollectionView.frame.width,
                                                        height: dressRefreshCollectionView.frame.height)
        let collectionViewLayout = CustomGridCollectionViewLayout()
        dressRefreshCollectionView.collectionViewLayout = collectionViewLayout
    }
    // MARK: - Assigning dress refresh data
    func assignDressRefreshData() {
        dressRefreshLabel.text = dressRefreshRealm.dressRefreshTitle
        dressRefreshTitleLabel.text = dressRefreshRealm.dressRefreshSubTitle
        dressRefressSubTitleLabel.text = dressRefreshRealm.dressRefreshDecsription
        dressRefreshCollectionView.reloadData()
    }
}
// MARK: - CollectionView Methods
extension DressRefreshTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if dressRefreshRealm != nil {
            return dressRefreshRealm.dressRefreshItems.count
        } else {
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if dressRefreshRealm != nil {
            let items = dressRefreshRealm.dressRefreshItems[indexPath.row]
            guard let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: "DressRefreshCollectionViewCell",
                    for: indexPath) as? DressRefreshCollectionViewCell else {
                return UICollectionViewCell()
            }
            cell.dressRefreshTitleLabel.text = items.dressRefreshMoodTitle
            let url = URL(string: items.dressRefreshImageUrl)
            NukeManager.sharedInstance.setImage(url: url, imageView: cell.dressRefreshImageView, withPlaceholder: true)
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let items = dressRefreshRealm.dressRefreshItems[indexPath.row]
        delegate?.naviagteToProductList(filters: items.filterList)
    }
}
