//
//  FeaturedProductsTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 01/06/21.
//

import UIKit
import Nuke

class FeaturedProductsTableViewCell: UITableViewCell {
    @IBOutlet weak var featuredProductsTitleLabel: UILabel!
    @IBOutlet weak var featuredProductsSubTitleLabel: UILabel!
    @IBOutlet weak var featuredProductImageView: UIImageView!
    @IBOutlet weak var shopNowButton: UIButton!
    @IBOutlet weak var featuredProductsLabel: UILabel!
    var featuredProductRealm: FeaturedProductRealmModel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        featuredProductImageView.contentMode = .scaleAspectFill
        featuredProductsLabel.textColor = ColourConstants.hex717171
        featuredProductsTitleLabel.textColor = ColourConstants.hex191919
        featuredProductsSubTitleLabel.textColor = ColourConstants.hex191919
        featuredProductsLabel.applyLetterSpacing(spacing: 1.98)
        shopNowButton.applyButtonLetterSpacing(spacing: 2.16)
    }
    // MARK: - Font Style Method
    func applyFont() {
        featuredProductsLabel.font = UIFont(name: FontConstants.regular, size: 11)
        featuredProductsTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 28)
        featuredProductsSubTitleLabel.font = UIFont(name: FontConstants.light, size: 18)
        shopNowButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
    }
    // MARK: - Assigning featured product data
    func assignfeaturedProductData() {
        featuredProductsLabel.text = featuredProductRealm.featuredProductTitle
        featuredProductsTitleLabel.text = featuredProductRealm.featuredProductSubTitle
        featuredProductsSubTitleLabel.text = featuredProductRealm.featuredProductDescription
        let url = URL(string: featuredProductRealm.featuredProductImageUrl)
        NukeManager.sharedInstance.setImage(url: url, imageView: featuredProductImageView, withPlaceholder: true)
        if featuredProductRealm.buttonText != "" {
            shopNowButton.setTitle(featuredProductRealm.buttonText, for: .normal)
            applyStyling()
        }
    }
}
