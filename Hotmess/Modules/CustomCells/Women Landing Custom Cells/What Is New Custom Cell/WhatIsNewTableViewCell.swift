//
//  WhatIsNewTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 01/06/21.
//

import UIKit
import CHIPageControl

class WhatIsNewTableViewCell: UITableViewCell {
    @IBOutlet weak var whatsNewCollectionView: UICollectionView!
    @IBOutlet weak var whatsNewCollectionViewPageControl: CHIPageControlJaloro!
    @IBOutlet weak var whatsNewTitleLabel: UILabel!
    @IBOutlet weak var whatsNewSubTitleLabel: UILabel!
    @IBOutlet weak var countOfItemLabel: UILabel!
    @IBOutlet weak var shopNowButton: UIButton!
    var whatIsNewRealm: WhatIsNewRealmModel!
    var numberOfPages = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
        setUpCollectionView()
        whatsNewCollectionViewPageControl.tintColor = .systemGray
        whatsNewCollectionViewPageControl.radius = 1
        whatsNewCollectionViewPageControl.currentPageTintColor = .black
        whatsNewCollectionViewPageControl.padding = 0
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    // MARK: - What Is New CollectionView SetUp Method
    func setUpCollectionView() {
        configureCollectionView()
        whatsNewCollectionView.dataSource = self
        whatsNewCollectionView.delegate = self
        whatsNewCollectionView.register(UINib(nibName: "WhatIsNewCollectionViewCell", bundle: nil),
                                        forCellWithReuseIdentifier: "WhatIsNewCollectionViewCell")
    }
    // MARK: - Font Spacing Method
    func applyStyling() {
        shopNowButton.applyButtonLetterSpacing(spacing: 2.16)
        countOfItemLabel.applyLetterSpacing(spacing: 1.98)
    }
    // MARK: - Font Style Method
    func applyFont() {
        countOfItemLabel.font = UIFont(name: FontConstants.regular, size: 11)
        whatsNewTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 28)
        whatsNewSubTitleLabel.font = UIFont(name: FontConstants.light, size: 18)
        shopNowButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
    }
    // MARK: - What Is New CollectionView Cell SetUp
    func configureCollectionView() {
        let cellSize = CGSize(width: 125,
                              height: whatsNewCollectionView.frame.height)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 13, bottom: 0, right: 13)
        layout.minimumLineSpacing = 12.67
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        whatsNewCollectionView.backgroundColor = .clear
        whatsNewCollectionView.bounces = false
        whatsNewCollectionView.isPagingEnabled = false
        whatsNewCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    // MARK: - Assigning whats new data
    func assignWhatsNewData() {
        let imagesCount = whatIsNewRealm.whatIsNewCollection.count
        let pageSize: Double = Double(125 * imagesCount)
        let tempOne = Double(UIScreen.main.bounds.width)
        numberOfPages = Int(round(pageSize/tempOne))
        whatsNewCollectionViewPageControl.numberOfPages = numberOfPages
        whatsNewTitleLabel.text = whatIsNewRealm.whatIsNewTitle
        whatsNewSubTitleLabel.text = whatIsNewRealm.whatIsNewSubTitle
        if whatIsNewRealm.buttonText != "" {
            shopNowButton.setTitle(whatIsNewRealm.buttonText, for: .normal)
            applyStyling()
        }
        whatsNewCollectionView.reloadData()
    }
}
// MARK: - CollectionView Methods
extension WhatIsNewTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if whatIsNewRealm != nil {
            return whatIsNewRealm.whatIsNewCollection.count
        } else {
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if whatIsNewRealm != nil {
            let collection = whatIsNewRealm.whatIsNewCollection[indexPath.row]
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WhatIsNewCollectionViewCell",
                                                                for: indexPath) as? WhatIsNewCollectionViewCell else {
                return UICollectionViewCell()
            }
            cell.productTypeLabel.text = collection.whatIsNewProductName
            let url = URL(string: collection.whatIsNewImageUrl)
            NukeManager.sharedInstance.setImage(url: url, imageView: cell.productTypeImageView, withPlaceholder: true)
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let index = Int(round(offSet/width))
        whatsNewCollectionViewPageControl.set(progress: index, animated: true)
    }
}
