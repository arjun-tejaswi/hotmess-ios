//
//  WhatIsNewCollectionViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 01/06/21.
//

import UIKit

class WhatIsNewCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productTypeLabel: UILabel!
    @IBOutlet weak var productTypeImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        productTypeLabel.textColor = ColourConstants.hex191919
        productTypeLabel.textAlignment = .center
        productTypeImageView.contentMode = .scaleAspectFill
    }
    // MARK: - Font Styling Method
    func applyFont() {
        productTypeLabel.font = UIFont(name: FontConstants.regular, size: 12)
    }
}
