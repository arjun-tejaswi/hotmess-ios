//
//  CelebrityPicksTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 28/06/21.
//

import UIKit

class CelebrityPicksTableViewCell: UITableViewCell {
    @IBOutlet weak var celebrityPicksTitle: UILabel!
    @IBOutlet weak var celebrityPicksSubTitle: UILabel!
    @IBOutlet weak var celebrityPicksDescription: UILabel!
    @IBOutlet weak var celebrityPicksImageView: UIImageView!
    @IBOutlet weak var shopNowButton: UIButton!
    var celebrityPicksRealm: CelebrityPicksRealm!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        celebrityPicksImageView.contentMode = .scaleAspectFill
        celebrityPicksTitle.textColor = ColourConstants.hex717171
        celebrityPicksSubTitle.textColor = ColourConstants.hex191919
        celebrityPicksDescription.textColor = ColourConstants.hex191919
        celebrityPicksTitle.applyLetterSpacing(spacing: 1.98)
        shopNowButton.applyButtonLetterSpacing(spacing: 2.16)
    }
    // MARK: - Font Style Method
    func applyFont() {
        celebrityPicksTitle.font = UIFont(name: FontConstants.regular, size: 11)
        celebrityPicksSubTitle.font = UIFont(name: FontConstants.semiBold, size: 28)
        celebrityPicksDescription.font = UIFont(name: FontConstants.light, size: 18)
        shopNowButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
    }
    // MARK: - Assigning celebrity picks data
    func assignCelebrityPicksData() {
        celebrityPicksTitle.text = celebrityPicksRealm.celebrityPicksTitle
        celebrityPicksSubTitle.text = celebrityPicksRealm.celebrityPicksSubTitle
        celebrityPicksDescription.text = celebrityPicksRealm.celebrityPicksDescription
        let url = URL(string: celebrityPicksRealm.placeHolderImage.image)
        NukeManager.sharedInstance.setImage(url: url, imageView: celebrityPicksImageView, withPlaceholder: true)
        if celebrityPicksRealm.buttonText != "" {
            shopNowButton.setTitle(celebrityPicksRealm.buttonText, for: .normal)
            applyStyling()
        }
    }
}
