//
//  SingleImageTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 02/06/21.
//

import UIKit

class SingleImageTableViewCell: UITableViewCell {
    @IBOutlet weak var offerImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        offerImageView.contentMode = .scaleAspectFill
    }
    @IBOutlet weak var singleImageView: UIImageView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
