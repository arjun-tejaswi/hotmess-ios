//
//  MoodCollectionCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 10/06/21.
//

import UIKit
import Nuke

class MoodCollectionCustomCell: UITableViewCell {
    @IBOutlet weak var moodCollectionButtonThree: UIButton!
    @IBOutlet weak var moodCollectionButtonTwo: UIButton!
    @IBOutlet weak var moodCollectionButtonOne: UIButton!
    @IBOutlet weak var moodCollectionTitleLabel: UILabel!
    @IBOutlet weak var moodCollectionSubTitleLabel: UILabel!
    @IBOutlet weak var moodCollectionImageOne: UIImageView!
    @IBOutlet weak var moodCollectionImageTwo: UIImageView!
    @IBOutlet weak var moodCollectionImageThree: UIImageView!
    @IBOutlet weak var moodCollectionDescription: UILabel!
    @IBOutlet weak var shopNowButton: UIButton!
    var moodCollectionRealm: MoodCollectionsRealm!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        moodCollectionTitleLabel.textColor = ColourConstants.hex717171
        moodCollectionSubTitleLabel.textColor = ColourConstants.hex191919
        moodCollectionDescription.textColor = ColourConstants.hex191919
        shopNowButton.applyButtonLetterSpacing(spacing: 2.16)
        moodCollectionTitleLabel.applyLetterSpacing(spacing: 1.98)
    }
    // MARK: - Font Styling Method
    func applyFont() {
        moodCollectionTitleLabel.font = UIFont(name: FontConstants.regular, size: 11)
        moodCollectionSubTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 28)
        moodCollectionDescription.font = UIFont(name: FontConstants.light, size: 18)
        shopNowButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
    }
    // MARK: - Assigning mood collection data
    func assignMoodCollectionsData() {
        moodCollectionTitleLabel.text = moodCollectionRealm.moodCollectionsTitle
        moodCollectionSubTitleLabel.text = moodCollectionRealm.moodCollectionsSubTitle
        moodCollectionDescription.text = moodCollectionRealm.moodCollectionsDescription
        let imageOneUrl = URL(string: moodCollectionRealm.placeHolderImageOne.imageUrl)
        NukeManager.sharedInstance.setImage(url: imageOneUrl, imageView: moodCollectionImageOne, withPlaceholder: true)
        let imageTwoUrl = URL(string: moodCollectionRealm.placeHolderImageTwo.imageUrl)
        NukeManager.sharedInstance.setImage(url: imageTwoUrl, imageView: moodCollectionImageTwo, withPlaceholder: true)
        let imageThreeUrl = URL(string: moodCollectionRealm.placeHolderImageThree.imageUrl)
        NukeManager.sharedInstance.setImage(url: imageThreeUrl,
                                            imageView: moodCollectionImageThree,
                                            withPlaceholder: true)
        if moodCollectionRealm.buttonText != "" {
            shopNowButton.setTitle(moodCollectionRealm.buttonText, for: .normal)
            applyStyling()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
