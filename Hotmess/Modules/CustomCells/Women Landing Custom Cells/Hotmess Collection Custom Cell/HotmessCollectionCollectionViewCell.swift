//
//  HotmessCollectionCollectionViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 02/07/21.
//

import UIKit

class HotmessCollectionCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var hotmessCollectionImageView: UIImageView!
    @IBOutlet weak var hotmesCollectionProductName: UILabel!
    @IBOutlet weak var hotmessCollectionproductDescriptionLabel: UILabel!
    @IBOutlet weak var hotmessCollectionProductPriceLabel: UILabel!
    @IBOutlet weak var hotmessCollectionCurrencyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        hotmesCollectionProductName.textColor = ColourConstants.hex191919
        hotmessCollectionproductDescriptionLabel.textColor = ColourConstants.hex717171
        hotmessCollectionCurrencyLabel.textColor = ColourConstants.hex191919
        hotmessCollectionProductPriceLabel.textColor = ColourConstants.hex191919
        hotmessCollectionImageView.contentMode = .scaleAspectFit
    }
    // MARK: - Font style method
    func applyFont() {
        hotmesCollectionProductName.font = UIFont(name: FontConstants.semiBold, size: 12)
        hotmessCollectionproductDescriptionLabel.font = UIFont(name: FontConstants.light, size: 10)
        hotmessCollectionProductPriceLabel.font = UIFont(name: FontConstants.semiBold, size: 12)
        hotmessCollectionCurrencyLabel.font = UIFont(name: FontConstants.semiBold, size: 12)
    }
}
