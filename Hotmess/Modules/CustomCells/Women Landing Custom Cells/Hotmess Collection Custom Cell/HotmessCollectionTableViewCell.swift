//
//  HotmessCollectionTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 02/07/21.
//

import UIKit
import CHIPageControl

protocol HotmessCollectionTableViewCellProtocol: AnyObject {
    func navigateToDeatilScreen(skuID: String, productID: String)
}

class HotmessCollectionTableViewCell: UITableViewCell {
    weak var delegate: HotmessCollectionTableViewCellProtocol?
    @IBOutlet weak var hotmessCollectionPageControl: CHIPageControlJaloro!
    @IBOutlet weak var hotmessCollectionTitleLabel: UILabel!
    @IBOutlet weak var hotmessCollectionSubTitleLabel: UILabel!
    @IBOutlet weak var hotmessCollectionDescriptionLabel: UILabel!
    @IBOutlet weak var hotmessCollectionCollectionView: UICollectionView!
    @IBOutlet weak var pageControlBottomConstraint: NSLayoutConstraint!
    var hotmessCollectionRealm: HotmessCollectionMetaDataRealm!
    var numberOfPages = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
        setUpCollectionView()
        hotmessCollectionPageControl.tintColor = .systemGray
        hotmessCollectionPageControl.radius = 1
        hotmessCollectionPageControl.currentPageTintColor = .black
        hotmessCollectionPageControl.padding = 0
        hotmessCollectionCollectionView.reloadData()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    // MARK: - Trending product CollectionView SetUp Method
    func setUpCollectionView() {
        configureCollectionView()
        hotmessCollectionCollectionView.dataSource = self
        hotmessCollectionCollectionView.delegate = self
        hotmessCollectionCollectionView.register(UINib(nibName: "HotmessCollectionCollectionViewCell", bundle: nil),
                                                 forCellWithReuseIdentifier: "HotmessCollectionCollectionViewCell")
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        hotmessCollectionTitleLabel.applyLetterSpacing(spacing: 1.98)
        hotmessCollectionTitleLabel.textColor = ColourConstants.hex717171
        hotmessCollectionSubTitleLabel.textColor = ColourConstants.hex191919
        hotmessCollectionDescriptionLabel.textColor = ColourConstants.hex191919
    }
    // MARK: - Font Style Method
    func applyFont() {
        hotmessCollectionTitleLabel.font = UIFont(name: FontConstants.regular, size: 11)
        hotmessCollectionSubTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 28)
        hotmessCollectionDescriptionLabel.font = UIFont(name: FontConstants.light, size: 18)
    }
    // MARK: - Trending Product CollectionView Cell SetUp
    func configureCollectionView() {
        let cellSize = CGSize(width: 125,
                              height: hotmessCollectionCollectionView.frame.height)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 13, bottom: 0, right: 13)
        layout.minimumLineSpacing = 12.67
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        hotmessCollectionCollectionView.backgroundColor = .clear
        hotmessCollectionCollectionView.bounces = false
        hotmessCollectionCollectionView.isPagingEnabled = false
        hotmessCollectionCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    // MARK: - Assigning hotmess collection data
    func assignHotmessMetadata() {
        let imageCount = hotmessCollectionRealm.hotmessProductListItem.count
        let pageSize: Double = Double(125 * imageCount)
        let tempOne = Double(UIScreen.main.bounds.width)
        numberOfPages = Int(round(pageSize/tempOne))
        pageControlBottomConstraint.constant = numberOfPages >= 6 ? 51 : 0
        hotmessCollectionPageControl.numberOfPages = numberOfPages
        hotmessCollectionTitleLabel.text = hotmessCollectionRealm.hotmessCollectionTitle
        hotmessCollectionSubTitleLabel.text = hotmessCollectionRealm.hotmessCollectionSubTitle
        hotmessCollectionDescriptionLabel.text = hotmessCollectionRealm.hotmessCollectionDescription
        hotmessCollectionCollectionView.reloadData()
    }
}
// MARK: - CollectionView Methods
extension HotmessCollectionTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if hotmessCollectionRealm != nil {
            return hotmessCollectionRealm.hotmessProductListItem.count
        } else {
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if hotmessCollectionRealm != nil {
            let collection = hotmessCollectionRealm.hotmessProductListItem[indexPath.row]
            guard let cell
                    = collectionView.dequeueReusableCell(
                    withReuseIdentifier: "HotmessCollectionCollectionViewCell",
            for: indexPath) as? HotmessCollectionCollectionViewCell else {
                return UICollectionViewCell()
            }
            let url = URL(string: collection.productImageURL)
            NukeManager.sharedInstance.setImage(url: url,
                                                imageView: cell.hotmessCollectionImageView,
                                                withPlaceholder: true)
            cell.hotmesCollectionProductName.text = collection.productName
            cell.hotmessCollectionproductDescriptionLabel.text = collection.productDescription
            cell.hotmessCollectionProductPriceLabel.text = String(collection.productPrice)
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let collection = hotmessCollectionRealm.hotmessProductListItem[indexPath.row]
        delegate?.navigateToDeatilScreen(skuID: collection.skuID, productID: collection.productID)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let index = Int(round(offSet/width))
        hotmessCollectionPageControl.set(progress: index, animated: true)
    }
}
