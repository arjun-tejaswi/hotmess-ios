//
//  ShopCategoryTableViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 04/06/21.
//

import UIKit

class ShopCategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var shopCategoryImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        shopCategoryImageView.contentMode = .scaleAspectFill
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
