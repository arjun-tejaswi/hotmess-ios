//
//  PriceListingCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 19/07/21.
//

import UIKit

class PriceListingCustomCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
    }
    func applyStyling() {
        titleLabel.textColor = ColourConstants.hex191919
        priceLabel.textColor = ColourConstants.hex191919
    }
    func applyRegularFont() {
        titleLabel.font = UIFont(name: FontConstants.regular, size: 14)
        priceLabel.font = UIFont(name: FontConstants.regular, size: 14)
    }
    func applyFontForTotal() {
        titleLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
        priceLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
    }
}
