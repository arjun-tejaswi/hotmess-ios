//
//  OrderSummaryCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 19/07/21.
//

import UIKit

struct PriceListingModel {
    var title: String?
    var price: String?
    init(title: String, price: String) {
        self.title = title
        self.price = price
    }
}

class OrderSummaryCustomCell: UITableViewCell {
    @IBOutlet weak var titleLabelheightConstaint: NSLayoutConstraint!
    @IBOutlet weak var orderSummaryTitleLabel: UILabel!
    @IBOutlet weak var priceTableView: UITableView!
    @IBOutlet weak var priceTableViewHeightConstaint: NSLayoutConstraint!
    var prices = [PriceListingModel]()
    var showTotal: Bool = true
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        configurePricesTableView()
    }
    func applyStyling() {
        orderSummaryTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        orderSummaryTitleLabel.textColor = ColourConstants.hex191919
    }
    override func layoutSubviews() {
        self.priceTableViewHeightConstaint.constant = self.priceTableView.intrinsicContentSize.height
    }
    func configurePricesTableView() {
        priceTableView.separatorStyle = .none
        priceTableView.delegate = self
        priceTableView.dataSource = self
        priceTableView.register(UINib(nibName: "PriceListingCustomCell", bundle: nil),
                                      forCellReuseIdentifier: "PriceListingCustomCell")
        priceTableView.reloadData()
    }
}
extension OrderSummaryCustomCell: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prices.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "PriceListingCustomCell") as? PriceListingCustomCell else {
            print("Cell not loaded ")
            return UITableViewCell()
        }
        cell.titleLabel.text = prices[indexPath.row].title ?? ""
        let price = String(format: "%.2f", prices[indexPath.row].price ?? "")
        print(price)
        cell.priceLabel.text = "\(prices[indexPath.row].price ?? "")0"
        if indexPath.row == prices.count - 1 {
            cell.applyFontForTotal()
        } else {
            cell.applyRegularFont()
        }
        return cell
    }
}
