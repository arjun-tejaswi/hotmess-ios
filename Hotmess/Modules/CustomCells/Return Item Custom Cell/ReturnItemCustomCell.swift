//
//  ReturnItemCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 08/09/21.
//

import UIKit

class ReturnItemCustomCell: UITableViewCell {
    @IBOutlet weak var separaterView: UIView!
    @IBOutlet weak var productCurrencyLabel: UILabel!
    @IBOutlet weak var productQuantityLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productSizeLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var productDesignerNameLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var checkImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    func applyStyling() {
        productDesignerNameLabel.applyLetterSpacing(spacing: 0.7)
    }
    func applyFont() {
        productDesignerNameLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        productDescriptionLabel.font = UIFont(name: FontConstants.light, size: 12)
        productSizeLabel.font = UIFont(name: FontConstants.regular, size: 14)
        productQuantityLabel.font = UIFont(name: FontConstants.regular, size: 14)
        productPriceLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        productCurrencyLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
    }
    func check() {
        checkImageView.image = UIImage(named: "returnSelectIcon")
        checkImageView.setImageColour(color: ColourConstants.hex191919)
    }
    func uncheck() {
        checkImageView.image = UIImage(named: "returnSelectIcon")
        checkImageView.setImageColour(color: ColourConstants.hexE4E4E4)
    }
}
