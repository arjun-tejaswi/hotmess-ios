//
//  ProductListImageCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 01/07/21.
//

import UIKit

class ProductListImageCustomCell: UICollectionViewCell {
    @IBOutlet weak var productListImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
    }
    func applyStyling() {
        productListImageView.clipsToBounds = true
        productListImageView.contentMode = .scaleAspectFill
    }
}
