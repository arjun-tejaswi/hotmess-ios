//
//  PromoCodeCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 19/07/21.
//

import UIKit

class PromoCodeCustomCell: UITableViewCell {
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var infoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
    }
    func applyStyling() {
        infoLabel.font = UIFont.init(name: FontConstants.light, size: 11)
        infoLabel.textColor = ColourConstants.hex3C3C3C
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = ColourConstants.hexD3D3D3.cgColor
        applyButton.backgroundColor = ColourConstants.hexDBDBDB
        applyButton.setTitleColor(ColourConstants.hex191919, for: .normal)
        applyButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        codeTextField.font = UIFont(name: FontConstants.regular, size: 14)
        codeTextField.setLeftPaddingPoints(20)
    }
}
