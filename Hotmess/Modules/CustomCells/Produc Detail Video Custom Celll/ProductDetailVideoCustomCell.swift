//
//  ProductDetailVideoCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 12/10/21.
//

import UIKit

class ProductDetailVideoCustomCell: UICollectionViewCell {
    @IBOutlet weak var productDetailThumbnailImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        productDetailThumbnailImageView.contentMode = .scaleAspectFill
    }

}
