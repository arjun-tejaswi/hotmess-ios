//
//  CompletedOrdersCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 24/08/21.
//

import UIKit

class CompletedOrdersCustomCell: UITableViewCell {
    @IBOutlet weak var separaterView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var multipleItemsIndicatorView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var orderedOnLabel: UILabel!
    @IBOutlet weak var totalItemLabel: UILabel!
    @IBOutlet weak var orderSummaryLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var returnedItemsContainerView: UIView!
    @IBOutlet weak var returnedItemCountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    func applyStyling() {
        returnedItemsContainerView.backgroundColor = ColourConstants.hexFFEBD6
        returnedItemCountLabel.textColor = ColourConstants.hexDE650D
        productImageView.layer.borderWidth = 2
        productImageView.layer.borderColor = UIColor.white.cgColor
        multipleItemsIndicatorView.backgroundColor = ColourConstants.hexEAEBEB
        orderIdLabel.textColor = ColourConstants.hex191919
        orderedOnLabel.textColor = ColourConstants.hex191919
        totalItemLabel.textColor = ColourConstants.hex191919
        orderSummaryLabel.textColor = ColourConstants.hex191919
        paymentLabel.textColor = ColourConstants.hex191919
        dateLabel.textColor = ColourConstants.hex191919
    }
    func applyFont() {
        orderIdLabel.font = UIFont(name: FontConstants.medium, size: 14)
        orderedOnLabel.font = UIFont(name: FontConstants.regular, size: 12)
        totalItemLabel.font = UIFont(name: FontConstants.regular, size: 12)
        orderSummaryLabel.font = UIFont(name: FontConstants.regular, size: 12)
        paymentLabel.font = UIFont(name: FontConstants.regular, size: 12)
        dateLabel.font = UIFont(name: FontConstants.regular, size: 12)
        statusLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        returnedItemCountLabel.font = UIFont(name: FontConstants.regular, size: 10)
    }
    func applyData(order: OrderListRealm) {
        self.backgroundColor = ColourConstants.hexFFFFFF
        let url = URL(string: order.lineItems.first?.featuredImageURL ?? "")
        let dateFormat = Utilities.sharedInstance.convertServerDate(value: order.createdAt, delivered: true)
        NukeManager.sharedInstance.setImage(url: url, imageView: productImageView, withPlaceholder: true)
        orderIdLabel.text = "ORDER ID #\(order.orderId)"
        orderedOnLabel.text = "Ordered on | \(dateFormat)"
        totalItemLabel.text = order.lineItems.count == 1 ? "Total Item | \(order.lineItems.count)" :
            "Total Items | \(order.lineItems.count)"
        orderSummaryLabel.text = "Order Summary | ₹ \(order.total)"
        paymentLabel.text = order.paymentType == "COD" ? "Payment | Cash" : "Payment | Card XXXX XX56"
        setStatusLabel(order: order)
        var returnedCount: Int = 0
        for item in order.lineItems where item.status == "RETURNED" {
            returnedCount += 1
        }
        if returnedCount > 0 {
            returnedProducts(isShow: true, count: returnedCount)
        } else {
            returnedProducts(isShow: false)
        }
        var count: Int = 0
        for _ in order.lineItems {
            count += 1
        }
        if count == 1 {
            setUpImages(isMultiple: false)
        } else {
            setUpImages(isMultiple: true)
        }
        cancelCellUIConfiguration(order: order)
    }
    func cancelCellUIConfiguration(order: OrderListRealm) {
        if order.status == "CANCELLED" {
            let todaysDate = Utilities.sharedInstance.convertTodayToString()
            if order.isCompleted {
                showCancelledBanner()
                returnedItemCountLabel.text = "Amount credited to source account"
                let cancelledDate = Utilities.sharedInstance.convertServerDate(
                    value: order.cancelledAt, delivered: true)
                if todaysDate == cancelledDate {
                    dateLabel.text = "Today"
                } else {
                    dateLabel.text = "On \(cancelledDate)"
                }
                self.backgroundColor = ColourConstants.hexFFFFFF
            } else {
                showCancelledBanner()
                returnedItemCountLabel.text = "Refund has been initiated."
                let cancelledDate = Utilities.sharedInstance.convertServerDate(
                    value: order.cancelledAt, delivered: true)
                if todaysDate == cancelledDate {
                    dateLabel.text = "Today"
                } else {
                    dateLabel.text = "On \(cancelledDate)"
                }
                self.backgroundColor = ColourConstants.hexFFF2F2
            }
        }
    }
    func showCancelledBanner() {
        returnedItemsContainerView.isHidden = false
        returnedItemsContainerView.backgroundColor = .clear
        returnedItemCountLabel.textColor = ColourConstants.hexEF5353
    }
    func setStatusLabel(order: OrderListRealm) {
        switch order.status {
        case "CREATED":
            statusLabel.textColor = ColourConstants.hex399861
            dateLabel.text = "\(order.createdAt)"
        case "CANCELLED":
            statusLabel.text = "CANCELLED"
            statusLabel.textColor = ColourConstants.hexEF5353
        case "DELIVERED":
            statusLabel.text = "DELIVERED"
            statusLabel.textColor = ColourConstants.hex399861
        case "RETURNED":
            statusLabel.text = "RETURNED"
            statusLabel.textColor = ColourConstants.hexDE650D
        default:
            print("\(order.status) is not configured yet")
        }
    }
    func returnedProducts(isShow: Bool, count: Int = 0) {
        if isShow {
            returnedItemsContainerView.isHidden = false
            returnedItemsContainerView.backgroundColor = ColourConstants.hexFFEBD6
            returnedItemCountLabel.text = count == 1 ? "\(count) Item Returned" : "\(count) Items Returned"
            returnedItemCountLabel.textColor = ColourConstants.hexDE650D
        } else {
            returnedItemsContainerView.isHidden = true
        }
    }
    func setUpImages(isMultiple: Bool) {
        if isMultiple {
            multipleItemsIndicatorView.isHidden = false
        } else {
            multipleItemsIndicatorView.isHidden = true
        }
    }
}
