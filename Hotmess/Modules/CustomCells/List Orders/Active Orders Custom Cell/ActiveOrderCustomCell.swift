//
//  ActiveOrderCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 24/08/21.
//

import UIKit

class ActiveOrderCustomCell: UITableViewCell {
    @IBOutlet weak var viewDetailsLabel: UILabel!
    @IBOutlet weak var separterView: UIView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var multipleItemsIndicatorView: UIView!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var orderedOnLabel: UILabel!
    @IBOutlet weak var deliveringOnTitleLabel: UILabel!
    @IBOutlet weak var deliveringOnDateLabel: UILabel!
    @IBOutlet weak var totalItemLabel: UILabel!
    @IBOutlet weak var orderSummaryLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    func applyStyling() {
        self.backgroundColor = ColourConstants.hexEBF7FD
        productImageView.layer.borderWidth = 2
        productImageView.layer.borderColor = UIColor.white.cgColor
        multipleItemsIndicatorView.backgroundColor = ColourConstants.hexEAEBEB
        orderIdLabel.textColor = ColourConstants.hex1B8CD3
        orderedOnLabel.textColor = ColourConstants.hex191919
        deliveringOnTitleLabel.textColor = ColourConstants.hex1B8CD3
        deliveringOnDateLabel.textColor = ColourConstants.hex191919
        totalItemLabel.textColor = ColourConstants.hex191919
        orderSummaryLabel.textColor = ColourConstants.hex191919
        paymentLabel.textColor = ColourConstants.hex191919
        viewDetailsLabel.textColor = ColourConstants.hex191919
        viewDetailsLabel.text = "VIEW DETAILS"
        viewDetailsLabel.layer.borderWidth = 1
        viewDetailsLabel.layer.borderColor = ColourConstants.hex191919.cgColor
        viewDetailsLabel.applyLetterSpacing(spacing: 2.34)
    }
    func applyFont() {
        orderIdLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        orderedOnLabel.font = UIFont(name: FontConstants.regular, size: 11)
        deliveringOnTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
        deliveringOnDateLabel.font = UIFont(name: FontConstants.regular, size: 11)
        totalItemLabel.font = UIFont(name: FontConstants.regular, size: 14)
        orderSummaryLabel.font = UIFont(name: FontConstants.light, size: 14)
        paymentLabel.font = UIFont(name: FontConstants.regular, size: 11)
        viewDetailsLabel.font = UIFont(name: FontConstants.regular, size: 13)
    }
    func applyData(order: OrderListRealm) {
        let dateFormat = Utilities.sharedInstance.convertServerDate(value: order.createdAt, delivered: false)
        let deliveryFromDateFormat = Utilities.sharedInstance.convertServerDate(
            value: order.deliveryDateFrom, delivered: false)
        let deliveryToDateFormat = Utilities.sharedInstance.convertServerDate(
            value: order.deliveryDateTo, delivered: false)
        let attribute1: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.regular, size: 14) ?? UIFont.systemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
        let attribute2: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.medium, size: 14) ?? UIFont.boldSystemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
        let orderSummery = NSMutableAttributedString(string: "Order Summary", attributes: attribute1)
        let price = NSMutableAttributedString(string: " ₹ \(order.total)", attributes: attribute2)
        orderSummery.append(price)
        orderIdLabel.text = "ORDER ID #\(order.orderId)"
        let url = URL(string: order.lineItems.first?.featuredImageURL ?? "")
        NukeManager.sharedInstance.setImage(url: url, imageView: productImageView, withPlaceholder: true)
        orderedOnLabel.text = "Ordered on \(dateFormat)"
        deliveringOnDateLabel.text = "\(deliveryFromDateFormat) - \(deliveryToDateFormat)"
        totalItemLabel.text = order.lineItems.count == 1 ?
            "Total Item: \(order.lineItems.count)" : "Total Items: \(order.lineItems.count)"
        orderSummaryLabel.attributedText = orderSummery
        if order.paymentType == "COD" {
            paymentLabel.text = "Payment | COD"
        } else {
            paymentLabel.text = "Payment | Card XXXX XX56"
        }
        var count: Int = 0
        for _ in order.lineItems {
            count += 1
        }
        if count == 1 {
            setUpImages(isMultiple: false)
        } else {
            setUpImages(isMultiple: true)
        }
    }
    func setUpImages(isMultiple: Bool) {
        if isMultiple {
            multipleItemsIndicatorView.isHidden = false
        } else {
            multipleItemsIndicatorView.isHidden = true
        }
    }
}
