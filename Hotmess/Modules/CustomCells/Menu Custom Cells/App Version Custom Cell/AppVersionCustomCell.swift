//
//  AppVersionCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 26/07/21.
//

import UIKit

class AppVersionCustomCell: UITableViewCell {
    @IBOutlet weak var versionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        versionLabel.font = UIFont(name: FontConstants.regular, size: 10)
        versionLabel.textColor = .lightGray
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        versionLabel.text = "Version: \(appVersion)"
    }
}
