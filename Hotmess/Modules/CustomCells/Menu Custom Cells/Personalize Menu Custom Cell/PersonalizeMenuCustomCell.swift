//
//  PersonalizeMenuCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 02/06/21.
//

import UIKit

class PersonalizeMenuCustomCell: UITableViewCell {
    @IBOutlet weak var personalizeLabel: UILabel!
    @IBOutlet weak var personalizeImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        personalizeLabel.textColor = ColourConstants.hex191919
        personalizeLabel.applyLetterSpacing(spacing: 1.3)
    }
    // MARK: - Font Styling Method
    func applyFont() {
        personalizeLabel.font = UIFont(name: FontConstants.regular, size: 13)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
