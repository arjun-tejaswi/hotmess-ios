//
//  TouchIDCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 14/09/21.
//

import UIKit

class TouchIDCustomCell: UITableViewCell {
    @IBOutlet weak var personalizeLabel: UILabel!
    @IBOutlet weak var personalizeImageView: UIImageView!
    @IBOutlet weak var switchButton: UISwitch!
    override func awakeFromNib() {
        applyStyling()
        applyFont()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        switchButton.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        personalizeLabel.textColor = ColourConstants.hex191919
        personalizeLabel.applyLetterSpacing(spacing: 1.3)
    }
    // MARK: Set selected state.
    func setSwitchState() {
        if UserDefaults.standard.bool(forKey: "isTouchIDEnabled") {
            switchButton.setOn(true, animated: false)
        } else {
            switchButton.setOn(false, animated: false)
        }
    }
    // MARK: - Font Styling Method
    func applyFont() {
        personalizeLabel.font = UIFont(name: FontConstants.regular, size: 13)
    }
    @IBAction func switchButtonAction(sender: UISwitch) {
        if sender.isOn {
            UserDefaults.standard.setValue(true, forKey: "isTouchIDEnabled")
        } else {
            UserDefaults.standard.setValue(false, forKey: "isTouchIDEnabled")
        }
    }
}
