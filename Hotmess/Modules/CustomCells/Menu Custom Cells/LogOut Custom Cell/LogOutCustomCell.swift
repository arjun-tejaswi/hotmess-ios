//
//  LogOutCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 02/06/21.
//

import UIKit

class LogOutCustomCell: UITableViewCell {
    @IBOutlet weak var logOutButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    func applyStyling() {
        let registerNowUnderLineAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: FontConstants.medium, size: 13) ?? UIFont.systemFont(ofSize: 13),
            .foregroundColor: UIColor(named: "Label Black") ?? UIColor.lightGray,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let registerNowAttribute = NSMutableAttributedString(string: "LOGOUT",
                                                        attributes: registerNowUnderLineAttributes)
        logOutButton.setAttributedTitle(registerNowAttribute, for: .normal)
        logOutButton.applyButtonLetterSpacing(spacing: 1.3)
    }
    func applyFont() {
        logOutButton.titleLabel?.font = UIFont(name: FontConstants.medium, size: 13)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
