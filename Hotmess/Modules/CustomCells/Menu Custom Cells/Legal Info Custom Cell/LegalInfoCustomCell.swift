//
//  LegalInfoCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 02/06/21.
//

import UIKit

class LegalInfoCustomCell: UITableViewCell {
    @IBOutlet weak var separationViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var separationView: UIView!
    @IBOutlet weak var leagalInfoLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        leagalInfoLabel.textColor = ColourConstants.hex717171
        leagalInfoLabel.applyLetterSpacing(spacing: 1.3)
    }
    // MARK: - Font Styling Method
    func applyFont() {
        leagalInfoLabel.font = UIFont(name: FontConstants.regular, size: 13)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
