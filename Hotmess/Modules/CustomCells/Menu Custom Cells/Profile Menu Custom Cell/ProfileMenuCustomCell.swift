//
//  ProfileMenuCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 02/06/21.
//

import UIKit

class ProfileMenuCustomCell: UITableViewCell {
    @IBOutlet weak var loginRegistrationButton: UIButton!
    @IBOutlet weak var myAccountView: UIView!
    @IBOutlet weak var profileMenuView: UIView!
    @IBOutlet weak var customerImageView: UIImageView!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var welcomLabel: UILabel!
    @IBOutlet weak var myAccountLabel: UILabel!
    @IBOutlet weak var myAccountButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        myAccountLabel.applyLetterSpacing(spacing: 1.4)
        customerNameLabel.applyLetterSpacing(spacing: 1.6)
        welcomLabel.applyLetterSpacing(spacing: 1.6)
        customerImageView.contentMode = .scaleAspectFill
        customerImageView.layer.cornerRadius = customerImageView.frame.width / 2
        profileMenuView.backgroundColor = ColourConstants.hexE0E0E0
        loginRegistrationButton.setTitle("LOGIN/REGISTER", for: .normal)
        loginRegistrationButton.setTitleColor(ColourConstants.hex191919, for: .normal)
    }
    // MARK: - Font Styling Method
    func applyFont() {
        welcomLabel.font = UIFont(name: FontConstants.medium, size: 16)
        customerNameLabel.font = UIFont(name: FontConstants.regular, size: 16)
        myAccountLabel.font = UIFont(name: FontConstants.regular, size: 14)
        loginRegistrationButton.titleLabel?.font = UIFont(name: FontConstants.semiBold, size: 16)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
