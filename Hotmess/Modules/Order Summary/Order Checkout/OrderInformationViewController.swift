//
//  OrderInformationViewController.swift
//  Hotmess
//
//  Created by Akshatha on 04/08/21.
//

import UIKit
import Razorpay

protocol ChangeAddressProtocol: AnyObject {
    func changedAddress(address: [ShippingAddressRealm])
    func clearAddresses()
}

class OrderInformationViewController: UIViewController {
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var totalTitleLabel: UILabel!
    @IBOutlet weak var orderSummeryTitleLabel: UILabel!
    @IBOutlet weak var orderSummeryPriceStackView: UIStackView!
    @IBOutlet weak var orderSummeryStackView: UIStackView!
    @IBOutlet weak var addAddressButton: UIButton!
    @IBOutlet weak var checkoutLabel: UILabel!
    @IBOutlet weak var shippingAddressHeader: UILabel!
    @IBOutlet weak var mobileNumberCodeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var shippingAddressView: UIView!
    @IBOutlet weak var deliveryOptionHeader: UILabel!
    @IBOutlet weak var freeDeliveryView: UIView!
    @IBOutlet weak var expressDeliveryView: UIView!
    @IBOutlet weak var freeDeliveryTitle: UILabel!
    @IBOutlet weak var freeDeliveryIsCheckedImageView: UIImageView!
    @IBOutlet weak var freeDeliverySubtitle: UILabel!
    @IBOutlet weak var expressDeliveryTitle: UILabel!
    @IBOutlet weak var expressDeliveryIsCheckedImageView: UIImageView!
    @IBOutlet weak var expressDeliverySubTitle: UILabel!
    @IBOutlet weak var paymentHeader: UILabel!
    @IBOutlet weak var cardPaymentView: UIView!
    @IBOutlet weak var cardPaymentLabel: UILabel!
    @IBOutlet weak var cardPaymentIsCheckedImageView: UIImageView!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var cashPaymentView: UIView!
    @IBOutlet weak var cashPaymentLabel: UILabel!
    @IBOutlet weak var cashPaymentIsCheckedImageView: UIImageView!
    @IBOutlet weak var packagingHeader: UILabel!
    @IBOutlet weak var regularPackingView: UIView!
    @IBOutlet weak var freeLabel: UILabel!
    @IBOutlet weak var regularPackingIsCheckedImageView: UIImageView!
    @IBOutlet weak var premiumPackingView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var regularPackingLabel: UILabel!
    @IBOutlet weak var premiumPackingLabel: UILabel!
    @IBOutlet weak var premiumPackingIsCheckedImageView: UIImageView!
    @IBOutlet weak var addMessageView: UIView!
    @IBOutlet weak var textFieldsView: UIView!
    @IBOutlet weak var addMessageLabel: UILabel!
    @IBOutlet weak var optionalLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var giftingToTextField: UITextField!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var premiumPackageBottomConstraint: NSLayoutConstraint!
    var displayAddress: String = ""
    var displayFullName: String = ""
    var displayMobileNumber: String = ""
    var razorpay: RazorpayCheckout!
    var presenter = OrderinformationPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        applyStyling()
        applyFont()
        addMessageView.isHidden = true
        textFieldsView.isHidden = true
        if presenter.isGuest {
            showGuestAddress()
            presenter.requestToCalculateGuestPrice()
        } else {
            self.view.showLoader()
            presenter.requestAddressList()
            presenter.requestToCalculatePrice()
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func addAddressButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "AddAddress", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "AddAddressViewController")
                as? AddAddressViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    internal func showPaymentForm() {
        let options = presenter.getRazorpayPayload()
        razorpay.open(options, displayController: self)
    }
}
extension OrderInformationViewController {
    // MARK: - Option Selection Action
    func freeDeliveryUI() {
        freeDeliveryIsCheckedImageView.image = UIImage(named: "accountCreatedCheck")
        expressDeliveryIsCheckedImageView.image = UIImage(named: "orderUncheck")
        presenter.selectedDeliveryOption = 0
        self.view.showLoader()
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            presenter.requestToCalculatePrice()
        } else {
            presenter.requestToCalculateGuestPrice()
        }
    }
    func expressDeliveryUI() {
        freeDeliveryIsCheckedImageView.image = UIImage(named: "orderUncheck")
        expressDeliveryIsCheckedImageView.image = UIImage(named: "accountCreatedCheck")
        presenter.selectedDeliveryOption = 1
        self.view.showLoader()
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            presenter.requestToCalculatePrice()
        } else {
            presenter.requestToCalculateGuestPrice()
        }
    }
    @IBAction func selectionButtonAction(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            freeDeliveryUI()
        case 1:
            expressDeliveryUI()
        case 2:
            cardPaymentIsCheckedImageView.image = UIImage(named: "accountCreatedCheck")
            cashPaymentIsCheckedImageView.image = UIImage(named: "orderUncheck")
            presenter.selectedPaymentOption = 0
            continueButton.backgroundColor = ColourConstants.hex191919
            print(presenter.calucatePriceInfo.total)
            let totalAmount = String(format: "%.2f", presenter.totalPrice)
            continueButton.setTitle("PAY ₹ \(totalAmount)", for: .normal)
            continueButton.isEnabled = true
        case 3:
            cardPaymentIsCheckedImageView.image = UIImage(named: "orderUncheck")
            cashPaymentIsCheckedImageView.image = UIImage(named: "accountCreatedCheck")
            presenter.selectedPaymentOption = 1
            continueButton.backgroundColor = ColourConstants.hex191919
            continueButton.setTitle("Confirm Your Order", for: .normal)
            continueButton.isEnabled = true
        case 4:
            regularPackageUI()
        case 5:
            premiumPackageUI()
        case 6:
            premiumPackageWithGiftMessageUI()
        default:
            print("Unknown option clicked")
        }
    }
    func regularPackageUI() {
        regularPackingIsCheckedImageView.image = UIImage(named: "accountCreatedCheck")
        premiumPackingIsCheckedImageView.image = UIImage(named: "orderUncheck")
        addMessageView.isHidden = true
        textFieldsView.isHidden = true
        presenter.selectedPackingOption = 0
        premiumPackageBottomConstraint.constant = 0
        presenter.isGift = false
        self.view.showLoader()
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            presenter.requestToCalculatePrice()
        } else {
            presenter.requestToCalculateGuestPrice()
        }
    }
    func premiumPackageUI() {
        regularPackingIsCheckedImageView.image = UIImage(named: "orderUncheck")
        premiumPackingIsCheckedImageView.image = UIImage(named: "accountCreatedCheck")
        addMessageView.isHidden = false
        presenter.selectedPackingOption = 1
        premiumPackageBottomConstraint.constant = 20
        presenter.isGift = true
        self.view.showLoader()
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            presenter.requestToCalculatePrice()
        } else {
            presenter.requestToCalculateGuestPrice()
        }
    }
    func premiumPackageWithGiftMessageUI() {
        addMessageView.isHidden = true
        textFieldsView.isHidden = false
        presenter.selectedPackingOption = 2
        premiumPackageBottomConstraint.constant = 20
        presenter.isGift = true
    }
    // MARK: - Back Button Action Method
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Change Address Button Action Method
    @IBAction func changeAddressButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "ShippingAddress", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ShippingAddressViewController")
                as? ShippingAddressViewController else { return }
        nextvc.changeAddressDelegate = self
        nextvc.presenter.userAddressList = presenter.userAddressList
        //        nextvc.presenter.selectedAddress = presenter.getDefaultAddress()
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    // MARK: - Control Button Action Method
    @IBAction func continueAndPayButtonAction(_ sender: UIButton) {
        if presenter.selectedPaymentOption == -1 ||
            presenter.selectedDeliveryOption == -1 ||
            presenter.selectedPackingOption == -1 {
            AlertManager.sharedInstance.showAlertWith(
                title: "Error",
                message: "Please select atleast one option in each section")
        } else {
            if presenter.selectedPackingOption == 2 {
                if  fromTextField.text == "" ||
                        giftingToTextField.text == "" ||
                        messageTextField.text == "" {
                    print("Cannot Proceed, please fill message details")
                    let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                        bounds: self.view.bounds,
                        title: "ADD MESSAGE",
                        subTitle: "Are you sure you want to continue without adding a message ?",
                        buttonTitle: "Yes") {
                        self.proceedToCreateOrder()
                    }
                    self.view.addSubview(popUp)
                } else {
                    proceedToCreateOrder()
                }
            } else {
                proceedToCreateOrder()
            }
        }
    }
    func proceedToCreateOrder() {
        self.view.showLoader()
        if !presenter.isGuest {
            presenter.selectedAddress = presenter.getSelectedAddress()
        }
        presenter.createOrder()
    }
    func goToOrderStatus(status: OrderStatus) {
        let storyBoard = UIStoryboard.init(name: "OrderStatus", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "OrderStatusViewController")
                as? OrderStatusViewController else { return }
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            nextvc.orderStatus = status
            nextvc.orderID = presenter.orderId
        } else {
            nextvc.orderStatus = status
            nextvc.orderID = presenter.orderId
            nextvc.orderFromGuest = true
        }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
extension OrderInformationViewController: OrderSummaryPresenterProtocol {
    func calculatePriceSuccess() {
        self.view.hideLoader()
        orderSummeryStackView.subviews.forEach({ $0.removeFromSuperview() })
        orderSummeryPriceStackView.subviews.forEach({ $0.removeFromSuperview() })
        let totalAmount = String(format: "%.2f", presenter.totalPrice)
        totalPriceLabel.text = "₹ \(totalAmount)"
        continueButton.setTitle("PAY ₹ \(totalAmount)", for: .normal)
        for orderTitles in presenter.priceBreakUp {
            let orderPriceTitleLabel = UILabel()
            orderPriceTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
            orderPriceTitleLabel.textColor = ColourConstants.hex191919
            orderPriceTitleLabel.text = orderTitles.title
            orderSummeryStackView.addArrangedSubview(orderPriceTitleLabel)
        }
        for orderPrice in presenter.priceBreakUp {
            let orderPriceLabel = UILabel()
            orderPriceLabel.font = UIFont(name: FontConstants.regular, size: 14)
            orderPriceLabel.textColor = ColourConstants.hex191919
            if orderPrice.title == "PromoCode Discount" {
                orderPriceLabel.text = "- ₹ \(orderPrice.price ?? "0.00")"
            } else {
                orderPriceLabel.text = "₹ \(orderPrice.price ?? "0.00")"
            }
            orderSummeryPriceStackView.addArrangedSubview(orderPriceLabel)
        }
    }
    func orderCreated() {
        razorpay = RazorpayCheckout.initWithKey(RazorPayConstants.apiKey, andDelegate: self)
        showPaymentForm()
    }
    func orderPlacedSuccessfully() {
        self.view.hideLoader()
        goToOrderStatus(status: .SUCCESS)
    }
    func orderPlacedFailed() {
        self.view.hideLoader()
        goToOrderStatus(status: .FAILURE)
    }
    func alertMessage(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if !status {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
    func listAddressSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if presenter.userAddressList.count > 0 {
            addAddressButton.isHidden = true
            shippingAddressView.isHidden = false
            changeButton.isHidden = false
            if presenter.selectedPaymentOption == -1 {
                continueButton.isEnabled = false
                continueButton.setTitle("Confirm Your Order", for: .normal)
                continueButton.backgroundColor = ColourConstants.hexCECECE
            } else {
                continueButton.isEnabled = true
                continueButton.backgroundColor = ColourConstants.hex191919
            }
            updateAddress(isDefault: true)
        } else {
            continueButton.isEnabled = false
            continueButton.backgroundColor = ColourConstants.hexE4E4E4
            shippingAddressView.isHidden = true
            addAddressButton.isHidden = false
            changeButton.isHidden = true
        }
    }
}
extension OrderInformationViewController: RazorpayPaymentCompletionProtocol {
    func onPaymentError(_ code: Int32, description str: String) {
        self.view.hideLoader()
        goToOrderStatus(status: .FAILURE)
        print("Payment failed: \(code) \(str)")
    }
    func onPaymentSuccess(_ paymentId: String) {
        print("Payment successful: \(paymentId)")
        if presenter.isGuest {
            presenter.verifyGuestPayment(paymentId: paymentId)
        } else {
            presenter.verifyPayment(paymentId: paymentId)
        }
    }
}
extension OrderInformationViewController: ChangeAddressProtocol {
    func changedAddress(address: [ShippingAddressRealm]) {
        presenter.userAddressList = address
        updateAddress(isDefault: false)
    }
    func clearAddresses() {
        presenter.userAddressList.removeAll()
    }
}
