//
//  OrderInformationViewControllerExtension.swift
//  Hotmess
//
//  Created by Flaxon on 01/10/21.
//

import Foundation

extension OrderInformationViewController {
    // MARK: - UI Styling Method
    func applyStyling() {
        shippingAddressView.addBorders(edges: [.top, .left, .right, .bottom],
                                       color: ColourConstants.hex8B8B8B)
        freeDeliveryView.addBorders(edges: [.top, .left, .right, .bottom],
                                    color: ColourConstants.hex8B8B8B)
        expressDeliveryView.addBorders(edges: [.left, .right, .bottom],
                                       color: ColourConstants.hex8B8B8B)
        regularPackingView.addBorders(edges: [.top, .left, .right, .bottom],
                                      color: ColourConstants.hex8B8B8B)
        premiumPackingView.addBorders(edges: [.left, .right, .bottom],
                                      color: ColourConstants.hex8B8B8B)
        cardPaymentView.addBorders(edges: [.top, .left, .right, .bottom],
                                   color: ColourConstants.hex8B8B8B)
        cashPaymentView.addBorders(edges: [.left, .right, .bottom],
                                   color: ColourConstants.hex8B8B8B)
        addAddressButton.addBorders(edges: [.top, .left, .right, .bottom], color: ColourConstants.hex8B8B8B)
        fromTextField.setLeftPaddingPoints(10)
        giftingToTextField.setLeftPaddingPoints(10)
        messageTextField.setLeftPaddingPoints(10)
        fromTextField.setRightPaddingPoints(10)
        giftingToTextField.setRightPaddingPoints(10)
        messageTextField.setRightPaddingPoints(10)
        freeDeliveryTitle.applyLetterSpacing(spacing: 1.56)
        checkoutLabel.applyLetterSpacing(spacing: 0.7)
        expressDeliveryTitle.applyLetterSpacing(spacing: 1.56)
        continueButton.applyButtonLetterSpacing(spacing: 2.52)
        orderSummeryTitleLabel.textColor = ColourConstants.hex191919
        orderSummeryTitleLabel.applyLetterSpacing(spacing: 0.9)
        totalTitleLabel.textColor = ColourConstants.hex191919
        totalPriceLabel.textColor = ColourConstants.hex191919
        if presenter.isGuest {
            changeButton.isHidden = true
        }
    }
    // MARK: - Font Style Method
    func applyFont() {
        checkoutLabel.font = UIFont(name: FontConstants.regular, size: 14)
        addAddressButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 16)
        shippingAddressHeader.font = UIFont(name: FontConstants.semiBold, size: 16)
        changeButton.titleLabel?.font = UIFont(name: FontConstants.semiBold, size: 14)
        nameLabel.font = UIFont(name: FontConstants.medium, size: 18)
        addressLabel.font = UIFont(name: FontConstants.light, size: 16)
        mobileNumberCodeLabel.font = UIFont(name: FontConstants.regular, size: 16)
        deliveryOptionHeader.font = UIFont(name: FontConstants.semiBold, size: 16)
        freeDeliveryTitle.font = UIFont(name: FontConstants.medium, size: 13)
        freeDeliverySubtitle.font = UIFont(name: FontConstants.light, size: 12)
        expressDeliveryTitle.font = UIFont(name: FontConstants.medium, size: 13)
        expressDeliverySubTitle.font = UIFont(name: FontConstants.light, size: 12)
        paymentHeader.font = UIFont(name: FontConstants.semiBold, size: 16)
        cardPaymentLabel.font = UIFont(name: FontConstants.regular, size: 14)
        cashPaymentLabel.font = UIFont(name: FontConstants.regular, size: 14)
        packagingHeader.font = UIFont(name: FontConstants.semiBold, size: 16)
        regularPackingLabel.font = UIFont(name: FontConstants.regular, size: 14)
        premiumPackingLabel.font = UIFont(name: FontConstants.regular, size: 14)
        addMessageLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        optionalLabel.font = UIFont(name: FontConstants.light, size: 12)
        priceLabel.font = UIFont(name: FontConstants.medium, size: 14)
        freeLabel.font = UIFont(name: FontConstants.medium, size: 14)
        continueButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        orderSummeryTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        totalTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
        totalPriceLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
    }
    func updateAddress(isDefault: Bool) {
        let addressToFill = isDefault ? presenter.getDefaultAddress() : presenter.getSelectedAddress()
        displayAddress = "\(addressToFill.addressLine1),\n\(addressToFill.addressLine2)" +
            "\n\(addressToFill.city), " +
            "\(addressToFill.state) - \n\(addressToFill.areaPinCode)" +
            "\n\(addressToFill.country)" +
            "\nLandmark : \(addressToFill.landMark)"
        displayMobileNumber = " \(addressToFill.mobileNumber)"
        displayFullName = "\(addressToFill.firstName)" + " \(addressToFill.lastName)"
        nameLabel.text = displayFullName
        addressLabel.text = displayAddress
        mobileNumberCodeLabel.text = "Mobile No: +91 \(displayMobileNumber)"
    }
    func showGuestAddress() {
        if let addressToFill = presenter.selectedAddress {
            displayAddress = "\(addressToFill.addressLine1),\n\(addressToFill.addressLine2)" +
                "\n\(addressToFill.city), " +
                "\(addressToFill.state) - \n\(addressToFill.areaPinCode)" +
                "\n\(addressToFill.country)" +
                "\nLandmark : \(addressToFill.landMark)"
            displayMobileNumber = " \(addressToFill.mobileNumber)"
            displayFullName = "\(addressToFill.firstName)" + " \(addressToFill.lastName)"
            nameLabel.text = displayFullName
            addressLabel.text = displayAddress
            mobileNumberCodeLabel.text = "Mobile No: +91 \(displayMobileNumber)"
        }
    }
}
