//
//  OrderSummaryPresenter.swift
//  Hotmess
//
//  Created by Akshatha on 20/07/21.
//

import Foundation
import SwiftyJSON
import RealmSwift

enum OrderSummarySectionType {
    case ORDERSUMMARY
    case SHIPPING
    case NOADDRESS
    case DELIVERY
    case PACKING
    case PAYMENT
    case GIFT
}
protocol OrderSummaryPresenterProtocol: AnyObject {
    func listAddressSuccess(status: Bool, title: String, message: String)
    func alertMessage(status: Bool, title: String, message: String)
    func orderCreated()
    func orderPlacedSuccessfully()
    func orderPlacedFailed()
    func calculatePriceSuccess()
}
class OrderinformationPresenter: NSObject {
    var sections = [OrderSummarySectionModel]()
    weak var delegate: OrderSummaryPresenterProtocol?
    var userAddressList = [ShippingAddressRealm]()
    var calucatePriceInfo = CalculateOrderPriceRealm()
    var isGift: Bool = false
    var giftingTo: String = ""
    var giftFrom: String = ""
    var giftMessage: String = ""
    var bagItems: [ShoppingBagRealm]!
    var selectedAddress: ShippingAddressRealm!
    var selectedDeliveryOption: Int = -1 // 0 = "Free Delivery" | 1 = "Express Delivery"
    var selectedPaymentOption: Int = -1 // 0 = "Card Payment" | 1 = "Cash Payment"
    var selectedPackingOption: Int = -1 // 0 = "Regular Packing" | 1 = "Gift Packing" | 2 = "Packing with message"
    var razorPayOrderId: String = ""
    var orderId: String = ""
    var priceBreakUp = [PriceListingModel]()
    var totalPrice: Double = 0
    var isGuest: Bool = false
    var guestEmail: String = ""
    var receiveNews: Bool = false
    func requestAddressList() {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.listAddress,
            prametres: [:],
            type: .withAuth,
            method: .POST,
            completion: self.handleAddressListResponse)
    }
    func handleAddressListResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.requestAddressList()
                        }
                    } else {
                        delegate?.listAddressSuccess(status: false, title: "Error", message: message)
                    }
                }
            } else {
                if let data = response["data"].array {
                    userAddressList.removeAll()
                    for aAddress in data {
                        if let aAddressDictionary = aAddress.dictionary {
                            let allAddresses = AddressParser.sharedInstance.parseAddressData(data: aAddressDictionary)
                            userAddressList.append(allAddresses)
                        }
                    }
                    delegate?.listAddressSuccess(status: true, title: "", message: "")
                }
            }
        }
    }
    func getDefaultAddress() -> ShippingAddressRealm {
        var defaultAddress: ShippingAddressRealm!
        for address in userAddressList where address.isDefaultAddress {
            defaultAddress = address
        }
        return defaultAddress
    }
    func getSelectedAddress() -> ShippingAddressRealm {
        var selectedAddress: ShippingAddressRealm!
        for address in userAddressList where address.isSelected {
            selectedAddress = address
        }
        if selectedAddress == nil {
            selectedAddress = getDefaultAddress()
        }
        return selectedAddress
    }
    func getRazorpayPayload() -> [String: Any] {
        let options: [String: Any] = [
            "modal": [
                "animation": true
            ],
            "currency": "INR",
            "description": "Clothing Items",
            "order_id": razorPayOrderId,
            "image": RazorPayConstants.logoUrl,
            "name": "Hotmess Fashion",
            "theme": [
                "backdrop_color": "#123456",
                "color": "#191919"
            ]
        ]
        return options
    }
    func verifyGuestPayment(paymentId: String) {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.verifyGuestPayment,
            prametres: [
                "email": guestEmail,
                "order_id": orderId,
                "payment_id": paymentId
            ], type: .withoutAuth,
            method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.verifyPayment(paymentId: paymentId)
                            }
                        } else {
                            self.delegate?.orderPlacedFailed()
                        }
                    }
                } else {
                    RealmDataManager.sharedInstance.deleteAllFromBag()
                    self.delegate?.orderPlacedSuccessfully()
                }
            }
        }
    }
    func verifyPayment(paymentId: String) {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.verifyPayment,
            prametres: [
                "order_id": orderId,
                "payment_id": paymentId
            ], type: .withAuth,
            method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.verifyPayment(paymentId: paymentId)
                            }
                        } else {
                            self.delegate?.orderPlacedFailed()
                        }
                    }
                } else {
                    self.removeAllFromShoppingBag()
                }
            }
        }
    }
    func createOrder() {
        var lineItems = [[String: Any]]()
        for item in bagItems {
            lineItems.append([
                "quantity": item.productQuantity,
                "product_id": item.productID,
                "sku_id": item.stockKeepUnitID
            ])
        }
        let parameters: [String: Any] = [
            "address": selectedAddress.addressLine1,
            "pin_code": selectedAddress.areaPinCode,
            "state": selectedAddress.state,
            "city": selectedAddress.city,
            "country_code": "+91",
            "country": selectedAddress.country,
            "landmark": selectedAddress.landMark,
            "payment_type": selectedPaymentOption == 0 ? "ONLINE" : "COD",
            "delivery_type": selectedDeliveryOption == 0 ? "FREE" : "EXPRESS_DELIVERY",
            "locality": selectedAddress.addressLine2,
            "first_name": selectedAddress.firstName,
            "last_name": selectedAddress.lastName,
            "phone_number": selectedAddress.mobileNumber,
            "gift_to": giftingTo,
            "is_gift": isGift,
            "gift_message": giftMessage,
            "gift_from": giftFrom,
            "promocode": "",
            "line_items": lineItems
        ]
        if isGuest {
            requestCreateGuestOrder()
        } else {
            NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.createOrder,
                                                         prametres: parameters, type: .withAuth,
                                                         method: .POST, completion: handleResponseForOrderCreate)
        }
    }
    func requestToCalculatePrice() {
        var lineItems = [[String: Any]]()
        for item in bagItems {
            lineItems.append([
                "quantity": item.productQuantity,
                "product_id": item.productID,
                "sku_id": item.stockKeepUnitID
            ])
        }
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.calculateOrderPrice,
            prametres: [
                "is_gift": isGift,
                "promocode": "",
                "delivery_type": selectedDeliveryOption == -1 || selectedDeliveryOption == 0 ? "FREE" :
                    "EXPRESS_DELIVERY",
                "line_items": lineItems
            ],
            type: .withoutAuth,
            method: .POST,
            completion: self.handleCalculateOrderPriceResponse)
    }
    func requestToCalculateGuestPrice() {
        var lineItems = [[String: Any]]()
        for item in bagItems {
            lineItems.append([
                "quantity": item.productQuantity,
                "product_id": item.productID,
                "sku_id": item.stockKeepUnitID
            ])
        }
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.calculateGuestPrice,
            prametres: [
                "is_gift": isGift,
                "promocode": "",
                "delivery_type": selectedDeliveryOption == -1 || selectedDeliveryOption == 0 ? "FREE" :
                    "EXPRESS_DELIVERY",
                "line_items": lineItems
            ],
            type: .withoutAuth,
            method: .POST,
            completion: self.handleCalculateOrderPriceResponse)
    }
}
extension OrderinformationPresenter {
    func removeAllFromShoppingBag() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            NetworkManager.sharedInstance.makeNetworkRequest(
                urlString: ApiConstants.removeAllFromBag,
                prametres: [:],
                type: .withAuth,
                method: .POST) { response in
                if let status = response["error_status"].bool {
                    if status {
                        if let message = response["message"].string {
                            if message == "requestForRefresh" {
                                NetworkManager.sharedInstance.refreshBearerToken {
                                    self.removeAllFromShoppingBag()
                                }
                            } else {
                                self.delegate?.alertMessage(status: false, title: "Error", message: message)
                            }
                        }
                    } else {
                        self.delegate?.orderPlacedSuccessfully()
                    }
                }
            }
        } else {
            RealmDataManager.sharedInstance.deleteAllFromBag()
            delegate?.orderPlacedSuccessfully()
        }
    }
    func handleResponseForOrderCreate(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.createOrder()
                        }
                    } else {
                        delegate?.alertMessage(status: false, title: "Error", message: message)
                    }
                }
            } else {
                self.orderId =  response["data"]["order_id"].string ?? ""
                if response["data"]["payment_type"].string == "ONLINE" {
                    if let razorPayId = response["data"]["payment_info"]["id"].string {
                        self.razorPayOrderId = razorPayId
                        self.delegate?.orderCreated()
                    } else {
                        print("Could not find order id to trigger payment.")
                    }
                } else {
                    removeAllFromShoppingBag()
                }
            }
        }
    }
    func handleCalculateOrderPriceResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                delegate?.alertMessage(status: false, title: "Error", message: response["message"].string ?? "")
            } else {
                if let data = response["data"].dictionary {
                    priceBreakUp.removeAll()
                    totalPrice = data["total"]?.double ?? 0.0
                    let shippingCharges = PriceListingModel.init(
                        title: "Shipping",
                        price: String(format: "%0.2f", data["shipping_charges"]?.double ?? 0.00))
                    let tax = PriceListingModel.init(
                        title: "Taxes",
                        price: String(format: "%0.2f", data["tax"]?.double ?? 0.00))
                    let subTotal = PriceListingModel.init(
                        title: "Item Subtotal",
                        price: String(format: "%0.2f", data["sub_total"]?.double ?? 0.00))
                    let expressDeliveryCharge = PriceListingModel.init(
                        title: "Express Delivery",
                        price: String(format: "%0.2f", data["express_delivery_charge"]?.double ?? 0.00))
                    let giftPackingAmount = PriceListingModel.init(
                        title: "Premium Gift Packing",
                        price: String(format: "%0.2f", data["gift_packing_amount"]?.double ?? 0.00))
                    let promoCodeDiscount = PriceListingModel.init(
                        title: "PromoCode Discount",
                        price: String(format: "%0.2f", data["promocode_discount"]?.double ?? 0.00))
                    if subTotal.price != "0.00" {
                        priceBreakUp.append(subTotal)
                    }
                    if shippingCharges.price != "0.00" {
                        priceBreakUp.append(shippingCharges)
                    }
                    if tax.price != "0.00" {
                        priceBreakUp.append(tax)
                    }
                    if expressDeliveryCharge.price != "0.00" {
                        priceBreakUp.append(expressDeliveryCharge)
                    }
                    if giftPackingAmount.price != "0.00" {
                        priceBreakUp.append(giftPackingAmount)
                    }
                    if promoCodeDiscount.price != "0.00" {
                        priceBreakUp.append(promoCodeDiscount)
                    }
                    delegate?.calculatePriceSuccess()
                }
            }
        }
    }
    func requestCreateGuestOrder() {
        var lineItems = [[String: Any]]()
        for item in bagItems {
            lineItems.append([
                "quantity": item.productQuantity,
                "product_id": item.productID,
                "sku_id": item.stockKeepUnitID
            ])
        }
        var parameters: [String: Any] = [
            "address": selectedAddress.addressLine1,
            "pin_code": selectedAddress.areaPinCode,
            "state": selectedAddress.state,
            "city": selectedAddress.city,
            "country": selectedAddress.country,
            "country_code": "+91",
            "landmark": selectedAddress.landMark,
            "payment_type": selectedPaymentOption == 0 ? "ONLINE" : "COD",
            "delivery_type": selectedDeliveryOption == 0 ? "FREE" : "EXPRESS_DELIVERY",
            "locality": selectedAddress.addressLine2,
            "first_name": selectedAddress.firstName,
            "last_name": selectedAddress.lastName,
            "email": guestEmail,
            "phone_number": selectedAddress.mobileNumber,
            "gift_to": giftingTo,
            "is_gift": isGift,
            "gift_message": giftMessage,
            "gift_from": giftFrom,
            "promocode": "",
            "line_items": lineItems
        ]
        parameters.updateValue(receiveNews, forKey: "receive_promotional")
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.createGuestOrder,
                                                         prametres: parameters, type: .withoutAuth,
                                                         method: .POST, completion: handleResponseForOrderCreate)
    }
}
