//
//  OrderStatusViewController.swift
//  Hotmess
//
//  Created by Akshatha on 05/08/21.
//

import UIKit
enum OrderStatus {
    case SUCCESS
    case FAILURE
}
class OrderStatusViewController: UIViewController {
    @IBOutlet weak var successTitle: UILabel!
    @IBOutlet weak var successSubTitleOne: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var trackStatusButton: UIButton!
    @IBOutlet weak var successSubTitleTwo: UILabel!
    @IBOutlet weak var failureTitle: UILabel!
    @IBOutlet weak var failureSubTitleOne: UILabel!
    @IBOutlet weak var failureSubTitleTwo: UILabel!
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var orderSuccessView: UIView!
    @IBOutlet weak var orderFailureView: UIView!
    @IBOutlet weak var hotmessLogoImageView: UIImageView!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var orderIDLabel: UILabel!
    @IBOutlet weak var orderConfirmedImageView: UIImageView!
    @IBOutlet weak var shareViewContainer: UIView!
    var orderStatus: OrderStatus = .FAILURE
    var orderID: String = ""
    var orderFromGuest: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyling()
        applyFont()
        switch orderStatus {
        case .SUCCESS:
            if orderFromGuest {
                shareViewContainer.isHidden = false
                orderFailureView.isHidden = true
                orderSuccessView.isHidden = false
                orderIDLabel.text = "Order ID #\(orderID)"
                trackStatusButton.isHidden = true
            } else {
                shareViewContainer.isHidden = false
                orderFailureView.isHidden = true
                orderSuccessView.isHidden = false
                orderIDLabel.text = "Order ID #\(orderID)"
                trackStatusButton.isHidden = false
            }
        case .FAILURE:
            orderSuccessView.isHidden = true
            orderFailureView.isHidden = false
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func trackStatusButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "MyOrders", bundle: nil)
        guard let nextVC = storyBoard.instantiateViewController(
                withIdentifier: "ViewOrderDetailsViewController") as? ViewOrderDetailsViewController else {
            return
        }
        nextVC.presenter.orderId = orderID
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    @IBAction func tryAgainButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func continueShoppingButtonAction(_ sender: UIButton) {
        if orderFromGuest {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as
                [UIViewController]
            self.navigationController!.popToViewController(
                viewControllers[viewControllers.count - 5],
                animated: true)
        } else {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as
                [UIViewController]
            self.navigationController!.popToViewController(
                viewControllers[viewControllers.count - 4],
                animated: true)
        }
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        shareLabel.textColor = ColourConstants.hex191919
        shareLabel.applyLetterSpacing(spacing: 2.52)
        trackStatusButton.applyButtonLetterSpacing(spacing: 2.52)
        continueButton.applyButtonLetterSpacing(spacing: 2.52)
        tryAgainButton.applyButtonLetterSpacing(spacing: 2.52)
        trackStatusButton.setTitleColor(ColourConstants.hex191919, for: .normal)
        continueButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        tryAgainButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        orderConfirmedImageView.image = UIImage(named: "Order Confirmed")
        trackStatusButton.layer.borderWidth = 1
        trackStatusButton.layer.borderColor = ColourConstants.hex191919.cgColor
    }
    // MARK: - Font Style Method
    func applyFont() {
        successTitle.font = UIFont(name: FontConstants.semiBold, size: 18)
        successTitle.textColor = ColourConstants.hex191919
        successSubTitleOne.font = UIFont(name: FontConstants.light, size: 16)
        successSubTitleOne.textColor = ColourConstants.hex191919
        successSubTitleTwo.font = UIFont(name: FontConstants.light, size: 16)
        successSubTitleTwo.textColor = ColourConstants.hex191919
        trackStatusButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        continueButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        failureTitle.font = UIFont(name: FontConstants.semiBold, size: 18)
        failureTitle.textColor = ColourConstants.hex191919
        failureSubTitleOne.font = UIFont(name: FontConstants.light, size: 16)
        failureSubTitleOne.textColor = ColourConstants.hex191919
        failureSubTitleTwo.font = UIFont(name: FontConstants.light, size: 16)
        failureSubTitleTwo.textColor = ColourConstants.hex191919
        tryAgainButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        orderIDLabel.font = UIFont(name: FontConstants.regular, size: 20)
        orderIDLabel.textColor = ColourConstants.hex191919
        shareLabel.font = UIFont(name: FontConstants.regular, size: 14)
    }
    @IBAction func shareButtonAction(_ sender: Any) {
        shareViewContainer.isHidden = true
        let screenShot = getScreenshot()
        let imageToShare = [ screenShot! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true) {
            self.shareViewContainer.isHidden = false
        }
    }
    func getScreenshot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, true, 0.0)
        if let context = UIGraphicsGetCurrentContext() { view.layer.render(in: context) }
        let screenshot: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return screenshot
    }
}
