//
//  CustomIntrinsicTableView.swift
//  Hotmess
//
//  Created by Flaxon on 02/06/21.
//

import UIKit

class CustomIntrinsicTableView: UITableView {
    override var contentSize: CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}
