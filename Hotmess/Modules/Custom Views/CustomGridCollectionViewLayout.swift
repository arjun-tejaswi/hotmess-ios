//
//  CustomGridCollectionViewLayout.swift
//  Hotmess
//
//  Created by Cedan Misquith on 23/06/21.
//

import UIKit

class CustomGridCollectionViewLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        setupLayout()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    override var itemSize: CGSize {
        get {
            // Setting the number of coloumns for collectionView.
            let numberOfColumns: CGFloat = 2
            let itemWidth = (self.collectionView?.frame.width)!/numberOfColumns
            return CGSize(width: itemWidth-6, height: 250)
        }
        set { _ = newValue }
    }
    func setupLayout() {
        minimumInteritemSpacing = 6 // Setting item spacing.
        minimumLineSpacing = 6 // Setting line spacing.
        scrollDirection = .vertical // Setting scroll direction.
    }
}

class CustomWishlistGridCollectionViewLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        setupLayout()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    override var itemSize: CGSize {
        get {
            // Setting the number of coloumns for collectionView.
            let numberOfColumns: CGFloat = 2
            let itemWidth = (self.collectionView?.frame.width)!/numberOfColumns
            return CGSize(width: itemWidth-16, height: (itemWidth * 1.5) + 171)
        }
        set { _ = newValue }
    }
    func setupLayout() {
        minimumInteritemSpacing = 16 // Setting item spacing.
        minimumLineSpacing = 0 // Setting line spacing.
        scrollDirection = .vertical // Setting scroll direction.
    }
}
