//
//  NewsLetterPresenter.swift
//  Hotmess
//
//  Created by Akshatha on 17/08/21.
//

import Foundation
import UIKit

protocol newsLetterSubscriptionProtocol: AnyObject {
    func fillTheFields(title: String, message: String)
    func subscriptionSuccess(status: Bool, title: String, message: String)
}
class NewsLetterPresenter: NSObject {
    weak var delegate: newsLetterSubscriptionProtocol?
    var updateParameters: [String: Any] = [
        "email_address": "",
        "women": false,
        "home": false,
        "kid": false
    ]
    func updateStatus(status: Bool, value: String) {
        switch value {
        case "WOMAN":
            updateParameters.updateValue(status, forKey: "women")
        case "HOME":
            updateParameters.updateValue(status, forKey: "home")
        case "KIDS":
            updateParameters.updateValue(status, forKey: "kid")
        default:
            print("unknown option")
        }
    }
    func requestNewsLetterSubscription(emailaddress: String, firstName: String, lastName: String) {
        let women = updateParameters["women"] as? Bool
        let home = updateParameters["home"] as? Bool
        let kid = updateParameters["kid"] as? Bool
        if women == false &&
            home == false &&
            kid == false {
            delegate?.fillTheFields(title: "Request not completed",
                                    message: "Please select an option to continue with your newsletter subscription.")
        } else {
            if firstName != "" {
                updateParameters.updateValue(firstName, forKey: "first_name")
            }
            if lastName != "" {
                updateParameters.updateValue(lastName, forKey: "last_name")
            }
            updateParameters.updateValue(emailaddress, forKey: "email_address")
            newsLetterSubscription()
        }
    }
    func newsLetterSubscription() {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.newsLetterSubscription,
            prametres: updateParameters,
            type: .withoutAuth,
            method: .POST) { response in
            if let status = response["error_status"].bool {
                if status {
                    self.delegate?.subscriptionSuccess(status: false,
                                                       title: "Error",
                                                       message: response["message"].string ?? "")
                } else {
                    self.delegate?.subscriptionSuccess(status: true,
                                                       title: "Success",
                                                       message: response["message"].string ?? "")
                }
            }
        }
    }
}
