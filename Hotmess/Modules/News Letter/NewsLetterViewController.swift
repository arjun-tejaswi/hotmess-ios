//
//  NewsLetterViewController.swift
//  Hotmess
//
//  Created by Akshatha on 16/08/21.
//

import UIKit

class NewsLetterViewController: UIViewController {
    @IBOutlet weak var newsLetterTitle: UILabel!
    @IBOutlet weak var newsLetterSubTitle: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailTopLabel: UILabel!
    @IBOutlet weak var sectionLabel: UILabel!
    @IBOutlet weak var newsLetterTableView: UITableView!
    @IBOutlet weak var subscribeButton: UIButton!
    @IBOutlet weak var newsLetterTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var newsLetterTopImageView: UIImageView!
    var newsLetterTableViewContent: [String] = ["WOMAN", "KIDS", "HOME"]
    let userDetails = RealmDataManager.sharedInstance.getUserDetails()
    var presenter = NewsLetterPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        newsLetterTableViewConfigure()
        applyFont()
        applyStyling()
        applyData()
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillLayoutSubviews() {
        self.newsLetterTableViewHeight.constant = self.newsLetterTableView.intrinsicContentSize.height
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func applyData() {
        if userDetails.userEmail == "" {
            emailTextField.text = ""
            emailTopLabel.isHidden = true
        } else {
            emailTextField.text = userDetails.userEmail
            emailTopLabel.isHidden = false
        }
    }
    // MARK: - Newsletter Tableview Configuration Method
    func newsLetterTableViewConfigure() {
        newsLetterTableView.delegate = self
        newsLetterTableView.dataSource = self
        newsLetterTableView.register(UINib(nibName: "NewsLetterCustomCell", bundle: nil),
                                     forCellReuseIdentifier: "NewsLetterCustomCell")
    }
    // MARK: - Font Styling Method
    func applyFont() {
        newsLetterTitle.font = UIFont(name: FontConstants.semiBold, size: 18)
        newsLetterSubTitle.font = UIFont(name: FontConstants.light, size: 14)
        emailTopLabel.font = UIFont(name: FontConstants.regular, size: 12)
        emailTextField.font = UIFont(name: FontConstants.regular, size: 14)
        sectionLabel.font = UIFont(name: FontConstants.regular, size: 14)
        subscribeButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        emailTextField.delegate = self
        newsLetterTitle.textColor = ColourConstants.hex191919
        newsLetterSubTitle.textColor = ColourConstants.hex191919
        emailTopLabel.textColor = ColourConstants.hex9B9B9B
        sectionLabel.textColor = ColourConstants.hex191919
        subscribeButton.applyButtonLetterSpacing(spacing: 2.52)
        subscribeButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        subscribeButton.backgroundColor = ColourConstants.hex191919
        emailTextField.layer.borderWidth = 0.5
        emailTextField.layer.borderColor = ColourConstants.hex717171.cgColor
        emailTextField.setLeftPaddingPoints(16.29)
        emailTextField.setRightPaddingPoints(10)
        newsLetterTopImageView.image = UIImage(named: "HOTMESS Logo")
        emailTextField.isUserInteractionEnabled = true
        emailTopLabel.isHidden = true
    }
    // MARK: - Subscribe Button Action
    @IBAction func subscribeButtonAction(_ sender: UIButton) {
        self.view.showLoader()
        presenter.requestNewsLetterSubscription(emailaddress: emailTextField.text ?? "",
                                                firstName: userDetails.userFirstName,
                                                lastName: userDetails.userLastName
        )
    }
    // MARK: - UI Switch Action
    @objc func switchChanged(_ sender: UISwitch) {
        if sender.isOn {
            sender.isOn = true
            newsLetterTableView.reloadData()
        } else {
            sender.isOn = false
            newsLetterTableView.reloadData()
        }
    }
}
// MARK: - Tableview Methods
extension NewsLetterViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsLetterTableViewContent.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = newsLetterTableView.dequeueReusableCell(withIdentifier: "NewsLetterCustomCell")
                as? NewsLetterCustomCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        cell.newsLetterTableViewTitle.text = newsLetterTableViewContent[indexPath.row]
        if indexPath.row == 2 {
            cell.newsLetterBottonView.isHidden = true
        }
        cell.selectionSwitch.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
        if cell.selectionSwitch.isOn {
            presenter.updateStatus(status: true,
                                   value: cell.newsLetterTableViewTitle.text ?? "")
        } else {
            presenter.updateStatus(status: false,
                                   value: cell.newsLetterTableViewTitle.text ?? "")
        }
        return cell
    }
}
// MARK: - Text Delegate Methods
extension NewsLetterViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if emailTextField.text  == "" {
            emailTopLabel.isHidden = true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTextField {
            emailTopLabel.isHidden = false
        }
    }
}
// MARK: - Protocol Methods
extension NewsLetterViewController: newsLetterSubscriptionProtocol {
    func fillTheFields(title: String, message: String) {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                        bounds: self.view.bounds,
                        title: title,
                        subTitle: message,
                        buttonTitle: "OK") { return }
                    self.view.addSubview(popUp)
    }
    func subscriptionSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                        bounds: self.view.bounds,
                        title: title,
                        subTitle: message,
                        buttonTitle: "OK") { return }
                    self.view.addSubview(popUp)
    }
}
