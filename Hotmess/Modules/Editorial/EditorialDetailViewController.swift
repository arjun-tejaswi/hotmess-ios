//
//  EditorialDetailViewController.swift
//  Hotmess
//
//  Created by Akshatha on 17/09/21.
//

import UIKit
import AVKit
import AVFoundation

class EditorialDetailViewController: UIViewController {
    @IBOutlet weak var editorialDetailTableview: UITableView!
    @IBOutlet weak var editorialDetailTopLabel: UILabel!
    var presenter = EditorialDetailPresenter()
    var dataValue = [ItemListRealm]()
    var topLabelValue: String = ""
    var player = AVPlayer()
    var playerViewController = AVPlayerViewController()
    var selectedVideo: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        editorialDetailTopLabel.text = topLabelValue
        applyFont()
        setUpEditorialDetailTableView()
        applyStyling()
        self.view.showLoader()
        presenter.requestEditorialDetail(categoryId: presenter.categoryID, editorialId: presenter.editorialID)
    }
    // MARK: - Statusbar Style Method
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - Back Button Action Method
    @IBAction func backButtonAction(_ sender: UIButton) {
        if presenter.isFromDeepLink {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                fatalError("Not of type 'AppDelegate'")
            }
            appDelegate.restartApp()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    // MARK: - Font Style Method
    func applyFont() {
        editorialDetailTopLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        editorialDetailTopLabel.applyLetterSpacing(spacing: 0.7)
        editorialDetailTopLabel.textColor = ColourConstants.hexFFFFFF
    }
    // MARK: - Video Play Button Action Method
    @objc func playButtonAction() {
        if let url = URL(string: selectedVideo) {
            let player = AVPlayer(url: url)
            playerViewController.player = player
            playerViewController.player?.play()
            self.present(playerViewController, animated: true, completion: nil)
        }
    }
    // MARK: - TableView SetUp Method
    func setUpEditorialDetailTableView() {
        editorialDetailTableview.delegate = self
        editorialDetailTableview.dataSource = self
        let celebrityMessageCell = "CelebrityMessageTableViewCell"
        let shopTheLookVideoCell = "ShopTheLookVideoTableViewCell"
        let shopTheLookCell = "ShopTheLookTableViewCell"
        let dressUpCell = "DressUpTableViewCell"
        let shopTheLookGalleryCell = "ShopTheLookGalleryTableViewCell"
        let recommendedCell = "RecommendedTableViewCell"
        let swipeCardCell = "EditorialSwipeCardTableViewCell"
        let topStoryCell = "EditorialDetailTopStoryTableViewCell"
        let imageCell = "ImageCollectionTableViewCell"
        let topMessageCell = "TopMessageTableViewCell"
        editorialDetailTableview.register(UINib(nibName: celebrityMessageCell, bundle: nil),
                                          forCellReuseIdentifier: celebrityMessageCell)
        editorialDetailTableview.register(UINib(nibName: shopTheLookVideoCell, bundle: nil),
                                          forCellReuseIdentifier: shopTheLookVideoCell)
        editorialDetailTableview.register(UINib(nibName: shopTheLookCell, bundle: nil),
                                          forCellReuseIdentifier: shopTheLookCell)
        editorialDetailTableview.register(UINib(nibName: dressUpCell, bundle: nil),
                                          forCellReuseIdentifier: dressUpCell)
        editorialDetailTableview.register(UINib(nibName: shopTheLookGalleryCell, bundle: nil),
                                          forCellReuseIdentifier: shopTheLookGalleryCell)
        editorialDetailTableview.register(UINib(nibName: recommendedCell, bundle: nil),
                                          forCellReuseIdentifier: recommendedCell)
        editorialDetailTableview.register(UINib(nibName: swipeCardCell, bundle: nil),
                                          forCellReuseIdentifier: swipeCardCell)
        editorialDetailTableview.register(UINib(nibName: topStoryCell, bundle: nil),
                                          forCellReuseIdentifier: topStoryCell)
        editorialDetailTableview.register(UINib(nibName: imageCell, bundle: nil),
                                          forCellReuseIdentifier: imageCell)
        editorialDetailTableview.register(UINib(nibName: topMessageCell, bundle: nil),
                                          forCellReuseIdentifier: topMessageCell)
    }
    // MARK: - Share PopUp Method
    func showSharePopUp(stringURL: String) {
        let textToShare = [stringURL]
        let activityViewController = UIActivityViewController(
            activityItems: textToShare,
            applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.airDrop,
            UIActivity.ActivityType.postToFacebook
        ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    @objc func shareButtonAction() {
        share()
    }
}
// MARK: - Tableview Methods
extension EditorialDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.editorialDetailContentDisplay.count
    }
    fileprivate func showOtherContents(_ section: EditorialDetailSections,
                                       _ tableView: UITableView) -> UITableViewCell {
        switch section.type {
        case "topStory":
            return getTopStoryTableViewCell(section: section)
        case "text":
            return getTopMessageTableViewCell(section: section)
        case "image":
            return getImageCollectionTableViewCell(section: section)
        case "quote" :
            return getCelebrityMessageTableViewCell(section: section)
        case "imageAndText":
            return getShopTheLookTableViewCell(section: section)
        case "videoAndText":
            return getShopTheLookVideoTableViewCell(section: section)
        case "products":
            return getDressUpTableViewCell(section: section)
        case "recommended":
            return getRecommendedTableViewCell(section: section)
        case "latest":
            return getEditorialSwipeCardTableViewCell(section: section)
        default:
            return UITableViewCell()
        }
    }
    func getTopMessageTableViewCell(section: EditorialDetailSections) -> UITableViewCell {
        guard let cell = editorialDetailTableview.dequeueReusableCell(withIdentifier: "TopMessageTableViewCell")
                as? TopMessageTableViewCell else {
            return UITableViewCell()
        }
        cell.dataValue = section.content ?? []
        cell.applyData()
        cell.selectionStyle = .none
        return cell
    }
    func getTopStoryTableViewCell(section: EditorialDetailSections) -> UITableViewCell {
        guard let cell = editorialDetailTableview.dequeueReusableCell(
                withIdentifier: "EditorialDetailTopStoryTableViewCell")
                as? EditorialDetailTopStoryTableViewCell else {
            return UITableViewCell()
        }
        cell.dataValue = section.editorialDetailInfo
        cell.applyData()
        cell.shareButton.addTarget(self, action: #selector(shareButtonAction), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    func getImageCollectionTableViewCell(section: EditorialDetailSections) -> UITableViewCell {
        guard let cell = editorialDetailTableview.dequeueReusableCell(withIdentifier: "ImageCollectionTableViewCell")
                as? ImageCollectionTableViewCell else {
            return UITableViewCell()
        }
        cell.dataValue = section.content ?? []
        dataValue = section.content ?? []
        cell.splitData()
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    func getCelebrityMessageTableViewCell(section: EditorialDetailSections) -> UITableViewCell {
        guard let cell = editorialDetailTableview.dequeueReusableCell(withIdentifier: "CelebrityMessageTableViewCell")
                as? CelebrityMessageTableViewCell else {
            return UITableViewCell()
        }
        cell.dataValue = section.content ?? []
        cell.applyData()
        cell.selectionStyle = .none
        return cell
    }
    func getShopTheLookTableViewCell(section: EditorialDetailSections) -> UITableViewCell {
        guard let cell = editorialDetailTableview.dequeueReusableCell(withIdentifier: "ShopTheLookTableViewCell")
                as? ShopTheLookTableViewCell else {
            return UITableViewCell()
        }
        cell.dataValue = section.content ?? []
        cell.applyData()
        cell.selectionStyle = .none
        return cell
    }
    func getShopTheLookVideoTableViewCell(section: EditorialDetailSections) -> UITableViewCell {
        guard let cell = editorialDetailTableview.dequeueReusableCell(withIdentifier: "ShopTheLookVideoTableViewCell")
                as? ShopTheLookVideoTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.dataValue = section.content ?? []
        cell.applyData()
        cell.playButton.addTarget(self, action: #selector(playButtonAction), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    func getDressUpTableViewCell(section: EditorialDetailSections) -> UITableViewCell {
        guard let cell = editorialDetailTableview.dequeueReusableCell(withIdentifier: "DressUpTableViewCell")
                as? DressUpTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.dataValue = section.content ?? []
        cell.applyData()
        if presenter.dressUpProductList.count != 0 {
        cell.dressUpProducts = presenter.dressUpProductList
        }
        cell.dressUpCollectionView.reloadData()
        cell.selectionStyle = .none
        return cell
    }
    func getRecommendedTableViewCell(section: EditorialDetailSections) -> UITableViewCell {
        guard let cell = editorialDetailTableview.dequeueReusableCell(withIdentifier: "RecommendedTableViewCell")
                as? RecommendedTableViewCell else {
            return UITableViewCell()
        }
        cell.dataValue = section.content ?? []
        cell.applyData()
        cell.selectionStyle = .none
        return cell
    }
    func getEditorialSwipeCardTableViewCell(section: EditorialDetailSections) -> UITableViewCell {
        guard let cell = editorialDetailTableview.dequeueReusableCell(withIdentifier: "EditorialSwipeCardTableViewCell")
                as? EditorialSwipeCardTableViewCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.cardImages = section.content ?? []
        cell.swipeCardView.reloadData()
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let section = presenter.editorialDetailContentDisplay[indexPath.row]
            return showOtherContents(section, tableView)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let post = presenter.editorialDetailContentDisplay[indexPath.row].content?.first,
           presenter.editorialDetailContentDisplay[indexPath.row].type == "recommended" {
            goToDetailScreen(editorialID: post.storyEditorialId, categoryId: post.storyCategoryId,
                             topLabel: post.storyTitle)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = presenter.editorialDetailContentDisplay[indexPath.row]
        if section.type == "products" && presenter.dressUpProductList.count == 0 {
            return 0
        } else {
            return UITableView.automaticDimension
        }
    }
}

// MARK: - Protocol Methods
extension EditorialDetailViewController: EditorialDetailPresenterProtocol {
    func editorialDetailSuccess(status: Bool, title: String, message: String) {
        if status {
            self.view.hideLoader()
            editorialDetailTableview.reloadData()
        }
    }
    func productListSuccess(status: Bool, title: String, message: String) {
        if !status {
            print("failure")
        }
    }
    func goToDetailScreen(editorialID: String, categoryId: String, topLabel: String) {
        let storyBoard = UIStoryboard.init(name: "EditorialDetail", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "EditorialDetailViewController")
                as? EditorialDetailViewController else { return }
        nextvc.presenter.editorialID = editorialID
        nextvc.presenter.categoryID = categoryId
        nextvc.topLabelValue = topLabel
        self.navigationController?.pushViewController(nextvc, animated: false)
    }
    func gotoImageCollection(selectedIndex: Int) {
        let storyBoard = UIStoryboard.init(name: "EditorialImageCollection", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "EditorialImageCollectionViewController")
                as? EditorialImageCollectionViewController else { return }
        nextvc.selectedIndex = selectedIndex
        nextvc.dataValue = dataValue
        self.navigationController?.pushViewController(nextvc, animated: false)
    }
}
extension EditorialDetailViewController: DressUpProtocol {
    func didSelectDress(skuId: String, productId: String) {
        let storyBoard = UIStoryboard(name: "ProductOrderDetail", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ProductOrderDetailViewController")
                as? ProductOrderDetailViewController else { return }
        nextvc.presenter.productSKUID = skuId
        nextvc.presenter.productID = productId
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    func dressUpContent(skuId: [String]) {
        presenter.requestDressUpContent(skuId: skuId)
    }
}
extension EditorialDetailViewController: shopTheLookVideoProtocol {
    func selectedVideo(videoUrl: String) {
        selectedVideo = videoUrl
    }
}
extension EditorialDetailViewController: KolodaDidSelectProtocol {
    func didSelectWith(editorialId: String, categoryId: String, topLabel: String) {
        goToDetailScreen(editorialID: editorialId, categoryId: categoryId, topLabel: topLabel)
    }
}
extension EditorialDetailViewController: ImageCollectionDidSelectProtocol {
    func didSelectIndex(selectedIndex: Int) {
        gotoImageCollection(selectedIndex: selectedIndex)
    }
}
