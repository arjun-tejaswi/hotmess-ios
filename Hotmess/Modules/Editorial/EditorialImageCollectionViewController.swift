//
//  EditorialImageCollectionViewController.swift
//  Hotmess
//
//  Created by Akshatha on 28/09/21.
//

import UIKit

class EditorialImageCollectionViewController: UIViewController {

    @IBOutlet weak var imagesCollectionView: UICollectionView!
    var dataValue = [ItemListRealm]()
    var imageList = [String]()
    var selectedIndex: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        imageList.removeAll()
        for data in dataValue {
            imageList.append(contentsOf: data.topStoryImageUrl)
        }
        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        configureCollectionView()
    }
    // MARK: - Statusbar Style Method
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - Back Button Action Method
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - ImageCollectionview Cell Configure Method
    func configureCollectionView() {
        let cellSize = CGSize(width: imagesCollectionView.bounds.size.width,
                              height: imagesCollectionView.bounds.size.height)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 50
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        imagesCollectionView.backgroundColor = .clear
        imagesCollectionView.bounces = false
        imagesCollectionView.isPagingEnabled = false
        imagesCollectionView.setCollectionViewLayout(layout, animated: true)
        imagesCollectionView.reloadData()
        imagesCollectionView.layoutIfNeeded()
        if selectedIndex > 0 {
                let indexPath = IndexPath(item: selectedIndex, section: 0)
                imagesCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .top)
        }
    }
}
// MARK: - CollectionView Methods
extension EditorialImageCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageList.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCollectionViewCell",
                                                            for: indexPath) as? ImageCollectionCollectionViewCell else {
            return UICollectionViewCell()
        }
        let url = URL(string: imageList[indexPath.row])
        NukeManager.sharedInstance.setImage(url: url, imageView: cell.imageView, withPlaceholder: true)
        return cell
    }
}
