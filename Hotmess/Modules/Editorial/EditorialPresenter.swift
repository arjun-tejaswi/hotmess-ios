//
//  EditorialPresenter.swift
//  Hotmess
//
//  Created by Akshatha on 16/09/21.
//

import Foundation
import SwiftyJSON
import RealmSwift

protocol EditorialPresenterProtocol: AnyObject {
    func listCategorySuccess(status: Bool, title: String, message: String)
}

enum EditorialLandingStructure {
    case TITLE
    case NORMAL
    case LATESTSTORY
}

struct EditorialSections {
    var content: ItemListRealm?
    var arrayContent: [ItemListRealm]?
    var type: EditorialLandingStructure
    init(arrayContent: [ItemListRealm]?, content: ItemListRealm?, type: EditorialLandingStructure) {
        self.arrayContent = arrayContent
        self.content = content
        self.type = type
    }
}
class EditorialCategoryRealm: Object {
    @objc dynamic var categoryID: String = ""
    @objc dynamic var categoryName: String = ""
    @objc dynamic var isSelected: Bool = false
}
class EditorialDetailRealm: Object {
    @objc dynamic var coverStoryImageUrl: String = ""
    @objc dynamic var coverStoryTitle: String = ""
    @objc dynamic var coverStoryDate: Int = 0
    @objc dynamic var coverStoryDescription: String = ""
    @objc dynamic var coverStorySummary: String = ""
    @objc dynamic var editorialID: String = ""
    @objc dynamic var categoryID: String = ""
    @objc dynamic var latest: Bool = false
}
class EditorialProductListRealm: Object {
    @objc dynamic var productSkuId: String = ""
    @objc dynamic var productDesignerName: String = ""
}
class ItemListRealm: Object {
    @objc dynamic var storyImageUrl: String = ""
    @objc dynamic var storyTitle: String = ""
    @objc dynamic var storySubTitle: String = ""
    @objc dynamic var storyDate: Int = 0
    @objc dynamic var storyCategoryId: String = ""
    @objc dynamic var storyEditorialId: String = ""
    @objc dynamic var itemType: String = ""
    @objc dynamic var storySummary: String = ""
    @objc dynamic var storyDescription: String = ""
    @objc dynamic var videoUrl: String = ""
    @objc dynamic var thumbNailImageUrl: String = ""
    var products = List<EditorialProductListRealm>()
    var topStoryImageUrl = List<String>()
    @objc dynamic var indexPathCount: Int = 0
    @objc dynamic var latest: Bool = false
}
class EditorialPresenter: NSObject {
    var editorialCategoryList = [EditorialCategoryRealm]()
    var contentToShow = [EditorialSections]()
    weak var delegate: EditorialPresenterProtocol?
    func requestEditorialCategory() {
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.editorialCategory,
                                                         prametres: [:],
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: editorialCategoryResponse)
    }
    func editorialCategoryResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                delegate?.listCategorySuccess(status: false, title: "ERROR", message: "")
            } else {
                if let data = response["data"].array {
                    for aCategory in data {
                        let category = EditorialCategoryRealm()
                        category.categoryID = aCategory["category_id"].string ?? ""
                        category.categoryName = aCategory["category_name"].string ?? ""
                        editorialCategoryList.append(category)
                    }
                }
            }
            requestEditorialContent()
        }
    }
    func requestEditorialContent(categoryId: String = "") {
        var parameters: Any = [:]
        if categoryId == "" {
            var arrayOfCategoryIds = [String]()
            for category in editorialCategoryList {
                arrayOfCategoryIds.append(category.categoryID)
            }
            parameters = ["categories_id": arrayOfCategoryIds]
        } else {
            parameters = ["category_id": categoryId]
        }
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.editorialCategoryContent,
                                                         prametres: parameters,
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: editorialItemResponse)
    }
    func appendItemToShow(valueArray: [JSON]) {
        var index: Int = 0
        for aItem in valueArray {
            let itemContent = ItemListRealm()
            itemContent.storyImageUrl = ApiBaseURL.imageBaseURL + "\( aItem["image_url"].string ?? "")"
            itemContent.storyTitle = aItem["category_name"].string ?? ""
            itemContent.storySubTitle = aItem["title"].string ?? ""
            itemContent.storyDate = aItem["posted_date"].int ?? 0
            itemContent.storyCategoryId = aItem["category_id"].string ?? ""
            itemContent.storyEditorialId = aItem["editorial_id"].string ?? ""
            if index == 0 {
                contentToShow.append(EditorialSections.init(arrayContent: [],
                                                            content: itemContent,
                                                            type: .TITLE))
            } else {
                contentToShow.append(EditorialSections.init(arrayContent: [],
                                                            content: itemContent,
                                                            type: .NORMAL))
            }
            index += 1
        }
    }
    func appendItemArrayToShow(valueArray: [JSON]) {
        var itemsArray = [ItemListRealm]()
        for aItem in valueArray {
            let itemContent = ItemListRealm()
            itemContent.storyImageUrl = ApiBaseURL.imageBaseURL + "\( aItem["image_url"].string ?? "")"
            itemContent.storyTitle = aItem["category_name"].string ?? ""
            itemContent.storySubTitle = aItem["title"].string ?? ""
            itemContent.storyDate = aItem["posted_date"].int ?? 0
            itemContent.storyCategoryId = aItem["category_id"].string ?? ""
            itemContent.storyEditorialId = aItem["editorial_id"].string ?? ""
            itemsArray.append(itemContent)
        }
        contentToShow.append(EditorialSections.init(arrayContent: itemsArray,
                                                    content: nil,
                                                    type: .LATESTSTORY))
    }
    fileprivate func sortItemsToDisplay(_ item: JSON) {
        let type = item["type"].string ?? ""
        if type.contains("main") {
            if let valueArray = item["value"].array {
                appendItemToShow(valueArray: valueArray)
            }
        } else {
            if let valueArray = item["value"].array {
                appendItemArrayToShow(valueArray: valueArray)
            }
        }
    }
    fileprivate func createContentToShow(_ response: JSON) {
        if let data = response["data"].array {
            contentToShow.removeAll()
            for item in data {
                sortItemsToDisplay(item)
            }
        }
    }
    func editorialItemResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                delegate?.listCategorySuccess(status: false, title: "ERROR", message: "")
            } else {
                createContentToShow(response)
                delegate?.listCategorySuccess(status: true, title: "", message: "")
            }
        }
    }
}
