//
//  EditorialDetailPresenter.swift
//  Hotmess
//
//  Created by Akshatha on 23/09/21.
//

import Foundation
import SwiftyJSON
import RealmSwift

protocol EditorialDetailPresenterProtocol: AnyObject {
    func productListSuccess(status: Bool, title: String, message: String)
    func editorialDetailSuccess(status: Bool, title: String, message: String)
}
struct EditorialDetailSections {
    var editorialDetailInfo: EditorialDetailRealm?
    var content: [ItemListRealm]?
    var type: String
    init(dictioanryContent: EditorialDetailRealm?, content: [ItemListRealm]?, type: String) {
        self.editorialDetailInfo = dictioanryContent
        self.content = content
        self.type = type
    }
}
class EditorialDetailPresenter: NSObject {
    weak var delegate: EditorialDetailPresenterProtocol?
    var categoryID: String = ""
    var editorialID: String = ""
    var isFromDeepLink: Bool = false
    var productList = [EditorialProductListRealm]()
    var lineItemList = [ItemListRealm]()
    var editorialDetail = EditorialDetailRealm()
    var editorialDetailContentDisplay = [EditorialDetailSections]()
    var dressUpProductList = [ProductListRealm]()
    var recommendedList = [ItemListRealm]()
    var latestList = [ItemListRealm]()
    var imageList = [String]()
    func structureContent() {
        editorialDetailContentDisplay.append(EditorialDetailSections.init(dictioanryContent: editorialDetail,
                                                          content: nil, type: "topStory"))
        for item in lineItemList {
            editorialDetailContentDisplay.append(EditorialDetailSections.init(dictioanryContent: nil,
                                                              content: [item], type: item.itemType))
        }
        if recommendedList.count >= 3 {
            for item in recommendedList.prefix(3) {
                editorialDetailContentDisplay.append(EditorialDetailSections.init(dictioanryContent: nil,
                                                                  content: [item], type: item.itemType))
            }
        } else if recommendedList.count > 0 && recommendedList.count < 3 {
            for item in recommendedList {
                editorialDetailContentDisplay.append(EditorialDetailSections.init(dictioanryContent: nil,
                                                                  content: [item], type: item.itemType))
            }
        }
        var latestContents = [ItemListRealm]()
        for item in latestList {
            latestContents.append(item)
        }
        if latestContents.count > 0 {
            editorialDetailContentDisplay.append(EditorialDetailSections.init(dictioanryContent: nil,
                                                              content: latestContents, type: "latest"))
        }
        delegate?.editorialDetailSuccess(status: true, title: "", message: "")
    }
    func requestEditorialDetail(categoryId: String, editorialId: String) {
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.editorialDetails,
                                                         prametres: [
                                                            "category_id": categoryId,
                                                            "editorial_id": editorialId
                                                         ],
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: editorialDetailResponse)
    }
    func requestDressUpContent(skuId: [String]) {
        let parameters: [String: Any] = [
            "pagination": [
                "per_page": 4,
                "page_number": 1
            ],
            "filters": [
                [
                    "key": "sku_id",
                    "value": skuId
                ]
            ]
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.productListing,
            prametres: parameters,
            type: .withoutAuth,
            method: .POST,
            completion: self.handleProductList)
    }
    func editorialDetailResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                delegate?.editorialDetailSuccess(status: false, title: "ERROR", message: "")
            } else {
                editorialDetailContent(response)
            }
        }
    }
    fileprivate func getRecommendedStorySection(_ data: [String: JSON]) {
        if let valueArray = data["recommended_section"]?.array {
            var indexValue: Int = 0
            for aItem in valueArray {
                let itemContent = ItemListRealm()
                itemContent.storyImageUrl = ApiBaseURL.imageBaseURL + "\(aItem["image_url"].string ?? "")"
                itemContent.storyTitle = aItem["category_name"].string ?? ""
                itemContent.storySubTitle = aItem["title"].string ?? ""
                itemContent.storyDate = aItem["posted_date"].int ?? 0
                itemContent.storyCategoryId = aItem["category_id"].string ?? ""
                itemContent.storyEditorialId = aItem["editorial_id"].string ?? ""
                itemContent.itemType = "recommended"
                itemContent.indexPathCount = indexValue
                indexValue += 1
                recommendedList.append(itemContent)
            }
        }
    }
    fileprivate func getLatestStorySection(_ data: [String: JSON]) {
        if let valueArray = data["latest_stories"]?.array {
            for aItem in valueArray {
                let itemContent = ItemListRealm()
                itemContent.storyImageUrl = ApiBaseURL.imageBaseURL + "\(aItem["image_url"].string ?? "")"
                itemContent.storyTitle = aItem["category_name"].string ?? ""
                itemContent.storySubTitle = aItem["title"].string ?? ""
                itemContent.storyDate = aItem["posted_date"].int ?? 0
                itemContent.storyCategoryId = aItem["category_id"].string ?? ""
                itemContent.storyEditorialId = aItem["editorial_id"].string ?? ""
                itemContent.itemType = "latest"
                latestList.append(itemContent)
            }
        }
    }
    fileprivate func editorialDetailSection(_ data: [String: JSON]) {
        editorialDetail.coverStoryTitle = data["category_name"]?.string ?? ""
        editorialDetail.coverStoryDate = data["posted_date"]?.int ?? 0
        editorialDetail.coverStoryDescription = data["title"]?.string ?? ""
        editorialDetail.coverStorySummary = data["summary"]?.string ?? ""
        editorialDetail.editorialID = data["editorial_id"]?.string ?? ""
        editorialDetail.categoryID = data["category_id"]?.string ?? ""
        editorialDetail.coverStoryImageUrl = ApiBaseURL.imageBaseURL + "\(data["image_url"]?.string ?? "")"
        editorialDetail.latest = data["latest"]?.bool ?? false
    }
    fileprivate func appendEditorialProducts(_ products: [JSON]) {
        for aProduct in products {
            let productDetail = EditorialProductListRealm()
            productDetail.productSkuId = aProduct["sku_id"].string ?? ""
            productDetail.productDesignerName = aProduct["designer_name"].string ?? ""
            productList.append(productDetail)
        }
    }
    fileprivate func appendImageList(_ topStoryImageUrls: [JSON]) {
        for image in topStoryImageUrls {
            imageList.append(ApiBaseURL.imageBaseURL + "\( image.string ?? "")")
        }
    }
    fileprivate func appendLineItems(_ lineItems: [JSON]) {
        for lineItem in lineItems {
            let lineItemContent = ItemListRealm()
            lineItemContent.itemType = lineItem["type"].string ?? ""
            if let lineItemData = lineItem["data"].dictionary {
                lineItemContent.storyTitle = lineItemData["title"]?.string ?? ""
                lineItemContent.storyImageUrl =
                    ApiBaseURL.imageBaseURL + "\(lineItemData["image_url"]?.string ?? "")"
                lineItemContent.storySummary = lineItemData["summary"]?.string ?? ""
                lineItemContent.storyDescription = lineItemData["description"]?.string ?? ""
                lineItemContent.videoUrl = lineItemData["vedio_url"]?.string ?? ""
                lineItemContent.thumbNailImageUrl =
                    ApiBaseURL.imageBaseURL + "\(lineItemData["thumbnail_image_url"]?.string ?? "")"
                lineItemContent.storySubTitle = lineItemData["subtitle"]?.string ?? ""
                if let products = lineItemData["products"]?.array {
                    appendEditorialProducts(products)
                    lineItemContent.products.append(objectsIn: productList)
                }
                if let topStoryImageUrls =  lineItemData["image_url"]?.array {
                    appendImageList(topStoryImageUrls)
                    lineItemContent.topStoryImageUrl.append(objectsIn: imageList)
                }
            }
            lineItemList.append(lineItemContent)
        }
    }
    fileprivate func editorialDetailContent(_ response: JSON) {
        if let data = response["data"].dictionary {
            lineItemList.removeAll()
            editorialDetailContentDisplay.removeAll()
            imageList.removeAll()
            editorialDetailSection(data)
            if let lineItems = data["line_items"]?.array {
                appendLineItems(lineItems)
            }
            getRecommendedStorySection(data)
            getLatestStorySection(data)
        }
        structureContent()
    }
    func handleProductList(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                delegate?.productListSuccess(
                    status: false,
                    title: "ERROR",
                    message: response["message"].string ?? "")
            } else {
                dressUpProductList.removeAll()
                if let data = response["data"].array {
                    for aProduct in data {
                        if let aProductDictionary = aProduct.dictionary {
                            let allproducts = ProductParser.sharedInstance.parseProductData(data: aProductDictionary)
                            dressUpProductList.append(allproducts)
                        }
                    }
                } else {
                    delegate?.productListSuccess(status: false, title: "ERROR", message: "Server Error")
                }
            }
        }
    }
}
