//
//  EditorialViewController.swift
//  Hotmess
//
//  Created by Flaxon on 28/05/21.
//

import UIKit

class EditorialViewController: UIViewController {
    @IBOutlet weak var editorialTableviewHeight: NSLayoutConstraint!
    @IBOutlet weak var editorialCategoryCollectionview: UICollectionView!
    @IBOutlet weak var editorialTableview: UITableView!
    @IBOutlet weak var editorialTopLabel: UILabel!
    var presenter = EditorialPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        editorialCategoryCollectionview.delegate = self
        editorialCategoryCollectionview.dataSource = self
        setUpTableView()
        applyFont()
        applyStyling()
        presenter.delegate = self
        self.view.showLoader()
        presenter.requestEditorialCategory()
    }
    override func viewWillLayoutSubviews() {
        self.editorialTableviewHeight.constant = self.editorialTableview.intrinsicContentSize.height
    }
    // MARK: - Font Style Method
    func applyFont() {
        editorialTopLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        editorialTopLabel.applyLetterSpacing(spacing: 0.9)
        editorialTopLabel.textColor = ColourConstants.hexFFFFFF
    }
    // MARK: - TableView SetUp Method
    func setUpTableView() {
        editorialTableview.delegate = self
        editorialTableview.dataSource = self
        editorialTableview.register(UINib(nibName: "DressUpTableViewCell", bundle: nil),
                                    forCellReuseIdentifier: "DressUpTableViewCell")
        editorialTableview.register(UINib(nibName: "EditorialCoverStoryTableViewCell", bundle: nil),
                                    forCellReuseIdentifier: "EditorialCoverStoryTableViewCell")
        editorialTableview.register(UINib(nibName: "FashionMemoTableViewCell", bundle: nil),
                                    forCellReuseIdentifier: "FashionMemoTableViewCell")
        editorialTableview.register(UINib(nibName: "DressUpTableViewCell", bundle: nil),
                                    forCellReuseIdentifier: "DressUpTableViewCell")
        editorialTableview.register(UINib(nibName: "EditorialSwipeCardTableViewCell", bundle: nil),
                                    forCellReuseIdentifier: "EditorialSwipeCardTableViewCell")
    }
}
// MARK: - CollectionView Methods
extension EditorialViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.editorialCategoryList.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "EditorialCategoryCollectionViewCell",
                for: indexPath) as? EditorialCategoryCollectionViewCell else {
            return UICollectionViewCell()
        }
        if presenter.editorialCategoryList[indexPath.row].isSelected {
            cell.categoryUnderView.backgroundColor = ColourConstants.hex000000
            cell.categoryUnderView.isHidden = false
        } else {
            cell.categoryUnderView.isHidden = true
        }
        cell.categoryTitleLabel.text =   "    \(presenter.editorialCategoryList[indexPath.item].categoryName)    "
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var index: Int = 0
        for category in presenter.editorialCategoryList {
            if index == indexPath.item {
                category.isSelected = true
                self.view.showLoader()
                presenter.requestEditorialContent(
                    categoryId: presenter.editorialCategoryList[indexPath.item].categoryID)
            } else {
                category.isSelected = false
            }
            index += 1
        }
    editorialCategoryCollectionview.reloadData()
    }
}
// MARK: - Tableview Methods
extension EditorialViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.contentToShow.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = presenter.contentToShow[indexPath.row]
        switch section.type {
        case .TITLE:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EditorialCoverStoryTableViewCell")
                    as? EditorialCoverStoryTableViewCell else {
                return UITableViewCell()
            }
            cell.dataValue = section.content
            cell.applyData()
            cell.selectionStyle = .none
            return cell
        case .NORMAL:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FashionMemoTableViewCell")
                    as? FashionMemoTableViewCell else {
                return UITableViewCell()
            }
            cell.dataValue = section.content
            cell.applyData()
            cell.selectionStyle = .none
            return cell
        case .LATESTSTORY:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EditorialSwipeCardTableViewCell")
                    as? EditorialSwipeCardTableViewCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            cell.cardImages = section.arrayContent ?? []
            cell.swipeCardView.reloadData()
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if presenter.contentToShow[indexPath.row].type != .LATESTSTORY {
            if let post = presenter.contentToShow[indexPath.row].content {
                goToDetailScreen(editorialID: post.storyEditorialId,
                                 categoryId: post.storyCategoryId, topLabel: post.storyTitle)
            }
        }
    }
    func goToDetailScreen(editorialID: String, categoryId: String, topLabel: String) {
        let storyBoard = UIStoryboard.init(name: "EditorialDetail", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "EditorialDetailViewController")
                as? EditorialDetailViewController else { return }
        nextvc.presenter.editorialID = editorialID
        nextvc.presenter.categoryID = categoryId
        nextvc.topLabelValue = topLabel
        self.navigationController?.pushViewController(nextvc, animated: false)
    }
}
// MARK: - Protocol Methods
extension EditorialViewController: EditorialPresenterProtocol {
    func listCategorySuccess(status: Bool, title: String, message: String) {
        if status {
            self.view.hideLoader()
            editorialCategoryCollectionview.reloadData()
            editorialTableview.reloadData()
        }
    }
}
extension EditorialViewController: KolodaDidSelectProtocol {
    func didSelectWith(editorialId: String, categoryId: String, topLabel: String) {
        goToDetailScreen(editorialID: editorialId, categoryId: categoryId, topLabel: topLabel)
    }
}
