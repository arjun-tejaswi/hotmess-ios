//
//  ProductOrderDetailViewController.swift
//  Hotmess
//
//  Created by Flaxon on 13/05/21.
//

import UIKit
import SideMenu
import Branch

class ProductOrderDetailViewController: UIViewController {
    @IBOutlet weak var productColourView: UIView!
    @IBOutlet weak var selectColour03Button: UIButton!
    @IBOutlet weak var selectColour02Button: UIButton!
    @IBOutlet weak var selectColour01Button: UIButton!
    @IBOutlet weak var colourSelectionHeightConstarint: NSLayoutConstraint!
    @IBOutlet weak var wishlistButton: UIButton!
    @IBOutlet weak var productDetailSeparationView: UIView!
    @IBOutlet weak var selectSizeArrowImage: UIImageView!
    @IBOutlet weak var sizeGuideButton: UIButton!
    @IBOutlet weak var selectSizeButton: UIButton!
    @IBOutlet weak var productScrollView: UIScrollView!
    @IBOutlet weak var mightLikeViewHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var styleWithViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var colourButton: UIButton!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var addToBagButton: UIButton!
    @IBOutlet weak var showHereWithSubLabel: UILabel!
    @IBOutlet weak var showHereWithLabel: UILabel!
    @IBOutlet weak var mightLikeLabel: UILabel!
    @IBOutlet weak var styleWithLabel: UILabel!
    @IBOutlet weak var detailAndCareLabel: UILabel!
    @IBOutlet weak var sizeAndFitLabel: UILabel!
    @IBOutlet weak var editorsNotesLabel: UILabel!
    @IBOutlet weak var sizeGuideLabel: UILabel!
    @IBOutlet weak var selectSizeLabel: UILabel!
    @IBOutlet weak var mrpTaxLabel: UILabel!
    @IBOutlet weak var productCostLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var productDecriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cartCountLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var detailAndCareTextView: UITextView!
    @IBOutlet weak var sizeAndFitTextView: UITextView!
    @IBOutlet weak var editorNotesTextView: UITextView!
    @IBOutlet weak var mightLikeCollectionView: UICollectionView!
    @IBOutlet weak var styleWithCollectionView: UICollectionView!
    @IBOutlet weak var colorSelectionView: UIView!
    @IBOutlet weak var addToBagView: UIView!
    @IBOutlet weak var selectSizeView: UIView!
    @IBOutlet weak var pageControlContainer: UIView!
    @IBOutlet weak var productDetailPageController: UIPageControl!
    @IBOutlet weak var productDetailCollectionView: UICollectionView!
    @IBOutlet weak var colorsCollectionView: UICollectionView!
    @IBOutlet weak var productUnavailableView: UIView!
    @IBOutlet weak var productUnavailableLabel: UILabel!
    @IBOutlet weak var colour01: UIView!
    @IBOutlet weak var colour01ImageView: UIImageView!
    @IBOutlet weak var colour02: UIView!
    @IBOutlet weak var colour02ImageView: UIImageView!
    @IBOutlet weak var colour03: UIView!
    @IBOutlet weak var colour03ImageView: UIImageView!
    @IBOutlet weak var colourMoreThan3: UIView!
    @IBOutlet weak var colour04ImageView: UIImageView!
    @IBOutlet weak var colourCountLabel: UILabel!
    @IBOutlet weak var selectedColorBorderView: UIView!
    var createColourPopUp: ColourPopUp!
    var selectSizePopUp: SizePopUp!
    var sizeGuidePopUp: SizeGuidePopUp!
    var addToBagPopup: AddToBagPopUp!
    var notifyProductInstockPopUp: ProductInStockPopUp!
    var presenter = ProductOrderDetailPresenter()
    var productInstockSuccessPopUp: InstockNotifySuccessPopup!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        applyStyling()
        applyFont()
        setUpProductDeatilCollectionView()
        setUpStyleWithCollectionView()
        setUpMightLikeCollectionView()
        self.view.showLoader()
        presenter.requestProductInfo(skuID: presenter.productSKUID, productID: presenter.productID)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(requestWishlistStatus),
                                               name: Notification.Name("WISHLIST_STATUS"),
                                               object: nil)
    }
    deinit {
        print("NO MEMORY LEAK: OS Reclaiming memory for 'ProductOrderDetailViewController'")
        NotificationCenter.default.removeObserver(self, name: Notification.Name("WISHLIST_STATUS"), object: nil)
    }
    @IBAction func selectColour02ButtonAction(_ sender: UIButton) {
        self.view.showLoader()
        presenter.requestProductInfo(
            skuID: presenter.productInfo.variants[1].stockKeepUnitID,
                                     productID: presenter.productInfo.variants[1].productID)
    }
    @IBAction func selectColour01ButtonAction(_ sender: UIButton) {
        self.view.showLoader()
        presenter.requestProductInfo(
            skuID: presenter.productInfo.variants[0].stockKeepUnitID,
                                     productID: presenter.productInfo.variants[0].productID)
    }
    // MARK: - Profile Button Action Method
    @IBAction func selectColour03ButtonAction(_ sender: UIButton) {
        self.view.showLoader()
        presenter.requestProductInfo(
            skuID: presenter.productInfo.variants[2].stockKeepUnitID,
                                     productID: presenter.productInfo.variants[2].productID)
    }
    @IBAction func profileButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "SlideMenu", bundle: nil)
        guard let slideVC = storyBoard.instantiateViewController(
                withIdentifier: "SlideMenuViewController") as? SlideMenuViewController
        else {
            return
        }
        let sideMenu = SideMenuNavigationController(rootViewController: slideVC)
        sideMenu.presentationStyle = .menuSlideIn
        sideMenu.isNavigationBarHidden = true
        sideMenu.blurEffectStyle = .dark
        sideMenu.menuWidth = self.view.frame.width - 64
        blurView.isHidden = false
        self.present(sideMenu, animated: true, completion: nil)
    }
    // MARK: - Shopping Bag Button Action Method
    @IBAction func shopBagButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "ShoppingBag", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ShoppingBagViewController")
                as? ShoppingBagViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    // MARK: - Back Button Action Method
    @IBAction func backButtonAction(_ sender: UIButton) {
        if presenter.isFromDeepLink {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                fatalError("Not of type 'AppDelegate'")
            }
            appDelegate.restartApp()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    // MARK: - Search Button Action Method
    @IBAction func searchButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "Search", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "SearchViewController")
                as? SearchViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    // MARK: - Size Guide Button Action Method
    @IBAction func sizeGuideButtonAction(_ sender: UIButton) {
        sizeGuidePopUp = Utilities.sharedInstance.showSizeGuidePopUp(bounds: UIScreen.main.bounds)
        sizeGuidePopUp.sizeGuide = presenter.getSizeGuide()
        sizeGuidePopUp.applyData(sizeGuideData: presenter.productInfo)
        self.view.addSubview(sizeGuidePopUp)
    }
    // MARK: - Add To Wishlist Button Action Method
    @IBAction func addToWishListButtonAction(_ sender: UIButton) {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") == false {
            presenter.saveProductToWishlist()
            let storyboardLogin = UIStoryboard(name: "Login", bundle: nil)
            guard let loginView = storyboardLogin.instantiateViewController(
                    withIdentifier: "LoginViewController") as? LoginViewController else {
                return
            }
            self.navigationController?.view.setNavigationTransitionLayer()
            self.navigationController?.pushViewController(loginView, animated: true)
        } else {
            self.view.showLoader()
            if presenter.addedWishlist {
                presenter.requestRemoveFromWishlist(skuID: presenter.productInfo.stockKeepUnitID)
            } else {
                presenter.requestToAddWishlist(
                    skuID: presenter.productInfo.stockKeepUnitID,
                    productID: presenter.productInfo.productID)
            }
        }
    }
    // MARK: - Size Selection Button Action Method
    @IBAction func selectSizeButtonAction(_ sender: UIButton) {
        var sizeArray = [SizeModel]()
        for size in presenter.productInfo.sizes {
            sizeArray.append(SizeModel.init(
                                sizeValue: size.size,
                                skuID: size.stockKeepUnitID,
                                productID: size.productID))
        }
        selectSizePopUp = Utilities.sharedInstance.showSizePopUp(bounds: UIScreen.main.bounds)
        selectSizePopUp.delegate = self
        selectSizePopUp.size = sizeArray
        selectSizePopUp.sizeGuideMatrics = presenter.getSizeGuide()
        selectSizePopUp.applyData(sizeGuideData: presenter.productInfo)
        self.view.addSubview(selectSizePopUp)
    }
    // MARK: - Colour Selection Button Action Method
    @IBAction func moreColourButtonAction(_ sender: UIButton) {
        productUnavailableView.isHidden = true
        colorSelectionView.isHidden = false
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseIn], animations: {
                        self.colourSelectionHeightConstarint.constant = 80
                        self.view.layoutIfNeeded()
                       })
        //        var colorsArray = [ColourModel]()
        //        for variant in presenter.productInfo.variants {
        //            colorsArray.append(ColourModel.init(
        //                                colourName: variant.colour,
        //                                colourHexCode: variant.colourCode,
        //                                skuID: variant.stockKeepUnitID,
        //                                productID: variant.productID))
        //        }
        //        createColourPopUp = Utilities.sharedInstance.showColourPopUp(bounds: UIScreen.main.bounds)
        //        createColourPopUp.delegate = self
        //        createColourPopUp.colours = colorsArray
        //        self.view.addSubview(createColourPopUp)
    }
    // MARK: - Add To Bag Button Action Method
    @IBAction func addToBagButtonAction(_ sender: UIButton) {
        if presenter.productInfo.productInStock {
            self.view.showLoader()
            if UserDefaults.standard.bool(forKey: "isLoggedIn") == true {
                presenter.requestAddToBagProduct(
                    skuID: presenter.productInfo.stockKeepUnitID,
                    productID: presenter.productInfo.productID)
            } else {
                presenter.saveProductToBag()
            }
            Vibration.pulse.vibrate()
        } else {
            notifyProductInstockPopUp = Utilities.sharedInstance.showNotifyProductInstockPopUp(
                bounds: UIScreen.main.bounds)
            notifyProductInstockPopUp.delegate = self
            notifyProductInstockPopUp.applyData(inStockData: presenter.productInfo)
            self.view.addSubview(notifyProductInstockPopUp)
        }
    }
    @IBAction func shareButtonAction(_ sender: UIButton) {
        share()
    }
}
extension ProductOrderDetailViewController {
    @objc func requestWishlistStatus() {
        presenter.requestWishlistStatus(skuID: presenter.productInfo.stockKeepUnitID)
    }
    // MARK: - Statusbar Style Method
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.showLoader()
        presenter.requestShoppingBag()
        if UserDefaults.standard.bool(forKey: "isLoggedIn") == false {
            RealmDataManager.sharedInstance.deleteAllFromWishlist()
            addedWishlist(status: false, title: "", message: "")
        } else {
            presenter.requestWishlistStatus(skuID: presenter.productInfo.stockKeepUnitID)
        }
    }
    func setUpProductDeatilCollectionView() {
        configureCollectionView()
        productDetailCollectionView.delegate = self
        productDetailCollectionView.dataSource = self
        productDetailCollectionView.register(UINib(nibName: "ProductListCustomCell", bundle: nil),
                                         forCellWithReuseIdentifier: "ProductListCustomCell")
        productDetailCollectionView.register(UINib(nibName: "ProductDetailVideoCustomCell", bundle: nil),
                                         forCellWithReuseIdentifier: "ProductDetailVideoCustomCell")
    }
    // MARK: - StyleWith CollectionView SetUp Method
    func setUpStyleWithCollectionView() {
        configureStyleWithCollectionView()
        styleWithCollectionView.delegate = self
        styleWithCollectionView.dataSource = self
        styleWithCollectionView.register(UINib(nibName: "ProductListCustomCell", bundle: nil),
                                         forCellWithReuseIdentifier: "ProductListCustomCell")
    }
    // MARK: - MightLike CollectionView SetUp Method
    func setUpMightLikeCollectionView() {
        configureMightLikeCollectionView()
        mightLikeCollectionView.delegate = self
        mightLikeCollectionView.dataSource = self
        mightLikeCollectionView.register(UINib(nibName: "ProductListCustomCell", bundle: nil),
                                         forCellWithReuseIdentifier: "ProductListCustomCell")
    }
    // MARK: - StyleWith CollectionView Cell SetUp
    func configureStyleWithCollectionView() {
        let cellSize = CGSize(width: 125,
                              height: styleWithCollectionView.frame.height)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 13)
        layout.minimumLineSpacing = 4
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        styleWithCollectionView.backgroundColor = .clear
        styleWithCollectionView.bounces = false
        styleWithCollectionView.isPagingEnabled = false
        styleWithCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    // MARK: - MightLike CollectionView Cell SetUp
    func configureMightLikeCollectionView() {
        let cellSize = CGSize(width: 125,
                              height: mightLikeCollectionView.frame.height)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 13)
        layout.minimumLineSpacing = 4
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        mightLikeCollectionView.backgroundColor = .clear
        mightLikeCollectionView.bounces = false
        mightLikeCollectionView.isPagingEnabled = false
        mightLikeCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    // MARK: - Product CollectionView Cell SetUp
    func configureCollectionView() {
        let cellSize = CGSize(width: productDetailCollectionView.frame.width,
                              height: productDetailCollectionView.frame.height)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        productDetailCollectionView.backgroundColor = .clear
        productDetailCollectionView.isPagingEnabled = true
        productDetailCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    // MARK: - Page Controller SetUp
    func configurePageController() {
        pageControlContainer.translatesAutoresizingMaskIntoConstraints = false
        productDetailPageController.translatesAutoresizingMaskIntoConstraints = false
        productDetailPageController.isUserInteractionEnabled = false
        let safeAreaGuide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            // center page control in its "holder" view
            productDetailPageController.centerXAnchor.constraint(equalTo: pageControlContainer.centerXAnchor),
            productDetailPageController.centerYAnchor.constraint(equalTo: pageControlContainer.centerYAnchor),
            // constrain holder view leading to view + 20, centerY
            pageControlContainer.leadingAnchor.constraint(
                equalTo: safeAreaGuide.leadingAnchor, constant: 20.0),
            pageControlContainer.centerYAnchor.constraint(
                equalTo: productDetailCollectionView.centerYAnchor, constant: 0.0),
            // constrain holder view WIDTH to page control HEIGHT
            pageControlContainer.widthAnchor.constraint(
                equalTo: productDetailPageController.heightAnchor),
            // constrain holder view HEIGHT to page control WIDTH
            pageControlContainer.heightAnchor.constraint(
                equalTo: productDetailPageController.widthAnchor)
        ])
        let numberOfPages = presenter.productInfo.assets.count
        productDetailPageController.numberOfPages = numberOfPages
        if numberOfPages == 1 {
            productDetailPageController.isHidden = true
        }
    }
    // MARK: - Share PopUp Method
    func showSharePopUp(stringURL: String) {
        let textToShare = [stringURL]
        let activityViewController = UIActivityViewController(
            activityItems: textToShare,
            applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.airDrop,
            UIActivity.ActivityType.postToFacebook
        ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    @objc func viewBagCheckOutAction(sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "ShoppingBag", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ShoppingBagViewController")
                as? ShoppingBagViewController else { return }
        addToBagPopup.dismiss()
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
