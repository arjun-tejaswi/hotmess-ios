//
//  ProductOrderDetailPresenterExtention.swift
//  Hotmess
//
//  Created by Flaxon on 20/07/21.
//

import Foundation
import SwiftyJSON

extension ProductOrderDetailPresenter {
    func getSizeGuide() -> [ProductSizeRealm] {
        var allSizes = [ProductSizeRealm]()
        for size in productInfo.sizes {
            allSizes.append(size)
        }
        return allSizes
    }
    func getStyleWithList() -> [ProductListRealm] {
        var allStyleWithList = [ProductListRealm]()
        for stylewithList in productStyleWithList {
            allStyleWithList.append(stylewithList)
        }
        return allStyleWithList
    }
    func handleRecommendedProducts(response: JSON) {
        print("Recommended" + "\(response)")
        if let status = response["error_status"].bool {
            if status {
                delegate?.productRecommendedSuccess(
                    status: false,
                    title: "Error",
                    message: response["message"].string ?? "")
            } else {
                if let pagination = response["pagination"].dictionary {
                    totalRecommendedProducts = pagination["total_records"]?.int64 ?? 0
                }
                if let data = response["data"].array {
                    for aProduct in data {
                        if let aProductDictionary = aProduct.dictionary {
                            let allproducts = ProductParser.sharedInstance.parseProductData(data: aProductDictionary)
                            productRecommendedList.append(allproducts)
                        }
                    }
                    if data.count <= 0 {
                        didRecommendedPageEnd = true
                    }
                    delegate?.productRecommendedSuccess(status: true, title: "", message: "")
                } else {
                    delegate?.productRecommendedSuccess(status: false, title: "Error", message: "Server Error")
                }
            }
        }
    }
    func handleStlyeWithProductList(response: JSON) {
        print("Stylewith" + "\(response)")
        if let status = response["error_status"].bool {
            if status {
                delegate?.productListStyleWithSuccess(
                    status: false,
                    title: "Error",
                    message: response["message"].string ?? "")
            } else {
                if let pagination = response["pagination"].dictionary {
                    totalStyleWithProducts = pagination["total_records"]?.int64 ?? 0
                }
                if let data = response["data"].array {
                    for aProduct in data {
                        if let aProductDictionary = aProduct.dictionary {
                            let allproducts = ProductParser.sharedInstance.parseProductData(data: aProductDictionary)
                            productStyleWithList.append(allproducts)
                        }
                    }
                    if data.count <= 0 {
                        didStyleWithPageEnd = true
                    }
                    delegate?.productListStyleWithSuccess(status: true, title: "", message: "")
                } else {
                    delegate?.productListStyleWithSuccess(status: false, title: "Error", message: "Server Error")
                }
            }
        }
    }
    func handleProductInfoResponse(response: JSON) {
        print("ProductInfo" + "\(response)")
        if let status = response["error_status"].bool {
            if status {
                delegate?.productInfoSuccess(status: false, title: "Error", message: response["message"].string ?? "")
            } else {
                if let data = response["data"].dictionary {
                    parseProductInfo(data: data)
                    cartTotalItems = RealmDataManager.sharedInstance.getAllProductsFromBag().count
                    delegate?.productInfoSuccess(status: true, title: "", message: "")
                } else {
                    delegate?.productInfoSuccess(status: false, title: "Error", message: "Server Error")
                }
            }
        }
    }
    fileprivate func parseProductInfo(data: [String: JSON]) {
        productInfo.productID = data["product_id"]?.string ?? ""
        productInfo.productName = data["name"]?.string ?? ""
        productInfo.productImageURL = ApiBaseURL.imageBaseURL +
            "\( data["featured_image_url"]?.string ?? "")"
        productInfo.editorNote = data["editor_note"]?.string ?? ""
        productInfo.productDescription = data["description"]?.string ?? ""
        productInfo.productStatus = data["product_status"]?.string ?? ""
        productInfo.productSizeFit = data["size_fit"]?.string ?? ""
        productInfo.productPrice = data["cost_price"]?.int64 ?? 0
        productInfo.productDetailsCare = data["details_care"]?.string ?? ""
        productInfo.productDesignerName = data["designer_name"]?.string ?? ""
        productInfo.productOfferPrice = data["offer_price"]?.int64 ?? 0
        productInfo.stockKeepUnitID = data["sku_id"]?.string ?? ""
        productInfo.productColour = data["colour"]?.string ?? ""
        productInfo.productSize = data["selcted_size"]?.string ?? ""
        productInfo.productSizeGuide = data["size_guide"]?.bool ?? false
        productInfo.productCategoryID = data["category_id"]?.string ?? ""
        productInfo.productColourCode = data["colour_code"]?.string ?? ""
        productInfo.productInStock = data["in_stock"]?.bool ?? false
        productInfo.categoryName = data["category_name"]?.string ?? ""
        if let assets = data["assets"]?.array {
            productInfo.assets.removeAll()
            let allAssets = ProductParser.sharedInstance.getAssetsRealmObject(assets: assets)
            productInfo.assets.append(objectsIn: allAssets)
        }
        if let sizes = data["size"]?.array {
            productInfo.sizes.removeAll()
            let allSizes = ProductParser.sharedInstance.getSizesRealmObject(sizes: sizes)
            productInfo.sizes.append(objectsIn: allSizes)
        }
        if let variants = data["variants"]?.array {
            productInfo.variants.removeAll()
            let allVariants = ProductParser.sharedInstance.getVariantsRealmObject(variants: variants)
            if allVariants.count > 0 {
                let selectedVariant = ProductVariantsRealm()
                selectedVariant.colour = data["colour"]?.string ?? ""
                selectedVariant.stockKeepUnitID = data["sku_id"]?.string ?? ""
                selectedVariant.colourCode = data["colour_code"]?.string ?? ""
                productInfo.variants.append(selectedVariant)
            }
            productInfo.variants.append(objectsIn: allVariants)
        }
    }
    func requestShoppingBag() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.shoppingBag,
                                                             prametres: [:],
                                                             type: .withAuth,
                                                             method: .POST,
                                                             completion: handleShoppingBagResponse)
        } else {
            shoppingBagItemArray.removeAll()
            cartTotalItems = RealmDataManager.sharedInstance.getAllProductsFromBag().count
            delegate?.dataFound(status: true, message: "")
        }
    }
    func handleShoppingBagResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                let message = response["message"].string ?? ""
                if message == "requestForRefresh" {
                    NetworkManager.sharedInstance.refreshBearerToken {
                        self.requestShoppingBag()
                    }
                }
            } else {
                if let items = response["data"].array {
                    cartTotalItems = 0
                    for _ in items {
                        cartTotalItems += 1
                    }
                    delegate?.dataFound(status: true, message: "")
                }
            }
        }
    }
}
