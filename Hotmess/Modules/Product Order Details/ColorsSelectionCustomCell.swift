//
//  ColorsSelectionCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 11/08/21.
//

import UIKit

class ColorsSelectionCustomCell: UICollectionViewCell {
    @IBOutlet weak var colorImageView: UIImageView!
    @IBOutlet weak var colorBorderView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    func showSelectedColor() {
        colorBorderView.layer.cornerRadius = colorBorderView.frame.width/2
        colorBorderView.layer.borderWidth = 1
        colorBorderView.layer.borderColor = UIColor.black.cgColor
    }
    func showUnSelectedColor() {
        colorBorderView.layer.borderColor = UIColor.clear.cgColor
    }
    func applyStyling() {
        colorImageView.layer.cornerRadius = colorImageView.frame.width/2
        nameLabel.textColor = ColourConstants.hex191919
    }
    func applyFont() {
        nameLabel.font = UIFont(name: FontConstants.regular, size: 12)
    }
}
