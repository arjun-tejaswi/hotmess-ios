//
//  ProductOrderDetailViewControllerProtocolExtension.swift
//  Hotmess
//
//  Created by Flaxon on 28/09/21.
//

import Foundation
import UIKit
import  SideMenu

extension ProductOrderDetailViewController: ProductInStockPopUpDelegate {
    func notifyMeData(email: String) {
        if notifyProductInstockPopUp != nil {
            presenter.productInstockNotify(email: email)
            self.view.showLoader()
            notifyProductInstockPopUp.dismiss()
        }
    }
}
extension ProductOrderDetailViewController: ColorPopUpDelegate {
    func selectedColor(skuID: String, productID: String) {
        if createColourPopUp != nil {
            presenter.requestProductInfo(skuID: skuID, productID: productID)
            self.view.showLoader()
            createColourPopUp.dismiss()
        }
    }
}
extension ProductOrderDetailViewController: SizePopUpDelegate {
    func selectedSize(skuID: String, productID: String) {
        if selectSizePopUp != nil {
            presenter.requestProductInfo(skuID: skuID, productID: productID)
            self.view.showLoader()
            selectSizePopUp.dismiss()
        }
    }
}
extension ProductOrderDetailViewController: AddToBagPopUpDelegate {
    func selectedStyleWithProduct(skuID: String, productID: String) {
        presenter.requestProductInfo(skuID: skuID, productID: productID)
        self.view.showLoader()
        addToBagPopup.dismiss()
    }
}
// MARK: - Hide Blur View for Menu Method
extension ProductOrderDetailViewController: SideMenuNavigationControllerDelegate {
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        blurView.isHidden = true
    }
}
extension ProductOrderDetailViewController {
    // MARK: Initailze UI After API Call
    func initializeUI() {
        configurePageController()
        productDetailPageController.currentPage = 0
        colorSelectionView.isHidden = true
        colourSelectionHeightConstarint.constant = 0
        cartCountLabel.text = String(presenter.cartTotalItems)
        titleLabel.text = presenter.productInfo.productDesignerName
        productDecriptionLabel.text = presenter.productInfo.productDescription
        productCostLabel.text = "\(presenter.productInfo.productPrice)"
        if presenter.productInfo.sizes.count == 1 {
            selectSizeLabel.text = presenter.productInfo.productSize
            selectSizeArrowImage.isHidden = true
            selectSizeButton.isEnabled = false
        } else {
            selectSizeLabel.text = presenter.productInfo.productSize
            selectSizeArrowImage.isHidden = false
            selectSizeButton.isEnabled = true
        }
        colourSelectionViewSetUp()
        if presenter.productInfo.productSizeGuide {
            sizeGuideButton.backgroundColor = .clear
            sizeGuideButton.isEnabled = true
        } else {
            sizeGuideButton.backgroundColor = ColourConstants.hexFFFFFF
            sizeGuideButton.isEnabled = false
        }
        if presenter.productInfo.productInStock {
            productUnavailableView.isHidden = true
            productColourView.isHidden = false
            selectSizeView.layer.borderColor = ColourConstants.hex717171.cgColor
            addToBagButton.setTitle("ADD TO BAG", for: .normal)
        } else {
            productUnavailableView.isHidden = false
            productColourView.isHidden = true
            selectSizeView.layer.borderColor = ColourConstants.hexEF5353.cgColor
            addToBagButton.setTitle("NOTIFY ME", for: .normal)
        }
        addToBagButton.applyButtonLetterSpacing(spacing: 2.52)
        editorNotesTextView.text = presenter.productInfo.editorNote
        sizeAndFitTextView.text = presenter.productInfo.productSizeFit
        detailAndCareTextView.text = presenter.productInfo.productDetailsCare
    }
    func setNoColor() {
        colour01.isHidden = true
        colour02.isHidden = true
        colour03.isHidden = true
        colourMoreThan3.isHidden = true
    }
    func colourSelectionViewSetUp() {
        if presenter.productInfo.variants.count == 0 {
            setNoColor()
        } else if presenter.productInfo.variants.count == 1 {
            colour01.isHidden = false
            colour01ImageView.backgroundColor = Utilities.sharedInstance.hexStringToUIColor(
                hex: presenter.productInfo.variants[0].colourCode)
            selectedColorBorderView.layer.borderColor = UIColor.clear.cgColor
            colour02.isHidden = true
            colour03.isHidden = true
            colourMoreThan3.isHidden = true
        } else if presenter.productInfo.variants.count == 2 {
            colour01.isHidden = false
            colour01ImageView.backgroundColor = Utilities.sharedInstance.hexStringToUIColor(
                hex: presenter.productInfo.variants[0].colourCode)
            colour02.isHidden = false
            colour02ImageView.backgroundColor = Utilities.sharedInstance.hexStringToUIColor(
                hex: presenter.productInfo.variants[1].colourCode)
            colour03.isHidden = true
            colourMoreThan3.isHidden = true
        } else if presenter.productInfo.variants.count == 3 {
            colour01.isHidden = false
            colour01ImageView.backgroundColor = Utilities.sharedInstance.hexStringToUIColor(
                hex: presenter.productInfo.variants[0].colourCode)
            colour02.isHidden = false
            colour02ImageView.backgroundColor = Utilities.sharedInstance.hexStringToUIColor(
                hex: presenter.productInfo.variants[1].colourCode)
            colour03.isHidden = false
            colour03ImageView.backgroundColor = Utilities.sharedInstance.hexStringToUIColor(
                hex: presenter.productInfo.variants[2].colourCode)
            colourMoreThan3.isHidden = true
        } else {
            colour01.isHidden = true
            colour02.isHidden = true
            colour03.isHidden = true
            colourMoreThan3.isHidden = false
            let count = presenter.productInfo.variants.count - 1
            colourCountLabel.text = "+\(count) COLOURS"
            colourButton.isEnabled = true
            colour04ImageView.backgroundColor = Utilities.sharedInstance.hexStringToUIColor(
                hex: presenter.productInfo.variants[0].colourCode)
        }
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        productDetailPageController.transform = productDetailPageController.transform.rotated(by: .pi/2)
        backButton.tintColor = UIColor.white
        selectSizeView.layer.borderWidth = 1
        selectSizeView.layer.borderColor = ColourConstants.hex717171.cgColor
        addToBagView.layer.borderWidth = 1
        addToBagView.layer.borderColor = ColourConstants.hex717171.cgColor
        addToBagButton.applyButtonLetterSpacing(spacing: 2.52)
        titleLabel.applyLetterSpacing(spacing: 0.7)
        editorNotesTextView.isEditable = false
        editorNotesTextView.dataDetectorTypes = .all
        editorNotesTextView.isScrollEnabled = false
        sizeAndFitTextView.isEditable = false
        sizeAndFitTextView.dataDetectorTypes = .all
        sizeAndFitTextView.isScrollEnabled = false
        detailAndCareTextView.isEditable = false
        detailAndCareTextView.dataDetectorTypes = .all
        detailAndCareTextView.isScrollEnabled = false
        productUnavailableView.backgroundColor = ColourConstants.hexFFE8E8
        productUnavailableLabel.textColor = ColourConstants.hexEF5353
        colour01ImageView.layer.borderColor = ColourConstants.hex717171.cgColor
        colour01ImageView.layer.borderWidth = 0.5
        colour02ImageView.layer.borderColor = ColourConstants.hex717171.cgColor
        colour02ImageView.layer.borderWidth = 0.5
        colour03ImageView.layer.borderColor = ColourConstants.hex717171.cgColor
        colour03ImageView.layer.borderWidth = 0.5
        colour01ImageView.layer.cornerRadius = 13
        colour02ImageView.layer.cornerRadius = 15
        colour03ImageView.layer.cornerRadius = 15
        colour04ImageView.layer.cornerRadius = 15
        selectedColorBorderView.layer.cornerRadius = 15
        selectedColorBorderView.layer.borderWidth = 1
        selectedColorBorderView.layer.borderColor = UIColor.black.cgColor
    }
    // MARK: - Font Style Method
    func applyFont() {
        titleLabel.font = UIFont(name: FontConstants.regular, size: 14)
        productDecriptionLabel.font = UIFont(name: FontConstants.regular, size: 12)
        currencyLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        productCostLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        selectSizeLabel.font = UIFont(name: FontConstants.regular, size: 12)
        sizeGuideLabel.font = UIFont(name: FontConstants.regular, size: 10)
        editorsNotesLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        sizeAndFitLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        detailAndCareLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        editorNotesTextView.font = UIFont(name: FontConstants.light, size: 11)
        sizeAndFitTextView.font = UIFont(name: FontConstants.light, size: 11)
        detailAndCareTextView.font = UIFont(name: FontConstants.light, size: 11)
        showHereWithLabel.font = UIFont(name: FontConstants.regular, size: 11)
        showHereWithSubLabel.font = UIFont(name: FontConstants.medium, size: 11)
        styleWithLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        mightLikeLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        mrpTaxLabel.font = UIFont(name: FontConstants.regular, size: 10)
        productUnavailableLabel.font = UIFont(name: FontConstants.regular, size: 13)
        colourCountLabel.font = UIFont(name: FontConstants.semiBold, size: 12)
    }

}
