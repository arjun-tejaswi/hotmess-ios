//
//  ProductDetailCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 14/05/21.
//

import UIKit

class ProductDetailCustomCell: UICollectionViewCell {
    @IBOutlet weak var productDetailImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        productDetailImageView.contentMode = .scaleAspectFill
    }
}
