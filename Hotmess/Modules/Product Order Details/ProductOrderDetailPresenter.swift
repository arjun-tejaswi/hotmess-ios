//
//  ProductOrderDetailPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 01/07/21.
//

import Foundation
import SwiftyJSON

protocol ProductOrderDetailPresenterProtocol: AnyObject {
    func productInfoSuccess(status: Bool, title: String, message: String)
    func productListStyleWithSuccess(status: Bool, title: String, message: String)
    func productExistsInBag(status: Bool, title: String, message: String)
    func productRecommendedSuccess(status: Bool, title: String, message: String)
    func addedWishlist(status: Bool, title: String, message: String)
    func removedFromWishlist(status: Bool, title: String, message: String)
    func subscriptionSuccess(status: Bool, title: String, message: String)
    func dataFound(status: Bool, message: String)
}

class ProductOrderDetailPresenter: NSObject {
    weak var delegate: ProductOrderDetailPresenterProtocol?
    var productSKUID: String = ""
    var productID: String = ""
    var productColour: String = ""
    var didStyleWithPageEnd: Bool = false
    var didRecommendedPageEnd: Bool = false
    var productInfo = ProductDetailRealm()
    var productStyleWithList = [ProductListRealm]()
    var productRecommendedList = [ProductListRealm]()
    var totalRecommendedProducts: Int64 = 0
    var totalStyleWithProducts: Int64 = 0
    var styleWithPageNumber: Int64 = 1
    var recommendedPageNumber: Int64 = 1
    var addedWishlist: Bool = false
    var cartTotalItems: Int = 0
    var productQuantity: Int64 = 1
    var isWishlisted: Bool = false
    var isFromDeepLink: Bool = false
    var shoppingBagItemArray = [ShoppingBagRealm]()
    func requestProductInfo(skuID: String, productID: String) {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.productDetail,
            prametres: [
                "product_id": productID,
                "sku_id": skuID
            ],
            type: .withoutAuth,
            method: .POST,
            completion: self.handleProductInfoResponse)
    }
    func requestStyleWith(skuID: String) {
        let parameters: [String: Any] = [
            "pagination": [
                "per_page": 10,
                "page_number": styleWithPageNumber
            ],
            "filters": [
                [
                    "key": "sku",
                    "value": [skuID]
                ]
            ]
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.productListing,
            prametres: parameters,
            type: .withoutAuth,
            method: .POST,
            completion: self.handleStlyeWithProductList)
    }
    func requestRecommendedProducts(productID: String, categoryID: String) {
        let parameters: [String: Any] = [
            "pagination": [
                "per_page": 10,
                "page_number": recommendedPageNumber
            ],
            "product_id": productID,
            "category_id": categoryID
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.recommendedProducts,
            prametres: parameters,
            type: .withoutAuth,
            method: .POST,
            completion: self.handleRecommendedProducts)
    }
    func requestToAddWishlist(skuID: String, productID: String) {
        let parameters: Any = [
            [
                "product_id": productID,
                "sku_id": skuID
            ]
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.addToWishlist,
            prametres: parameters,
            type: .withAuth,
            method: .POST) { response in
            if let status  = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.requestToAddWishlist(
                                    skuID: skuID,
                                    productID: productID)
                            }
                        } else {
                            self.delegate?.addedWishlist(status: false, title: "Error", message: message)
                        }
                    }
                } else {
                    self.delegate?.addedWishlist(status: true, title: "", message: "")
                }
            }
        }
    }
    func requestRemoveFromWishlist(skuID: String) {
        let parameters: Any = [
            [
                "sku_id": skuID
            ]
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.removeFromWishlist,
            prametres: parameters,
            type: .withAuth,
            method: .POST) { response in
            if let status  = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.requestRemoveFromWishlist(skuID: skuID)
                            }
                        } else {
                            self.delegate?.removedFromWishlist(status: false, title: "Error", message: message)
                        }
                    }
                } else {
                    self.delegate?.removedFromWishlist(status: true, title: "", message: "")
                }
            }
        }
    }
    func requestAddToBagProduct(skuID: String, productID: String) {
        let parameters: Any = [
            [
                "product_id": productID,
                "sku_id": skuID,
                "quantity": productQuantity
            ]
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.addToBag,
            prametres: parameters,
            type: .withAuth,
            method: .POST) { response in
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.requestAddToBagProduct(
                                    skuID: skuID,
                                    productID: productID)
                            }
                        } else {
                            self.delegate?.productExistsInBag(status: false, title: "Error", message: message)
                        }
                    }
                } else {
                    self.cartTotalItems = Int(response["count"].int64 ?? 0)
                    self.delegate?.productExistsInBag(status: true,
                                                      title: "Success",
                                                      message: response["message"].string ?? "")
                }
            }
        }
    }
    func requestWishlistStatus(skuID: String) {
        let parameters: Any = [
            [
                "sku_id": skuID
            ]
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.wishlistStatus,
            prametres: parameters,
            type: .withAuth,
            method: .POST) { response in
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.requestWishlistStatus(skuID: skuID)
                            }
                        }
                    }
                } else {
                    self.isWishlisted = response["wishlisted"].bool ?? false
                    if self.isWishlisted {
                        self.delegate?.addedWishlist(status: true, title: "", message: "")
                    } else {
                        self.delegate?.addedWishlist(status: false, title: "", message: "")
                    }
                }
            }
        }
    }
    func productInstockNotify(email: String) {
        let parameters: [String: Any] = [
            "email": email,
            "sku_id": productInfo.stockKeepUnitID
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.productSubscription,
            prametres: parameters,
            type: .withoutAuth,
            method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if status {
                    self.delegate?.subscriptionSuccess(status: false,
                                                       title: "Error",
                                                       message: response["message"].string ?? "")
                } else {
                    self.delegate?.subscriptionSuccess(status: true, title: "", message: "")
                }
            }
        }
    }
    func saveProductToWishlist() {
        let productItem = WishlishRealm()
        productItem.productID = productInfo.productID
        productItem.stockKeepUnitID = productInfo.stockKeepUnitID
        productItem.productColour = productInfo.productColour
        productItem.productImageURL = productInfo.productImageURL
        productItem.productPrice = productInfo.productPrice
        productItem.productName = productInfo.productName
        productItem.productSize = productInfo.productSize
        productItem.productColourCode = productInfo.productDesignerName
        productItem.productDescription = productInfo.productDescription
        RealmDataManager.sharedInstance.saveProductToWishlist(productInfo: productItem)
    }
    func saveProductToBag() {
        if RealmDataManager.sharedInstance.isProductInBag(
            productID: productInfo.productID,
            skuID: productInfo.stockKeepUnitID) {
            RealmDataManager.sharedInstance.increaseProductQuantity(
                productId: productInfo.productID,
                skuId: productInfo.stockKeepUnitID)
            cartTotalItems = RealmDataManager.sharedInstance.getAllProductsFromBag().count
            print(RealmDataManager.sharedInstance.getAllProductsFromBag())
            delegate?.productExistsInBag(status: true, title: "", message: "")
        } else {
            let productItem = ShoppingBagRealm()
            productItem.productID = productInfo.productID
            productItem.stockKeepUnitID = productInfo.stockKeepUnitID
            productItem.productColour = productInfo.productColour
            productItem.productImageURL = productInfo.productImageURL
            productItem.productPrice = productInfo.productPrice
            productItem.productName = productInfo.productName
            productItem.productSize = productInfo.productSize
            productItem.productQuantity += 1
            productItem.productDescription = productInfo.productDescription
            productItem.productDesignerName = productInfo.productDesignerName
            productItem.productColorCode = productInfo.productColourCode
            productItem.productInStock = productInfo.productInStock
            RealmDataManager.sharedInstance.saveProductToBag(productInfo: productItem)
            print(RealmDataManager.sharedInstance.getAllProductsFromBag())
            cartTotalItems = RealmDataManager.sharedInstance.getAllProductsFromBag().count
            delegate?.productExistsInBag(status: true, title: "", message: "")
        }
    }
}
