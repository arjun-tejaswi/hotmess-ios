//
//  ProductOrderDeatilViewContollerExtension.swift
//  Hotmess
//
//  Created by Flaxon on 14/07/21.
//

import UIKit
import AVKit
import AVFoundation

// MARK: - CollectionView Methods
extension ProductOrderDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == productDetailCollectionView {
            return presenter.productInfo.assets.count
        } else if collectionView == styleWithCollectionView {
            return presenter.productStyleWithList.count
        } else if collectionView == colorsCollectionView {
            return presenter.productInfo.variants.count
        } else {
            return presenter.productRecommendedList.count
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == productDetailCollectionView {
            return getProductDetailCollectionViewCell(indexPath: indexPath)
        } else if collectionView == styleWithCollectionView {
            return getStyleWithCollectionViewCell(indexPath: indexPath)
        } else if collectionView == colorsCollectionView {
            return getColourSelectionCollectionViewCell(indexPath: indexPath)
        } else {
            return getMightLikeCollectionViewCell(indexPath: indexPath)
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        if collectionView == styleWithCollectionView {
            if indexPath.item == presenter.productStyleWithList.count - 1 && !presenter.didStyleWithPageEnd {
                presenter.styleWithPageNumber += 1
                presenter.requestStyleWith(skuID: presenter.productInfo.stockKeepUnitID)
            }
        } else if collectionView == mightLikeCollectionView {
            if indexPath.item == presenter.productRecommendedList.count - 1 && !presenter.didRecommendedPageEnd {
                presenter.recommendedPageNumber += 1
                presenter.requestRecommendedProducts(
                    productID: presenter.productInfo.productID,
                    categoryID: presenter.productInfo.productCategoryID)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == styleWithCollectionView {
            presenter.requestProductInfo(
                skuID: presenter.productStyleWithList[indexPath.row].stockKeepUnitID,
                productID: presenter.productStyleWithList[indexPath.row].productID)
            self.view.showLoader()
        } else if collectionView == mightLikeCollectionView {
            presenter.requestProductInfo(
                skuID: presenter.productRecommendedList[indexPath.row].stockKeepUnitID,
                productID: presenter.productRecommendedList[indexPath.row].productID)
            self.view.showLoader()
        } else if collectionView == colorsCollectionView {
            colorSelectionView.isHidden = true
            colourSelectionHeightConstarint.constant = 0
            self.view.showLoader()
            presenter.requestProductInfo(
                skuID: presenter.productInfo.variants[indexPath.row].stockKeepUnitID,
                productID: presenter.productInfo.variants[indexPath.row].productID)
        } else if collectionView == productDetailCollectionView &&
                    presenter.productInfo.assets[indexPath.row].type == "video" {
            productDetailCollectionView.deselectItem(at: indexPath, animated: true)
            playVideo(at: indexPath)
        }
    }
    func playVideo(at indexPath: IndexPath) {
        let videoURL = presenter.productInfo.assets[indexPath.row].imageURL
        guard let productDemoVideoURL = URL(string: videoURL) else { return }
            let player = AVPlayer(url: productDemoVideoURL)
            let productDemoVC = AVPlayerViewController()
            productDemoVC.player = player
            present(productDemoVC, animated: true) {
                player.play()
            }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == productDetailCollectionView {
            let currentPage = Int(scrollView.contentOffset.y) / Int(scrollView.frame.height)
            self.productDetailPageController.currentPage = currentPage
        }
    }
    // MARK: - Colour Selection CollectionView Configuration
    func getColourSelectionCollectionViewCell(indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = colorsCollectionView.dequeueReusableCell(
                withReuseIdentifier: "ColorsSelectionCustomCell",
                for: indexPath) as? ColorsSelectionCustomCell else {
            return UICollectionViewCell()
        }
        if indexPath.row == 0 {
            cell.showSelectedColor()
        } else {
            cell.showUnSelectedColor()
        }
        let colourCode = presenter.productInfo.variants[indexPath.row].colourCode
        cell.colorImageView.backgroundColor = Utilities.sharedInstance.hexStringToUIColor(
            hex: colourCode)
        cell.nameLabel.text = presenter.productInfo.variants[indexPath.row].colour
        return cell
    }
    // MARK: - Product Deatil CollectionView Configuration
    func getProductDetailCollectionViewCell(indexPath: IndexPath) -> UICollectionViewCell {
        if presenter.productInfo.assets[indexPath.row].type == "video" {
            guard let cell = productDetailCollectionView.dequeueReusableCell(
                    withReuseIdentifier: "ProductDetailVideoCustomCell",
                    for: indexPath) as? ProductDetailVideoCustomCell else {
                return UICollectionViewCell()
            }
            NukeManager.sharedInstance.setImage(
                url: presenter.productInfo.assets[indexPath.row].thumbnailImageURL,
                imageView: cell.productDetailThumbnailImageView,
                withPlaceholder: true)
            return cell
        } else {
            guard let cell = productDetailCollectionView.dequeueReusableCell(
                    withReuseIdentifier: "ProductDetailCustomCell",
                    for: indexPath) as? ProductDetailCustomCell else {
                return UICollectionViewCell()
            }
            NukeManager.sharedInstance.setImage(
                url: presenter.productInfo.assets[indexPath.row].imageURL,
                imageView: cell.productDetailImageView,
                withPlaceholder: true)
            return cell
        }
    }
    // MARK: - MightAlsoLike CollectionView Configuration
    func getMightLikeCollectionViewCell(indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = mightLikeCollectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCustomCell",
                                                            for: indexPath) as? ProductListCustomCell else {
            return UICollectionViewCell()
        }
        NukeManager.sharedInstance.setImage(
            url: presenter.productRecommendedList[indexPath.row].productImageURL,
            imageView: cell.productListImageView,
            withPlaceholder: true)
        cell.productListTiltleLabel.text = presenter.productRecommendedList[indexPath.row].productDesignerName
        cell.productListSubTitleLabel.text = presenter.productRecommendedList[indexPath.row].productDescription
        cell.priceLabel.text = "\(presenter.productRecommendedList[indexPath.row].productPrice)"
        return cell
    }
    // MARK: - StyleWith CollectionView Configuration
    func getStyleWithCollectionViewCell(indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = styleWithCollectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCustomCell",
                                                            for: indexPath) as? ProductListCustomCell else {
            return UICollectionViewCell()
        }
        NukeManager.sharedInstance.setImage(
            url: presenter.productStyleWithList[indexPath.row].productImageURL,
            imageView: cell.productListImageView,
            withPlaceholder: true)
        cell.priceLabel.text = String(presenter.productStyleWithList[indexPath.row].productPrice)
        cell.productListTiltleLabel.text = presenter.productStyleWithList[indexPath.row].productDesignerName
        cell.productListSubTitleLabel.text = presenter.productStyleWithList[indexPath.row].productDescription
        return cell
    }
}
extension ProductOrderDetailViewController: ProductOrderDetailPresenterProtocol {
    func dataFound(status: Bool, message: String) {
        self.view.hideLoader()
        if status {
            cartCountLabel.text = "\(presenter.cartTotalItems)"
        }
    }
    func subscriptionSuccess(status: Bool, title: String, message: String) {
        if status {
            self.view.hideLoader()
            productInstockSuccessPopUp = Utilities.sharedInstance.showProductNotifySuccessPopUp(
                bounds: UIScreen.main.bounds)
            productInstockSuccessPopUp.applyData(successData: presenter.productInfo)
            self.view.addSubview(productInstockSuccessPopUp)
        } else {
            self.view.hideLoader()
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
    func removedFromWishlist(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            wishlistButton.setImage(UIImage(named: "wishList_Icon"), for: .normal)
            presenter.addedWishlist = false
        } else {
            wishlistButton.setImage(UIImage(named: "wishList_Icon_Filled"), for: .normal)
            presenter.addedWishlist = true
        }
    }
    func addedWishlist(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            wishlistButton.setImage(UIImage(named: "wishList_Icon_Filled"), for: .normal)
            presenter.addedWishlist = true
        } else {
            wishlistButton.setImage(UIImage(named: "wishList_Icon"), for: .normal)
            presenter.addedWishlist = false
        }
    }
    func productRecommendedSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if presenter.totalRecommendedProducts == 0 {
            mightLikeViewHeightConstaint.constant = 0
            mightLikeCollectionView.reloadData()
        } else {
            mightLikeViewHeightConstaint.constant = 330
            mightLikeCollectionView.reloadData()
        }
    }
    func productExistsInBag(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        cartCountLabel.text = "\(presenter.cartTotalItems)"
        if status {
            addToBagPopup = Utilities.sharedInstance.showAddToBagPopUp(bounds: UIScreen.main.bounds)
            addToBagPopup.productStyleWithList = presenter.getStyleWithList()
            addToBagPopup.applyData(productData: presenter.productInfo)
            addToBagPopup.delegate = self
            addToBagPopup.viewBagButton.addTarget(self, action: #selector(viewBagCheckOutAction), for: .touchUpInside)
            self.view.addSubview(addToBagPopup)
        } else {
            cartCountLabel.text = "\(presenter.cartTotalItems)"
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)        }
    }
    func productListStyleWithSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if presenter.totalStyleWithProducts == 0 {
            styleWithViewHeightConstraint.constant = 0
            styleWithCollectionView.reloadData()
        } else {
            styleWithViewHeightConstraint.constant = 330
            styleWithCollectionView.reloadData()
        }
    }
    func productInfoSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if !status {
            self.navigationController?.popViewController(animated: true)
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        } else {
            initializeUI()
            productDetailCollectionView.scrollToItem(at: [0], at: .top, animated: true)
            productScrollView.setContentOffset(CGPoint.zero, animated: true)
            productDetailCollectionView.reloadData()
            colorsCollectionView.reloadData()
            if UserDefaults.standard.bool(forKey: "isLoggedIn") {
                self.view.showLoader()
                presenter.requestWishlistStatus(skuID: presenter.productInfo.stockKeepUnitID)
            }
            presenter.productStyleWithList.removeAll()
            presenter.productRecommendedList.removeAll()
            presenter.styleWithPageNumber = 1
            presenter.recommendedPageNumber = 1
            presenter.requestStyleWith(skuID: presenter.productInfo.stockKeepUnitID)
            presenter.requestRecommendedProducts(
                productID: presenter.productInfo.productID,
                categoryID: presenter.productInfo.productCategoryID)
            self.view.showLoader()
            presenter.requestShoppingBag()
        }
    }
}
