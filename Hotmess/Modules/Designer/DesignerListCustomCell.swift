//
//  DesignerListCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 12/10/21.
//

import UIKit

class DesignerListCustomCell: UITableViewCell {
    @IBOutlet weak var initialsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    func applyStyling() {
        initialsLabel.textColor = ColourConstants.hex1A1A1A
        initialsLabel.backgroundColor = ColourConstants.hexEAEAEA
        initialsLabel.layer.cornerRadius = 12
        initialsLabel.clipsToBounds = true
        nameLabel.textColor = ColourConstants.hex191919
    }
    func applyFont() {
        initialsLabel.font = UIFont(name: FontConstants.medium, size: 16)
        nameLabel.font = UIFont(name: FontConstants.regular, size: 14)
    }
}
