//
//  DesignerViewController.swift
//  Hotmess
//
//  Created by Flaxon on 28/05/21.
//

import UIKit
import SwiftyJSON
import SectionIndexView

struct DesignerListModel {
    var name: String!
    var designerId: String!
    var showInitials: Bool!
    init(name: String, desginerId: String, showInitials: Bool) {
        self.name = name
        self.designerId = desginerId
        self.showInitials = showInitials
    }
}
struct DesignerSection {
    var title: String!
    var section: [DesignerListModel]
    init(title: String, section: [DesignerListModel]) {
        self.title = title
        self.section = section
    }
}
class DesignerViewController: UIViewController {
    @IBOutlet weak var searchBarContainerView: UIView!
    @IBOutlet weak var clearSearchButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var isSearchActive: Bool = false
    let alphabetList = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    var designers = [DesignerListModel]()
    var sections = [DesignerSection]()
    var filteredSections = [DesignerSection]()
    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyling()
    }
    func applyStyling() {
        searchTextField.addTarget(self, action: #selector(didEnterSearchText), for: .editingChanged)
        searchBarContainerView.layer.borderWidth = 1
        searchBarContainerView.layer.borderColor = ColourConstants.hexD3D3D3.cgColor
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.showLoader()
        getDesignersList()
    }
    @objc func didEnterSearchText(textfield: UITextField) {
        if let searchText = textfield.text {
            if searchText == "" {
                isSearchActive = false
                clearSearchButton.isHidden = true
            } else {
                isSearchActive = true
                clearSearchButton.isHidden = false
                filteredSections.removeAll()
                filteredSections = sections.filter { $0.section.contains { (item) -> Bool in
                    let temp: NSString = item.name as NSString
                    let range = temp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                    return range.location != NSNotFound
                }}
            }
            tableView.reloadData()
        }
    }
    func configureSectionIndexTitles() {
        var titles = [String]()
        for section in sections {
            if let firstCharacter = section.title.first {
                titles.append(firstCharacter.uppercased())
            }
        }
        let items = titles.compactMap { (title) -> SectionIndexViewItem? in
            let item = SectionIndexViewItemView.init()
            item.title = title
            item.titleSelectedColor = .white
            let indicator = SectionIndexViewItemIndicator.init(title: title)
            indicator.titleColor = .white
            indicator.indicatorBackgroundColor = ColourConstants.hex191919
            item.indicator = indicator
            item.selectedColor = ColourConstants.hex191919
            return item
        }
        self.tableView.sectionIndexView(items: items)
    }
    func getDesignersList() {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.designersList,
            prametres: [:],
            type: .withoutAuth,
            method: .POST) { response in
            self.view.hideLoader()
            print(response)
            if let status = response["error_status"].bool {
                if !status {
                    self.designers.removeAll()
                    if let designersListArray = response["data"].array {
                        var designersList = [DesignerListModel]()
                        for designer in designersListArray {
                            designersList.append(DesignerListModel.init(
                                                    name: designer["title"].string ?? "",
                                                    desginerId: designer["id"].string ?? "",
                                                    showInitials: false))
                        }
                        self.prepareListStructure(list: designersList)
                    }
                } else {
                    let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                        bounds: self.view.bounds,
                        title: "Error",
                        subTitle: "Could not load designers now. \nTry again later",
                        buttonTitle: "OK", buttonAction: { return })
                    self.view.addSubview(popUp)
                }
            }
        }
    }
    func prepareListStructure(list: [DesignerListModel]) {
        let sortedList = list.sorted(by: { $0.name.lowercased() < $1.name.lowercased() })
        sections.removeAll()
        for alphaIndex: Int in 0 ..< (alphabetList.count) {
            var groupedDesingers = [DesignerListModel]()
            for listIndex: Int in 0 ..< sortedList.count {
                let aDesigner = sortedList[listIndex]
                let aDesignerIndex = alphabetList.index(alphabetList.startIndex, offsetBy: alphaIndex)
                let initialCharacter = alphabetList[aDesignerIndex]
                if aDesigner.name.first == initialCharacter {
                    groupedDesingers.append(aDesigner)
                }
            }
            let index = alphabetList.index(alphabetList.startIndex, offsetBy: alphaIndex)
            if groupedDesingers.count > 0 {
                sections.append(DesignerSection.init(title: String(alphabetList[index]), section: groupedDesingers))
            }
        }
        configureSectionIndexTitles()
        self.tableView.reloadData()
    }
    @IBAction func clearSearchButtonAction(_ sender: UIButton) {
        searchTextField.text = ""
        clearSearchButton.isHidden = true
        isSearchActive = false
        filteredSections.removeAll()
        tableView.reloadData()
    }
}
extension DesignerViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if isSearchActive {
            return filteredSections.count
        } else {
            return sections.count
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive {
            return filteredSections[section].section.count
        } else {
            return sections[section].section.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "DesignerListCustomCell") as? DesignerListCustomCell else {
            let cell = UITableViewCell()
            cell.backgroundColor = .red
            return cell
        }
        var designer: DesignerListModel!
        if isSearchActive {
            designer = filteredSections[indexPath.section].section[indexPath.row]
        } else {
            designer = sections[indexPath.section].section[indexPath.row]
        }
        cell.nameLabel.text = designer.name.capitalizingFirstLetter()
        cell.initialsLabel.text = String(designer.name.prefix(1)).uppercased()
        if indexPath.row == 0 {
            cell.initialsLabel.isHidden = false
        } else {
            cell.initialsLabel.isHidden = true
        }
        return cell
    }
}
