//
//  ShopViewController.swift
//  Hotmess
//
//  Created by Flaxon on 28/05/21.
//

import UIKit
import SideMenu
import Nuke

class ShopViewController: UIViewController {
    @IBOutlet weak var shoppingBagCountLabel: UILabel!
    @IBOutlet weak var shopLabel: UILabel!
    @IBOutlet weak var shopCategoryTableView: UITableView!
    var presenter: ShopPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        applyFont()
    }
    override func viewWillAppear(_ animated: Bool) {
        presenter = ShopPresenter()
        presenter.delegate = self
        self.view.showLoader()
        presenter.requestShoppingBagAPI()
        presenter.requestShopWomenCategory()
    }
    @IBAction func searchButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "Search", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "SearchViewController")
                as? SearchViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @IBAction func shoppingBagButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "ShoppingBag", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ShoppingBagViewController")
                as? ShoppingBagViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @IBAction func profileButtonAction(_ sender: UIButton) {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") == false {
            let storyboardLogin = UIStoryboard(name: "Login", bundle: nil)
            guard let loginView = storyboardLogin.instantiateViewController(
                    withIdentifier: "LoginViewController") as? LoginViewController else {
                return
            }
            self.navigationController?.view.setNavigationTransitionLayer()
            self.navigationController?.pushViewController(loginView, animated: true)
        } else {
            NotificationCenter.default.post(name: Notification.Name("SHOW_SLIDE_MENU"),
                                            object: nil)
        }
    }
    // MARK: - Statusbar Style Method
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - Font Styling Method
    func applyFont() {
        shopLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        shopLabel.textColor = ColourConstants.hexFFFFFF
        shopLabel.applyLetterSpacing(spacing: 0.9)
    }
    // MARK: - TableView SetUp Method
    func setUpTableView() {
        shopCategoryTableView.delegate = self
        shopCategoryTableView.dataSource = self
        shopCategoryTableView.register(UINib(nibName: "ShopCategoryTableViewCell", bundle: nil),
                                       forCellReuseIdentifier: "ShopCategoryTableViewCell")
    }
}
// MARK: - Table View Methods
extension ShopViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.shopData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShopCategoryTableViewCell")
                as? ShopCategoryTableViewCell else {
            return UITableViewCell()
        }
        if indexPath.row == 0 {
            cell.imageViewTopConstraint.constant = 20
        } else {
            cell.imageViewTopConstraint.constant = 12.76
        }
        let url = URL(string: presenter.shopData[indexPath.row].productCategoryImageURL)
        NukeManager.sharedInstance.setImage(url: url, imageView: cell.shopCategoryImageView, withPlaceholder: true)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "ProductListing", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ProductListViewController")
                as? ProductListViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
extension ShopViewController: ShopPresenterProtocol {
    func shoppingBagSuccess(status: Bool, message: String) {
        shoppingBagCountLabel.text = "\(presenter.shoppingCartCount)"
    }
    func shopCategoryListSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        shopCategoryTableView.reloadData()
        if !status {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)        }
    }
}
