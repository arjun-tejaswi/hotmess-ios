//
//  ShopPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 29/06/21.
//

import Foundation
import SwiftyJSON
protocol ShopPresenterProtocol: AnyObject {
    func shopCategoryListSuccess(status: Bool, title: String, message: String)
    func shoppingBagSuccess(status: Bool, message: String)
}

class ShopPresenter: NSObject {
    weak var delegate: ShopPresenterProtocol?
    var shopData = [ShopCategoryRealm]()
    var shoppingBagItemList = [ShoppingBagRealm]()
    var shoppingCartCount: Int = 0
    func requestShopWomenCategory() {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.shopWomenCategory,
            prametres: ["type": "women"],
            type: .withoutAuth,
            method: .POST,
            completion: self.handleShopWomenResponse)
    }
    func requestShoppingBagAPI() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.shoppingBag,
                                                             prametres: [:],
                                                             type: .withAuth,
                                                             method: .POST,
                                                             completion: handleShoppingBagAPIResponse)
        } else {
            shoppingBagItemList.removeAll()
            shoppingCartCount = RealmDataManager.sharedInstance.getAllProductsFromBag().count
            delegate?.shoppingBagSuccess(status: true, message: "")
        }
    }
    func handleShoppingBagAPIResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                let message = response["message"].string ?? ""
                if message == "requestForRefresh" {
                    NetworkManager.sharedInstance.refreshBearerToken {
                        self.requestShoppingBagAPI()
                    }
                }
            } else {
                if let items = response["data"].array {
                    shoppingCartCount = 0
                    for _ in items {
                        shoppingCartCount += 1
                    }
                    delegate?.shoppingBagSuccess(status: true, message: "")
                }
            }
        }
    }
    func handleShopWomenResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                delegate?.shopCategoryListSuccess(status: false,
                                                  title: "Error",
                                                  message: response["message"].string ?? "")
            } else {
                if let data = response["data"].array {
                    for aShopCategory in data {
                        if let aShopCategoryDictionary = aShopCategory.dictionary {
                            let allShopCategory = shopCategoryParseData(data: aShopCategoryDictionary)
                            shopData.append(allShopCategory)
                        }
                    }
                    delegate?.shopCategoryListSuccess(status: true, title: "", message: "")
                }
            }
        }
    }
    func shopCategoryParseData(data: [String: JSON]) -> ShopCategoryRealm {
        let shopCategoryData = ShopCategoryRealm()
        shopCategoryData.productCategoryImageURL =
            ApiBaseURL.imageBaseURL + "\(data["image_url"]?.string ?? "")"
        return shopCategoryData
    }
}
