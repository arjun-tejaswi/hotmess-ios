//
//  LandingViewController.swift
//  Hotmess
//
//  Created by Flaxon on 28/05/21.
//

import UIKit
import SideMenu

enum LandingPageContentType {
    case whatsNew
    case featuredProduct
    case dressRefresh
    case celebrityPicks
    case trendingProduct
    case moodCollection
    case singleImage
    case hotmessCollection
}

class LandingViewController: UIViewController {
    @IBOutlet weak var shoppingBagCountLabel: UILabel!
    @IBOutlet weak var landingTitleLabel: UILabel!
    @IBOutlet weak var womenLandingTableView: UITableView!
    var presenter: LandingViewPresenter!
    var landingPageSections: [LandingPageContentType] = [
        .whatsNew,
        .featuredProduct,
        .dressRefresh,
        .celebrityPicks,
        .trendingProduct,
        .moodCollection,
        .singleImage,
        .hotmessCollection
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyling()
        applyFont()
        setUpTableView()
        presenter = LandingViewPresenter()
        presenter.delegate = self
        self.view.showLoader()
        presenter.callAllApi()
    }
    override func viewWillAppear(_ animated: Bool) {
        presenter.requestShoppingBag()
        NetworkManager.sharedInstance.checkForUpdate { (status) in
            guard status != .UPDATENOTNEEDED else {
                return
            }
            if status == .UPDATEAVAILABLE {
                AlertManager.sharedInstance.showAlertWithTwoAction(
                    parameters: AlertManagerParameters.init(
                        title: status.title,
                        message: status.message,
                        leftButtonTitle: "Cancel",
                        rightButtonTitle: "Update")) {
                    // Do Nothing
                } rightButtonAction: {
                    print("Update button clicked")
                    if let url = URL(string: "") {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            } else {
                AlertManager.sharedInstance.showAlertWithOneAction(
                    parameters: AlertManagerParameters.init(
                        title: status.title,
                        message: status.message,
                        leftButtonTitle: "",
                        rightButtonTitle: "Update")) {
                    print("Update Button Clicked")
                    if let url = URL(string: "") {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            }
        }
    }
    // MARK: - Statusbar Style Method
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func searchButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "Search", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "SearchViewController")
                as? SearchViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @IBAction func shoppingBagButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "ShoppingBag", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ShoppingBagViewController")
                as? ShoppingBagViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @IBAction func profileButtonAction(_ sender: UIButton) {
            NotificationCenter.default.post(name: Notification.Name("SHOW_SLIDE_MENU"),
                                            object: nil)
    }
    // MARK: - Font Style Method
    func applyFont() {
        landingTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        landingTitleLabel.textColor = ColourConstants.hexFFFFFF
        landingTitleLabel.applyLetterSpacing(spacing: 0.9)
    }
    // MARK: - TableView SetUp Method
    func setUpTableView() {
        womenLandingTableView.delegate = self
        womenLandingTableView.dataSource = self
        womenLandingTableView.register(UINib(nibName: "WhatIsNewTableViewCell", bundle: nil),
                                       forCellReuseIdentifier: "WhatIsNewTableViewCell")
        womenLandingTableView.register(UINib(nibName: "FeaturedProductsTableViewCell", bundle: nil),
                                       forCellReuseIdentifier: "FeaturedProductsTableViewCell")
        womenLandingTableView.register(UINib(nibName: "DressRefreshTableViewCell", bundle: nil),
                                       forCellReuseIdentifier: "DressRefreshTableViewCell")
        womenLandingTableView.register(UINib(nibName: "CelebrityPicksTableViewCell", bundle: nil),
                                       forCellReuseIdentifier: "CelebrityPicksTableViewCell")
        womenLandingTableView.register(UINib(nibName: "TrendingProductsTableViewCell", bundle: nil),
                                       forCellReuseIdentifier: "TrendingProductsTableViewCell")
        womenLandingTableView.register(UINib(nibName: "SingleImageTableViewCell", bundle: nil),
                                       forCellReuseIdentifier: "SingleImageTableViewCell")
        womenLandingTableView.register(UINib(nibName: "MoodCollectionCustomCell", bundle: nil),
                                       forCellReuseIdentifier: "MoodCollectionCustomCell")
        womenLandingTableView.register(UINib(nibName: "HotmessCollectionTableViewCell", bundle: nil),
                                       forCellReuseIdentifier: "HotmessCollectionTableViewCell")
    }
    @objc func whatsNewShopNowButtonAction() {
        let storyBoard = UIStoryboard(name: "ProductListing", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ProductListViewController")
                as? ProductListViewController else { return }
        nextvc.presenter.titleLabelText = presenter.whatIsNewInfo.whatIsNewTitle
        print(presenter.getWhatsNewFilters())
        nextvc.presenter.filters = presenter.getWhatsNewFilters()
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @objc func featuredProductShopNowButtonAction() {
        let storyBoard = UIStoryboard(name: "ProductListing", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ProductListViewController")
                as? ProductListViewController else { return }
        nextvc.presenter.filters = presenter.getFeaturedProductFilters()
        nextvc.presenter.titleLabelText = presenter.featuredProductInfo.featuredProductTitle
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @objc func celebrityPicksShopNowButtonAction() {
        let storyBoard = UIStoryboard(name: "ProductListing", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ProductListViewController")
                as? ProductListViewController else { return }
        nextvc.presenter.titleLabelText = presenter.celebrityPicksInfo.celebrityPicksTitle
        nextvc.presenter.filters = presenter.getCelebrityPicksFilters()
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @objc func moodCollectionsShopNowButtonAction() {
        let storyBoard = UIStoryboard(name: "ProductListing", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ProductListViewController")
                as? ProductListViewController else { return }
        nextvc.presenter.titleLabelText = presenter.moodCollectionInfo.moodCollectionsTitle
        nextvc.presenter.filters = presenter.getMoodCollectionFilters(moodCollectionSection: 4)
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @objc func moodCollectionButtonOneAction() {
        let storyBoard = UIStoryboard(name: "ProductListing", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ProductListViewController")
                as? ProductListViewController else { return }
        nextvc.presenter.titleLabelText = presenter.moodCollectionInfo.moodCollectionsTitle
        nextvc.presenter.filters = presenter.getMoodCollectionFilters(moodCollectionSection: 1)
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @objc func moodCollectionButtonTwoAction() {
        let storyBoard = UIStoryboard(name: "ProductListing", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ProductListViewController")
                as? ProductListViewController else { return }
        nextvc.presenter.titleLabelText = presenter.moodCollectionInfo.moodCollectionsTitle
        nextvc.presenter.filters = presenter.getMoodCollectionFilters(moodCollectionSection: 2)
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @objc func moodCollectionButtonThreeAction() {
        let storyBoard = UIStoryboard(name: "ProductListing", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ProductListViewController")
                as? ProductListViewController else { return }
        nextvc.presenter.titleLabelText = presenter.moodCollectionInfo.moodCollectionsTitle
        nextvc.presenter.filters = presenter.getMoodCollectionFilters(moodCollectionSection: 3)
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
