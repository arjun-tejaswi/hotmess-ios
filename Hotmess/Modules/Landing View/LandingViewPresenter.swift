//
//  LandingViewPresenter.swift
//  Hotmess
//
//  Created by Akshatha on 21/06/21.
//

import Foundation
import SwiftyJSON
import RealmSwift

protocol LandingViewPresenterProtocol: AnyObject {
    func dataFound(status: Bool, message: String)
    func dataNotFound(status: Bool, title: String, message: String)
}
class LandingViewPresenter: NSObject {
    weak var delegate: LandingViewPresenterProtocol?
    var whatIsNewInfo: WhatIsNewRealmModel!
    var dressRefreshInfo: DressRefreshRealmModel!
    var featuredProductInfo: FeaturedProductRealmModel!
    var moodCollectionInfo: MoodCollectionsRealm!
    var celebrityPicksInfo: CelebrityPicksRealm!
    var trendingProductMetaInfo: TrendingProductMetaDataRealm!
    var hotmessCollectionInfo: HotmessCollectionMetaDataRealm!
    var productList = [ProductListRealm]()
    var hotmessProductList = [HotmessProductListRealm]()
    var shoppingBagItemArray = [ShoppingBagRealm]()
    var shoppinBagCount: Int = 0
    var moodCollectionFilter01 = [String]()
    var moodCollectionFilter02 = [String]()
    var moodCollectionFilter03 = [String]()
    var moodCollectionAllFilters = [String]()
    func callAllApi() {
        getWhatIsNewData()
        getFeaturedProductData()
        getDressRefreshData()
        getMoodCollectionData()
        getCelebrityPicksData()
        getTrendingProductsMetaData()
        getHotmessCollectionMetaData()
    }
    func getMoodCollectionFilters(moodCollectionSection: Int) -> Any {
        var filters: Any = [:]
        let categoryKey: String = "category_name"
        let curatedKey: String = "curated_list"
        if moodCollectionSection == 1 {
            filters = [["key": categoryKey, "value": moodCollectionFilter01],
                      ["key": curatedKey, "value": moodCollectionInfo.curatedList]]
        } else if moodCollectionSection == 2 {
            filters = [["key": categoryKey, "value": moodCollectionFilter02],
                      ["key": curatedKey, "value": moodCollectionInfo.curatedList]]
        } else if moodCollectionSection == 3 {
            filters = [["key": categoryKey, "value": moodCollectionFilter03],
                      ["key": curatedKey, "value": moodCollectionInfo.curatedList]]
        } else {
            filters = [["key": categoryKey, "value": moodCollectionAllFilters],
                      ["key": curatedKey, "value": moodCollectionInfo.curatedList]]
        }
        return filters
    }
    func getWhatsNewFilters() -> Any {
        var filters: Any = [:]
        var value = [String]()
        for item in whatIsNewInfo.whatIsNewCollection {
            value.append(item.skuID)
        }
        filters = [["key": "sku_id", "value": value]]
        return filters
    }
    func getTrendingFilters() -> Any {
        let filter = [
            [
                "key": "trending",
                "value": ["true"]
            ]
        ]
        return filter
    }
    func getHotmessCollectionFilters() -> Any {
        let filter = [
            [
                "key": "curated_list",
                "value": ["bac6a120-dafe-11eb-9eb2-5b59ce368a47"]
            ]
        ]
        return filter
    }
    func getFeaturedProductFilters() -> Any {
        if let filterList = featuredProductInfo.filterList.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: filterList, options: .mutableContainers)
                return json
            } catch {
                print("Something went wrong")
                return []
            }
        } else {
            return []
        }
    }
    func getDressRefressFilters(filterListFromCell: String) -> Any {
        if let filterList = filterListFromCell.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: filterList, options: .mutableContainers)
                return json
            } catch {
                print("Something went wrong")
                return []
            }
        } else {
            return []
        }
    }
    func getCelebrityPicksFilters() -> Any {
        if let filterList = celebrityPicksInfo.filterList.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: filterList, options: .mutableContainers)
                return json
            } catch {
                print("Something went wrong")
                return []
            }
        } else {
            return []
        }
    }
    func getWhatIsNewData() {
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.whatsNew,
                                                         prametres: [:],
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: whatIsNewResponse)
    }
    func getFeaturedProductData() {
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.womenFeaturedProducts,
                                                         prametres: [:],
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: featuredProductsResponse)
    }
    func getDressRefreshData() {
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.whatToWearCollections,
                                                         prametres: [:],
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: dressRefreshResponse)
    }
    func getMoodCollectionData() {
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.moodCollections,
                                                         prametres: [:],
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: moodCollectionsResponse)
    }
    func getCelebrityPicksData() {
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.celebrityPicks,
                                                         prametres: [:],
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: celebrityPicksResponse)
    }
    func getTrendingProductsMetaData() {
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.trendingProductsMetaData,
                                                         prametres: [:],
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: trendingProductsMetaDataResponse)
    }
    func requestShoppingBag() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.shoppingBag,
                                                             prametres: [:],
                                                             type: .withAuth,
                                                             method: .POST,
                                                             completion: handleShoppingBagResponse)
        } else {
            shoppingBagItemArray.removeAll()
            shoppinBagCount = RealmDataManager.sharedInstance.getAllProductsFromBag().count
            delegate?.dataFound(status: true, message: "")
        }
    }
    func getTrendingProducts() {
        let parameters: [String: Any] = [
            "pagination": [
                "per_page": 10,
                "page_number": 1
            ],
            "filters": getTrendingFilters()
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.trendingProducts,
                                                         prametres: parameters,
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: trendingProductResponse)
    }
    func getHotmessCollectionMetaData() {
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.hotmessCollectionMetaData,
                                                         prametres: [:],
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: hotmessCollectionMetaDataResponse)
    }
    func gethotmessCollectionProducts() {
        let parameters: [String: Any] = [
            "filters": getHotmessCollectionFilters(),
            "pagination": [
                "per_page": 10,
                "page_number": 1
            ]
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.hotmessCollectionProducts,
                                                         prametres: parameters,
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: hotmessCollectionProductsResponse)
    }
}
