//
//  LandingViewPresenterExtension.swift
//  Hotmess
//
//  Created by Akshatha on 09/07/21.
//

import Foundation
import SwiftyJSON
extension LandingViewPresenter {
    func handleShoppingBagResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                let message = response["message"].string ?? ""
                if message == "requestForRefresh" {
                    NetworkManager.sharedInstance.refreshBearerToken {
                        self.requestShoppingBag()
                    }
                }
            } else {
                if let items = response["data"].array {
                    shoppinBagCount = 0
                    for _ in items {
                        shoppinBagCount += 1
                    }
                    delegate?.dataFound(status: true, message: "")
                }
            }
        }
    }
    func whatIsNewResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                delegate?.dataNotFound(status: false, title: "Error", message: "whats new data not found")
            } else {
                if let data = response["data"].dictionary {
                    whatIsNewInfo = WhatIsNewRealmModel()
                    whatIsNewInfo.whatIsNewTitle = data["title"]?.string ?? ""
                    whatIsNewInfo.whatIsNewSubTitle = data["sub_title"]?.string ?? ""
                    whatIsNewInfo.buttonText = data["cta_text"]?.string ?? ""
                    whatIsNewInfo.totalCollections = data["total_collections"]?.string ?? ""
                    if let whatIsNewCollections = data["whats_new_collections"]?.array {
                        var collectionList = [WhatIsNewCollection]()
                        for collection in whatIsNewCollections {
                            let whatIsNewCollectionRealm = WhatIsNewCollection()
                            whatIsNewCollectionRealm.whatIsNewProductName = collection["name"].string ?? ""
                            whatIsNewCollectionRealm.skuID = collection["sku_id"].string ?? ""
                            whatIsNewCollectionRealm.productID = collection["product_id"].string ?? ""
                            whatIsNewCollectionRealm.whatIsNewImageUrl = ApiBaseURL.imageBaseURL +
                                "\(collection["image_url"].string ?? "")"
                            collectionList.append(whatIsNewCollectionRealm)
                        }
                        whatIsNewInfo.whatIsNewCollection.append(objectsIn: collectionList)
                    }
                    delegate?.dataFound(status: true, message: "Whats new data found")
                } else {
                    delegate?.dataNotFound(status: false, title: "Error", message: "whats new data not found")
                }
            }
        }
    }
    func featuredProductsResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                delegate?.dataNotFound(status: false, title: "Error", message: "featured product data not found")
            } else {
                if let data = response["data"].dictionary {
                    featuredProductInfo = FeaturedProductRealmModel()
                    featuredProductInfo.featuredProductTitle = data["title"]?.string ?? ""
                    featuredProductInfo.featuredProductSubTitle = data["sub_title"]?.string ?? ""
                    featuredProductInfo.featuredProductDescription = data["description"]?.string ?? ""
                    featuredProductInfo.featuredProductImageUrl =
                        ApiBaseURL.imageBaseURL + "\(data["image_url"]?.string ?? "")"
                    if let filterParameters = data["filter_parameters"]?.rawString() {
                        featuredProductInfo.filterList = filterParameters
                    }
                    featuredProductInfo.buttonText = data["cta_text"]?.string ?? ""
                    delegate?.dataFound(status: true, message: "featured product data found")
                } else {
                    delegate?.dataNotFound(status: false, title: "Error", message: "featured product data not found")
                }
            }
        }
    }
    func dressRefreshResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                delegate?.dataNotFound(status: false, title: "Error", message: "dress refresh data not found")
            } else {
                if let data = response["data"].dictionary {
                    dressRefreshInfo = DressRefreshRealmModel()
                    dressRefreshInfo.dressRefreshTitle = data["title"]?.string ?? ""
                    dressRefreshInfo.dressRefreshSubTitle = data["sub_title"]?.string ?? ""
                    dressRefreshInfo.dressRefreshDecsription = data["description"]?.string ?? ""
                    if let dressRefreshItems = data["items"]?.array {
                        var itemList = [DressRefreshItems]()
                        for dressRefreshItem in dressRefreshItems {
                            let dressRefreshItemRealm = DressRefreshItems()
                            dressRefreshItemRealm.dressRefreshMoodTitle = dressRefreshItem["title"].string ?? ""
                            dressRefreshItemRealm.dressRefreshImageUrl  =
                                ApiBaseURL.imageBaseURL + "\(dressRefreshItem["image_url"].string ?? "")"
                            if let filterParameters = dressRefreshItem["filter_parameters"].rawString() {
                                dressRefreshItemRealm.filterList = filterParameters
                            }
                            itemList.append(dressRefreshItemRealm)
                        }
                        dressRefreshInfo.dressRefreshItems.append(objectsIn: itemList)
                    }
                    delegate?.dataFound(status: true, message: "dress refresh data found")
                } else {
                    delegate?.dataNotFound(status: false, title: "Error", message: "dress refresh data not found")
                }
            }
        }
    }
    func getCuratedList(_ data: [JSON]) -> [String] {
        var allCuratedList = [String]()
        for list in data {
            allCuratedList.append(list.string ?? "")
        }
        return allCuratedList
    }
    fileprivate func getPlaceholderOne(_ data: [String: JSON]) {
        if let placeHolderImageOne = data["place_holder_one"]?.dictionary {
            let placeHolderOneRealm = PlaceHolderImageOneRealm()
            placeHolderOneRealm.imageUrl =
                ApiBaseURL.imageBaseURL + "\(placeHolderImageOne["image_url"]?.string ?? "")"
            moodCollectionInfo.placeHolderImageOne = placeHolderOneRealm
            if let values = placeHolderImageOne["value"]?.array {
                moodCollectionFilter01.removeAll()
                for value in values {
                    moodCollectionFilter01.append(value.string ?? "")
                }
                moodCollectionAllFilters.append(contentsOf: moodCollectionFilter01)
            }
        }
    }
    fileprivate func getPlaceholderTwo(_ data: [String: JSON]) {
        if let placeHolderImageTwo = data["place_holder_two"]?.dictionary {
            let placeHolderTwoRealm = PlaceHolderImageTwoRealm()
            placeHolderTwoRealm.imageUrl =
                ApiBaseURL.imageBaseURL + "\(placeHolderImageTwo["image_url"]?.string ?? "")"
            moodCollectionInfo.placeHolderImageTwo = placeHolderTwoRealm
            if let values = placeHolderImageTwo["value"]?.array {
                moodCollectionFilter02.removeAll()
                for value in values {
                    moodCollectionFilter02.append(value.string ?? "")
                }
                moodCollectionAllFilters.append(contentsOf: moodCollectionFilter02)
            }
        }
    }
    fileprivate func getPlaceholderThree(_ data: [String: JSON]) {
        if let placeHolderImageThree = data["place_holder_three"]?.dictionary {
            let placeHolderThreeRealm = PlaceHolderImageThreeRealm()
            placeHolderThreeRealm.imageUrl =
                ApiBaseURL.imageBaseURL + "\(placeHolderImageThree["image_url"]?.string ?? "")"
            moodCollectionInfo.placeHolderImageThree = placeHolderThreeRealm
            if let values = placeHolderImageThree["value"]?.array {
                moodCollectionFilter03.removeAll()
                for value in values {
                    moodCollectionFilter03.append(value.string ?? "")
                }
                moodCollectionAllFilters.append(contentsOf: moodCollectionFilter03)
            }
        }
    }
    func moodCollectionsResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                delegate?.dataNotFound(status: false,
                                       title: "Error", message: "mood collection data not found")
            } else {
                if let data = response["data"].dictionary {
                    moodCollectionAllFilters.removeAll()
                    moodCollectionInfo = MoodCollectionsRealm()
                    moodCollectionInfo.moodCollectionsTitle = data["title"]?.string ?? ""
                    moodCollectionInfo.moodCollectionsSubTitle = data["sub_title"]?.string ?? ""
                    moodCollectionInfo.moodCollectionsDescription = data["description"]?.string ?? ""
                    moodCollectionInfo.buttonText = data["cta_text"]?.string ?? ""
                    moodCollectionInfo.curatedList.append(contentsOf: getCuratedList(data["value"]?.array ?? []))
                    getPlaceholderOne(data)
                    getPlaceholderTwo(data)
                    getPlaceholderThree(data)
                    delegate?.dataFound(status: true, message: "mood collection data found")
                } else {
                    delegate?.dataNotFound(status: false,
                                           title: "Error", message: "mood collection data not found")
                }
            }
        }
    }
    func celebrityPicksResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                delegate?.dataNotFound(status: false,
                                       title: "Error", message: "celebrity picks data not found")
            } else {
                if let data = response["data"].dictionary {
                    celebrityPicksInfo = CelebrityPicksRealm()
                    celebrityPicksInfo.celebrityPicksTitle = data["title"]?.string ?? ""
                    celebrityPicksInfo.celebrityPicksSubTitle = data["sub_title"]?.string ?? ""
                    celebrityPicksInfo.celebrityPicksDescription = data["description"]?.string ?? ""
                    celebrityPicksInfo.buttonText = data["cta_text"]?.string ?? ""
                    if let filterParameters = data["filter_parameters"]?.rawString() {
                        celebrityPicksInfo.filterList = filterParameters
                    }
                    if let placeHolderImage = data["place_holder"]?.dictionary {
                        let placeHolderRealm = PlaceHolderImageRealm()
                        placeHolderRealm.image = ApiBaseURL.imageBaseURL +
                            "\(placeHolderImage["image_url"]?.string ?? "")"
                        placeHolderRealm.searchKey = placeHolderImage["search_key"]?.string ?? ""
                        placeHolderRealm.searchValue = placeHolderImage["search_value"]?.string ?? ""
                        celebrityPicksInfo.placeHolderImage = placeHolderRealm
                    }
                    delegate?.dataFound(status: true,
                                        message: "celebrity picks data found")
                } else {
                    delegate?.dataNotFound(status: false,
                                           title: "Error", message: "celebrity picks data not found")
                }
            }
        }
    }
    func trendingProductsMetaDataResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                delegate?.dataNotFound(status: false,
                                       title: "Error", message: "trending product data not found")
            } else {
                if let data = response["data"].dictionary {
                    trendingProductMetaInfo = TrendingProductMetaDataRealm()
                    trendingProductMetaInfo.trendingProductTitle =  data["title"]?.string ?? ""
                    trendingProductMetaInfo.trendingProductSubTitle = data["sub_title"]?.string ?? ""
                    trendingProductMetaInfo.trendingProductDescription = data["description"]?.string ?? ""
                    delegate?.dataFound(status: true, message: "trending product data found")
                } else {
                    delegate?.dataNotFound(status: false,
                                           title: "Error", message: "trending product data not found")
                }
            }
            getTrendingProducts()
        }
    }
    func trendingProductResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                delegate?.dataNotFound(status: false,
                                       title: "Error", message: response["message"].string ?? "")
            } else {
                if let data = response["data"].array {
                    for aProduct in data {
                        if let aProductDictionary = aProduct.dictionary {
                            let allproducts = ProductParser.sharedInstance.parseProductData(data: aProductDictionary)
                            productList.append(allproducts)
                        }
                    }
                    trendingProductMetaInfo.productListItem.append(objectsIn: productList)
                    delegate?.dataFound(status: true,
                                        message: "trending product list data found")
                } else {
                    delegate?.dataNotFound(status: false,
                                           title: "Error",
                                           message: "trending product list data not found")
                }
            }
        }
    }
    func hotmessCollectionMetaDataResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                delegate?.dataNotFound(status: false,
                                       title: "Error", message: "hotmess collection data not found")
            } else {
                if let data = response["data"].dictionary {
                    hotmessCollectionInfo = HotmessCollectionMetaDataRealm()
                    hotmessCollectionInfo.hotmessCollectionTitle = data["title"]?.string ?? ""
                    hotmessCollectionInfo.hotmessCollectionSubTitle = data["sub_title"]?.string ?? ""
                    hotmessCollectionInfo.hotmessCollectionDescription = data["description"]?.string ?? ""
                    delegate?.dataFound(status: false,
                                        message: "hotmess collection data found")
                } else {
                    delegate?.dataNotFound(status: false,
                                           title: "Error",
                                           message: "hotmess collection data not found")
                }
            }
        }
        gethotmessCollectionProducts()
    }
    func hotmessCollectionProductsResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                delegate?.dataNotFound(status: false,
                                       title: "Error", message: response["message"].string ?? "")
            } else {
                if let data = response["data"].array {
                    for aSource in data {
                        let product = HotmessProductListRealm()
                        product.productImageURL =  ApiBaseURL.imageBaseURL +
                            "\(aSource["featured_image_url"].string ?? "")"
                        product.productName = aSource["designer_name"].string ?? ""
                        product.productDescription = aSource["description"].string ?? ""
                        product.productPrice = aSource["cost_price"].int64 ?? 0
                        product.productID = aSource["product_id"].string ?? ""
                        product.skuID = aSource["sku_id"].string ?? ""
                        hotmessProductList.append(product)
                    }
                    hotmessCollectionInfo.hotmessProductListItem.append(objectsIn: hotmessProductList)
                    delegate?.dataFound(status: true,
                                        message: "hotmess collection list data found")
                } else {
                    delegate?.dataNotFound(status: false,
                                           title: "Error",
                                           message: "hotmess collection list data not found")
                }
            }
        }
    }
}
