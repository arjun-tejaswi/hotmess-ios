//
//  LandingViewControllerExtension.swift
//  Hotmess
//
//  Created by Flaxon on 27/09/21.
//

import Foundation
import UIKit

// MARK: - Table View Methods
extension LandingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch landingPageSections[indexPath.row] {
        case .whatsNew:
            return presenter.whatIsNewInfo != nil ? UITableView.automaticDimension : 0
        case .featuredProduct:
            return presenter.featuredProductInfo != nil ? UITableView.automaticDimension : 0
        case .dressRefresh:
            return presenter.dressRefreshInfo != nil ? UITableView.automaticDimension : 0
        case .celebrityPicks:
            return presenter.celebrityPicksInfo != nil ? UITableView.automaticDimension : 0
        case .moodCollection:
            return presenter.moodCollectionInfo != nil ? UITableView.automaticDimension : 0
        case .singleImage:
            return UITableView.automaticDimension
        case .trendingProduct:
            return presenter.trendingProductMetaInfo != nil ? UITableView.automaticDimension : 0
        case .hotmessCollection:
            return presenter.hotmessCollectionInfo != nil ? UITableView.automaticDimension : 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return landingPageSections.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch landingPageSections[indexPath.row] {
        case .whatsNew:
            return getWhatsNewCell()
        case .featuredProduct:
            return getFeaturedProductCell()
        case .dressRefresh:
            return getDressRefreshCell()
        case .celebrityPicks:
            return getCelebrityPicksCell()
        case .moodCollection:
            return getMoodCollectionCell()
        case .singleImage:
            return getSingleImageCell()
        case .trendingProduct:
            return getTrendingProductCell()
        case .hotmessCollection:
            return gethotmessCollectionCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch landingPageSections[indexPath.row] {
        case .whatsNew:
            print("Not Available")
        case .featuredProduct:
            print("Not Available")
        case .dressRefresh:
            print("Not Available")
        case .celebrityPicks:
            print("Not Available")
        case .moodCollection:
            print("Not Available")
        case .singleImage:
            print("Not Available")
        case .trendingProduct:
            let storyBoard = UIStoryboard(name: "ProductListing", bundle: nil)
            guard let nextvc = storyBoard.instantiateViewController(
                    withIdentifier: "ProductListViewController")
                    as? ProductListViewController else { return }
            nextvc.presenter.filters = presenter.getTrendingFilters()
            nextvc.presenter.titleLabelText = "TRENDING PRODUCTS"
            self.navigationController?.pushViewController(nextvc, animated: true)
        case .hotmessCollection:
            let storyBoard = UIStoryboard(name: "ProductListing", bundle: nil)
            guard let nextvc = storyBoard.instantiateViewController(
                    withIdentifier: "ProductListViewController")
                    as? ProductListViewController else { return }
            nextvc.presenter.filters = presenter.getHotmessCollectionFilters()
            nextvc.presenter.titleLabelText = "HOTMESS COLLECTIONS TIMELINE"
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }

    func getWhatsNewCell() -> UITableViewCell {
        guard let cell = womenLandingTableView.dequeueReusableCell(withIdentifier: "WhatIsNewTableViewCell")
                as? WhatIsNewTableViewCell else {
            return UITableViewCell()
        }
        if presenter.whatIsNewInfo != nil {
            cell.whatIsNewRealm = presenter.whatIsNewInfo
            cell.assignWhatsNewData()
        }
        cell.shopNowButton.addTarget(self, action: #selector(whatsNewShopNowButtonAction), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    func getFeaturedProductCell() -> UITableViewCell {
        guard let cell = womenLandingTableView.dequeueReusableCell(withIdentifier: "FeaturedProductsTableViewCell")
                as? FeaturedProductsTableViewCell else {
            return UITableViewCell()
        }
        if presenter.featuredProductInfo != nil {
            cell.featuredProductRealm = presenter.featuredProductInfo
            cell.assignfeaturedProductData()
        }
        cell.shopNowButton.addTarget(self, action: #selector(featuredProductShopNowButtonAction), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    func getDressRefreshCell() -> UITableViewCell {
        guard let cell = womenLandingTableView.dequeueReusableCell(withIdentifier: "DressRefreshTableViewCell")
                as? DressRefreshTableViewCell else {
            return UITableViewCell()
        }
        if presenter.dressRefreshInfo != nil {
            cell.dressRefreshRealm = presenter.dressRefreshInfo
            cell.assignDressRefreshData()
        }
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    func getCelebrityPicksCell() -> UITableViewCell {
        guard let cell = womenLandingTableView.dequeueReusableCell(withIdentifier: "CelebrityPicksTableViewCell")
                as? CelebrityPicksTableViewCell else {
            return UITableViewCell()
        }
        if presenter.celebrityPicksInfo != nil {
            cell.celebrityPicksRealm = presenter.celebrityPicksInfo
            cell.assignCelebrityPicksData()
        }
        cell.shopNowButton.addTarget(self, action: #selector(celebrityPicksShopNowButtonAction), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    func getMoodCollectionCell() -> UITableViewCell {
        guard let cell = womenLandingTableView.dequeueReusableCell(withIdentifier: "MoodCollectionCustomCell")
                as? MoodCollectionCustomCell else {
            return UITableViewCell()
        }
        if presenter.moodCollectionInfo != nil {
            cell.moodCollectionRealm = presenter.moodCollectionInfo
            cell.assignMoodCollectionsData()
        }
        cell.moodCollectionButtonOne.addTarget(self, action: #selector(moodCollectionButtonOneAction),
                                               for: .touchUpInside)
        cell.moodCollectionButtonTwo.addTarget(self, action: #selector(moodCollectionButtonTwoAction),
                                               for: .touchUpInside)
        cell.moodCollectionButtonThree.addTarget(self, action: #selector(moodCollectionButtonThreeAction),
                                                 for: .touchUpInside)
        cell.shopNowButton.addTarget(self, action: #selector(moodCollectionsShopNowButtonAction), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    func getSingleImageCell() -> UITableViewCell {
        guard let cell = womenLandingTableView.dequeueReusableCell(withIdentifier: "SingleImageTableViewCell")
                as? SingleImageTableViewCell else {
            return UITableViewCell()
        }
        cell.singleImageView.image = UIImage(named: "SingleImage")
        cell.selectionStyle = .none
        return cell
    }
    func getTrendingProductCell() -> UITableViewCell {
        guard let cell = womenLandingTableView.dequeueReusableCell(withIdentifier: "TrendingProductsTableViewCell")
                as? TrendingProductsTableViewCell else {
            return UITableViewCell()
        }
        if presenter.trendingProductMetaInfo != nil {
            cell.trendingProductMetaDataRealm = presenter.trendingProductMetaInfo
            cell.assignTrendingProductData()
        }
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    func gethotmessCollectionCell() -> UITableViewCell {
        guard let cell = womenLandingTableView.dequeueReusableCell(withIdentifier: "HotmessCollectionTableViewCell")
                as? HotmessCollectionTableViewCell else {
            return UITableViewCell()
        }
        if presenter.hotmessCollectionInfo != nil {
            cell.hotmessCollectionRealm = presenter.hotmessCollectionInfo
            cell.assignHotmessMetadata()
        }
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
}
// MARK: - delegate Methods
extension LandingViewController: LandingViewPresenterProtocol {
    func dataNotFound(status: Bool, title: String, message: String) {
        print(message)
        self.view.hideLoader()
    }
    func dataFound(status: Bool, message: String) {
        print(message)
        self.view.hideLoader()
        shoppingBagCountLabel.text = "\(presenter.shoppinBagCount)"
        womenLandingTableView.reloadData()
    }
}

extension LandingViewController: HotmessCollectionTableViewCellProtocol {
    func navigateToDeatilScreen(skuID: String, productID: String) {
        let storyBoard = UIStoryboard(name: "ProductOrderDetail", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ProductOrderDetailViewController")
                as? ProductOrderDetailViewController else { return }
        nextvc.presenter.productSKUID = skuID
        nextvc.presenter.productID = productID
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
extension LandingViewController: TrendingProductsTableViewCellProtocol {
    func naviagteToDetailScreen(skuID: String, productID: String) {
        let storyBoard = UIStoryboard(name: "ProductOrderDetail", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ProductOrderDetailViewController")
                as? ProductOrderDetailViewController else { return }
        nextvc.presenter.productSKUID = skuID
        nextvc.presenter.productID = productID
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
extension LandingViewController: DressRefreshTableViewCellProtocol {
    func naviagteToProductList(filters: String) {
        let storyBoard = UIStoryboard(name: "ProductListing", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ProductListViewController")
                as? ProductListViewController else { return }
        nextvc.presenter.titleLabelText = presenter.dressRefreshInfo.dressRefreshTitle
        nextvc.presenter.filters = presenter.getDressRefressFilters(filterListFromCell: filters)
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
