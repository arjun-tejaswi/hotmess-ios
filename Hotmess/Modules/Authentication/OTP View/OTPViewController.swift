//
//  OTPViewController.swift
//  Hotmess
//
//  Created by Flaxon on 12/06/21.
//

import UIKit
import SVPinView

class OTPViewController: UIViewController {
    @IBOutlet weak var editEmailButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var didntReceiveLabel: UILabel!
    @IBOutlet weak var otpSentToMobileLabel: UILabel!
    @IBOutlet weak var verifymobileTitleLabel: UILabel!
    @IBOutlet weak var otpPinView: SVPinView!
    var presenter = OTPPresenter()
    var enteredPin: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        otpPinViewSetUp()
        applyStyling()
        applyFont()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func editEmailButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func resendButtonAction(_ sender: UIButton) {
        presenter.resendOTPRequest()
        self.view.showLoader()
    }
    @IBAction func verifyButtonAction(_ sender: UIButton) {
        presenter.verifyOTPRequest(code: enteredPin)
        self.view.showLoader()
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        verifymobileTitleLabel.textColor = ColourConstants.hex191919
        otpSentToMobileLabel.textColor = ColourConstants.hex717171
        didntReceiveLabel.textColor = ColourConstants.hex717171
        backButton.layer.borderWidth = 1
        backButton.layer.borderColor = ColourConstants.hex9B9B9B.cgColor
        if !verifyButton.isEnabled {
            verifyButton.backgroundColor = ColourConstants.hexF0F0F0
            verifyButton.titleLabel?.tintColor = ColourConstants.hex9B9B9B
        }
        let resendUnderLineAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: FontConstants.regular, size: 12) ?? UIFont.systemFont(ofSize: 12),
            .foregroundColor: ColourConstants.hex191919,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let resendAttribute = NSMutableAttributedString(string: "Resend",
                                                        attributes: resendUnderLineAttributes)
        resendButton.setAttributedTitle(resendAttribute, for: .normal)
        let editEmailUnderLineAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: FontConstants.regular, size: 12) ?? UIFont.systemFont(ofSize: 12),
            .foregroundColor: ColourConstants.hex191919,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let editEmailAttribute = NSMutableAttributedString(string: "Edit Email",
                                                           attributes: editEmailUnderLineAttributes)
        if presenter.otpFromForgotPassword {
            editEmailButton.isHidden = false
            editEmailButton.setAttributedTitle(editEmailAttribute, for: .normal)
        }
        editEmailButton.setAttributedTitle(editEmailAttribute, for: .normal)
        verifyButton.applyButtonLetterSpacing(spacing: 2.16)
        backButton.applyButtonLetterSpacing(spacing: 2.16)
    }
    // MARK: - Font Styling Method
    func applyFont() {
        verifymobileTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        otpSentToMobileLabel.font = UIFont(name: FontConstants.regular, size: 12)
        didntReceiveLabel.font = UIFont(name: FontConstants.regular, size: 12)
        verifyButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
        backButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
    }
    // MARK: - OTP View SetUp
    func otpPinViewSetUp() {
        otpPinView.style = .box
        otpPinView.pinLength = 6
        otpPinView.activeBorderLineColor = ColourConstants.hex717171
        otpPinView.borderLineColor = ColourConstants.hex717171
        otpPinView.activeBorderLineThickness = 1
        otpPinView.borderLineThickness = 1
        otpPinView.textColor = ColourConstants.hex191919
        otpPinView.shouldSecureText = true
        otpPinView.keyboardAppearance = .default
        otpPinView.keyboardType = .numberPad
        otpPinView.didChangeCallback = { pin in
            self.backButton.isHidden = true
            if pin.count == 6 {
                self.verifyButton.isEnabled = true
                self.verifyButton.backgroundColor = ColourConstants.hex191919
            }
            self.enteredPin = pin
        }
    }
}
extension OTPViewController: OTPPresenterProtocol {
    func resendOTPValidate(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if !status {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
    func proceedTOResetPassword(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            let storyboardNewPassword = UIStoryboard(name: "SetNewPassword", bundle: nil)
            guard let setNewPassword = storyboardNewPassword.instantiateViewController(
                    withIdentifier: "SetNewPasswordViewController") as? SetNewPasswordViewController else {
                return
            }
            setNewPassword.presenter.userEmail = presenter.userEmail
            self.navigationController?.view.setNavigationTransitionLayer()
            self.navigationController?.pushViewController(setNewPassword, animated: true)
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
    func accountCreated(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            let storyboardAccountCreated = UIStoryboard(name: "AccountCreated", bundle: nil)
            guard let accountCreated = storyboardAccountCreated.instantiateViewController(
                    withIdentifier: "AccountCreatedViewController") as? AccountCreatedViewController else {
                return
            }
            accountCreated.presenter.messageTitle = presenter.titleMessage
            accountCreated.presenter.messageSubTitle = presenter.subTitleMessage
            self.navigationController?.view.setNavigationTransitionLayer()
            self.navigationController?.pushViewController(accountCreated, animated: true)
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
}
