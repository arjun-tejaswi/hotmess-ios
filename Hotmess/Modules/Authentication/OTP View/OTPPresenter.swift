//
//  OTPPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 14/06/21.
//

import Foundation
import SwiftyJSON

protocol OTPPresenterProtocol: AnyObject {
    func accountCreated(status: Bool, title: String, message: String)
    func proceedTOResetPassword(status: Bool, title: String, message: String)
    func resendOTPValidate(status: Bool, title: String, message: String)
}
class OTPPresenter: NSObject {
    weak var delegate: OTPPresenterProtocol?
    var titleMessage: String = ""
    var subTitleMessage: String = ""
    var otpFromForgotPassword: Bool = false
    var userEmail: String = ""
    var userSession: String = ""
    var userIDToken: String = ""
    func verifyOTPRequest(code: String) {
        print(userEmail, userSession, code)
        let parameters: [String: Any] = [
            "session": userSession,
            "otp": code,
            "email": userEmail
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.verifyOTP,
            prametres: parameters,
            type: .withoutAuth,
            method: .POST,
            completion: self.handleOTPResponse)
    }
    func resendOTPRequest() {
        if otpFromForgotPassword {
            NetworkManager.sharedInstance.makeNetworkRequest(
                urlString: ApiConstants.requestOTP,
                prametres: ["email": userEmail],
                type: .withoutAuth,
                method: .POST,
                completion: self.handleResendOTPResponse)
        } else {
            NetworkManager.sharedInstance.makeNetworkRequest(
                urlString: ApiConstants.requestOTP,
                prametres: [
                    "email": userEmail,
                    "otpType": "CUSTOMER_SIGNUP"],
                type: .withoutAuth,
                method: .POST,
                completion: self.handleResendOTPResponse)
        }
    }
    func requestAddToBagProduct() {
        let allProductsInBag = RealmDataManager.sharedInstance.getAllProductsFromBag()
        var arrayOfItems = [[String: Any]]()
        for product in allProductsInBag {
            let item: [String: Any] = [
                "product_id": product.productID,
                "sku_id": product.stockKeepUnitID,
                "quantity": product.productQuantity
            ]
            arrayOfItems.append(item)
        }
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.addToBag,
            prametres: arrayOfItems,
            type: .withAuth,
            method: .POST,
            completion: self.handleAddToBagResponse)
    }
    func requestAddToWishlist() {
        let allProductsInWishList = RealmDataManager.sharedInstance.getAllProductsFromWishlist()
        var arrayOfItems = [[String: Any]]()
        for product in allProductsInWishList {
            let item: [String: Any] = [
                "product_id": product.productID,
                "sku_id": product.stockKeepUnitID
            ]
            arrayOfItems.append(item)
            RealmDataManager.sharedInstance.deleteFromBag(
                productId: product.productID,
                skuID: product.stockKeepUnitID)
        }
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.addToWishlist,
            prametres: arrayOfItems,
            type: .withAuth,
            method: .POST,
            completion: self.handleAddToWishlistResponse)
    }
    func handleAddToWishlistResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.requestAddToWishlist()
                        }
                    }
                }
            } else {
                RealmDataManager.sharedInstance.deleteAllFromWishlist()
                NotificationCenter.default.post(name: Notification.Name("WISHLIST_STATUS"), object: nil)
            }
        }
    }
    func handleAddToBagResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.requestAddToBagProduct()
                        }
                    } else {
                        delegate?.accountCreated(status: false, title: "Error", message: message)
                    }
                }
            } else {
                RealmDataManager.sharedInstance.deleteAllFromBag()
                UserDefaults.standard.setValue(true, forKey: "isLoggedIn")
                delegate?.accountCreated(status: true, title: "", message: "")
            }
        }
    }
    func handleOTPResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                delegate?.accountCreated(status: false, title: "Error", message: response["message"].string ?? "")
            } else {
                if let data = response["data"].dictionary {
                    UserDefaults.standard.setValue(data["id_token"]?.string ?? "", forKey: "userIdToken")
                    NetworkManager.sharedInstance.updateUserIdToken()
                    if otpFromForgotPassword {
                        userEmail = data["email"]?.string ?? ""
                        delegate?.proceedTOResetPassword(status: true, title: "", message: "")
                    } else {
                        titleMessage = data["title"]?.string ?? ""
                        subTitleMessage = data["description"]?.string ?? ""
                        RealmDataManager.sharedInstance.updateUserDetails(
                            accessToken: data["access_token"]?.string ?? "",
                            refreshToken: data["refresh_token"]?.string ?? "",
                            idToken: data["id_token"]?.string ?? "")
                        if RealmDataManager.sharedInstance.getAllProductsFromWishlist().count > 0 {
                            requestAddToWishlist()
                        }
                        if RealmDataManager.sharedInstance.getAllProductsFromBag().count > 0 {
                            requestAddToBagProduct()
                        } else {
                            UserDefaults.standard.setValue(true, forKey: "isLoggedIn")
                            delegate?.accountCreated(status: true, title: "", message: "")
                        }
                    }
                } else {
                    delegate?.accountCreated(status: false, title: "Error", message: "Server Error")
                }
            }
        }
    }
    func handleResendOTPResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                delegate?.resendOTPValidate(status: false, title: "Error", message: response["message"].string ?? "" )
            } else {
                if let data = response["data"].dictionary {
                    userEmail = data["email"]?.string ?? ""
                    userSession = data["session"]?.string ?? ""
                    delegate?.resendOTPValidate(status: true, title: "", message: "")
                } else {
                    delegate?.resendOTPValidate(status: false, title: "Error", message: "Server Error")
                }
            }
        }
    }
}
