//
//  Authentication.swift
//  Hotmess
//
//  Created by Flaxon on 11/06/21.
//

import SwiftyJSON
import GoogleSignIn

class AuthenticationParser: NSObject {
    static let sharedInstance = AuthenticationParser()
    var otpToSignUp: Bool = false
    func parseUserData(data: [String: JSON]) -> UserRealm {
        let userData = UserRealm()
        userData.userFirstName = data["firstName"]?.string ?? ""
        userData.userLastName = data["lastName"]?.string ?? ""
        userData.userEmail = data["email"]?.string ?? ""
        userData.userMobileNumber = data["phoneNumber"]?.string ?? ""
        userData.userImageURL = data["imageUrl"]?.string ?? ""
        userData.verifiedEmailID = data["emailVerified"]?.bool ?? false
        userData.verifiedMobileNumeber = data["phoneVerified"]?.bool ?? false
        userData.userId = data["userId"]?.string ?? ""
        userData.countryCode = data["countryCode"]?.string ?? ""
        userData.idToken = data["id_token"]?.string ?? ""
        userData.accessToken = data["access_token"]?.string ?? ""
        userData.refreshToken = data["refresh_token"]?.string ?? ""
        return userData
    }
}
