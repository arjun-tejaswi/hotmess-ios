//
//  AccountCreatedViewController.swift
//  Hotmess
//
//  Created by Flaxon on 12/06/21.
//

import UIKit

class AccountCreatedViewController: UIViewController {
    @IBOutlet weak var viewAccountButton: UIButton!
    @IBOutlet weak var accountCreatedLabel: UILabel!
    @IBOutlet weak var accountCreatedDescriptionLabel: UILabel!
    var presenter = AccountCreatedPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyling()
        applyFont()

    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func viewAccountButtonAction(_ sender: UIButton) {
        if presenter.fromSetNewPassword {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as
                [UIViewController]
            self.navigationController!.popToViewController(
                viewControllers[viewControllers.count - 5],
                animated: true)
        } else {
            NotificationCenter.default.post(name: Notification.Name("WISHLIST_STATUS"), object: nil)
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as
                [UIViewController]
            self.navigationController!.popToViewController(
                viewControllers[viewControllers.count - 5],
                animated: true)
        }
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        accountCreatedLabel.text = presenter.messageTitle
        accountCreatedLabel.textColor = ColourConstants.hex191919
        accountCreatedDescriptionLabel.text = presenter.messageSubTitle
        accountCreatedDescriptionLabel.textColor = ColourConstants.hex191919
        viewAccountButton.applyButtonLetterSpacing(spacing: 2.16)
    }
    // MARK: - Font Styling Method
    func applyFont() {
        accountCreatedLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        accountCreatedDescriptionLabel.font = UIFont(name: FontConstants.regular, size: 10)
    }
}
