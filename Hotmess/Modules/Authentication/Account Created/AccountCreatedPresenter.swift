//
//  AccountCreatedPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 16/06/21.
//

import Foundation

class AccountCreatedPresenter: NSObject {
    var messageTitle: String = ""
    var messageSubTitle: String = ""
    var fromSetNewPassword: Bool = false
}
