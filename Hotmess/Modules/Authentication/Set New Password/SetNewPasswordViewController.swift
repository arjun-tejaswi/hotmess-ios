//
//  SetNewPasswordViewController.swift
//  Hotmess
//
//  Created by Flaxon on 14/06/21.
//

import UIKit

class SetNewPasswordViewController: UIViewController {
    @IBOutlet weak var showNewPasswordButton: UIButton!
    @IBOutlet weak var showConfirmNewPasswordButton: UIButton!
    @IBOutlet weak var updatePasswordConstraintLabel: UILabel!
    @IBOutlet weak var confirmNewPasswordTopLabel: UILabel!
    @IBOutlet weak var newPasswordTopLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var updatePasswordButton: UIButton!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var createNewPasswordLabel: UILabel!
    @IBOutlet weak var setNewPasswordTitleLabel: UILabel!
    var presenter = SetNewPasswordPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyling()
        applyFont()
        presenter.delegate = self
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func showConfirmNewPasswordButtonAction(_ sender: UIButton) {
        if showConfirmNewPasswordButton.isSelected == true {
            confirmPasswordTextField.isSecureTextEntry = false
            showConfirmNewPasswordButton.setImage(UIImage(named: "showPasswodIcon"), for: .normal)
            showConfirmNewPasswordButton.isSelected = false
        } else {
            confirmPasswordTextField.isSecureTextEntry = true
            showConfirmNewPasswordButton.setImage(UIImage(named: "hidePasswordIcon"), for: .normal)
            showConfirmNewPasswordButton.isSelected = true
        }
    }
    @IBAction func showNewPasswordButtonAction(_ sender: UIButton) {
        if showNewPasswordButton.isSelected == true {
            newPasswordTextField.isSecureTextEntry = false
            showNewPasswordButton.setImage(UIImage(named: "showPasswodIcon"), for: .normal)
            showNewPasswordButton.isSelected = false
        } else {
            newPasswordTextField.isSecureTextEntry = true
            showNewPasswordButton.setImage(UIImage(named: "hidePasswordIcon"), for: .normal)
            showNewPasswordButton.isSelected = true
        }
    }
    @IBAction func updatePasswordButtonAction(_ sender: UIButton) {
        presenter.updatePasswordRequest(data: UpdatePasswordModel.init(
                                            newPassword: newPasswordTextField.text ?? "",
                                            confirmNewPassword: confirmPasswordTextField.text ?? ""))
        self.view.showLoader()
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - TextField Styling Method
    func aplyTextFieldStyling(textFields: [UITextField]) {
        for textField in textFields {
            textField.font = UIFont(name: FontConstants.regular, size: 13)
            textField.layer.borderWidth = 1
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
            textField.textColor = ColourConstants.hex191919
            textField.setLeftPaddingPoints(10)
            textField.delegate = self
        }
    }
    // MARK: - UI styling Method
    func applyStyling() {
        aplyTextFieldStyling(textFields: [
            newPasswordTextField,
            confirmPasswordTextField
        ])
        updatePasswordButton.applyButtonLetterSpacing(spacing: 2.16)
        backButton.applyButtonLetterSpacing(spacing: 2.16)
    }
    // MARK: - Font Styling Method
    func applyFont() {
        setNewPasswordTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        createNewPasswordLabel.font = UIFont(name: FontConstants.regular, size: 12)
        updatePasswordConstraintLabel.font = UIFont(name: FontConstants.regular, size: 11)
        newPasswordTopLabel.font = UIFont(name: FontConstants.regular, size: 8)
        confirmNewPasswordTopLabel.font = UIFont(name: FontConstants.regular, size: 8)
    }
}
// MARK: Set New Password TextField Delegate Method
extension SetNewPasswordViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == newPasswordTextField {
            newPasswordTopLabel.isHidden = false
        } else {
            newPasswordTopLabel.isHidden = true
        }
        if textField == confirmPasswordTextField {
            confirmNewPasswordTopLabel.isHidden = false
        } else {
            confirmNewPasswordTopLabel.isHidden = true
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text != "" {
            updatePasswordButton.isEnabled = true
            updatePasswordButton.backgroundColor = ColourConstants.hex191919
        } else {
            updatePasswordButton.isEnabled = false
            updatePasswordButton.backgroundColor = ColourConstants.hexF0F0F0
        }
    }
}
// MARK: Set New Password Protocol Method
extension SetNewPasswordViewController: SetNewPasswordPresenterProtocol {
    func passwordUpdated(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            let storyboardAccountCreated = UIStoryboard(name: "AccountCreated", bundle: nil)
            guard let accountCreated = storyboardAccountCreated.instantiateViewController(
                    withIdentifier: "AccountCreatedViewController") as? AccountCreatedViewController else {
                return
            }
            accountCreated.presenter.messageTitle = presenter.messagetitle
            accountCreated.presenter.messageSubTitle = presenter.messageDescription
            accountCreated.presenter.fromSetNewPassword = true
            self.navigationController?.view.setNavigationTransitionLayer()
            self.navigationController?.pushViewController(accountCreated, animated: true)
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
    func validatePassword(title: String, message: String) {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: title,
            subTitle: message,
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)    }
}
