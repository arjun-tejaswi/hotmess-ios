//
//  SetNewPasswordPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 22/06/21.
//

import Foundation
import SwiftyJSON

protocol SetNewPasswordPresenterProtocol: AnyObject {
    func validatePassword(title: String, message: String)
    func passwordUpdated(status: Bool, title: String, message: String)
}
class SetNewPasswordPresenter: NSObject {
    weak var delegate: SetNewPasswordPresenterProtocol?
    var userEmail: String = ""
    var messagetitle: String = ""
    var messageDescription: String = ""
    // MARK: - Update Password Request Method
    func updatePasswordRequest(data: UpdatePasswordModel) {
        print(userEmail, UserDefaults.standard.string(forKey: "userIdToken") ?? "")
        if data.newPassword != data.confirmNewPassword {
            delegate?.validatePassword(title: "Error", message: "Entered Passwords are not same!")
        } else if data.newPassword == "" || data.confirmNewPassword == "" {
            delegate?.validatePassword(title: "Error", message: "Enter Both the Fields!")
        } else {
            NetworkManager.sharedInstance.makeNetworkRequest(
                urlString: ApiConstants.updatePassword,
                prametres: [
                    "email": userEmail,
                    "password": data.newPassword ?? ""
                ],
                type: .withAuth,
                method: .POST) { response in
                if let status = response["error_status"].bool {
                    if status {
                        if let message = response["message"].string {
                            if message == "requestForRefresh" {
                                NetworkManager.sharedInstance.refreshBearerToken {
                                    self.updatePasswordRequest(data: data)
                                }
                            } else {
                                self.delegate?.passwordUpdated(status: false,
                                                               title: "Error",
                                                               message: message)
                            }
                        }
                    } else {
                        if let data = response["data"].dictionary {
                            self.messagetitle = data["title"]?.string ?? ""
                            self.messageDescription = data["description"]?.string ?? ""
                            self.delegate?.passwordUpdated(status: true, title: "", message: "")
                        } else {
                            self.delegate?.passwordUpdated(status: false, title: "Error", message: "Server Error")
                        }
                    }
                }
            }
        }
    }
}
