//
//  SignUpPopUp.swift
//  Hotmess
//
//  Created by Flaxon on 11/06/21.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import AuthenticationServices

class SignUpViewController: UIViewController {
    @IBOutlet weak var mobileNumberView: UIView!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var mobileNoTopLabel: UILabel!
    @IBOutlet weak var emailTopLabel: UILabel!
    @IBOutlet weak var confirmPasswordTopLabel: UILabel!
    @IBOutlet weak var passwordTopLabel: UILabel!
    @IBOutlet weak var lastNameTopLabel: UILabel!
    @IBOutlet weak var firstNameTopLabel: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var alreadyHaveAccountLabel: UILabel!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var showConfirmPasswordButton: UIButton!
    @IBOutlet weak var showPasswordButton: UIButton!
    @IBOutlet weak var termsAndConditionsLabel: UILabel!
    @IBOutlet weak var promotionalOfferLabel: UILabel!
    @IBOutlet weak var termsAndConditionsCheckBoxButton: UIButton!
    @IBOutlet weak var promotionalOffersCheckBoxButton: UIButton!
    @IBOutlet weak var registerWithLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var signUpLabel: UILabel!
    var presenter: SignUpPresenter!
    var fbLogin = LoginManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = SignUpPresenter()
        GIDSignIn.sharedInstance()?.presentingViewController = self
        presenter.delegate = self
        applyStyling()
        applyFont()
        textfieldSetUp()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func appleSignUpButtonAction(_ sender: UIButton) {
        appleSignUp()
    }
    @IBAction func googleSignUpButtonAction(_ sender: UIButton) {
        self.view.showLoader()
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    @IBAction func loginButtonAction(_ sender: UIButton) {
        self.navigationController?.view.closeNavigationTransitionLayer()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func continueButtonAction(_ sender: UIButton) {
        presenter.makeSignUp(data: SignUpDataModel.init(
                                firstname: firstNameTextField.text ?? "",
                                lastname: lastNameTextField.text ?? "",
                                password: passwordTextField.text ?? "",
                                confirmPassword: confirmPasswordTextField.text ?? "",
                                email: emailTextField.text ?? "",
                                code: "+91",
                                mobileNumber: mobileNumberTextField.text ?? "")
        )
        self.view.showLoader()
    }
    @IBAction func faceBookSignUpButtonAction(_ sender: UIButton) {
        fbLogin.logOut()
        fbLogin.logIn(permissions: ["public_profile", "email"], from: self) { (_, error) in
            if error != nil {
                let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                    bounds: self.view.bounds,
                    title: "Error",
                    subTitle: "Facebook login failed. Please try again.",
                    buttonTitle: "OK") { return }
                self.view.addSubview(popUp)
                self.view.isUserInteractionEnabled = true
            } else {
                self.getFacebookUserDetails()
            }
        }
    }
    @IBAction func showPasswordActionButton(_ sender: UIButton) {
        if showPasswordButton.isSelected == true {
            passwordTextField.isSecureTextEntry = false
            showPasswordButton.setImage(UIImage(named: "showPasswodIcon"), for: .normal)
            showPasswordButton.isSelected = false
        } else {
            passwordTextField.isSecureTextEntry = true
            showPasswordButton.setImage(UIImage(named: "hidePasswordIcon"), for: .normal)
            showPasswordButton.isSelected = true
        }
    }
    @IBAction func confirmShowPasswordButtonAction(_ sender: UIButton) {
        if showConfirmPasswordButton.isSelected == true {
            confirmPasswordTextField.isSecureTextEntry = false
            showConfirmPasswordButton.setImage(UIImage(named: "showPasswodIcon"), for: .normal)
            showConfirmPasswordButton.isSelected = false
        } else {
            confirmPasswordTextField.isSecureTextEntry = true
            showConfirmPasswordButton.setImage(UIImage(named: "hidePasswordIcon"), for: .normal)
            showConfirmPasswordButton.isSelected = true
        }
    }
    @IBAction func termsAndConditionsButtonAction(_ sender: UIButton) {
        if presenter.isTermsAgreed == false {
            termsAndConditionsCheckBoxButton.setImage(UIImage(named: "checkboxActive"), for: .normal)
            presenter.isTermsAgreed = true
            continueButton.isEnabled = true
            continueButton.backgroundColor = ColourConstants.hex191919
        } else {
            termsAndConditionsCheckBoxButton.setImage(UIImage(named: "checkbox"), for: .normal)
            presenter.isTermsAgreed = false
            continueButton.isEnabled = false
            continueButton.backgroundColor = ColourConstants.hexF0F0F0
        }
    }
    @IBAction func promotionalOffersButtonAction(_ sender: UIButton) {
        if presenter.isPromotionalAgreed == false {
            promotionalOffersCheckBoxButton.setImage(UIImage(named: "checkboxActive"), for: .normal)
            presenter.isPromotionalAgreed = true
            continueButton.isEnabled = true
        } else {
            promotionalOffersCheckBoxButton.setImage(UIImage(named: "checkbox"), for: .normal)
            presenter.isPromotionalAgreed = false
            continueButton.isEnabled = false
        }
    }
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.navigationController?.view.closeNavigationTransitionLayer()
        self.navigationController?.popViewController(animated: true)
    }
}
extension SignUpViewController {
    // MARK: - Apple SignUp Request Method
    @objc func appleSignUp() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }
    }
    // MARK: - TextField Delegate SetUp Method
    func textfieldSetUp() {
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        emailTextField.delegate = self
        mobileNumberTextField.delegate = self
    }
    // MARK: - Facebook User Details Method
    func getFacebookUserDetails() {
        if AccessToken.current != nil {
            guard let accessToken = FBSDKLoginKit.AccessToken.current else { return }
            let graphRequest = FBSDKLoginKit.GraphRequest(
                graphPath: "me",
                parameters: ["fields": "first_name, last_name, email, picture"],
                tokenString: accessToken.tokenString,
                version: nil,
                httpMethod: .get)
            graphRequest.start { (_, result, error) -> Void in
                if error == nil {
                    self.view.showLoader()
                    self.presenter.makeFacebookSignUp(result: result ?? "")
                } else {
                    self.view.hideLoader()
                }
            }
        } else {
            self.view.isUserInteractionEnabled = true
            self.view.hideLoader()
        }
    }
    // MARK: - TextField Styling
    func applyTextFieldStyling(textFields: [UITextField]) {
        for textField in textFields {
            textField.font = UIFont(name: FontConstants.regular, size: 13)
            textField.layer.borderWidth = 1
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
            textField.textColor = ColourConstants.hex191919
            textField.setLeftPaddingPoints(10)
        }
    }
    @objc func maxValueReached(_ textField: UITextField) {
        if let textValue = textField.text {
            if textValue.count >= 10 {
                self.view.endEditing(true)
            }
        }
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        signUpLabel.textColor = ColourConstants.hex191919
        applyTextFieldStyling(textFields: [
            firstNameTextField,
            lastNameTextField,
            passwordTextField,
            confirmPasswordTextField,
            emailTextField
        ])
        mobileNumberTextField.addTarget(self, action: #selector(maxValueReached), for: .editingChanged)
        mobileNumberView.layer.borderWidth = 1
        mobileNumberView.layer.borderColor = ColourConstants.hex717171.cgColor
        mobileNumberTextField.textColor = ColourConstants.hex191919
        mobileNumberTextField.setLeftPaddingPoints(10)
        countryCodeLabel.textColor = ColourConstants.hex191919
        countryCodeLabel.applyLetterSpacing(spacing: 3.9)
        loginView.layer.borderWidth = 1
        loginView.layer.borderColor = ColourConstants.hexD3D3D3.cgColor
        promotionalOfferLabel.textColor = ColourConstants.hex717171
        termsAndConditionsLabel.textColor = ColourConstants.hex717171
        continueButton.titleLabel?.textColor = ColourConstants.hexCECECE
        continueButton.applyButtonLetterSpacing(spacing: 2.16)
        termsAndConditionsLabel.attributedText = presenter.createTermsAttributedText(
            text: termsAndConditionsLabel.text ??
                "By registering, you agree to our Terms and Conditions & Privacy Policy")
        termsAndConditionsLabel.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapLabel(_:)))
        termsAndConditionsLabel.addGestureRecognizer(tap)
    }
    // MARK: - Font Styling Method
    func applyFont() {
        signUpLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        registerWithLabel.font = UIFont(name: FontConstants.regular, size: 14)
        loginLabel.font = UIFont(name: FontConstants.medium, size: 16)
        alreadyHaveAccountLabel.font = UIFont(name: FontConstants.regular, size: 12)
        mobileNumberTextField.font = UIFont(name: FontConstants.regular, size: 13)
        countryCodeLabel.font = UIFont(name: FontConstants.regular, size: 13)
        promotionalOfferLabel.font = UIFont(name: FontConstants.light, size: 10)
        termsAndConditionsLabel.font = UIFont(name: FontConstants.light, size: 10)
        continueButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
        firstNameTopLabel.font = UIFont(name: FontConstants.regular, size: 8)
        lastNameTopLabel.font = UIFont(name: FontConstants.regular, size: 8)
        passwordTopLabel.font = UIFont(name: FontConstants.regular, size: 8)
        confirmPasswordTopLabel.font = UIFont(name: FontConstants.regular, size: 8)
        emailTopLabel.font = UIFont(name: FontConstants.regular, size: 8)
        mobileNoTopLabel.font = UIFont(name: FontConstants.regular, size: 8)
    }
    // MARK: - T&C Label Tap Method
    @objc func tapLabel(_ sender: UITapGestureRecognizer) {
        let storyBoard = UIStoryboard(name: "WebView", bundle: nil)
        guard let webViewVC = storyBoard.instantiateViewController(
                withIdentifier: "WebViewController") as? WebViewController else {
            return
        }
        let text = termsAndConditionsLabel.text ??
            "By registering, you agree to our Terms and Conditions & Privacy Policy"
        let termsRange = (text as NSString).range(of: "Terms and Conditions")
        let privacyRange = (text as NSString).range(of: "Privacy Policy")
        if sender.didTapAttributedTextInLabel(
            label: termsAndConditionsLabel, inRange: termsRange) {
            self.navigationController?.pushViewController(webViewVC, animated: true)
        } else if sender.didTapAttributedTextInLabel(
                    label: termsAndConditionsLabel,
                    inRange: privacyRange) {
            webViewVC.state = .PRIVACY
            self.navigationController?.pushViewController(webViewVC, animated: true)
        }
    }
}
