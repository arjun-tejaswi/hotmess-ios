//
//  SignUpPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 11/06/21.
//

import GoogleSignIn
import SwiftyJSON

protocol SignUpPresenterProtocol: AnyObject {
    func fillAllFields()
    func passwordMismatch()
    func acceptPromotional()
    func acceptTerms()
    func hideLoader()
    func signUpError(message: String)
    func loginValidate(status: Bool, title: String, message: String)
    func proceedToOTP(status: Bool, title: String, message: String)
}

class SignUpPresenter: NSObject {
    weak var delegate: SignUpPresenterProtocol?
    var isTermsAgreed: Bool = false
    var isPromotionalAgreed: Bool = false
    var proceedToOTPScreen: Bool = false
    var userEmail: String = ""
    var userSession: String = ""
    // MARK: - API Request For SignUp With Email And Password
    func makeSignUp(data: SignUpDataModel) {
        if data.firstname == "" || data.lastname == "" ||
            data.password == "" || data.confirmPassword == "" ||
            data.email == "" || data.code == "" ||
            data.mobileNumber == "" {
            delegate?.fillAllFields()
        } else if data.password != data.confirmPassword {
            delegate?.passwordMismatch()
        } else if !isPromotionalAgreed {
            delegate?.acceptPromotional()
        } else if !isTermsAgreed {
            delegate?.acceptTerms()
        } else {
            let parameters: [String: Any] = [
                "email": data.email ?? "",
                "firstName": data.firstname ?? "",
                "password": data.password ?? "",
                "lastName": data.lastname ?? "",
                "countryCode": data.code ?? "",
                "phoneNumber": data.mobileNumber ?? "",
                "deviceType": "0"
            ]
            NetworkManager.sharedInstance.makeNetworkRequest(
                urlString: ApiConstants.userSignUp,
                prametres: parameters,
                type: .withoutAuth,
                method: .POST,
                completion: self.handleSignupResponse)
        }
    }
    // MARK: - API Request For SignUp With Facebook
    func makeFacebookSignUp(result: Any) {
        let resultJSON = JSON(result)
        print(resultJSON)
        let email = resultJSON["email"].string ?? ""
        let firstName = resultJSON["first_name"].string ?? ""
        let lastName = resultJSON["last_name"].string ?? ""
        let pictureURL = resultJSON["picture"]["data"]["url"].string ?? ""
        let parameters: [String: Any] = [
            "email": email,
            "firstName": firstName,
            "lastName": lastName,
            "deviceType": "0",
            "imageUrl": pictureURL
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.faceBookSignUp,
            prametres: parameters,
            type: .withoutAuth,
            method: .POST,
            completion: self.handleSignupResponse)
    }
    // MARK: - API Request For SignUp With Google
    func makeGoogleSignUp(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("Google sign in error: \(error)")
            }
            delegate?.hideLoader()
            return
        }
        var userImage: URL!
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let userEmail = user.profile.email
        if let profileImage = user.profile.imageURL(withDimension: 100) {
            userImage = profileImage
        }
        let parameters: [String: Any] = [
            "email": userEmail ?? "",
            "firstName": givenName ?? "",
            "lastName": familyName ?? "",
            "imageUrl": userImage.absoluteString,
            "deviceType": "0"
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.googleSignUp,
            prametres: parameters,
            type: .withoutAuth,
            method: .POST,
            completion: self.handleSignupResponse)
    }
    // MARK: - API Request For SignUp With Apple
    func makeAppleSignUp(firstName: String, lastName: String, email: String, userId: String, token: String) {
        var parameters: [String: Any] = [
            "apple_user_id": userId,
            "apple_token": token
        ]
        if firstName != "" {
            parameters.updateValue(firstName, forKey: "first_name")
        }
        if lastName != "" {
            parameters.updateValue(lastName, forKey: "last_name")
        }
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.appleSignIn,
            prametres: parameters,
            type: .withoutAuth,
            method: .POST,
            completion: self.handleSignupResponse)
    }
    func requestAddToBagProduct() {
        let allProductsInBag = RealmDataManager.sharedInstance.getAllProductsFromBag()
        var arrayOfItems = [[String: Any]]()
        for product in allProductsInBag {
            let item: [String: Any] = [
                "product_id": product.productID,
                "sku_id": product.stockKeepUnitID,
                "quantity": product.productQuantity
            ]
            arrayOfItems.append(item)
        }
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.addToBag,
            prametres: arrayOfItems,
            type: .withAuth,
            method: .POST,
            completion: self.handleAddToBagResponse)
    }
    func requestAddToWishlist() {
        let allProductsInWishList = RealmDataManager.sharedInstance.getAllProductsFromWishlist()
        var arrayOfItems = [[String: Any]]()
        for product in allProductsInWishList {
            let item: [String: Any] = [
                "product_id": product.productID,
                "sku_id": product.stockKeepUnitID
            ]
            arrayOfItems.append(item)
            RealmDataManager.sharedInstance.deleteFromBag(
                productId: product.productID,
                skuID: product.stockKeepUnitID)
        }
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.addToWishlist,
            prametres: arrayOfItems,
            type: .withAuth,
            method: .POST,
            completion: self.handleAddToWishlistResponse)
    }
    func handleAddToWishlistResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.requestAddToWishlist()
                        }
                    }
                }
            } else {
                RealmDataManager.sharedInstance.deleteAllFromWishlist()
                NotificationCenter.default.post(name: Notification.Name("WISHLIST_STATUS"), object: nil)
            }
        }
    }
    func handleAddToBagResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.requestAddToBagProduct()
                        }
                    } else {
                        delegate?.loginValidate(status: false, title: "Error", message: message)
                    }
                }
            } else {
                RealmDataManager.sharedInstance.deleteAllFromBag()
                UserDefaults.standard.setValue(true, forKey: "isLoggedIn")
                delegate?.loginValidate(status: true, title: "", message: "")
            }
        }
    }
    // MARK: - SignUp Response Handler
    func handleSignupResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                delegate?.loginValidate(status: false, title: "Error", message: response["message"].string ?? "")
            } else {
                if let data = response["data"].dictionary {
                    let userData = AuthenticationParser.sharedInstance.parseUserData(data: data)
                    UserDefaults.standard.setValue(userData.idToken, forKey: "userIdToken")
                    NetworkManager.sharedInstance.updateUserIdToken()
                    RealmDataManager.sharedInstance.deleteUserDetails()
                    RealmDataManager.sharedInstance.saveUserDetails(details: userData)
                    userSession = data["session"]?.string ?? ""
                    if userSession != "" {
                        userEmail = data["email"]?.string ?? ""
                        UserDefaults.standard.setValue(false, forKey: "isLoggedIn")
                        delegate?.proceedToOTP(status: true, title: "", message: "")
                    } else {
                        if RealmDataManager.sharedInstance.getAllProductsFromWishlist().count > 0 {
                            requestAddToWishlist()
                        }
                        if RealmDataManager.sharedInstance.getAllProductsFromBag().count > 0 {
                            requestAddToBagProduct()
                        } else {
                            UserDefaults.standard.setValue(true, forKey: "isLoggedIn")
                            delegate?.loginValidate(status: true, title: "", message: "")
                        }
                    }
                }
            }
        }
    }
    // MARK: - Creating UnderLine to the Label Method
    func createTermsAttributedText(text: String) -> NSMutableAttributedString {
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms and Conditions")
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle,
                                          value: NSUnderlineStyle.single.rawValue,
                                          range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor,
                                          value: ColourConstants.hex717171,
                                          range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.font,
                                          value: UIFont(
                                            name: FontConstants.light, size: 10) ?? UIFont.systemFont(ofSize: 13),
                                          range: range1)
        let range2 = (text as NSString).range(of: "Privacy Policy")
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle,
                                          value: NSUnderlineStyle.single.rawValue,
                                          range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor,
                                          value: ColourConstants.hex717171,
                                          range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.font,
                                          value: UIFont(
                                            name: FontConstants.light, size: 10) ?? UIFont.systemFont(ofSize: 13),
                                          range: range2)
        return underlineAttriString
    }
}
