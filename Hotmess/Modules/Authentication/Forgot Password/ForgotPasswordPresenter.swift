//
//  ForgotPasswordPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 14/06/21.
//

import Foundation
import SwiftyJSON

protocol ForgotPasswordPresenterProtocol: AnyObject {
    func proceedToOTP(status: Bool, title: String, message: String)
}

class ForgotPasswordPresenter: NSObject {
    weak var delegate: ForgotPasswordPresenterProtocol?
    var otpToForgotPassword: Bool = false
    var userEmail: String = ""
    var userSession: String = ""
    // MARK: - Forgot Password API Request Method
    func makeOTPRequest(email: String) {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.forgotPassword,
            prametres: ["email": email],
            type: .withoutAuth,
            method: .POST,
            completion: self.handleForgotResponse)
    }
    // MARK: - Forgot Password Response Handler Method
    func handleForgotResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                delegate?.proceedToOTP(status: false, title: "Error", message: response["message"].string ?? "" )
            } else {
                if let data = response["data"].dictionary {
                    userEmail = data["email"]?.string ?? ""
                    userSession = data["session"]?.string ?? ""
                    otpToForgotPassword = true
                    delegate?.proceedToOTP(status: true, title: "", message: "")
                } else {
                    delegate?.proceedToOTP(status: false, title: "Error", message: "Server Error")
                }
            }
        }
    }

}
