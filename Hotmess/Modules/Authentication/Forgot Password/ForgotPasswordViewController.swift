//
//  ForgotPasswordViewController.swift
//  Hotmess
//
//  Created by Flaxon on 14/06/21.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailTopLabel: UILabel!
    @IBOutlet weak var requestOTPButton: UIButton!
    @IBOutlet weak var backbutton: UIButton!
    @IBOutlet weak var enterEmailMobileLabel: UILabel!
    @IBOutlet weak var forgotPasswordTitleLabel: UILabel!
    var presenter: ForgotPasswordPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ForgotPasswordPresenter()
        presenter.delegate = self
        applyStyling()
        applyFont()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func requestOTPButtonAction(_ sender: UIButton) {
        print(emailTextField.text ?? "")
        presenter.makeOTPRequest(email: emailTextField.text ?? "")
        self.view.showLoader()
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        forgotPasswordTitleLabel.textColor = ColourConstants.hex191919
        enterEmailMobileLabel.textColor = ColourConstants.hex717171
        emailTextField.delegate = self
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.borderColor = ColourConstants.hex717171.cgColor
        emailTextField.setLeftPaddingPoints(10)
        requestOTPButton.applyButtonLetterSpacing(spacing: 2.16)
        backbutton.backgroundColor = ColourConstants.hexFFFFFF
        backbutton.layer.borderWidth = 1
        backbutton.layer.borderColor = ColourConstants.hex9B9B9B.cgColor
        backbutton.applyButtonLetterSpacing(spacing: 2.16)
    }
    // MARK: - Font Styling Method
    func applyFont() {
        forgotPasswordTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        enterEmailMobileLabel.font = UIFont(name: FontConstants.regular, size: 12)
        requestOTPButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
        emailTopLabel.font = UIFont(name: FontConstants.regular, size: 8)
        backbutton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
    }
}
extension ForgotPasswordViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTextField {
            emailTopLabel.isHidden = false
        } else {
            emailTopLabel.isHidden = true
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if emailTextField.text != "" {
            requestOTPButton.isEnabled = true
            requestOTPButton.backgroundColor = ColourConstants.hex191919
        } else {
            requestOTPButton.isEnabled = false
            requestOTPButton.backgroundColor = ColourConstants.hexF0F0F0
        }
    }
}
extension ForgotPasswordViewController: ForgotPasswordPresenterProtocol {
    func proceedToOTP(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            let storyboardOTP = UIStoryboard(name: "OTPView", bundle: nil)
            guard let otpView = storyboardOTP.instantiateViewController(
                    withIdentifier: "OTPViewController") as? OTPViewController else {
                return
            }
            otpView.presenter.otpFromForgotPassword = presenter.otpToForgotPassword
            otpView.presenter.userEmail = presenter.userEmail
            otpView.presenter.userSession = presenter.userSession
            print(presenter.otpToForgotPassword, presenter.userEmail, presenter.userSession)
            self.navigationController?.view.setNavigationTransitionLayer()
            self.navigationController?.pushViewController(otpView, animated: true)
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
}
