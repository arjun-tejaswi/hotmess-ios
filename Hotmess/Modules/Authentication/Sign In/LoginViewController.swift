//
//  OrderPlacingLoginPopUp.swift
//  Hotmess
//
//  Created by Flaxon on 17/05/21.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import AuthenticationServices

class LoginViewController: UIViewController {
    @IBOutlet weak var emailTopLabel: UILabel!
    @IBOutlet weak var passwordTopLabel: UILabel!
    @IBOutlet weak var showPasswordButton: UIButton!
    @IBOutlet weak var signUpRegisterLabel: UILabel!
    @IBOutlet weak var signUpView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var dontHaveAccountLabel: UILabel!
    @IBOutlet weak var loginWithLabel: UILabel!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var loginTitleLabel: UILabel!
    @IBOutlet weak var appleSignInButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    var presenter: LoginPresenter!
    var fbLogin = LoginManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyling()
        applyFont()
        presenter = LoginPresenter()
        presenter.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func loginButtonAction(_ sender: UIButton) {
        self.view.showLoader()
        presenter.makeLogin(email: emailTextField.text ?? "", password: passwordTextField.text ?? "")
    }
    @IBAction func forgotPasswordButtonAtion(_ sender: UIButton) {
        let storyboardForgotPassword = UIStoryboard(name: "ForgotPassword", bundle: nil)
        guard let forgotPasswordView = storyboardForgotPassword.instantiateViewController(
                withIdentifier: "ForgotPasswordViewController") as? ForgotPasswordViewController else {
            return
        }
        self.navigationController?.view.setNavigationTransitionLayer()
        self.navigationController?.pushViewController(forgotPasswordView, animated: true)
    }
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.navigationController?.view.closeNavigationTransitionLayer()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func appleSignInButtonAction(_ sender: UIButton) {
        appleSignIn()
    }
    @IBAction func faceBookSinInButtonAction(_ sender: UIButton) {
        fbLogin.logOut()
        fbLogin.logIn(permissions: ["public_profile", "email"], from: self) { (_, error) in
            if error != nil {
                let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                                bounds: self.view.bounds,
                                title: "Error",
                                subTitle: "Facebook login failed. Please try again.",
                                buttonTitle: "OK") { return }
                            self.view.addSubview(popUp)
                self.view.isUserInteractionEnabled = true
            } else {
                self.getFacebookUserDetails()
            }
        }
    }
    @IBAction func googleSignInButtonAction(_ sender: UIButton) {
        self.view.showLoader()
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    @IBAction func signUpButtonAction(_ sender: UIButton) {
        let storyboardSignUp = UIStoryboard(name: "SignUp", bundle: nil)
        guard let signUpView = storyboardSignUp.instantiateViewController(
                withIdentifier: "SignUpViewController") as? SignUpViewController else {
            return
        }
        self.navigationController?.view.setNavigationTransitionLayer()
        self.navigationController?.pushViewController(signUpView, animated: true)
    }
    @IBAction func showHidePasswordButtonAction(_ sender: UIButton) {
        if showPasswordButton.isSelected == true {
            passwordTextField.isSecureTextEntry = false
            showPasswordButton.setImage(UIImage(named: "showPasswodIcon"), for: .normal)
            showPasswordButton.isSelected = false
        } else {
            passwordTextField.isSecureTextEntry = true
            showPasswordButton.setImage(UIImage(named: "hidePasswordIcon"), for: .normal)
            showPasswordButton.isSelected = true
        }
    }
    // MARK: - Facebook User Details Method
    func getFacebookUserDetails() {
        if AccessToken.current != nil {
            guard let accessToken = FBSDKLoginKit.AccessToken.current else { return }
            let graphRequest = FBSDKLoginKit.GraphRequest(
                graphPath: "me",
                parameters: ["fields": "first_name, last_name, email, picture"],
                tokenString: accessToken.tokenString,
                version: nil,
                httpMethod: .get)
            graphRequest.start { (_, result, error) -> Void in
                if error == nil {
                    self.view.showLoader()
                    self.presenter.makeFacebookSigIn(result: result ?? "")
                } else {
                    self.view.hideLoader()
                }
            }
        } else {
            self.view.isUserInteractionEnabled = true
            self.view.hideLoader()
        }
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.borderColor = ColourConstants.hex717171.cgColor
        emailTextField.textColor = ColourConstants.hex191919
        emailTextField.setLeftPaddingPoints(10)
        passwordTextField.layer.borderWidth = 1
        passwordTextField.layer.borderColor = ColourConstants.hex717171.cgColor
        passwordTextField.textColor = ColourConstants.hex191919
        passwordTextField.setLeftPaddingPoints(10)
        passwordTextField.delegate = self
        emailTextField.delegate = self
        let forgotPasswordUnderLineAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: FontConstants.regular, size: 12) ?? UIFont.systemFont(ofSize: 12),
            .foregroundColor: ColourConstants.hex717171,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let forgotPasswordAttribute = NSMutableAttributedString(string: "Forgot Password",
                                                        attributes: forgotPasswordUnderLineAttributes)
        forgotPasswordButton.setAttributedTitle(forgotPasswordAttribute, for: .normal)
        appleSignInButton.layer.cornerRadius = appleSignInButton.frame.width/2
        signUpView.layer.borderWidth = 1
        signUpView.layer.borderColor = ColourConstants.hexD3D3D3.cgColor
        signUpRegisterLabel.text = "SIGN UP / REGISTER"
        signUpRegisterLabel.textColor = ColourConstants.hex191919
        loginButton.setTitle("LOGIN", for: .normal)
        loginButton.applyButtonLetterSpacing(spacing: 2.16)
    }
    // MARK: - Font Style Method
    func applyFont() {
        emailTopLabel.font = UIFont(name: FontConstants.regular, size: 8)
        passwordTopLabel.font = UIFont(name: FontConstants.regular, size: 8)
        loginTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        loginWithLabel.font = UIFont(name: FontConstants.regular, size: 14)
        signUpRegisterLabel.font = UIFont(name: FontConstants.medium, size: 16)
        dontHaveAccountLabel.font = UIFont(name: FontConstants.regular, size: 12)
        emailTextField.font = UIFont(name: FontConstants.regular, size: 13)
        passwordTextField.font = UIFont(name: FontConstants.regular, size: 13)
        forgotPasswordButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
        loginButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
    }
    // MARK: - Apple SignUp Request Method
    @objc func appleSignIn() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }
    }
}
extension LoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if passwordTextField.text == "" {
            passwordTopLabel.isHidden = true
        }
        if emailTextField.text == "" {
            emailTopLabel.isHidden = true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTextField {
            emailTopLabel.isHidden = false
        }
        if textField == passwordTextField {
            passwordTopLabel.isHidden = false
        }
    }
}
// MARK: - Google Sign In Delegate Method
extension LoginViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        presenter.makeGoogleSignIn(signIn, didSignInFor: user, withError: error)
    }
}
// MARK: - Login Protocol Method
extension LoginViewController: LoginPresenterProtocol {
    func hideLoader() {
        self.view.hideLoader()
    }
    func loginValidate(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            NotificationCenter.default.post(name: Notification.Name("WISHLIST_STATUS"), object: nil)
            self.navigationController?.popViewController(animated: true)
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                            bounds: self.view.bounds,
                            title: title,
                            subTitle: message,
                            buttonTitle: "OK") { return }
                        self.view.addSubview(popUp)
        }
    }
}
// MARK: - Apple SignIn Delegate Methods
extension LoginViewController: ASAuthorizationControllerPresentationContextProviding,
                                ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    @available(iOS 13.0, *)
    func authorizationController(
        controller: ASAuthorizationController,
        didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            let token = appleIDCredential.identityToken
            let tokenString = String(data: token!, encoding: .utf8) ?? ""
            let userId = appleIDCredential.user
            let firstname = appleIDCredential.fullName?.givenName ?? ""
            let lastname = appleIDCredential.fullName?.familyName ?? ""
            let email = appleIDCredential.email ?? ""
            print(tokenString)
            getCredentialState(userId: userId)
            self.view.showLoader()
            presenter.makeAppleSignUp(
                firstName: firstname,
                lastName: lastname,
                email: email,
                userId: userId,
                token: tokenString)
        } else {
            AlertManager.sharedInstance.showAlertWith(
                title: "No Email",
                message: "Unable to get Name & Email from Apple Sign In, please try a different method to sign up.")
        }
    }
    func getCredentialState(userId: String) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            appleIDProvider.getCredentialState(forUserID: userId) { (credentialState, _) in
                switch credentialState {
                case .authorized:
                    print("Authorized")
                case .revoked:
                    print("Revoked")
                    self.appleSignIn()
                case .notFound:
                    print("Not found")
                    self.appleSignIn()
                default:
                    break
                }
            }
        }
    }
}
