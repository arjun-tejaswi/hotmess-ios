//
//  LoginViewControllerExtension.swift
//  Hotmess
//
//  Created by Cedan Misquith on 25/07/21.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import AuthenticationServices

// MARK: - UIText Field Delegate Method
extension SignUpViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if textField == mobileNumberTextField {
            let maxLength = 10
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if firstNameTextField.text == "" {
            firstNameTopLabel.isHidden = true
        }
        if lastNameTextField.text == "" {
            lastNameTopLabel.isHidden = true
        }
        if passwordTextField.text == "" {
            passwordTopLabel.isHidden = true
        }
        if confirmPasswordTextField.text == "" {
            confirmPasswordTopLabel.isHidden = true
        }
        if emailTextField.text == "" {
            emailTopLabel.isHidden = true
        }
        if mobileNumberTextField.text == "" {
            mobileNoTopLabel.isHidden = true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == firstNameTextField {
            firstNameTopLabel.isHidden = false
        }
        if textField == lastNameTextField {
            lastNameTopLabel.isHidden = false
        }
        if textField == passwordTextField {
            passwordTopLabel.isHidden = false
        }
        if textField == confirmPasswordTextField {
            confirmPasswordTopLabel.isHidden = false
        }
        if textField == emailTextField {
            emailTopLabel.isHidden = false
        }
        if textField == mobileNumberTextField {
            mobileNoTopLabel.isHidden = false
        }
    }
}
// MARK: - Google Sign In Delegate Method
extension SignUpViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        presenter.makeGoogleSignUp(signIn, didSignInFor: user, withError: error)
    }
}
// MARK: - Apple SignIn Delegate Methods
extension SignUpViewController: ASAuthorizationControllerPresentationContextProviding,
                                ASAuthorizationControllerDelegate {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    @available(iOS 13.0, *)
    func authorizationController(
        controller: ASAuthorizationController,
        didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            let token = appleIDCredential.identityToken
            let tokenString = String(data: token!, encoding: .utf8) ?? ""
            let userId = appleIDCredential.user
            let firstname = appleIDCredential.fullName?.givenName ?? ""
            let lastname = appleIDCredential.fullName?.familyName ?? ""
            let email = appleIDCredential.email ?? ""
            print(tokenString)
            getCredentialState(userId: userId)
            self.view.showLoader()
            presenter.makeAppleSignUp(
                firstName: firstname,
                lastName: lastname,
                email: email,
                userId: userId,
                token: tokenString)
        } else {
            AlertManager.sharedInstance.showAlertWith(
                title: "No Email",
                message: "Unable to get Name & Email from Apple Sign In, please try a different method to sign up.")
        }
    }
    func getCredentialState(userId: String) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            appleIDProvider.getCredentialState(forUserID: userId) { (credentialState, _) in
                switch credentialState {
                case .authorized:
                    print("Authorized")
                case .revoked:
                    print("Revoked")
                    self.appleSignUp()
                case .notFound:
                    print("Not found")
                    self.appleSignUp()
                default:
                    break
                }
            }
        }
    }
}
// MARK: - SignUp Presenter Protocol Methods
extension SignUpViewController: SignUpPresenterProtocol {
    func hideLoader() {
        self.view.hideLoader()
    }
    func signUpError(message: String) {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: "Error",
            subTitle: message,
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)
    }
    func passwordMismatch() {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: "Error",
            subTitle: "Plaese Check Your Password.",
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)
    }
    func acceptPromotional() {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: "Error",
            subTitle: "Plaese Read & Accept Promotional Offers and Services.",
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)
    }
    func acceptTerms() {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: "Error",
            subTitle: "Please read & accept our terms of service.",
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)
    }
    func fillAllFields() {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: "Error",
            subTitle: "Plaese Fill All the Fields.",
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)
        self.view.isUserInteractionEnabled = true
    }
    // MARK: - Social SignIn Validation Method
    func loginValidate(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            NotificationCenter.default.post(name: Notification.Name("WISHLIST_STATUS"), object: nil)
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as
                [UIViewController]
            self.navigationController!.popToViewController(
                viewControllers[viewControllers.count - 3],
                animated: true)
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
    // MARK: - Navigate to the OTP Screen
    func proceedToOTP(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            let storyboardOTP = UIStoryboard(name: "OTPView", bundle: nil)
            guard let otpView = storyboardOTP.instantiateViewController(
                    withIdentifier: "OTPViewController") as? OTPViewController else {
                return
            }
            otpView.presenter.userEmail = presenter.userEmail
            otpView.presenter.userSession = presenter.userSession
            otpView.presenter.otpFromForgotPassword = false
            self.navigationController?.view.setNavigationTransitionLayer()
            self.navigationController?.pushViewController(otpView, animated: true)
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
}
