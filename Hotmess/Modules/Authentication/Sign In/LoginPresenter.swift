//
//  OrderPlacinglLoginPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 20/05/21.
//

import Foundation
import SwiftyJSON
import GoogleSignIn
protocol LoginPresenterProtocol: AnyObject {
    func loginValidate(status: Bool, title: String, message: String)
    func hideLoader()
}

class LoginPresenter: NSObject {
    weak var delegate: LoginPresenterProtocol?
    // MARK: API Request For Facebook SignIn
    func makeFacebookSigIn(result: Any) {
        let resultJSON = JSON(result)
        print(resultJSON)
        let email = resultJSON["email"].string ?? ""
        print(email)
        let firstName = resultJSON["first_name"].string ?? ""
        let lastName = resultJSON["last_name"].string ?? ""
        let pictureURL = resultJSON["picture"]["data"]["url"].string ?? ""
        let parameters: [String: Any] = [
            "email": email,
            "firstName": firstName,
            "lastName": lastName,
            "deviceType": "0",
            "imageUrl": pictureURL
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.faceBookSignUp,
            prametres: parameters,
            type: .withoutAuth,
            method: .POST,
            completion: self.handleLoginResponse)
    }
    // MARK: API Request For Google SignIn
    func makeGoogleSignIn(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("Google sign in error: \(error)")
            }
            delegate?.hideLoader()
            return
        }
        var userImage: URL!
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let userEmail = user.profile.email
        if let profileImage = user.profile.imageURL(withDimension: 320) {
            userImage = profileImage
        }
        let parameters: [String: Any] = [
            "email": userEmail ?? "",
            "firstName": givenName ?? "",
            "lastName": familyName ?? "",
            "imageUrl": userImage.absoluteString,
            "deviceType": "0"
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.googleSignUp,
            prametres: parameters,
            type: .withoutAuth,
            method: .POST,
            completion: self.handleLoginResponse)
    }
    func makeLogin(email: String, password: String) {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.userLogin,
            prametres: [
                "email": email,
                "password": password
            ],
            type: .withoutAuth,
            method: .POST,
            completion: self.handleLoginResponse)
    }
    // MARK: - API Request For SignIn With Apple
    func makeAppleSignUp(firstName: String, lastName: String, email: String, userId: String, token: String) {
        var parameters: [String: Any] = [
            "apple_user_id": userId,
            "apple_token": token
        ]
        if firstName != "" {
            parameters.updateValue(firstName, forKey: "first_name")
        }
        if lastName != "" {
            parameters.updateValue(lastName, forKey: "last_name")
        }
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.appleSignIn,
            prametres: parameters,
            type: .withoutAuth,
            method: .POST,
            completion: self.handleLoginResponse)
    }
    func requestAddToWishlist() {
        let allProductsInWishList = RealmDataManager.sharedInstance.getAllProductsFromWishlist()
        var arrayOfItems = [[String: Any]]()
        for product in allProductsInWishList {
            let item: [String: Any] = [
                "product_id": product.productID,
                "sku_id": product.stockKeepUnitID
            ]
            arrayOfItems.append(item)
            RealmDataManager.sharedInstance.deleteFromBag(
                productId: product.productID,
                skuID: product.stockKeepUnitID)
        }
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.addToWishlist,
            prametres: arrayOfItems,
            type: .withAuth,
            method: .POST,
            completion: self.handleAddToWishlistResponse)
    }
    func requestAddToBagProduct() {
        let allProductsInBag = RealmDataManager.sharedInstance.getAllProductsFromBag()
        var arrayOfItems = [[String: Any]]()
        for product in allProductsInBag {
            let item: [String: Any] = [
                "product_id": product.productID,
                "sku_id": product.stockKeepUnitID,
                "quantity": product.productQuantity
            ]
            arrayOfItems.append(item)
        }
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.addToBag,
            prametres: arrayOfItems,
            type: .withAuth,
            method: .POST,
            completion: self.handleAddToBagResponse)
    }
    func handleAddToBagResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.requestAddToBagProduct()
                        }
                    } else {
                        delegate?.loginValidate(status: false, title: "Error", message: message)
                    }
                }
            } else {
                RealmDataManager.sharedInstance.deleteAllFromBag()
                delegate?.loginValidate(status: true, title: "", message: "")
            }
        }
    }
    func handleAddToWishlistResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.requestAddToWishlist()
                        }
                    }
                }
            } else {
                RealmDataManager.sharedInstance.deleteAllFromWishlist()
                NotificationCenter.default.post(name: Notification.Name("WISHLIST_STATUS"), object: nil)
            }
        }
    }
    // MARK: - Login Response Handler
    func handleLoginResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                delegate?.loginValidate(status: false, title: "Error", message: response["message"].string ?? "")
            } else {
                if let data = response["data"].dictionary {
                    let userData = AuthenticationParser.sharedInstance.parseUserData(data: data)
                    setFreshChatUser(userData: userData)
                    UserDefaults.standard.setValue(userData.idToken, forKey: "userIdToken")
                    NetworkManager.sharedInstance.updateUserIdToken()
                    RealmDataManager.sharedInstance.deleteUserDetails()
                    RealmDataManager.sharedInstance.saveUserDetails(details: userData)
                    UserDefaults.standard.setValue(true, forKey: "isLoggedIn")
                    if RealmDataManager.sharedInstance.getAllProductsFromWishlist().count > 0 {
                        requestAddToWishlist()
                    }
                    if RealmDataManager.sharedInstance.getAllProductsFromBag().count > 0 {
                        requestAddToBagProduct()
                    } else {
                        delegate?.loginValidate(status: true, title: "", message: "")
                    }
                } else {
                    delegate?.loginValidate(status: false, title: "Error", message: "Server Error")
                }
            }
        }
    }
    func setFreshChatUser(userData: UserRealm) {
        // Create a user object
        let user = FreshchatUser.sharedInstance()
        // To set an identifiable first name for the user
        user.firstName = userData.userFirstName
        // To set an identifiable last name for the user
        user.lastName = userData.userLastName
        // To set user's email id
        user.email = userData.userEmail
        // To set user's phone number
        // user.phoneCountryCode = ""
        // user.phoneNumber = "9999999999"
        Freshchat.sharedInstance().setUser(user)
    }

}
