//
//  CustomAlertPopUp.swift
//  Hotmess
//
//  Created by Cedan Misquith on 23/07/21.
//

import UIKit

class CustomAlertPopUp: UIView {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    func applyStyling() {
        titleLabel.textColor = ColourConstants.hex191919
        subtitleLabel.textColor = ColourConstants.hex191919
        actionButton.backgroundColor = ColourConstants.hex191919
        actionButton.setTitleColor(.white, for: .normal)
    }
    func applyFont() {
        titleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        subtitleLabel.font = UIFont(name: FontConstants.light, size: 13)
        actionButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
    }
    // MARK: - Close PopUp Method
    func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0, options: [], animations: {
                        self.cardView.frame = CGRect(x: self.cardView.frame.minX,
                                                     y: self.frame.height,
                                                     width: self.cardView.frame.width,
                                                     height: self.cardView.frame.height)
                       }, completion: { (finished: Bool) in
                        if finished {
                            self.removeFromSuperview()
                        }
                       })
    }
    @IBAction func dismissButtonAction(_ sender: UIButton) {
        dismiss()
    }
}
