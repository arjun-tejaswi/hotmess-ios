//
//  AddToBagPopUp.swift
//  Hotmess
//
//  Created by Flaxon on 19/07/21.
//

import UIKit
import SwiftyJSON

protocol AddToBagPopUpDelegate: AnyObject {
    func selectedStyleWithProduct(skuID: String, productID: String)
}

class AddToBagPopUp: UIView {
    @IBOutlet weak var styleWithHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var styleWithCollectionView: UICollectionView!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var viewBagButton: UIButton!
    @IBOutlet weak var productQuantityLabel: UILabel!
    @IBOutlet weak var productColourLabel: UILabel!
    @IBOutlet weak var productSizeLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var productDesignerNameLabel: UILabel!
    @IBOutlet weak var itemAddedTitleLabel: UILabel!
    @IBOutlet weak var styleWithLabel: UILabel!
    var pageNumber: Int = 1
    var totalStyleWithProducts: Int64 = 1
    var productStyleWithList = [ProductListRealm]()
    var didReachPageEnd: Bool = false
    weak var delegate: AddToBagPopUpDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
        setUpStyleWithCollectionView()
    }
    @IBAction func closeButton(_ sender: UIButton) {
        dismiss()
    }
    @IBAction func viewBagCheckOutButtonAction(_ sender: UIButton) {
    }
    func applyData(productData: ProductDetailRealm) {
        NukeManager.sharedInstance.setImage(url: productData.productImageURL,
                                            imageView: productImageView,
                                            withPlaceholder: true)
        productDesignerNameLabel.text = productData.productDesignerName
        productDescriptionLabel.text = productData.productDescription
        productPriceLabel.text = "\(productData.productPrice)"
        productSizeLabel.text = "Size\t\t:  " + "\(productData.productSize)"
        productColourLabel.text = "Colour\t:  " + "\(productData.productColour)"
        productQuantityLabel.text = "Quantity\t:  1"
        if productStyleWithList.count  <= 0 {
            styleWithHeightConstraint.constant = 0
        } else {
            styleWithHeightConstraint.constant = 290.5
            styleWithCollectionView.reloadData()
        }
    }
    func applyStyling() {
        let attributedString = NSMutableAttributedString.init(string: "STYLE WITH")
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle,
                                      value: 1,
                                      range: NSRange.init(location: 0,
                                                          length: attributedString.length))
        styleWithLabel.attributedText = attributedString
        productDesignerNameLabel.applyLetterSpacing(spacing: 1.2)
        styleWithLabel.applyLetterSpacing(spacing: 1.12)
        viewBagButton.applyButtonLetterSpacing(spacing: 2.16)
    }
    func applyFont() {
        itemAddedTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        productDesignerNameLabel.font = UIFont(name: FontConstants.semiBold, size: 12)
        productDescriptionLabel.font = UIFont(name: FontConstants.light, size: 11)
        currencyLabel.font = UIFont(name: FontConstants.medium, size: 14)
        productPriceLabel.font = UIFont(name: FontConstants.medium, size: 14)
        productSizeLabel.font = UIFont(name: FontConstants.light, size: 10)
        productColourLabel.font = UIFont(name: FontConstants.light, size: 10)
        productQuantityLabel.font = UIFont(name: FontConstants.light, size: 10)
    }
    func setUpStyleWithCollectionView() {
        configureStyleWithCollectionView()
        styleWithCollectionView.delegate = self
        styleWithCollectionView.dataSource = self
        styleWithCollectionView.register(UINib(nibName: "ProductListCustomCell", bundle: nil),
                                         forCellWithReuseIdentifier: "ProductListCustomCell")
    }
    // MARK: - StyleWith CollectionView Cell SetUp
    func configureStyleWithCollectionView() {
        let cellSize = CGSize(width: 125,
                              height: styleWithCollectionView.frame.height)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 13)
        layout.minimumLineSpacing = 4
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        styleWithCollectionView.backgroundColor = .clear
        styleWithCollectionView.bounces = false
        styleWithCollectionView.isPagingEnabled = false
        styleWithCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    // MARK: - Close PopUp Method
    func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [], animations: {
                        self.cardView.frame = CGRect(x: self.cardView.frame.minX,
                                                     y: self.frame.height,
                                                     width: self.cardView.frame.width,
                                                     height: self.cardView.frame.height)
                       }, completion: { (finished: Bool) in
                        if finished {
                            self.removeFromSuperview()
                        }
                       })
    }
}
extension AddToBagPopUp: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productStyleWithList.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCustomCell",
                                                            for: indexPath) as? ProductListCustomCell else {
            return UICollectionViewCell()
        }
        NukeManager.sharedInstance.setImage(
            url: productStyleWithList[indexPath.row].productImageURL,
            imageView: cell.productListImageView,
            withPlaceholder: true)
        cell.priceLabel.text = "\(productStyleWithList[indexPath.row].productPrice)"
        cell.productListTiltleLabel.text = productStyleWithList[indexPath.row].productDesignerName
        cell.productListSubTitleLabel.text = productStyleWithList[indexPath.row].productDescription
        cell.oneLeftHeightConstraint.constant = 0
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.selectedStyleWithProduct(
            skuID: productStyleWithList[indexPath.row].stockKeepUnitID,
            productID: productStyleWithList[indexPath.row].productID)
    }
}
