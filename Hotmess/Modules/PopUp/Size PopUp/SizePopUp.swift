//
//  SizePopUp.swift
//  Hotmess
//
//  Created by Flaxon on 17/05/21.
//

import UIKit

protocol SizePopUpDelegate: AnyObject {
    func selectedSize(skuID: String, productID: String)
}

class SizePopUp: UIView {
    @IBOutlet weak var sizeTableHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var sizeGuideButton: UIButton!
    @IBOutlet weak var sizeTableView: UITableView!
    @IBOutlet weak var selectSizeTitleLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    var sizeGuideProductData = ProductDetailRealm()
    var sizeGuideMatrics = [ProductSizeRealm]()
    var showSizeGuidePopUp: SizeGuidePopUp!
    var size = [SizeModel]()
    weak var delegate: SizePopUpDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        setUpsizeTabelView()
        sizeTableView.reloadData()
        print(sizeGuideMatrics)
    }
    override func layoutSubviews() {
        self.sizeTableHeightConstaint.constant = self.sizeTableView.intrinsicContentSize.height
    }
    func applyData(sizeGuideData: ProductDetailRealm) {
        sizeGuideProductData = sizeGuideData
    }
    func setUpsizeTabelView() {
        sizeTableView.dataSource = self
        sizeTableView.delegate = self
        sizeTableView.register(UINib(nibName: "SizeSelectionCustomCell", bundle: nil),
                               forCellReuseIdentifier: "SizeSelectionCustomCell")
    }
    @IBAction func closeButtonAction(_ sender: UIButton) {
        dismiss()
    }
    @IBAction func viewSizeGuideButtonAction(_ sender: UIButton) {
        showSizeGuidePopUp = Utilities.sharedInstance.showSizeGuidePopUp(bounds: UIScreen.main.bounds)
        showSizeGuidePopUp.applyData(sizeGuideData: sizeGuideProductData)
        showSizeGuidePopUp.sizeGuide = sizeGuideMatrics
        print(showSizeGuidePopUp.sizeGuide)
        self.addSubview(showSizeGuidePopUp)
    }
    // MARK: - Font Style Method
    func applyFont() {
        selectSizeTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        sizeGuideButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
    }
    // MARK: - Close PopUp Method
    func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [], animations: {
                        self.cardView.frame = CGRect(x: self.cardView.frame.minX,
                                                     y: self.frame.height,
                                                     width: self.cardView.frame.width,
                                                     height: self.cardView.frame.height)
                       }, completion: { (finished: Bool) in
                        if finished {
                            self.removeFromSuperview()
                        }
                       })
    }
    @objc func selectSizeButtonAction(sender: UIButton) {
        print(sender.tag)
        delegate?.selectedSize(skuID: size[sender.tag].skuID ?? "",
                               productID: size[sender.tag].productID ?? "")
    }
}
// MARK: - TableView Methods
extension SizePopUp: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return size.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SizeSelectionCustomCell",
                                                       for: indexPath) as? SizeSelectionCustomCell else {
            return UITableViewCell()
        }
        cell.countrySizeLabel.text = size[indexPath.row].sizeValue
        cell.selectSizeButton.tag = indexPath.row
        cell.selectSizeButton.addTarget(self, action: #selector(selectSizeButtonAction), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selectedSize(skuID: size[indexPath.row].skuID ?? "",
                               productID: size[indexPath.row].productID ?? "")
    }
}
