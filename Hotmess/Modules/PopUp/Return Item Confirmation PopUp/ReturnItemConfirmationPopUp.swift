//
//  ReturnItemConfirmationPopUp.swift
//  Hotmess
//
//  Created by Flaxon on 14/09/21.
//

import UIKit

class ReturnItemConfirmationPopUp: UIView {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var tableviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var productRetunedTableView: UITableView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var confirmationReturnLabel: UILabel!
    @IBOutlet weak var confirmReturnLabel: UILabel!
    var returnOrderInfo = [LineItemsRealm]()
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        applyStyling()
        productRetunedTableView.delegate = self
        productRetunedTableView.dataSource = self
        productRetunedTableView.register(UINib(nibName: "OrderDetailCustomCell", bundle: nil),
                                         forCellReuseIdentifier: "OrderDetailCustomCell")
    }
    override func layoutSubviews() {
        self.tableviewHeightConstraint.constant = self.productRetunedTableView.intrinsicContentSize.height
    }
    @IBAction func closeButtonAction(_ sender: UIButton) {
        dismiss()
    }
    func applyFont() {
        confirmReturnLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        confirmationReturnLabel.font = UIFont(name: FontConstants.regular, size: 12)
        confirmButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
    }
    func applyStyling() {
        confirmReturnLabel.textColor = ColourConstants.hexDE650D
        confirmationReturnLabel.textColor = ColourConstants.hex191919
        confirmButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        confirmButton.backgroundColor = ColourConstants.hex191919
        confirmButton.applyButtonLetterSpacing(spacing: 2.52)
    }
    func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [], animations: {
                        self.cardView.frame = CGRect(x: self.cardView.frame.minX,
                                                     y: self.frame.height,
                                                     width: self.cardView.frame.width,
                                                     height: self.cardView.frame.height)
                       }, completion: { (finished: Bool) in
                        if finished {
                            self.removeFromSuperview()
                        }
                       })
    }
}
// MARK: - TableView Methods
extension ReturnItemConfirmationPopUp: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return returnOrderInfo.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailCustomCell",
                                                       for: indexPath) as? OrderDetailCustomCell else {
            return UITableViewCell()
        }
        let url = returnOrderInfo[indexPath.row].featuredImageURL
        NukeManager.sharedInstance.setImage(url: url,
                                            imageView: cell.productImageView,
                                            withPlaceholder: true)
        cell.productDesignerNameLabel.text = returnOrderInfo[indexPath.row].designerName
        cell.productDescriptionLabel.text = returnOrderInfo[indexPath.row].itemDescription
        cell.productPriceLabel.text = "\(returnOrderInfo[indexPath.row].costPrice)"
        let attribute1: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.regular, size: 14) ?? UIFont.systemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
        let attribute2: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.medium, size: 14) ?? UIFont.boldSystemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
        let size = NSMutableAttributedString(string: "Size\t\t\t", attributes: attribute1)
        let sizeValue = NSMutableAttributedString(
            string: "\(returnOrderInfo[indexPath.row].productSize)",
            attributes: attribute2)
        size.append(sizeValue)
        let quantity = NSMutableAttributedString(string: "Qunatity\t", attributes: attribute1)
        let quantityValue = NSMutableAttributedString(
            string: "\(returnOrderInfo[indexPath.row].quantity)",
            attributes: attribute2)
        quantity.append(quantityValue)
        cell.productQuantityLabel.attributedText = quantity
        cell.productSizeLabel.attributedText = size
        if indexPath.row == returnOrderInfo.count - 1 {
            cell.separaterView.isHidden = true
        } else {
            cell.separaterView.isHidden = false
        }
        cell.selectionStyle = .none
        return cell
    }
}
