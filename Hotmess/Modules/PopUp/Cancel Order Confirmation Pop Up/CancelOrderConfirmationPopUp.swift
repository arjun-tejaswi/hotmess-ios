//
//  CancelOrderConfirmationPopUp.swift
//  Hotmess
//
//  Created by Cedan Misquith on 08/09/21.
//

import UIKit

class CancelOrderConfirmationPopUp: UIView {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var cautionMessageLabel: UILabel!
    @IBOutlet weak var cancelOrderButton: UIButton!
    @IBOutlet weak var keepOrderButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    func applyStyling() {
        titleLabel.textColor = ColourConstants.hex191919
        subtitleLabel.textColor = ColourConstants.hex191919
        cautionMessageLabel.textColor = ColourConstants.hexEF5353
        cancelOrderButton.backgroundColor = ColourConstants.hex191919
        cancelOrderButton.setTitleColor(.white, for: .normal)
        keepOrderButton.layer.borderWidth = 1
        keepOrderButton.layer.borderColor = ColourConstants.hex191919.cgColor
        keepOrderButton.setTitleColor(ColourConstants.hex191919, for: .normal)
    }
    func applyFont() {
        titleLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        subtitleLabel.font = UIFont(name: FontConstants.regular, size: 16)
        cautionMessageLabel.font = UIFont(name: FontConstants.regular, size: 16)
        cancelOrderButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        keepOrderButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
    }
    // MARK: - Close PopUp Method
    func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0, options: [], animations: {
                        self.cardView.frame = CGRect(x: self.cardView.frame.minX,
                                                     y: self.frame.height,
                                                     width: self.cardView.frame.width,
                                                     height: self.cardView.frame.height)
                       }, completion: { (finished: Bool) in
                        if finished {
                            self.removeFromSuperview()
                        }
                       })
    }
    @IBAction func closeButtonAction(_ sender: UIButton) {
        dismiss()
    }
}
