//
//  SortByPopUp.swift
//  Hotmess
//
//  Created by Cedan Misquith on 28/07/21.
//

import UIKit

class SortByPopUp: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet var filterButtons: [UIButton]!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    func applyStyling() {
        titleLabel.textColor = ColourConstants.hex191919
        for button in filterButtons {
            button.titleLabel?.font = UIFont.init(name: FontConstants.medium, size: 14)
            button.setTitleColor(ColourConstants.hex717171, for: .normal)
        }
    }
    func applyFont() {
        titleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
    }
    // MARK: - Close PopUp Method
    func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0, options: [], animations: {
                        self.cardView.frame = CGRect(x: self.cardView.frame.minX,
                                                     y: self.frame.height,
                                                     width: self.cardView.frame.width,
                                                     height: self.cardView.frame.height)
                       }, completion: { (finished: Bool) in
                        if finished {
                            self.removeFromSuperview()
                        }
                       })
    }
    @IBAction func dismissButtonAction(_ sender: UIButton) {
        dismiss()
    }
    @IBAction func filterButtonAction(_ sender: UIButton) {
        dismiss()
    }
}
