//
//  ColourPopUp.swift
//  Hotmess
//
//  Created by Flaxon on 17/05/21.
//

import UIKit

protocol ColorPopUpDelegate: AnyObject {
    func selectedColor(skuID: String, productID: String)
}
class ColourPopUp: UIView {
    @IBOutlet weak var selectColourTitleLabel: UILabel!
    @IBOutlet weak var colourSelectionCollectionView: UICollectionView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cardView: UIView!
    weak var delegate: ColorPopUpDelegate?
    var colours = [ColourModel]()
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        setUpCollectionView()
        colourSelectionCollectionView.reloadData()
    }
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        dismiss()
    }
    // MARK: - Font Style Method
    func applyFont() {
        selectColourTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
    }
    func setUpCollectionView() {
        colourSelectionCollectionView.register(UINib(nibName: "ColourSelectionCustomCell", bundle: nil),
                                               forCellWithReuseIdentifier: "ColourSelectionCustomCell")
        colourSelectionCollectionView.delegate = self
        colourSelectionCollectionView.dataSource = self
        configureCollectionView()
    }
    // MARK: - Colour Collection View SetUp
    func configureCollectionView() {
        let cellSize = CGSize(width: 48,
                              height: colourSelectionCollectionView.frame.height)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 13, bottom: 0, right: 13)
        layout.minimumLineSpacing = 26
        layout.minimumInteritemSpacing = 0
        colourSelectionCollectionView.backgroundColor = .clear
        colourSelectionCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    // MARK: - Close Popup Method
    func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseOut], animations: {
                        self.cardView.frame = CGRect(x: self.cardView.frame.minX,
                                                     y: self.frame.height,
                                                     width: self.cardView.frame.width,
                                                     height: self.cardView.frame.height)
                       }, completion: { (finished: Bool) in
                        if finished {
                            self.removeFromSuperview()
                        }
                       })
    }
}
// MARK: - CollectionView Methods
extension ColourPopUp: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colours.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColourSelectionCustomCell",
                                                            for: indexPath) as? ColourSelectionCustomCell else {
            return UICollectionViewCell()
        }
        cell.colourSelectionButton.backgroundColor = Utilities.sharedInstance.hexStringToUIColor(
            hex: colours[indexPath.row].colourHexCode ?? "#000000")
        cell.colourLabel.text = colours[indexPath.row].colourName
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ColourSelectionCustomCell else {
            return
        }
        cell.selectionCircleView.isHidden = false
        cell.selectionCircleView.layer.borderWidth = 1
        cell.selectionCircleView.layer.borderColor = ColourConstants.hex9B9B9B.cgColor
        delegate?.selectedColor(skuID: colours[indexPath.row].skuID ?? "",
                                productID: colours[indexPath.row].productID ?? "")
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ColourSelectionCustomCell else {
            return
        }
        cell.selectionCircleView.isHidden = true
        cell.selectionCircleView.layer.borderWidth = 1
        cell.selectionCircleView.layer.borderColor = ColourConstants.hexFFFFFF.cgColor
    }
}
