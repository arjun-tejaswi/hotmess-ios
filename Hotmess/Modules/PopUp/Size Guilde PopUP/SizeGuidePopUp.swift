//
//  SizeGuidePopUp.swift
//  Hotmess
//
//  Created by Flaxon on 17/05/21.
//

import UIKit

class SizeGuidePopUp: UIView {
    @IBOutlet weak var instructionFromTeamLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var measurementTitleLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var productDesignerNameLabel: UILabel!
    @IBOutlet weak var sizeGuideTitleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var centimeterTabView: UIView!
    @IBOutlet weak var inchesTabView: UIView!
    @IBOutlet weak var inchesButton: UIButton!
    @IBOutlet weak var measurementInstructionLabel: UILabel!
    @IBOutlet weak var centimeterButton: UIButton!
    @IBOutlet weak var measurementCollectionView: UICollectionView!
    @IBOutlet weak var cardView: UIView!
    var sizeGuide = [ProductSizeRealm]()
    var imageURL: String = ""
    var metrics: Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
//        let list = items.toBulletList()
//        descriptionTextView.text = list
        setUpMeasurementCollectionView()

    }
    func applyData(sizeGuideData: ProductDetailRealm) {
        NukeManager.sharedInstance.setImage(url: sizeGuideData.productImageURL,
                                            imageView: productImageView,
                                            withPlaceholder: true)
        productDesignerNameLabel.text = sizeGuideData.productDesignerName
        productDescriptionLabel.text = sizeGuideData.productDescription
        descriptionTextView.text = sizeGuideData.productSizeFit
    }
    @IBAction func tabButtonAction(_ sender: UIButton) {
        switch sender.tag {
        case 0: switchToCentimeter()
        default: switchToInches()
        }
    }
    @IBAction func closeButtonAction(_ sender: UIButton) {
        dismiss()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        descriptionTextView.isEditable = false
        descriptionTextView.dataDetectorTypes = .all
        descriptionTextView.isScrollEnabled = false
        productDesignerNameLabel.applyLetterSpacing(spacing: 1.2)
        switchToCentimeter()
    }
    // MARK: - Font Style Method
    func applyFont() {
        sizeGuideTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        productDesignerNameLabel.font = UIFont(name: FontConstants.semiBold, size: 12)
        descriptionTextView.font = UIFont(name: FontConstants.light, size: 11)
        measurementTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 12)
        centimeterButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
        inchesButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
        measurementInstructionLabel.font = UIFont(name: FontConstants.regular, size: 10)
        instructionFromTeamLabel.font = UIFont(name: FontConstants.regular, size: 10)
        productDescriptionLabel.font = UIFont(name: FontConstants.regular, size: 12)
    }
    // MARK: - Measurement CollectionView SetUp Method
    func setUpMeasurementCollectionView() {
        configureMeasurementCollectionView()
        measurementCollectionView.delegate = self
        measurementCollectionView.dataSource = self
        measurementCollectionView.register(UINib(nibName: "ProductMeasurementCustomCell", bundle: nil),
                                           forCellWithReuseIdentifier: "ProductMeasurementCustomCell")
    }
    // MARK: - Measurement CollectionView Cell SetUp Method
    func configureMeasurementCollectionView() {
        let cellSize = CGSize(width: measurementCollectionView.frame.width / 3,
                              height: 159)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        measurementCollectionView.backgroundColor = .clear
        measurementCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    // MARK: - Tab Switch Methods
    func switchToCentimeter() {
        centimeterTabView.isHidden = false
        inchesTabView.isHidden = true
        metrics = 0
        measurementCollectionView.reloadData()
    }
    func switchToInches() {
        centimeterTabView.isHidden = true
        inchesTabView.isHidden = false
        metrics = 1
        measurementCollectionView.reloadData()
    }
    // MARK: - Close PopUp Method
    func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseOut], animations: {
                        self.cardView.frame = CGRect(x: self.cardView.frame.minX,
                                                     y: self.frame.height,
                                                     width: self.cardView.frame.width,
                                                     height: self.cardView.frame.height)
                       }, completion: { (finished: Bool) in
                        if finished {
                            self.removeFromSuperview()
                        }
                       })
    }
}
// MARK: - CollectionView Methods
extension SizeGuidePopUp: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sizeGuide.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductMeasurementCustomCell",
                                                            for: indexPath) as? ProductMeasurementCustomCell else {
            return UICollectionViewCell()
        }
        cell.sizeLabel.text = sizeGuide[indexPath.item].size
        var allAttributes = [SizeAttributesRealm]()
        for attribute in sizeGuide[indexPath.item].sizeAttributes {
            allAttributes.append(attribute)
        }
        cell.metrics = metrics
        cell.attributes = allAttributes
        cell.measurementTableView.reloadData()
        return cell
    }
}
