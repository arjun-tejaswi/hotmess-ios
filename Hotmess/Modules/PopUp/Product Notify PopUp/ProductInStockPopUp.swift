//
//  ProductInStockPopUp.swift
//  Hotmess
//
//  Created by Akshatha on 24/08/21.
//

import UIKit

protocol ProductInStockPopUpDelegate: AnyObject {
    func notifyMeData(email: String)
}
class ProductInStockPopUp: UIView {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var notifyMeButton: UIButton!
    @IBOutlet weak var notifyTitleLabel: UILabel!
    @IBOutlet weak var emailTopLabel: UILabel!
    weak var delegate: ProductInStockPopUpDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        emailTextfield.delegate = self
        applyStyling()
        applyFont()
    }
    // MARK: - Apply Data To Textfield Method
    func applyData(inStockData: ProductDetailRealm) {
        let message: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.regular, size: 13) ?? UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex191919]
        let size: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.semiBold, size: 13) ?? UIFont.boldSystemFont(ofSize: 13),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex191919]
        let messageTwo: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.regular, size: 13) ?? UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex191919]
        let messageString = NSMutableAttributedString(
            string: "Would you like us to let you know when the size ",
            attributes: message)
        let sizeString = NSMutableAttributedString(string: "\(inStockData.productSize)", attributes: size)
        let finalString = NSMutableAttributedString(string: " becomes available?", attributes: messageTwo)
        sizeString.append(finalString)
        messageString.append(sizeString)
        notifyTitleLabel.attributedText = messageString
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        notifyTitleLabel.textColor = ColourConstants.hex191919
        emailTopLabel.textColor = ColourConstants.hex9B9B9B
        emailTextfield.layer.borderWidth = 0.5
        emailTextfield.layer.borderColor = ColourConstants.hex717171.cgColor
        emailTextfield.setLeftPaddingPoints(12)
        emailTextfield.setRightPaddingPoints(12)
        notifyMeButton.applyButtonLetterSpacing(spacing: 2.52)
        emailTopLabel.isHidden = true
        notifyMeButton.isEnabled = false
        notifyMeButton.setTitleColor(ColourConstants.hex9B9B9B, for: .normal)
        notifyMeButton.backgroundColor = ColourConstants.hexE0E0E0
    }
    // MARK: - Font Style Method
    func applyFont() {
        notifyTitleLabel.font = UIFont(name: FontConstants.regular, size: 13)
        emailTopLabel.font = UIFont(name: FontConstants.regular, size: 12)
        emailTextfield.font = UIFont(name: FontConstants.regular, size: 14)
        notifyMeButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
    }
    // MARK: - Close PopUp Method
    func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0, options: [], animations: {
                        self.cardView.frame = CGRect(x: self.cardView.frame.minX,
                                                     y: self.frame.height,
                                                     width: self.cardView.frame.width,
                                                     height: self.cardView.frame.height)
                       }, completion: { (finished: Bool) in
                        if finished {
                            self.removeFromSuperview()
                        }
                       })
    }
    @IBAction func dismissButtonAction(_ sender: UIButton) {
        dismiss()
    }
    // MARK: - Notify Me Button Action Method
    @IBAction func notifyMeButtonAction(_ sender: UIButton) {
        delegate?.notifyMeData(email: emailTextfield.text ?? "")
    }
}
// MARK: - Text Delegate Method
extension ProductInStockPopUp: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if emailTextfield.text == "" {
            emailTopLabel.isHidden = true
            notifyMeButton.isEnabled = false
        } else {
            emailTopLabel.isHidden = false
            notifyMeButton.isEnabled = true
            notifyMeButton.backgroundColor = ColourConstants.hex191919
            notifyMeButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTextfield {
            emailTopLabel.isHidden = false
        }
    }
}
