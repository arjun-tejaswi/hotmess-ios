//
//  NotifySuccessPopup.swift
//  Hotmess
//
//  Created by Akshatha on 24/08/21.
//

import UIKit
class InstockNotifySuccessPopup: UIView {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var thankYouLabel: UILabel!
    @IBOutlet weak var notifyTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
        applyFont()
    }
    // MARK: - Apply Data To Textfield Method
    func applyData(successData: ProductDetailRealm) {
        let message: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.regular, size: 13) ?? UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex191919]
        let size: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.semiBold, size: 13) ?? UIFont.boldSystemFont(ofSize: 13),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex191919]
        let messageTwo: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.regular, size: 13) ?? UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex191919]
        let messageString = NSMutableAttributedString(
            string: "We will send you a notification when the size ",
            attributes: message)
        let sizeString = NSMutableAttributedString(string: "\(successData.productSize)", attributes: size)
        let messageTwoString = NSMutableAttributedString(string: " becomes available.", attributes: messageTwo)
        sizeString.append(messageTwoString)
        messageString.append(sizeString)
        notifyTitleLabel.attributedText = messageString
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        thankYouLabel.textColor = ColourConstants.hex191919
        notifyTitleLabel.textColor = ColourConstants.hex191919
    }
    // MARK: - Font Style Method
    func applyFont() {
        thankYouLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        notifyTitleLabel.font = UIFont(name: FontConstants.regular, size: 13)
    }
    // MARK: - Close PopUp Method
    func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 0.7,
                       initialSpringVelocity: 0, options: [], animations: {
                        self.cardView.frame = CGRect(x: self.cardView.frame.minX,
                                                     y: self.frame.height,
                                                     width: self.cardView.frame.width,
                                                     height: self.cardView.frame.height)
                       }, completion: { (finished: Bool) in
                        if finished {
                            self.removeFromSuperview()
                        }
                       })
    }
    @IBAction func dismissButtonAction(_ sender: UIButton) {
        dismiss()
    }
}
