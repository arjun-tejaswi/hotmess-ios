//
//  RemainingItemsPopUp.swift
//  Hotmess
//
//  Created by Cedan Misquith on 07/09/21.
//

import UIKit

class RemainingItemsPopUp: UIView {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var orderDetails: OrderRealm!
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "OrderDetailCustomCell", bundle: nil),
                                                    forCellReuseIdentifier: "OrderDetailCustomCell")
    }
    override func layoutSubviews() {
        self.tableViewHeightConstraint.constant = self.tableView.intrinsicContentSize.height
    }
    // MARK: - Close Popup Method
    func dismiss() {
        UIView.animate(withDuration: 0.5, delay: 0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0, options: [.curveEaseOut], animations: {
                        self.cardView.frame = CGRect(x: self.cardView.frame.minX,
                                                     y: self.frame.height,
                                                     width: self.cardView.frame.width,
                                                     height: self.cardView.frame.height)
                       }, completion: { (finished: Bool) in
                        if finished {
                            self.removeFromSuperview()
                        }
                       })
    }
    @IBAction func dismissButtonAction(_ sender: UIButton) {
        dismiss()
    }
}
extension RemainingItemsPopUp: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderDetails.lineItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailCustomCell",
                                                       for: indexPath) as? OrderDetailCustomCell else {
            return UITableViewCell()
        }
        let url = orderDetails.lineItems[indexPath.row].featuredImageURL
        NukeManager.sharedInstance.setImage(url: URL(string: url),
                                            imageView: cell.productImageView,
                                            withPlaceholder: true)
        cell.productDesignerNameLabel.text = orderDetails.lineItems[indexPath.row].designerName
        cell.productDescriptionLabel.text = orderDetails.lineItems[indexPath.row].itemDescription
        cell.productPriceLabel.text = "\(orderDetails.lineItems[indexPath.row].costPrice)"
        let attribute1: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.regular, size: 14) ?? UIFont.systemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
        let attribute2: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.medium, size: 14) ?? UIFont.boldSystemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
        let size = NSMutableAttributedString(string: "Size\t\t\t", attributes: attribute1)
        let sizeValue = NSMutableAttributedString(
            string: "\(orderDetails.lineItems[indexPath.row].productSize)",
            attributes: attribute2)
        size.append(sizeValue)
        let quantity = NSMutableAttributedString(string: "Qunatity\t", attributes: attribute1)
        let quantityValue = NSMutableAttributedString(
            string: "\(orderDetails.lineItems[indexPath.row].quantity)",
            attributes: attribute2)
        quantity.append(quantityValue)
        cell.productQuantityLabel.attributedText = quantity
        cell.productSizeLabel.attributedText = size
        cell.selectionStyle = .none
        if indexPath.row == orderDetails.lineItems.count - 1 {
            cell.separaterView.isHidden = true
        } else {
            cell.separaterView.isHidden = false
        }
        return cell
    }
}
