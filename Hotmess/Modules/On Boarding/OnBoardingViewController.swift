//
//  OnBoardingViewController.swift
//  Hotmess
//
//  Created by Akshatha on 13/05/21.
//

import UIKit

struct OnBoardImage {
    var image: String?
}
class OnBoardingViewController: UIViewController {
    @IBOutlet weak var onBoardingCollectionView: UICollectionView!
    var onBoardImageList = [OnBoardImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        let onBoardImage = OnBoardImage(image: "onBoardingImage_1")
        onBoardImageList.append(onBoardImage)
        let onBoardImageTwo = OnBoardImage(image: "onBoardingImage_2")
        onBoardImageList.append(onBoardImageTwo)
        let onBoardImageThree = OnBoardImage(image: "onBoardingImage_3")
        onBoardImageList.append(onBoardImageThree)
        configureCollectionView()
        onBoardingCollectionView.reloadData()
    }
    // MARK: - Statusbar Style Method
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    // MARK: Collection view configuration
    func configureCollectionView() {
        let cellSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        onBoardingCollectionView.backgroundColor = .clear
        onBoardingCollectionView.isPagingEnabled = true
        onBoardingCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    @objc func pageControlButtonAction(sender: UIButton) {
        // Go to page 1
        onBoardingCollectionView.setContentOffset(CGPoint(x: 0 * self.view.frame.width, y: 0), animated: true)
    }
    @objc func pageControlrButtonTwoAction(sender: UIButton) {
        // Go to page 2
        onBoardingCollectionView.setContentOffset(CGPoint(x: 1 * self.view.frame.width, y: 0), animated: true)
    }
    @objc func pageControlButtonThreeAction(sender: UIButton) {
        // Go to page 3
        onBoardingCollectionView.setContentOffset(CGPoint(x: 2 * self.view.frame.width, y: 0), animated: true)
    }
    @objc func navigateButtonAction(sender: UIButton) {
        // Go to next view
        UserDefaults.standard.set(true, forKey: "notFirstInApp")
        let storyBoard = UIStoryboard(name: "FeaturedBanners", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(withIdentifier: "FeaturedBannersViewController") as?
                FeaturedBannersViewController else {
            return
        }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
// MARK: Collection view methods
extension OnBoardingViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        onBoardImageList.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "OnBoardingCollectionViewCell",
                for: indexPath) as? OnBoardingCollectionViewCell else {
            return UICollectionViewCell()
        }
        if indexPath.row == 0 {
            cell.navigateButton.setTitle("  S K I P  ", for: .normal)
            cell.pagingButtonOneHeight.constant = 32
            cell.pagingButtonTwoHeight.constant = 16
            cell.pagingButtonThreeHeight.constant = 16
        } else if indexPath.row == 1 {
            cell.navigateButton.setTitle("  S K I P  ", for: .normal)
            cell.pagingButtonOneHeight.constant = 16
            cell.pagingButtonTwoHeight.constant = 32
            cell.pagingButtonThreeHeight.constant = 16
        } else {
            cell.navigateButton.setTitle("  C O N T I N U E  ", for: .normal)
            cell.pagingButtonOneHeight.constant = 16
            cell.pagingButtonTwoHeight.constant = 16
            cell.pagingButtonThreeHeight.constant = 32
        }
        cell.pagingButtonOne.addTarget(self, action: #selector(pageControlButtonAction), for: .touchUpInside)
        cell.pagingButtonTwo.addTarget(self, action: #selector(pageControlrButtonTwoAction), for: .touchUpInside)
        cell.pagingButtonThree.addTarget(self, action: #selector(pageControlButtonThreeAction), for: .touchUpInside)
        cell.navigateButton.addTarget(self, action: #selector(navigateButtonAction), for: .touchUpInside)
        cell.onBoardingImageView.image = UIImage(named: onBoardImageList[indexPath.item].image ?? "")
        return cell
    }
}
