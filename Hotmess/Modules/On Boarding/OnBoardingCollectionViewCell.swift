//
//  OnBoardingCollectionViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 13/05/21.
//

import UIKit
class OnBoardingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var onBoardingSubTitleLabel: UILabel!
    @IBOutlet weak var onBoardingTitleLabel: UILabel!
    @IBOutlet weak var navigateButton: UIButton!
    @IBOutlet weak var pagingButtonOne: UIButton!
    @IBOutlet weak var pagingButtonOneHeight: NSLayoutConstraint!
    @IBOutlet weak var pagingButtonTwo: UIButton!
    @IBOutlet weak var pagingButtonTwoHeight: NSLayoutConstraint!
    @IBOutlet weak var pagingButtonThree: UIButton!
    @IBOutlet weak var pagingButtonThreeHeight: NSLayoutConstraint!
    @IBOutlet weak var onBoardingImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        onBoardingImageView.contentMode = .scaleAspectFill
        navigateButton.backgroundColor = UIColor.clear.withAlphaComponent(0.1)
    }
    // MARK: - Font Style Method
    func applyFont() {
        navigateButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 11)
        onBoardingTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
        onBoardingSubTitleLabel.font = UIFont(name: FontConstants.regular, size: 12)
        navigateButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 11)
    }
}
