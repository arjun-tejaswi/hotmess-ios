//
//  ProductParser.swift
//  Hotmess
//
//  Created by Flaxon on 28/06/21.
//

import Foundation
import SwiftyJSON

class ProductParser: NSObject {
    static let  sharedInstance = ProductParser()
    func parseProductData(data: [String: JSON]) -> ProductListRealm {
        let productData = ProductListRealm()
        productData.productID = data["product_id"]?.string ?? ""
        productData.productName = data["name"]?.string ?? ""
        productData.createdAt = data["created_at"]?.int64 ?? 0
        productData.productImageURL = ApiBaseURL.imageBaseURL + "\(data["featured_image_url"]?.string ?? "")"
        productData.updatedAt = data["updated_at"]?.int64 ?? 0
        productData.productSize = data["size"]?.string ?? ""
        productData.categoryName = data["category_name"]?.string ?? ""
        productData.editorNote = data["editor_note"]?.string ?? ""
        productData.productDescription = data["description"]?.string ?? ""
        productData.productAltTag = data["alt_tag"]?.string ?? ""
        productData.productStatus = data["product_status"]?.string ?? ""
        productData.productSizeFit = data["size_fit"]?.string ?? ""
        productData.isRecommended = data["is_recommended"]?.bool ?? false
        productData.productCategoryURL = data["category_url"]?.string ?? ""
        productData.productPrice = data["cost_price"]?.int64 ?? 0
        productData.productDetailsCare = data["details_care"]?.string ?? ""
        productData.productDesignerName = data["designer_name"]?.string ?? ""
        productData.productDesignerURL = data["designer_url"]?.string ?? ""
        productData.productCategoryID = data["category_id"]?.string ?? ""
        productData.productOfferPrice = data["offer_price"]?.int64 ?? 0
        productData.isnew = data["is_new"]?.bool ?? false
        productData.stockKeepUnitID = data["sku_id"]?.string ?? ""
        productData.productColour = data["colour"]?.string ?? ""
        return productData
    }
    func getAssetsRealmObject(assets: [JSON]) -> [ProductAssestsRealm] {
        var allAssets = [ProductAssestsRealm]()
        allAssets.removeAll()
        for aAsset in assets {
            let asset = ProductAssestsRealm()
            asset.type = aAsset["type"].string ?? ""
            asset.imageURL = ApiBaseURL.imageBaseURL + "\(aAsset["url"].string ?? "")"
            asset.altText = aAsset["alt_text"].string ?? ""
            asset.thumbnailImageURL = ApiBaseURL.imageBaseURL + "\(aAsset["thumbnail_image_url"].string ?? "")"
            allAssets.append(asset)
        }
        return allAssets
    }
    func getSizesRealmObject(sizes: [JSON]) -> [ProductSizeRealm] {
        var allSizes = [ProductSizeRealm]()
        allSizes.removeAll()
        for aSize in sizes {
            let size = ProductSizeRealm()
            size.size = aSize["size"].string ?? ""
            size.stockKeepUnitID = aSize["sku_id"].string ?? ""
            size.productID = aSize["product_id"].string ?? ""
            if let sizeAttributes = aSize["size_attributes"].array {
                let attributes = getSizeAttributes(attributes: sizeAttributes)
                size.sizeAttributes.append(objectsIn: attributes)
            }
            allSizes.append(size)
        }
        return allSizes
    }
    func getSizeAttributes(attributes: [JSON]) -> [SizeAttributesRealm] {
        var allAttributes = [SizeAttributesRealm]()
        for aAttribute in attributes {
            let attribute = SizeAttributesRealm()
            attribute.key = aAttribute["key"].string ?? ""
            attribute.value = aAttribute["value"].int64 ?? 0
            allAttributes.append(attribute)
        }
        return allAttributes
    }
    func getVariantsRealmObject(variants: [JSON]) -> [ProductVariantsRealm] {
        var allVariants = [ProductVariantsRealm]()
        allVariants.removeAll()
        for aVariant in variants {
            let variant = ProductVariantsRealm()
            variant.colour = aVariant["colour"].string ?? ""
            variant.offerPrice = aVariant["offer_price"].int64 ?? 0
            variant.productPrice = aVariant["cost_price"].int64 ?? 0
            variant.colourID = aVariant["colour_id"].string ?? ""
            variant.colourIcon = aVariant["colour_icon"].string ?? ""
            variant.productID = aVariant["product_id"].string ?? ""
            variant.productSize = aVariant["size"].string ?? ""
            variant.stockKeepUnitID = aVariant["sku_id"].string ?? ""
            variant.sizeID = aVariant["size_id"].string ?? ""
            variant.colourCode = aVariant["colour_code"].string ?? ""
            if let assets = aVariant["assets"].array {
                var allassets = [ProductAssestsRealm]()
                for aAsset in assets {
                    let asset = ProductAssestsRealm()
                    asset.type = aAsset["type"].string ?? ""
                    asset.imageURL = ApiBaseURL.imageBaseURL + "\(aAsset["url"].string ?? "")"
                    asset.altText = aAsset["alt_text"].string ?? ""
                    allassets.append(asset)
                }
                variant.assets.append(objectsIn: allassets)
            }
            allVariants.append(variant)
        }
        return allVariants
    }
}
