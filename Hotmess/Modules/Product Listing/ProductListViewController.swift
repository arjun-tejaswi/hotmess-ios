//
//  ProductListViewController.swift
//  Hotmess
//
//  Created by Cedan Misquith on 25/06/21.
//

import UIKit
import SideMenu
import HPParallaxHeader

enum CollectionViewState {
    case gridType01
    case gridType02
    case gridType03
    case gridType04
}

class ProductListViewController: UIViewController {
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var cartCountLabel: UILabel!
    @IBOutlet weak var productListingSlider: UISlider!
    @IBOutlet weak var filterCollectionView: UICollectionView!
    @IBOutlet weak var subFilterCollectionView: UICollectionView!
    @IBOutlet weak var productListingCollectionView: UICollectionView!
    var gridState: CollectionViewState = .gridType04
    var presenter = ProductListingPresenter()
    var sampleMainFiltersText = ["TRENDING COLLECTIONS", "COLOR", "PATTERN", "LENGTH", "SLEEVE", "NECK", "CUT"]
    var sampleSubFiltersText = ["Bohemian", "Cut Out", "Floral", "Embellished",
                                "White", "Sundress", "Bohemian",
                                "Cut Out", "Floral", "Embellished", "White", "Sundress"]
    var sampleSubFiltersImages: [UIImage?] = [
        UIImage(named: "bohemian"),
        UIImage(named: "CutOut"),
        UIImage(named: "floral"),
        UIImage(named: "embellished"),
        UIImage(named: "white"),
        UIImage(named: "sunDress"),
        UIImage(named: "bohemian"),
        UIImage(named: "CutOut"),
        UIImage(named: "floral"),
        UIImage(named: "embellished"),
        UIImage(named: "white"),
        UIImage(named: "sunDress")
    ]
    var laidOutOnLoad: Bool = false
    var showOnlyImage: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyling()
        applyFont()
        presenter.delegate = self
        productListingCollectionView.parallaxHeader.view = headerView
        productListingCollectionView.parallaxHeader.height = 138
        productListingCollectionView.parallaxHeader.mode = .fill
        productListingCollectionView.bounces = false
        productListingCollectionView.parallaxHeader.minimumHeight = 0
        productListingCollectionViewSetUp()
        productListingSlider.value = 0
    }
    override func viewWillAppear(_ animated: Bool) {
        print(presenter.searchProductList)
        if presenter.isFromSearch {
            self.view.showLoader()
            presenter.requestForSearch()
        } else {
            presenter.requestProductList()
            self.view.showLoader()
        }
        presenter.requestShoppingBag()
    }
    override func viewDidLayoutSubviews() {
        if !laidOutOnLoad {
            laidOutOnLoad = true
            filterValueChangedAction(productListingSlider)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - Product Listing CollectionView Cell Configure
    func configureProductListingCollectionView(cellWidth: CGFloat, cellHeight: CGFloat) {
        let cellSize = CGSize(width: cellWidth,
                              height: cellHeight)
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .vertical
        productListingCollectionView.backgroundColor = .clear
        productListingCollectionView.showsHorizontalScrollIndicator = false
        productListingCollectionView.isPagingEnabled = false
        productListingCollectionView.bounces = false
        productListingCollectionView.collectionViewLayout = layout
        productListingCollectionView.reloadData()// .setCollectionViewLayout(layout, animated: true)
        productListingCollectionView.setContentOffset(CGPoint(x: 0, y: -138), animated: false)
    }
    // MARK: - Product Listing Collectionview Grid Styling Method
    func setCollectionViewGridStyle(state: CollectionViewState) {
        switch state {
        case .gridType01:
            configureProductListingCollectionView(
                cellWidth: productListingCollectionView.frame.width,
                cellHeight: productListingCollectionView.frame.width * 1.45)
        case .gridType02:
            configureProductListingCollectionView(
                cellWidth: productListingCollectionView.frame.width / 2,
                cellHeight: productListingCollectionView.frame.width / 2 * 1.65)
        case .gridType03:
            configureProductListingCollectionView(
                cellWidth: productListingCollectionView.frame.width / 3,
                cellHeight: productListingCollectionView.frame.width / 3 * 2)
        case .gridType04:
            configureProductListingCollectionView(
                cellWidth: productListingCollectionView.frame.width / 4,
                cellHeight: productListingCollectionView.frame.width / 4 * 1.4)
        }
    }
    // MARK: - Product Listing CollectionView Cell Configure
    func productListingCollectionViewSetUp() {
        productListingCollectionView.register(UINib(nibName: "ProductListCustomCell", bundle: nil),
                                              forCellWithReuseIdentifier: "ProductListCustomCell")
        productListingCollectionView.register(UINib(nibName: "ProductListImageCustomCell", bundle: nil),
                                              forCellWithReuseIdentifier: "ProductListImageCustomCell")
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        categoryTitleLabel.applyLetterSpacing(spacing: 0.7)
        categoryTitleLabel.textColor = ColourConstants.hexFFFFFF
        cartCountLabel.textColor = ColourConstants.hexFFFFFF
    }
    // MARK: - Font Styling Method
    func applyFont() {
        categoryTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
        cartCountLabel.font = UIFont.systemFont(ofSize: 7)
    }
    @IBAction func filterValueChangedAction(_ sender: UISlider) {
        let sliderValue = sender.value
        if sliderValue >= 0 && sliderValue <= 0.25 {
            showOnlyImage = true
            setCollectionViewGridStyle(state: .gridType04)
        } else if sliderValue >= 0.26 && sliderValue <= 0.50 {
            showOnlyImage = false
            setCollectionViewGridStyle(state: .gridType03)
        } else if sliderValue >= 0.51 && sliderValue <= 0.75 {
            showOnlyImage = false
            setCollectionViewGridStyle(state: .gridType02)
        } else if sliderValue >= 0.76 && sliderValue <= 1 {
            showOnlyImage = false
            setCollectionViewGridStyle(state: .gridType01)
        }
    }
    @IBAction func profileButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "SlideMenu", bundle: nil)
        guard let slideVC = storyBoard.instantiateViewController(
                withIdentifier: "SlideMenuViewController") as? SlideMenuViewController
        else {
            return
        }
        let sideMenu = SideMenuNavigationController(rootViewController: slideVC)
        sideMenu.presentationStyle = .menuSlideIn
        sideMenu.isNavigationBarHidden = true
        sideMenu.blurEffectStyle = .dark
        sideMenu.menuWidth = self.view.frame.width - 64
        blurView.isHidden = false
        self.present(sideMenu, animated: true, completion: nil)
    }
    @IBAction func shopBagButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "ShoppingBag", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ShoppingBagViewController")
                as? ShoppingBagViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @IBAction func searchButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "Search", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "SearchViewController")
                as? SearchViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - CollectionView DataSource Method
extension ProductListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == filterCollectionView {
            return sampleMainFiltersText.count
        } else if collectionView == subFilterCollectionView {
            return sampleSubFiltersText.count
        } else {
            if presenter.isFromSearch {
                return presenter.searchProductList.count
            } else {
                return presenter.productList.count
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == filterCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainFilterCustomCell",
                                                                for: indexPath) as? MainFilterCustomCell else {
                return UICollectionViewCell()
            }
            cell.filterLabel.text = "   \(sampleMainFiltersText[indexPath.item])   "
            return cell
        } else if collectionView == subFilterCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubFilterCustomCell",
                                                                for: indexPath) as? SubFilterCustomCell else {
                return UICollectionViewCell()
            }
            cell.subFilterImageView.image = sampleSubFiltersImages[indexPath.item]
            cell.subFilterLabel.text = sampleSubFiltersText[indexPath.item]
            return cell
        } else {
            return getProductListingCollectionViewCell(indexPath: indexPath)
        }
    }
    func getProductListingCollectionViewCell(indexPath: IndexPath) -> UICollectionViewCell {
        if showOnlyImage {
            guard let cell = productListingCollectionView.dequeueReusableCell(
                    withReuseIdentifier: "ProductListImageCustomCell",
                    for: indexPath) as? ProductListImageCustomCell else {
                return UICollectionViewCell()
            }
            if presenter.isFromSearch {
                NukeManager.sharedInstance.setImage(
                    url: presenter.searchProductList[indexPath.row].productImageURL,
                    imageView: cell.productListImageView,
                    withPlaceholder: true)
            } else {
                NukeManager.sharedInstance.setImage(
                    url: presenter.productList[indexPath.row].productImageURL,
                    imageView: cell.productListImageView,
                    withPlaceholder: true)
            }
            return cell
        } else {
            guard let cell = productListingCollectionView.dequeueReusableCell(
                    withReuseIdentifier: "ProductListCustomCell",
                    for: indexPath) as? ProductListCustomCell else {
                return UICollectionViewCell()
            }
            if presenter.isFromSearch {
                print("here")
                NukeManager.sharedInstance.setImage(
                    url: presenter.searchProductList[indexPath.row].productImageURL,
                    imageView: cell.productListImageView,
                    withPlaceholder: true)
                cell.priceLabel.text = "\(presenter.searchProductList[indexPath.row].productPrice)"
                cell.productListTiltleLabel.text = presenter.searchProductList[indexPath.row].productDesignerName
                cell.productListSubTitleLabel.text = presenter.searchProductList[indexPath.row].productDescription
            } else {
                NukeManager.sharedInstance.setImage(
                    url: presenter.productList[indexPath.row].productImageURL,
                    imageView: cell.productListImageView,
                    withPlaceholder: true)
                cell.priceLabel.text = "\(presenter.productList[indexPath.row].productPrice)"
                cell.productListTiltleLabel.text = presenter.productList[indexPath.row].productDesignerName
                cell.productListSubTitleLabel.text = presenter.productList[indexPath.row].productDescription
            }
            return cell
        }
    }
}
// MARK: - CollectionView Delegate Method
extension ProductListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        if presenter.isFromSearch {
            if indexPath.row == presenter.searchProductList.count - 1 && !presenter.didReachPageEnd {
                presenter.searchPageNumber += 1
                presenter.requestForSearch()
            }
        } else if indexPath.item == presenter.productList.count - 1 && !presenter.didReachPageEnd {
            presenter.pageNumber += 1
            presenter.requestProductList()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == filterCollectionView {
            guard let cell = collectionView.cellForItem(at: indexPath) as? MainFilterCustomCell else {
                return
            }
            cell.filterLabel.textColor = ColourConstants.hex191919
            cell.filterLabel.font = UIFont(name: FontConstants.semiBold, size: 12)
            cell.filterUnderLineView.isHidden = false
        } else if collectionView == subFilterCollectionView {
            guard let cell = collectionView.cellForItem(at: indexPath) as? SubFilterCustomCell else {
                return
            }
            cell.subFilterImageView.setImageColour(color: ColourConstants.hex000000)
            cell.cardView.backgroundColor = ColourConstants.hexF4F4F4
            cell.subFilterLabel.textColor = ColourConstants.hex000000
        } else if collectionView == productListingCollectionView {
            let storyBoard = UIStoryboard(name: "ProductOrderDetail", bundle: nil)
            guard let nextvc = storyBoard.instantiateViewController(
                    withIdentifier: "ProductOrderDetailViewController")
                    as? ProductOrderDetailViewController else { return }
            if presenter.isFromSearch {
                nextvc.presenter.productSKUID = presenter.searchProductList[indexPath.row].stockKeepUnitID
                nextvc.presenter.productID = presenter.searchProductList[indexPath.row].productID
            } else {
                nextvc.presenter.productSKUID = presenter.productList[indexPath.row].stockKeepUnitID
                nextvc.presenter.productID = presenter.productList[indexPath.row].productID
            }
            self.navigationController?.pushViewController(nextvc, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == filterCollectionView {
            guard let cell = collectionView.cellForItem(at: indexPath) as? MainFilterCustomCell else {
                return
            }
            cell.filterLabel.textColor = ColourConstants.hex717171
            cell.filterLabel.font = UIFont(name: FontConstants.regular, size: 12)
            cell.filterUnderLineView.isHidden = true
        } else if collectionView == subFilterCollectionView {
            guard let cell = collectionView.cellForItem(at: indexPath) as? SubFilterCustomCell else {
                return
            }
            cell.subFilterImageView.setImageColour(color: ColourConstants.hex717171)
            cell.cardView.backgroundColor = ColourConstants.hexFFFFFF
            cell.applyStyling()
        }
    }
}

// MARK: - Hide Blur View for Menu Method
extension ProductListViewController: SideMenuNavigationControllerDelegate {
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        blurView.isHidden = true
    }
}
extension ProductListViewController: ProductListingPresenterProtocol {
    func shoppingBagCount(status: Bool, message: String) {
        if status {
            cartCountLabel.text = "\(presenter.shoppinBagCount)"
        }
    }
    func productListSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        productListingCollectionView.reloadData()
        if presenter.isFromSearch {
            categoryTitleLabel.text = presenter.searchText
        } else {
            categoryTitleLabel.text = presenter.titleLabelText
        }
        if !status {
            if presenter.isFromSearch {
                let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                    bounds: self.view.bounds,
                    title: title,
                    subTitle: message,
                    buttonTitle: "OK") {
                    self.navigationController?.popViewController(animated: true)
                }
                self.view.addSubview(popUp)
            } else {
                let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                    bounds: self.view.bounds,
                    title: title,
                    subTitle: message,
                    buttonTitle: "OK") { return }
                self.view.addSubview(popUp)
            }
        }
    }
}
