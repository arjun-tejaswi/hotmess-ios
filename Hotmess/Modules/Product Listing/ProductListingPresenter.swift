//
//  ProductListingPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 25/06/21.
//

import Foundation
import SwiftyJSON
enum FilterCondition {
    case ANDCondition
    case ORCondition
}
protocol ProductListingPresenterProtocol: AnyObject {
    func productListSuccess(status: Bool, title: String, message: String)
    func shoppingBagCount(status: Bool, message: String)
}

class ProductListingPresenter: NSObject {
    weak var delegate: ProductListingPresenterProtocol?
    var titleLabelText: String = ""
    var pageNumber: Int = 1
    var totalPage: Int = 5
    var listCount: Int = 0
    var productList = [ProductListRealm]()
    var searchProductList = [ProductListRealm]()
    var isFromSearch: Bool = false
    var searchText: String = ""
    var searchPageNumber: Int = 1
    var didReachPageEnd: Bool = false
    var filters: Any = [:]
    var shoppingBagItemArray = [ShoppingBagRealm]()
    var shoppinBagCount: Int = 0
    func getAFilter(key: String, value: String) -> [String: Any] {
        return [
            "key": key, "value": value
        ]
    }
//    func getFilters() {
//        let filter1 = getAFilter(key: "variants.colour", value: "black")
//        let filter2 = getAFilter(key: "variants.colour", value: "red")
//        let filter3 = getAFilter(key: "variants.colour", value: "black")
//        let filter4 = getAFilter(key: "variants.colour", value: "red")
//        let AndFilters: Any = [filter1, filter2]
//        let ORFilters: Any = [filter3, filter4]
//        let params: [String: Any] = [
//            "filters": ["AND": AndFilters, "OR": ORFilters],
//            "pagination": [
//                "per_page": 10,
//                "page_number": 1
//            ]
//        ]
//
//    }

    func requestProductList() {
//        let filter1 = getAFilter(key: "variants.colour", value: "black")
//        let filter2 = getAFilter(key: "variants.colour", value: "red")
//        let filter3 = getAFilter(key: "variants.colour", value: "black")
//        let filter4 = getAFilter(key: "variants.colour", value: "red")
//        let andFilters: Any = [filter1, filter2]
//        let orFilters: Any = [filter3, filter4]
        let params: [String: Any] = [
            "filters": filters,
            "pagination": [
                "per_page": 10,
                "page_number": pageNumber
            ]
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.productListing,
            prametres: params,
            type: .withoutAuth,
            method: .POST,
            completion: self.handelProductListResponse)
    }
    func requestForSearch() {
        let pagination: [String: Any] = [
            "value": searchText,
            "pagination": [
                "per_page": 10,
                "page_number": searchPageNumber
            ]
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.searchProduct,
            prametres: pagination,
            type: .withoutAuth,
            method: .POST,
            completion: self.handleSearchListResponse)
    }
    func handleSearchListResponse(resposnse: JSON) {
        print(resposnse)
        if let status = resposnse["error_status"].bool {
            if status {
                delegate?.productListSuccess(status: false, title: "Error", message: resposnse["message"].string ?? "")
            } else {
                let totalRecords = resposnse["pagination"]["total_records"]
                print(totalRecords)
                if  totalRecords <= 0 {
                    delegate?.productListSuccess(status: false, title: "Error", message: "No Such Products Found!")
                } else {
                    if let data = resposnse["data"].array {
                        for aProduct in data {
                            if let aProductDictionary = aProduct.dictionary {
                                let allproducts = ProductParser.sharedInstance.parseProductData(
                                    data: aProductDictionary)
                                searchProductList.append(allproducts)
                            }
                        }
                        if data.count <= 0 {
                            didReachPageEnd = true
                        }
                        delegate?.productListSuccess(status: true, title: "", message: "")
                    } else {
                        delegate?.productListSuccess(status: false, title: "Error", message: "Server Error")
                    }
                }
            }
        }
    }
    func handelProductListResponse(resposnse: JSON) {
        print(resposnse)
        if let status = resposnse["error_status"].bool {
            if status {
                delegate?.productListSuccess(status: false, title: "Error", message: resposnse["message"].string ?? "")
            } else {
                if let data = resposnse["data"].array {
                    for aProduct in data {
                        if let aProductDictionary = aProduct.dictionary {
                            let allproducts = ProductParser.sharedInstance.parseProductData(data: aProductDictionary)
                            productList.append(allproducts)
                        }
                    }
                    if data.count <= 0 {
                        didReachPageEnd = true
                    }
                    delegate?.productListSuccess(status: true, title: "", message: "")
                } else {
                    delegate?.productListSuccess(status: false, title: "Error", message: "Server Error")
                }
            }
        }
    }
    func requestShoppingBag() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.shoppingBag,
                                                             prametres: [:],
                                                             type: .withAuth,
                                                             method: .POST,
                                                             completion: handleShoppingBagResponse)
        } else {
            shoppingBagItemArray.removeAll()
            shoppinBagCount = RealmDataManager.sharedInstance.getAllProductsFromBag().count
            delegate?.shoppingBagCount(status: true, message: "")
        }
    }
    func handleShoppingBagResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                let message = response["message"].string ?? ""
                if message == "requestForRefresh" {
                    NetworkManager.sharedInstance.refreshBearerToken {
                        self.requestShoppingBag()
                    }
                }
            } else {
                if let items = response["data"].array {
                    shoppinBagCount = 0
                    for _ in items {
                        shoppinBagCount += 1
                    }
                    delegate?.shoppingBagCount(status: true, message: "")
                }
            }
        }
    }
}
