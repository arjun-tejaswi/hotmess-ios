//
//  OrderCancelledViewController.swift
//  Hotmess
//
//  Created by Cedan Misquith on 01/09/21.
//

import UIKit

class OrderCancelledReturnedViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    var isCancel: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyling()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func applyStyling() {
        titleLabel.textColor = ColourConstants.hex191919
        subtitleLabel.textColor = ColourConstants.hex191919
        descriptionLabel.textColor = ColourConstants.hex191919
        titleLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        subtitleLabel.font = UIFont(name: FontConstants.regular, size: 16)
        descriptionLabel.font = UIFont(name: FontConstants.regular, size: 15)
        okButton.backgroundColor = ColourConstants.hex191919
        okButton.setTitleColor(.white, for: .normal)
        if isCancel {
            titleLabel.text = "THAT’S IT, CANCELED"
            subtitleLabel.text = "Your cancelation request was successful"
        } else {
            titleLabel.text = "RETURN REQUEST SUBMITTED"
            subtitleLabel.text = "Your request was successful"
        }
        let descriptionLabelContent01 = "Refund has been initiated. "
        let descriptionLabelContent02 = "It will take 5-7 working days to reflect in your source account."
        descriptionLabel.text = "\(descriptionLabelContent01)\(descriptionLabelContent02)"
    }
    @IBAction func okButtonAction(sender: UIButton) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as
            [UIViewController]
        self.navigationController!.popToViewController(
            viewControllers[viewControllers.count - 4],
            animated: true)
        NotificationCenter.default.post(name: Notification.Name("ORDER_CANCELLED"), object: nil)
    }
}
