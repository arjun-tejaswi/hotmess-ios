//
//  ListOrdersViewController.swift
//  Hotmess
//
//  Created by Cedan Misquith on 17/08/21.
//

import UIKit

class ListOrdersViewController: UIViewController {
    @IBOutlet weak var noOrderView: UIView!
    @IBOutlet weak var backtoShopButton: UIButton!
    @IBOutlet weak var noOrdersDescriptionLabel: UILabel!
    @IBOutlet weak var noOrdersTitleLabel: UILabel!
    @IBOutlet weak var myOrdersTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var presenter: ListOrdersPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        applyFont()
        applyStyling()
        configureTableView()
        presenter = ListOrdersPresenter()
        presenter.delegate = self
        self.view.showLoader()
        presenter.getOrders()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(orderCancelledRefresh),
                                               name: Notification.Name("ORDER_CANCELLED"),
                                               object: nil)
    }
    deinit {
        print("NO MEMORY LEAK: OS Reclaiming memory for 'ListOrdersViewController'")
        NotificationCenter.default.removeObserver(self, name: Notification.Name("ORDER_CANCELLED"), object: nil)
    }
    @objc func orderCancelledRefresh() {
        presenter.ordersSection.removeAll()
        presenter.orderList.removeAll()
        self.view.showLoader()
        presenter.getOrders()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func applyFont() {
        myOrdersTitleLabel.font = UIFont(name: FontConstants.medium, size: 14)
        noOrdersTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        noOrdersDescriptionLabel.font = UIFont(name: FontConstants.regular, size: 15)
        backtoShopButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
    }
    func applyStyling() {
        noOrdersTitleLabel.textColor = ColourConstants.hex191919
        noOrdersDescriptionLabel.textColor = ColourConstants.hex191919
        myOrdersTitleLabel.applyLetterSpacing(spacing: 0.7)
        backtoShopButton.backgroundColor = ColourConstants.hex191919
        backtoShopButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        backtoShopButton.applyButtonLetterSpacing(spacing: 2.52)
    }
    func configureTableView() {
        tableView.register(UINib(nibName: "ActiveOrderCustomCell", bundle: nil),
                           forCellReuseIdentifier: "ActiveOrderCustomCell")
        tableView.register(UINib(nibName: "CompletedOrdersCustomCell", bundle: nil),
                           forCellReuseIdentifier: "CompletedOrdersCustomCell")
    }
    @IBAction func backToShopButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func trackOrderButtonAction(sender: UIButton) {
        guard let nextvc = storyboard?.instantiateViewController(
                withIdentifier: "ViewOrderDetailsViewController") as? ViewOrderDetailsViewController else {
            return
        }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
extension ListOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.ordersSection[section].orders.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.ordersSection.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return presenter.ordersSection[section].name
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if presenter.ordersSection[section].name != "" {
            return 45
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = UIColor.white
        if let header = view as? UITableViewHeaderFooterView {
            header.textLabel?.font = UIFont(name: FontConstants.semiBold, size: 16)
            header.textLabel?.textColor = ColourConstants.hex191919
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let order = presenter.ordersSection[indexPath.section].orders[indexPath.row]
        let count = presenter.ordersSection[indexPath.section].orders.count
        if order.status == "CREATED" {
            guard let cell = tableView.dequeueReusableCell(
                    withIdentifier: "ActiveOrderCustomCell") as? ActiveOrderCustomCell else {
                return UITableViewCell()
            }
            cell.applyData(order: order)
            if indexPath.row == count - 1 {
                cell.separterView.isHidden = true
            } else {
                cell.separterView.isHidden = false
            }
            cell.selectionStyle = .none
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(
                    withIdentifier: "CompletedOrdersCustomCell") as? CompletedOrdersCustomCell else {
                return UITableViewCell()
            }
            if order.status == "DELIVERED" {
                let deliveryDate = Utilities.sharedInstance.convertServerDate(value: order.deliveredAt, delivered: true)
                cell.applyData(order: order)
                cell.dateLabel.text = "On \(deliveryDate)"
                cell.selectionStyle = .none
                return cell
            } else if order.status == "CANCELLED"{
                cell.applyData(order: order)
                cell.selectionStyle = .none
                return cell
            } else if order.status == "RETURNED" {
                cell.applyData(order: order)
                cell.selectionStyle = .none
                return cell
            } else {
                cell.applyData(order: order)
                cell.selectionStyle = .none
                return cell
            }
        }
    }
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if presenter.orderList.count < presenter.totalRecords &&
//            indexPath.section == presenter.ordersSection.count - 1 &&
//            indexPath.row == presenter.ordersSection[indexPath.section].orders.count - 1 {
//            presenter.pageNumber += 1
//            self.view.showLoader()
//            presenter.getOrders()
//        }
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let nextvc = storyboard?.instantiateViewController(
                withIdentifier: "ViewOrderDetailsViewController") as? ViewOrderDetailsViewController else {
            return
        }
        nextvc.presenter.orderId = presenter.ordersSection[indexPath.section].orders[indexPath.row].orderId
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
extension ListOrdersViewController: ListOrdersPresenterProtocol {
    func loadData(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if !status {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
    func showNoOrders() {
        self.view.hideLoader()
        noOrderView.isHidden = false
    }
    func loadDataInList() {
        self.view.hideLoader()
        tableView.reloadData()
    }
}
