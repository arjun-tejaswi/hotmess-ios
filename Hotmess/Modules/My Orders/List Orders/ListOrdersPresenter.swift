//
//  ListOrdersPresenter.swift
//  Hotmess
//
//  Created by Cedan Misquith on 19/08/21.
//

import Foundation
import SwiftyJSON
import RealmSwift

class OrderListRealm: Object {
    @objc dynamic var locality: String = ""
    @objc dynamic var giftFrom: String = ""
    @objc dynamic var city: String = ""
    @objc dynamic var customerLastName: String = ""
    @objc dynamic var customerFirstName: String = ""
    @objc dynamic var customerId: String = ""
    @objc dynamic var paymentType: String = ""
    @objc dynamic var updatedAt: Int64 = 0
    @objc dynamic var orderId: String = ""
    @objc dynamic var state: String = ""
    @objc dynamic var country: String = ""
    @objc dynamic var shippedAt: Int64 = 0
    @objc dynamic var subTotal: Int = 0
    @objc dynamic var giftTo: String = ""
    @objc dynamic var paymentCapturedAt: Int64 = 0
    @objc dynamic var status: String = ""
    @objc dynamic var pinCode: String = ""
    @objc dynamic var landmark: String = ""
    @objc dynamic var isGift: Bool = false
    @objc dynamic var promocodeDiscount: Int = 0
    @objc dynamic var paymentRefundedAt: Int64 = 0
    @objc dynamic var cancelledAt: Int64 = 0
    @objc dynamic var customerPhoneNumber: Int = 0
    @objc dynamic var deliveredAt: Int64 = 0
    @objc dynamic var address: String = ""
    @objc dynamic var orderReturnTime: Int64 = 0
    @objc dynamic var total: Int = 0
    @objc dynamic var paymentStatus: String = ""
    @objc dynamic var returnedAt: Int = 0
    @objc dynamic var paymentInitiatedAt: Int64 = 0
    @objc dynamic var promocode: String = ""
    @objc dynamic var paymentDeclainedAt: Int64 = 0
    @objc dynamic var orderCancellationTime: Int64 = 0
    @objc dynamic var giftMessage: String = ""
    @objc dynamic var gettingReadyAt: Int64 = 0
    @objc dynamic var createdAt: Int64 = 0
    @objc dynamic var shippingCharges: Int = 0
    @objc dynamic var deliveryDateFrom: Int64 = 0
    @objc dynamic var deliveryDateTo: Int64 = 0
    @objc dynamic var isCompleted: Bool = false
    var paymentInfo: PaymentInfoRealm!
    var lineItems = List<LineItemsRealm>()
    convenience init(order: JSON) {
        self.init()
        customerId = order["customer_id"].string ?? ""
        paymentType = order["payment_type"].string ?? ""
        orderId = order["order_id"].string ?? ""
        shippedAt = order["shipped_at"].int64 ?? 0
        subTotal = order["sub_total"].int ?? 0
        paymentCapturedAt = order["payment_captured_at"].int64 ?? 0
        status = order["status"].string ?? ""
        paymentRefundedAt = order["payment_refunded_at"].int64 ?? 0
        cancelledAt = order["cancelled_at"].int64 ?? 0
        customerPhoneNumber = order["customer_phone_number"].int ?? 0
        deliveredAt = order["delivered_at"].int64 ?? 0
        address = order["address"].string ?? ""
        orderReturnTime = order["order_return_time"].int64 ?? 0
        total = order["total"].int ?? 0
        paymentStatus = order["payment_status"].string ?? ""
        returnedAt = order["returned_at"].int ?? 0
        paymentInitiatedAt = order["payment_initiated_at"].int64 ?? 0
        promocode = order["promocode"].string ?? ""
        paymentDeclainedAt = order["payment_declained_at"].int64 ?? 0
        orderCancellationTime = order["order_cancellation_time"].int64 ?? 0
        gettingReadyAt = order["getting_ready_at"].int64 ?? 0
        createdAt = order["created_at"].int64 ?? 0
        shippingCharges = order["shipping_charges"].int ?? 0
        deliveryDateFrom = order["delivery_date_from"].int64 ?? 0
        deliveryDateTo = order["delivery_date_to"].int64 ?? 0
        isCompleted = order["is_completed"].bool ?? false
        if let paymentInfoDict = order["payment_info"].dictionary {
            paymentInfo = PaymentInfoRealm.init(info: paymentInfoDict)
        }
        if let lineItemsDict = order["line_items"].array {
            var itemsArray = [LineItemsRealm]()
            for item in lineItemsDict {
                itemsArray.append(LineItemsRealm.init(item: item.dictionary))
            }
            lineItems.append(objectsIn: itemsArray)
        }
    }
}

protocol ListOrdersPresenterProtocol: AnyObject {
    func loadDataInList()
    func showNoOrders()
    func loadData(status: Bool, title: String, message: String)
}

struct OrderListSection {
    var name: String
    var orders: [OrderListRealm]
    init(name: String, orders: [OrderListRealm]) {
        self.name = name
        self.orders = orders
    }
}

class ListOrdersPresenter: NSObject {
    weak var delegate: ListOrdersPresenterProtocol?
    var pageNumber: Int = 1
    var totalRecords: Int = 0
    var didReachPageEnd: Bool = false
    var ordersSection = [OrderListSection]()
    var orderList = [OrderListRealm]()
    func getOrders() {
        let params: [String: Any] = [
            "pagination": [
                "per_page": 200,
                "page_number": 1
            ]
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.orderList,
            prametres: params,
            type: .withAuth, method: .POST, completion: handleResponseForOrderList)
    }
    func handleResponseForOrderList(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.getOrders()
                        }
                    } else {
                        delegate?.loadData(status: false, title: "Error", message: message)
                    }
                }
            } else {
                if let paginationData = response["pagination"].dictionary {
                    totalRecords = paginationData["total_records"]?.int ?? 0
                }
                if let data = response["data"].array {
                    for order in data {
                        orderList.append(OrderListRealm.init(order: order))
                    }
                    if data.count <= 0 {
                        didReachPageEnd = true
                    }
                    if orderList.count > 0 {
                        sortList(orders: orderList)
                    } else {
                        delegate?.showNoOrders()
                    }
                }
            }
        }
    }
    func sortList(orders: [OrderListRealm]) {
        ordersSection.removeAll()
        var completedOrders = [OrderListRealm]()
        var notCompletedOrders = [OrderListRealm]()
        for order in orders {
            if order.isCompleted {
                completedOrders.append(order)
            } else {
                notCompletedOrders.append(order)
            }
        }
        if notCompletedOrders.count > 0 {
            ordersSection.append(OrderListSection.init(name: "", orders: notCompletedOrders))
        }
        if completedOrders.count > 0 {
            ordersSection.append(OrderListSection.init(name: "COMPLETED ORDERS", orders: completedOrders))
        }
        delegate?.loadDataInList()
    }
}
