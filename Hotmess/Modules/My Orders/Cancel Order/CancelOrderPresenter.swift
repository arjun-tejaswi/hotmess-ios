//
//  CancelOrderPresenter.swift
//  Hotmess
//
//  Created by Cedan Misquith on 03/09/21.
//

import UIKit

protocol CancelOrderPresenterProtocol: AnyObject {
    func orderCancelled()
    func errorCancelling(message: String)
}

class CancelOrderPresenter: NSObject {
    weak var delegate: CancelOrderPresenterProtocol?
    var selectedReason: Int = -1
    var isTermsAgreed: Bool = false
    var orderDetails: OrderRealm!
    var cancelReasons = [
        "Change in delivery address",
        "Recipient not available at the estimated time",
        "Cash not available for COD",
        "Different from photograph",
        "Cheaper alternative available for lesser price",
        "Product is not required anymore"
    ]
    var priceBreakUp: [PriceListingModel] = []
    func configurePriceBreakUp() {
        for item in orderDetails.lineItems {
            let price = String(format: "%0.2f", Double(item.subTotal))
            priceBreakUp.append(PriceListingModel.init(
                                    title: "\(item.designerName)\n(\(item.itemDescription))",
                                    price: "₹ \(price)\n"))
        }
        if orderDetails.shippingChargers > 0 {
            priceBreakUp.append(PriceListingModel.init(
                                    title: "Shipping Charges",
                                    price: String(format: "%0.2f", Double(orderDetails.shippingChargers))))
        }
    }
    func cancelOrder(comment: String) {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.cancelOrder,
            prametres: [
                "order_id": orderDetails.orderId,
                "comments": comment,
                "cancellation_reason": cancelReasons[selectedReason]
            ],
            type: .withAuth, method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.cancelOrder(comment: comment)
                            }
                        } else {
                            self.delegate?.errorCancelling(message: message)
                        }
                    }
                } else {
                    self.delegate?.orderCancelled()
                }
            }
        }
    }
    func getUnderlineAttribute(font: UIFont, color: UIColor, text: String) -> NSAttributedString {
        let policyButtonAttributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let attributedString = NSMutableAttributedString(
            string: text,
            attributes: policyButtonAttributes
        )
        return attributedString
    }
    func createTermsAttributedText(text: String) -> NSMutableAttributedString {
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms and Conditions")
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle,
                                          value: NSUnderlineStyle.single.rawValue,
                                          range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor,
                                          value: ColourConstants.hex717171,
                                          range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.font,
                                          value: UIFont(
                                            name: FontConstants.regular, size: 11) ?? UIFont.systemFont(ofSize: 11),
                                          range: range1)
        let range2 = (text as NSString).range(of: "Return Policy")
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle,
                                          value: NSUnderlineStyle.single.rawValue,
                                          range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor,
                                          value: ColourConstants.hex717171,
                                          range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.font,
                                          value: UIFont(
                                            name: FontConstants.regular, size: 11) ?? UIFont.systemFont(ofSize: 11),
                                          range: range2)
        return underlineAttriString
    }
}
