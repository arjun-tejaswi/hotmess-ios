//
//  CancelOptionsCustomCell.swift
//  Hotmess
//
//  Created by Cedan Misquith on 06/09/21.
//

import UIKit

class CancelOptionsCustomCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkBoxImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyStyling()
    }
    func applyStyling() {
        titleLabel.font = UIFont(name: FontConstants.regular, size: 14)
        titleLabel.textColor = ColourConstants.hex191919
    }
    func check() {
        checkBoxImageView.image = UIImage(named: "selectedSingleCheckIcon")
    }
    func uncheck() {
        checkBoxImageView.image = UIImage(named: "unselectedSingleCheckIcon")
    }
}
