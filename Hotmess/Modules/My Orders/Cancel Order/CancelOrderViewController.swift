//
//  CancelOrderViewController.swift
//  Hotmess
//
//  Created by Cedan Misquith on 03/09/21.
//

import UIKit

class CancelOrderViewController: UIViewController {
    @IBOutlet weak var refundSummeryTitleLabel: UILabel!
    @IBOutlet weak var orderSummeryPriceStackView: UIStackView!
    @IBOutlet weak var orderSummeryStackView: UIStackView!
    @IBOutlet weak var paymentTypeLabel: UILabel!
    @IBOutlet weak var orderSummeryLabel: UILabel!
    @IBOutlet weak var totalItemLabel: UILabel!
    @IBOutlet weak var deliveryDateLabel: UILabel!
    @IBOutlet weak var deliveredOnLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var mutipleProductView: UIView!
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var cancelDisclaimerView: UIView!
    @IBOutlet weak var cancelDisclaimerLabel: UILabel!
    @IBOutlet weak var cancelOptionsTableView: UITableView!
    @IBOutlet weak var cancelOptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewPolicyButton: UIButton!
    @IBOutlet weak var viewItemsButton: UIButton!
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var refundSummaryContainer: UIView!
    @IBOutlet weak var shippingAddressTitleLabel: UILabel!
    @IBOutlet weak var addressContainerView: UIView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var contactContainerView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var termsConditionsLabel: UILabel!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var updateToTitleLabel: UILabel!
    @IBOutlet weak var emailIdTitleLabel: UILabel!
    @IBOutlet weak var mobileNoTitleLabel: UILabel!
    @IBOutlet weak var emailIdLabel: UILabel!
    @IBOutlet weak var mobileNoLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var refundStatusTitleLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var amountDisclaimerLabel: UILabel!
    var presenter = CancelOrderPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        presenter.configurePriceBreakUp()
        applyStyling()
        applyAttributedText()
        applyFonts()
        applyData()
    }
    override func viewWillLayoutSubviews() {
        self.cancelOptionHeightConstraint.constant = self.cancelOptionsTableView.intrinsicContentSize.height
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func applyStyling() {
        productImageView.layer.borderWidth = 2
        productImageView.layer.borderColor = UIColor.white.cgColor
        mutipleProductView.backgroundColor = ColourConstants.hexEAEBEB
        cancelDisclaimerLabel.textColor = ColourConstants.hexEF5353
        cancelDisclaimerView.backgroundColor = ColourConstants.hexFFF2F2
        commentsTextView.backgroundColor = ColourConstants.hexF8F8F8
        commentsTextView.placeholder = "If any additional comments"
        refundSummaryContainer.backgroundColor = ColourConstants.hexF8F8F8
        shippingAddressTitleLabel.textColor = ColourConstants.hex191919
        addressContainerView.layer.borderWidth = 0.5
        addressContainerView.layer.borderColor = ColourConstants.hexCECECE.cgColor
        contactContainerView.backgroundColor = ColourConstants.hexF8F8F8
        contactContainerView.layer.borderWidth = 0.5
        contactContainerView.layer.borderColor = ColourConstants.hexCECECE.cgColor
        cancelButton.backgroundColor = ColourConstants.hex191919
        cancelButton.setTitleColor(.white, for: .normal)
        termsConditionsLabel.textColor = ColourConstants.hex717171
        nameLabel.textColor = ColourConstants.hex191919
        addressLabel.textColor = ColourConstants.hex191919
        phoneNumberLabel.textColor = ColourConstants.hex191919
        updateToTitleLabel.textColor = ColourConstants.hex191919
        emailIdTitleLabel.textColor = ColourConstants.hex191919
        mobileNoTitleLabel.textColor = ColourConstants.hex191919
        emailIdLabel.textColor = ColourConstants.hex191919
        mobileNoLabel.textColor = ColourConstants.hex191919
        totalLabel.textColor = ColourConstants.hex191919
        amountLabel.textColor = ColourConstants.hex191919
        refundStatusTitleLabel.textColor = ColourConstants.hex191919
        cardNumberLabel.textColor = ColourConstants.hex191919
        amountDisclaimerLabel.textColor = ColourConstants.hexDE650D
        orderDateLabel.textColor = ColourConstants.hex191919
        deliveredOnLabel.textColor = ColourConstants.hex1B8CD3
        deliveryDateLabel.textColor = ColourConstants.hex191919
        totalItemLabel.textColor = ColourConstants.hex191919
        orderSummeryLabel.textColor = ColourConstants.hex191919
        paymentTypeLabel.textColor = ColourConstants.hex191919
        refundSummeryTitleLabel.applyLetterSpacing(spacing: 0.8)
    }
    func applyAttributedText() {
        let viewPolicyAttributeText = presenter.getUnderlineAttribute(
            font: UIFont(name: FontConstants.medium, size: 13) ?? UIFont.systemFont(ofSize: 14),
            color: ColourConstants.hex191919,
            text: "View Policy")
        let viewItemsAttributeText = presenter.getUnderlineAttribute(
            font: UIFont(name: FontConstants.medium, size: 13) ?? UIFont.systemFont(ofSize: 14),
            color: ColourConstants.hex191919,
            text: "View items in this order")
        viewPolicyButton.setAttributedTitle(viewPolicyAttributeText, for: .normal)
        viewItemsButton.setAttributedTitle(viewItemsAttributeText, for: .normal)
        termsConditionsLabel.attributedText = presenter.createTermsAttributedText(
            text: "Please accept our Terms and Conditions & Return Policy")
        let termsLabepTap = UITapGestureRecognizer(target: self, action: #selector(didTapTermsLabel(_:)))
        termsConditionsLabel.addGestureRecognizer(termsLabepTap)
    }
    func applyFonts() {
        shippingAddressTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        cancelButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        termsConditionsLabel.font = UIFont(name: FontConstants.regular, size: 11)
        nameLabel.font = UIFont(name: FontConstants.medium, size: 18)
        addressLabel.font = UIFont(name: FontConstants.light, size: 16)
        phoneNumberLabel.font = UIFont(name: FontConstants.regular, size: 16)
        updateToTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        emailIdTitleLabel.font = UIFont(name: FontConstants.medium, size: 14)
        mobileNoTitleLabel.font = UIFont(name: FontConstants.medium, size: 14)
        emailIdLabel.font = UIFont(name: FontConstants.regular, size: 14)
        mobileNoLabel.font = UIFont(name: FontConstants.regular, size: 14)
        totalLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
        amountLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
        refundStatusTitleLabel.font = UIFont(name: FontConstants.medium, size: 12)
        cardNumberLabel.font = UIFont(name: FontConstants.medium, size: 12)
        amountDisclaimerLabel.font = UIFont(name: FontConstants.regular, size: 12)
        orderDateLabel.font = UIFont(name: FontConstants.regular, size: 11)
        deliveredOnLabel.font = UIFont(name: FontConstants.regular, size: 14)
        deliveryDateLabel.font = UIFont(name: FontConstants.regular, size: 11)
        totalItemLabel.font = UIFont(name: FontConstants.regular, size: 14)
        orderSummeryLabel.font = UIFont(name: FontConstants.regular, size: 14)
        paymentTypeLabel.font = UIFont(name: FontConstants.regular, size: 11)
        refundSummeryTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
    }
    func applyData() {
        let cancellTillDate = Utilities.sharedInstance.convertServerDate(
            value: presenter.orderDetails.orderCancellationTime, delivered: true)
        let orderedDate = Utilities.sharedInstance.convertServerDate(
            value: presenter.orderDetails.createdAt, delivered: false)
        let deliveryDateFrom = Utilities.sharedInstance.convertServerDate(
            value: presenter.orderDetails.deliveryDateFrom, delivered: false)
        let deliveryDateTo = Utilities.sharedInstance.convertServerDate(
            value: presenter.orderDetails.deliveryDateTo, delivered: false)
        cancelDisclaimerLabel.text = "Cancellation available till \(cancellTillDate)"
        orderDateLabel.text = "Ordered on \(orderedDate)"
        deliveryDateLabel.text = "\(deliveryDateFrom) - \(deliveryDateTo)"
        if let order = presenter.orderDetails {
            applyLineItemsData()
            let attribute1: [NSAttributedString.Key: Any] = [
                NSAttributedString.Key.font:
                    UIFont(name: FontConstants.regular, size: 14) ?? UIFont.systemFont(ofSize: 14),
                NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
            let attribute2: [NSAttributedString.Key: Any] = [
                NSAttributedString.Key.font:
                    UIFont(name: FontConstants.medium, size: 14) ?? UIFont.boldSystemFont(ofSize: 14),
                NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
            let orderSummery = NSMutableAttributedString(string: "Order Summary", attributes: attribute1)
            let price = NSMutableAttributedString(string: " ₹ \(order.total)", attributes: attribute2)
            orderSummery.append(price)
            orderSummeryLabel.attributedText = orderSummery
            paymentTypeLabel.text = order.paymentType == "COD" ? "Payment | COD" : "Payment |  Card XXXX XXXX"
            let url = URL(string: order.lineItems.first?.featuredImageURL ?? "")
            NukeManager.sharedInstance.setImage(url: url, imageView: productImageView, withPlaceholder: true)
            cardNumberLabel.text = order.paymentType == "COD" ? "COD" : "XXXX XXXX"
            amountLabel.text = "₹ \(order.total)"
            nameLabel.text = "\(order.customerFirstName) \(order.customerLastName)"
            addressLabel.text = "\(order.address),\n\(order.locality)" +
                "\n\(order.city), " +
                "\(order.state) - \n\(order.pinCode)" +
                "\n\(order.country)" +
                "\nLandmark : \(order.landmark)"
            phoneNumberLabel.text = "Mobile No. +91 \(order.customerPhoneNumber)"
            emailIdLabel.text = order.customerId
            mobileNoLabel.text = "+91 \(order.customerPhoneNumber)"
        }
        applyPriceBreakUpData()
    }
}
extension CancelOrderViewController {
    func applyLineItemsData() {
        let orderLineItems = presenter.orderDetails.lineItems.count
        totalItemLabel.text = orderLineItems == 1 ?
            "Total Item: \(orderLineItems)" : "Total Items: \(orderLineItems)"
        if orderLineItems > 1 {
            mutipleProductView.isHidden = false
            viewItemsButton.isHidden = false
        } else {
            mutipleProductView.isHidden = true
            viewItemsButton.isHidden = true
        }
    }
    func applyPriceBreakUpData() {
        for orderTitles in presenter.priceBreakUp {
            let orderPriceTitleLabel = UILabel()
            orderPriceTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
            orderPriceTitleLabel.textColor = ColourConstants.hex191919
            orderPriceTitleLabel.text = orderTitles.title
            orderPriceTitleLabel.numberOfLines = 2
            orderSummeryStackView.addArrangedSubview(orderPriceTitleLabel)
        }
        for orderPrice in presenter.priceBreakUp {
            let orderPriceLabel = UILabel()
            orderPriceLabel.font = UIFont(name: FontConstants.regular, size: 14)
            orderPriceLabel.textColor = ColourConstants.hex191919
            orderPriceLabel.numberOfLines = 2
            if orderPrice.title == "PromoCode Discount" {
                orderPriceLabel.text = "- \(orderPrice.price ?? "0.0")"
            } else {
                orderPriceLabel.text = "\(orderPrice.price ?? "0.0")"
            }
            orderSummeryPriceStackView.addArrangedSubview(orderPriceLabel)
        }
    }
    @objc func didTapTermsLabel(_ sender: UITapGestureRecognizer) {
        let text = termsConditionsLabel.text ?? "Please accept our Terms and Conditions & Return Policy"
        let termsRange = (text as NSString).range(of: "Terms and Conditions")
        let privacyRange = (text as NSString).range(of: "Return Policy")
        let storyBoard = UIStoryboard(name: "WebView", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "WebViewController") as? WebViewController else {
            return
        }
        if sender.didTapAttributedTextInLabel(label: termsConditionsLabel, inRange: termsRange) {
            nextvc.state = .TERMS
        } else if sender.didTapAttributedTextInLabel(label: termsConditionsLabel,
                                                     inRange: privacyRange) {
            nextvc.state = .RETURNPOLICY
        }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @objc func confirmCancelOrderButtonAction(sender: UIButton) {
        self.view.showLoader()
        presenter.cancelOrder(comment: commentsTextView.text ?? "")
    }
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        if presenter.selectedReason > -1 {
            if presenter.isTermsAgreed {
                let popUp = Utilities.sharedInstance.showCancelOrderConfirmationPopUp(bounds: self.view.bounds)
                popUp.cancelOrderButton.addTarget(self,
                                                  action: #selector(confirmCancelOrderButtonAction),
                                                  for: .touchUpInside)
                self.view.addSubview(popUp)
            } else {
                let alertPopUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                    bounds: self.view.bounds,
                    title: "Accept Terms",
                    subTitle: "Please accept our terms and conditions to proceed.",
                    buttonTitle: "OK") { return }
                self.view.addSubview(alertPopUp)
            }
        } else {
            let alertPopUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: "Reason ?",
                subTitle: "Please select a reason for your cancellation.",
                buttonTitle: "OK") { return }
            self.view.addSubview(alertPopUp)
        }
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func viewPolicyButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "WebView", bundle: nil)
        guard let webViewVC = storyBoard.instantiateViewController(
                withIdentifier: "WebViewController") as? WebViewController else {
            return
        }
        webViewVC.state = .CANCELPOLICY
        self.navigationController?.pushViewController(webViewVC, animated: true)
    }
    @IBAction func checkBoxButtonAction(_ sender: UIButton) {
        if presenter.isTermsAgreed {
            presenter.isTermsAgreed = false
            checkBoxButton.setImage(UIImage(named: "uncheckBoxIcon"), for: .normal)
        } else {
            presenter.isTermsAgreed = true
            checkBoxButton.setImage(UIImage(named: "checkBoxIcon"), for: .normal)
        }
    }
    @IBAction func viewItemsButtonAction(_ sender: UIButton) {
        let remainingItemsPopUp = Utilities.sharedInstance.showRemainingItemsPopUp(bounds: UIScreen.main.bounds)
        remainingItemsPopUp.orderDetails = presenter.orderDetails
        self.view.addSubview(remainingItemsPopUp)
    }
}
extension CancelOrderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.cancelReasons.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "CancelOptionsCustomCell") as? CancelOptionsCustomCell else {
            return UITableViewCell()
        }
        if indexPath.row == presenter.selectedReason {
            cell.check()
        } else {
            cell.uncheck()
        }
        cell.titleLabel.text = presenter.cancelReasons[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == cancelOptionsTableView {
            presenter.selectedReason = indexPath.row
            cancelOptionsTableView.reloadData()
        }
    }
}
extension CancelOrderViewController: CancelOrderPresenterProtocol {
    func orderCancelled() {
        self.view.hideLoader()
        let storyBoard = UIStoryboard(name: "MyOrders", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "OrderCancelledReturnedViewController") as? OrderCancelledReturnedViewController else {
            return
        }
        nextvc.isCancel = true
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    func errorCancelling(message: String) {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: "Error Cancelling",
            subTitle: message,
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)
    }
}
