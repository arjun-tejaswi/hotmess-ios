//
//  ViewOrderDetailViewContollerExtention.swift
//  Hotmess
//
//  Created by Flaxon on 09/09/21.
//

import UIKit
import Foundation

extension ViewOrderDetailsViewController {
    func applyData() {
        titleLabel.text = "ORDER ID #\(presenter.orderId)"
        let totalPrice = String(format: "%0.2f", Double(presenter.orderDetails.total))
        totalPriceLabel.text = "₹ \(totalPrice)"
        emailLabel.text = presenter.orderDetails.customerId
        mobileLabel.text = "\(presenter.orderDetails.customerPhoneNumber)"
        nameLabel.text = "\(presenter.orderDetails.customerFirstName) " + presenter.orderDetails.customerLastName
        let fullAddress = "\(presenter.orderDetails.address)," + "\n\(presenter.orderDetails.locality)," +
            "\n\(presenter.orderDetails.state), " + "\(presenter.orderDetails.city)," +
            "\n\(presenter.orderDetails.country)"
        addressLabel.text = fullAddress
        phoneNumberLabel.text = "Mobile No. +91 \(presenter.orderDetails.customerPhoneNumber)"
        paymentStatusIDLabel.text = presenter.orderDetails.paymentType == "COD" ? "COD" : "XXXX XX56"
        configureStatus(status: presenter.orderDetails.status)
        if presenter.orderDetails.orderPlaced &&
            !presenter.orderDetails.orderPrepared &&
            !presenter.orderDetails.orderCollected && !presenter.orderDetails.delivered {
            configureOrderProgress(totalProgress: presenter.getOrderProgress(progress: .ORDERPLACED))
        } else if presenter.orderDetails.orderPlaced &&
                    presenter.orderDetails.orderPrepared &&
                    !presenter.orderDetails.orderCollected && !presenter.orderDetails.delivered {
            configureOrderProgress(totalProgress: presenter.getOrderProgress(progress: .BEINGPREPARED))
        } else if presenter.orderDetails.orderPlaced &&
                    presenter.orderDetails.orderPrepared &&
                    presenter.orderDetails.orderCollected && !presenter.orderDetails.delivered {
            configureOrderProgress(totalProgress: presenter.getOrderProgress(progress: .COLLECTED))
        } else if presenter.orderDetails.orderPlaced &&
                    presenter.orderDetails.orderPrepared &&
                    presenter.orderDetails.orderCollected && presenter.orderDetails.delivered {
            configureOrderProgress(totalProgress: presenter.getOrderProgress(progress: .DELIVERED))
        } else {
            configureOrderProgress(totalProgress: presenter.getOrderProgress(progress: .NONE))
        }
    }
    func configureOrderProgress(totalProgress: [OrderProgressUI]) {
        for progress in totalProgress {
            switch progress {
            case .ORDERPLACED:
                let orderPlacedDate = Utilities .sharedInstance.convertServerDate(
                    value: presenter.orderDetails.orderPlacedDate, delivered: false)
                progressDots[0].backgroundColor = ColourConstants.hex191919
                orderPlacedDateLabel.text = orderPlacedDate
            case .BEINGPREPARED:
                let orderPreparedDate = Utilities .sharedInstance.convertServerDate(
                    value: presenter.orderDetails.orderPreparedDate, delivered: false)
                progressLines[0].backgroundColor = ColourConstants.hex191919
                progressDots[1].backgroundColor = ColourConstants.hex191919
                orderPreparedDateLabel.text = orderPreparedDate
            case .COLLECTED:
                let orderCollectedDate = Utilities .sharedInstance.convertServerDate(
                    value: presenter.orderDetails.orderCollectedDate, delivered: false)
                progressLines[1].backgroundColor = ColourConstants.hex191919
                progressDots[2].backgroundColor = ColourConstants.hex191919
                orderCollectedDateLabel.text = orderCollectedDate
            case .DELIVERED:
                let orderDelivered = Utilities .sharedInstance.convertServerDate(
                    value: presenter.orderDetails.deliveredDate, delivered: false)
                progressLines[2].backgroundColor = ColourConstants.hex191919
                progressDots[3].backgroundColor = ColourConstants.hex191919
                orderDeliveredDateLabel.text = orderDelivered
            case .NONE:
                print("Order is Not Placed")
            }
        }
    }
    func savePDF(file: URL) {
        let activityController = UIActivityViewController(activityItems: [file], applicationActivities: nil)
        DispatchQueue.main.async {
            self.view.hideLoader()
            self.present(activityController, animated: true, completion: nil)
        }
    }
}
extension ViewOrderDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == orderItemTableView {
            if presenter.orderDetails != nil {
                return presenter.orderDetails.lineItems.count
            } else {
                return 0
            }
        } else {
            if presenter.orderDetails != nil {
                var returnedCount: Int = 0
                for item in presenter.orderDetails.lineItems where item.status == "RETURNED" {
                    returnedCount += 1
                }
                return returnedCount
            } else {
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == orderItemTableView {
            return getOrderItemTableViewCell(indexPath: indexPath)
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailCustomCell",
                                                           for: indexPath) as? OrderDetailCustomCell else {
                return UITableViewCell()
            }
            let url = presenter.orderDetails.lineItems[indexPath.row].featuredImageURL
            NukeManager.sharedInstance.setImage(url: URL(string: url),
                                                imageView: cell.productImageView,
                                                withPlaceholder: true)
            cell.productDesignerNameLabel.text = presenter.orderDetails.lineItems[indexPath.row].designerName
            cell.productDescriptionLabel.text = presenter.orderDetails.lineItems[indexPath.row].itemDescription
            cell.productPriceLabel.text = "\(presenter.orderDetails.lineItems[indexPath.row].costPrice)"
            let attribute1: [NSAttributedString.Key: Any] = [
                NSAttributedString.Key.font:
                    UIFont(name: FontConstants.regular, size: 14) ?? UIFont.systemFont(ofSize: 14),
                NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
            let attribute2: [NSAttributedString.Key: Any] = [
                NSAttributedString.Key.font:
                    UIFont(name: FontConstants.medium, size: 14) ?? UIFont.boldSystemFont(ofSize: 14),
                NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
            let size = NSMutableAttributedString(string: "Size\t\t\t", attributes: attribute1)
            let sizeValue = NSMutableAttributedString(
                string: "\(presenter.orderDetails.lineItems[indexPath.row].productSize)",
                attributes: attribute2)
            size.append(sizeValue)
            let quantity = NSMutableAttributedString(string: "Qunatity\t", attributes: attribute1)
            let quantityValue = NSMutableAttributedString(
                string: "\(presenter.orderDetails.lineItems[indexPath.row].quantity)",
                attributes: attribute2)
            quantity.append(quantityValue)
            cell.productQuantityLabel.attributedText = quantity
            cell.productSizeLabel.attributedText = size
            cell.overlayView.isHidden = false
            cell.selectionStyle = .none
            if indexPath.row == presenter.orderDetails.lineItems.count - 1 {
                cell.separaterView.isHidden = true
            } else {
                cell.separaterView.isHidden = false
            }
            return cell
        }
    }
    func getOrderItemTableViewCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = orderItemTableView.dequeueReusableCell(withIdentifier: "OrderDetailCustomCell",
                                                       for: indexPath) as? OrderDetailCustomCell else {
            return UITableViewCell()
        }
        let orderItems = presenter.orderDetails.lineItems[indexPath.row]
        let url = orderItems.featuredImageURL
        NukeManager.sharedInstance.setImage(url: URL(string: url),
                                            imageView: cell.productImageView,
                                            withPlaceholder: true)
        cell.productDesignerNameLabel.text = orderItems.designerName
        cell.productDescriptionLabel.text = orderItems.itemDescription
        cell.productPriceLabel.text = "\(orderItems.costPrice)"
        let attribute1: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.regular, size: 14) ?? UIFont.systemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
        let attribute2: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font:
                UIFont(name: FontConstants.medium, size: 14) ?? UIFont.boldSystemFont(ofSize: 14),
            NSAttributedString.Key.foregroundColor: ColourConstants.hex3C3C3C]
        let size = NSMutableAttributedString(string: "Size\t\t\t", attributes: attribute1)
        let sizeValue = NSMutableAttributedString(string: "\(orderItems.productSize)",
            attributes: attribute2)
        size.append(sizeValue)
        let quantity = NSMutableAttributedString(string: "Qunatity\t", attributes: attribute1)
        let quantityValue = NSMutableAttributedString(string: "\(orderItems.quantity)",
            attributes: attribute2)
        quantity.append(quantityValue)
        cell.productQuantityLabel.attributedText = quantity
        cell.productSizeLabel.attributedText = size
        if orderItems.status == "RETURNED" {
            cell.returnView.isHidden = false
            cell.returnLabel.text = "Returned"
        } else {
            cell.returnView.isHidden = true
        }
        cell.selectionStyle = .none
        if indexPath.row == presenter.orderDetails.lineItems.count - 1 {
            cell.separaterView.isHidden = true
        } else {
            cell.separaterView.isHidden = false
        }
        return cell
    }
}
extension ViewOrderDetailsViewController: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let documentsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let destinationPath = documentsPath.appendingPathComponent("Order Summary - \(presenter.orderId).pdf")
            try? FileManager.default.removeItem(at: destinationPath)
        do {
            try FileManager.default.copyItem(at: location, to: destinationPath)
            savePDF(file: destinationPath)
        } catch {
            self.view.hideLoader()
            print("Copy Error: \(error.localizedDescription)")
        }
    }
}
extension ViewOrderDetailsViewController: ViewOrderProtocol {
    func downloadSummaryFailed() {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: "Error",
            subTitle: "Could not download order summary now. Try again later.",
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)
    }
    func showDownloadSummary(url: String) {
        guard let pdfURL = URL(string: url) else { return }
        let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        let downloadTask = urlSession.downloadTask(with: pdfURL)
        downloadTask.resume()
    }
    func loadData(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            applyData()
            orderItemTableView.reloadData()
            for orderTitles in presenter.priceBreakUp {
                let orderPriceTitleLabel = UILabel()
                orderPriceTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
                orderPriceTitleLabel.textColor = ColourConstants.hex191919
                orderPriceTitleLabel.text = orderTitles.title
                orderSummeryStackView.addArrangedSubview(orderPriceTitleLabel)
            }
            for orderPrice in presenter.priceBreakUp {
                let orderPriceLabel = UILabel()
                orderPriceLabel.font = UIFont(name: FontConstants.regular, size: 14)
                orderPriceLabel.textColor = ColourConstants.hex191919
                if orderPrice.title == "PromoCode Discount" {
                    orderPriceLabel.text = "- ₹ \(orderPrice.price ?? "0.00")"
                } else {
                    orderPriceLabel.text = "₹ \(orderPrice.price ?? "0.00")"
                }
                orderSummeryPriceStackView.addArrangedSubview(orderPriceLabel)
            }
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
}
