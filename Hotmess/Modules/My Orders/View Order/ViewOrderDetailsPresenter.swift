//
//  ViewOrderDetailsPresenter.swift
//  Hotmess
//
//  Created by Cedan Misquith on 01/09/21.
//

import Foundation
import RealmSwift
import SwiftyJSON

enum OrderProgressUI {
    case ORDERPLACED
    case BEINGPREPARED
    case COLLECTED
    case DELIVERED
    case NONE
}

class OrderRealm: Object {
    @objc dynamic var orderReturnTime: Int64 = 0
    @objc dynamic var giftFrom: String = ""
    @objc dynamic var paymentStatus: String = ""
    @objc dynamic var giftMessage: String = ""
    @objc dynamic var updatedAt: Int64 = 0
    @objc dynamic var state: String = ""
    @objc dynamic var createdAt: Int64 = 0
    @objc dynamic var orderId: String = ""
    @objc dynamic var cancelledAt: Int64 = 0
    @objc dynamic var promocodeDiscount: Int = 0
    @objc dynamic var customerFirstName: String = ""
    @objc dynamic var paymentRefundedAt: Int64 = 0
    @objc dynamic var status: String = ""
    @objc dynamic var shippedAt: Int64 = 0
    @objc dynamic var locality: String = ""
    @objc dynamic var country: String = ""
    @objc dynamic var paymentType: String = ""
    @objc dynamic var isGift: Bool = false
    @objc dynamic var customerLastName: String = ""
    @objc dynamic var shippingChargers: Int = 0
    @objc dynamic var tax: Int = 0
    @objc dynamic var expressDeliveryCharges: Int = 0
    @objc dynamic var premiumGiftPackageCharges: Int = 0
    @objc dynamic var landmark: String = ""
    @objc dynamic var total: Int = 0
    @objc dynamic var paymentCapturedAt: Int64 = 0
    @objc dynamic var customerPhoneNumber: String = ""
    @objc dynamic var address: String = ""
    @objc dynamic var pinCode: String = ""
    @objc dynamic var paymentInitiatedAt: Int64 = 0
    @objc dynamic var customerId: String = ""
    @objc dynamic var subTotal: Int = 0
    @objc dynamic var gettingReadyAt: Int64 = 0
    @objc dynamic var deliveredAt: Int64 = 0
    @objc dynamic var promocode: String = ""
    @objc dynamic var returnedAt: Int64 = 0
    @objc dynamic var giftTo: String = ""
    @objc dynamic var orderCancellationTime: Int64 = 0
    @objc dynamic var paymentDeclinedAt: Int64 = 0
    @objc dynamic var city: String = ""
    @objc dynamic var deliveryDateFrom: Int64 = 0
    @objc dynamic var deliveryDateTo: Int64 = 0
    @objc dynamic var isCompleted: Bool = false
    @objc dynamic var orderPlaced: Bool = false
    @objc dynamic var orderPrepared: Bool = false
    @objc dynamic var orderCollected: Bool = false
    @objc dynamic var delivered: Bool = false
    @objc dynamic var orderPlacedDate: Int64 = 0
    @objc dynamic var orderPreparedDate: Int64 = 0
    @objc dynamic var orderCollectedDate: Int64 = 0
    @objc dynamic var deliveredDate: Int64 = 0
    @objc dynamic var isCancellable: Bool = false
    @objc dynamic var isReturnable: Bool = false
    @objc dynamic var refundAmmount: Int64 = 0
    @objc dynamic var returnReason: String = ""
    @objc dynamic var orderTrackingURL: String = ""
    var paymentInfo: PaymentInfoRealm!
    var lineItems = List<LineItemsRealm>()
    convenience init(order: [String: JSON]) {
        self.init()
        orderReturnTime = order["order_return_time"]?.int64 ?? 0
        paymentStatus = order["payment_status"]?.string ?? ""
        state = order["state"]?.string ?? ""
        createdAt = order["created_at"]?.int64 ?? 0
        orderId = order["order_id"]?.string ?? ""
        cancelledAt = order["cancelled_at"]?.int64 ?? 0
        promocodeDiscount = order["promocode_discount"]?.int ?? 0
        customerFirstName = order["customer_first_name"]?.string ?? ""
        status = order["status"]?.string ?? ""
        locality = order["locality"]?.string ?? ""
        country = order["country"]?.string ?? ""
        paymentType = order["payment_type"]?.string ?? ""
        customerLastName = order["customer_last_name"]?.string ?? ""
        shippingChargers = order["shipping_charges"]?.int ?? 0
        tax = order["tax"]?.int ?? 0
        premiumGiftPackageCharges = order["gift_packing_amount"]?.int ?? 0
        expressDeliveryCharges = order["express_delivery_charge"]?.int ?? 0
        landmark = order["landmark"]?.string ?? ""
        total = order["total"]?.int ?? 0
        customerPhoneNumber = order["customer_phone_number"]?.string ?? ""
        address = order["address"]?.string ?? ""
        pinCode = order["pin_code"]?.string ?? ""
        customerId = order["customer_id"]?.string ?? ""
        subTotal = order["sub_total"]?.int ?? 0
        deliveredAt = order["delivered_at"]?.int64 ?? 0
        promocode = order["promocode"]?.string ?? ""
        returnedAt = order["returned_at"]?.int64 ?? 0
        orderCancellationTime = order["order_cancellation_time"]?.int64 ?? 0
        city = order["city"]?.string ?? ""
        deliveryDateFrom = order["delivery_date_from"]?.int64 ?? 0
        deliveryDateTo = order["delivery_date_to"]?.int64 ?? 0
        isCompleted = order["is_completed"]?.bool ?? false
        parseOrderParameters(order)
        if let paymentInfoDict = order["payment_info"]?.dictionary {
            paymentInfo = PaymentInfoRealm.init(info: paymentInfoDict)
        }
        parseLineItems(order)
    }
    fileprivate func parseLineItems(_ order: [String: JSON]) {
        if let lineItemsDict = order["line_items"]?.array {
            var itemsArray = [LineItemsRealm]()
            for item in lineItemsDict {
                itemsArray.append(LineItemsRealm.init(item: item.dictionary))
            }
            lineItems.append(objectsIn: itemsArray)
        }
    }
    fileprivate func parseOrderParameters(_ order: [String: JSON]) {
        orderPlaced = order["order_placed"]?.bool ?? false
        orderPrepared = order["order_prepared"]?.bool ?? false
        orderCollected = order["order_collected"]?.bool ?? false
        delivered = order["delivered"]?.bool ?? false
        orderPlacedDate = order["order_placed_at"]?.int64 ?? 0
        orderPreparedDate = order["order_prepared_at"]?.int64 ?? 0
        orderCollectedDate = order["order_collected_at"]?.int64 ?? 0
        deliveredDate = order["delivered_at"]?.int64 ?? 0
        isCancellable = order["order_cancellation"]?.bool ?? false
        isReturnable = order["order_return"]?.bool ?? false
        refundAmmount = order["refund_amount"]?.int64 ?? 0
        returnReason = order["return_reason"]?.string ?? ""
        orderTrackingURL = order["order_tracking_url"]?.string ?? "https://staging.hotmessfashion.com/"
    }
}
class PaymentInfoRealm: Object {
    @objc dynamic var status: String = ""
    @objc dynamic var orderId: String = ""
    @objc dynamic var createdAt: Int64 = 0
    @objc dynamic var amountDue: Int = 0
    @objc dynamic var attempts: Int = 0
    @objc dynamic var receipt: String = ""
    @objc dynamic var amount: Int = 0
    @objc dynamic var entity: String = ""
    @objc dynamic var amountPaid: String = ""
    @objc dynamic var currency: String = ""
    @objc dynamic var offerId: String = ""
    convenience init(info: [String: JSON]) {
        self.init()
        status = info["status"]?.string ?? ""
        orderId = info["id"]?.string ?? ""
        createdAt = info["created_at"]?.int64 ?? 0
        amountDue = info["amount_due"]?.int ?? 0
        attempts = info["attempts"]?.int ?? 0
        receipt = info["receipt"]?.string ?? ""
        amount = info["amount"]?.int ?? 0
        entity = info["entity"]?.string ?? ""
        amountPaid = info["amount_paid"]?.string ?? ""
        currency = info["currency"]?.string ?? ""
        offerId = info["offer_id"]?.string ?? ""
    }
}
class LineItemsRealm: Object {
    @objc dynamic var subTotal: Int = 0
    @objc dynamic var costPrice: Int = 0
    @objc dynamic var categoryId: String = ""
    @objc dynamic var updatedQuanity: Int = 0
    @objc dynamic var colour: String = ""
    @objc dynamic var productId: String = ""
    @objc dynamic var inStock: Bool = false
    @objc dynamic var quantity: Int = 0
    @objc dynamic var status: String = ""
    @objc dynamic var oldQuantity: Int = 0
    @objc dynamic var packingCharges: Int = 0
    @objc dynamic var featuredImageURL: String = ""
    @objc dynamic var designerName: String = ""
    @objc dynamic var skuId: String = ""
    @objc dynamic var itemDescription: String = ""
    @objc dynamic var colourCode: String = ""
    @objc dynamic var productSize: String = ""
    @objc dynamic var isSelected: Bool = false
    convenience init(item: [String: JSON]?) {
        self.init()
        subTotal = item?["sub_total"]?.int ?? 0
        costPrice = item?["cost_price"]?.int ?? 0
        categoryId = item?["category_id"]?.string ?? ""
        updatedQuanity = item?["new_quanity"]?.int ?? 0
        colour = item?["colour"]?.string ?? ""
        productId = item?["product_id"]?.string ?? ""
        inStock = item?["in_stock"]?.bool ?? false
        quantity = item?["quantity"]?.int ?? 0
        status = item?["status"]?.string ?? ""
        oldQuantity = item?["old_quantity"]?.int ?? 0
        packingCharges = item?["packing_charges"]?.int ?? 0
        featuredImageURL = ApiBaseURL.imageBaseURL + "\(item?["featured_image_url"]?.string ?? "")"
        designerName = item?["designer_name"]?.string ?? ""
        skuId = item?["sku_id"]?.string ?? ""
        itemDescription = item?["description"]?.string ?? ""
        colourCode = item?["colour_code"]?.string ?? ""
        productSize = item?["size"]?.string ?? ""
    }
}

protocol ViewOrderProtocol: AnyObject {
    func loadData(status: Bool, title: String, message: String)
    func showDownloadSummary(url: String)
    func downloadSummaryFailed()
}
class ViewOrderDetailsPresenter: NSObject {
    var priceBreakUp = [PriceListingModel]()
    var orderId: String = ""
    var orderDetails: OrderRealm!
    weak var delegate: ViewOrderProtocol?
    func getOrderDetails() {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.orderDetails,
            prametres: ["order_id": orderId],
            type: .withAuth, method: .POST,
            completion: handleResponseForOrderDetails)
    }
    fileprivate func orderDetailDataParsing(_ response: JSON) {
        if let data = response["data"].dictionary {
            orderDetails = OrderRealm.init(order: data)
            let shippingCharges = PriceListingModel.init(
                title: "Shipping",
                price: String(format: "%0.2f", Double(orderDetails.shippingChargers)))
            let tax = PriceListingModel.init(
                title: "Taxes",
                price: String(format: "%0.2f", Double(orderDetails.tax)))
            let subTotal = PriceListingModel.init(
                title: "Item Subtotal",
                price: String(format: "%0.2f", Double(orderDetails.subTotal)))
            let expressDeliveryCharge = PriceListingModel.init(
                title: "Express Delivery",
                price: String(format: "%0.2f", Double(orderDetails.expressDeliveryCharges)))
            let giftPackingAmount = PriceListingModel.init(
                title: "Premium Gift Packing",
                price: String(format: "%0.2f", Double(orderDetails.premiumGiftPackageCharges)))
            let promoCodeDiscount = PriceListingModel.init(
                title: "PromoCode Discount",
                price: String(format: "%0.2f", Double(orderDetails.promocodeDiscount)))
            if subTotal.price != "0.00" {
                priceBreakUp.append(subTotal)
            }
            if shippingCharges.price != "0.00" {
                priceBreakUp.append(shippingCharges)
            }
            if tax.price != "0.00" {
                priceBreakUp.append(tax)
            }
            if expressDeliveryCharge.price != "0.00" {
                priceBreakUp.append(expressDeliveryCharge)
            }
            if giftPackingAmount.price != "0.00" {
                priceBreakUp.append(giftPackingAmount)
            }
            if promoCodeDiscount.price != "0.00" {
                priceBreakUp.append(promoCodeDiscount)
            }
            delegate?.loadData(status: true, title: "", message: "")
        }
    }
    func handleResponseForOrderDetails(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.getOrderDetails()
                        }
                    } else {
                        delegate?.loadData(status: false, title: "Error", message: message)
                    }
                }
            } else {
                orderDetailDataParsing(response)
            }
        }
    }
    func getOrderProgress(progress: OrderProgressUI) -> [OrderProgressUI] {
        switch progress {
        case .ORDERPLACED:
            return [.ORDERPLACED]
        case .BEINGPREPARED:
            return [.ORDERPLACED, .BEINGPREPARED]
        case .COLLECTED:
            return [.ORDERPLACED, .BEINGPREPARED, .COLLECTED]
        case .DELIVERED:
            return [.ORDERPLACED, .BEINGPREPARED, .COLLECTED, .DELIVERED]
        case .NONE:
            return []
        }
    }
    func getOrderSummary() {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.getOrderSummaryPDF,
            prametres: ["order_id": orderId],
            type: .withAuth, method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if !status {
                    self.delegate?.showDownloadSummary(url: response["pdf_file_url"].string ?? "")
                } else {
                    self.delegate?.downloadSummaryFailed()
                }
            }
        }
    }
}
