//
//  ViewOrderDetailsViewController.swift
//  Hotmess
//
//  Created by Cedan Misquith on 01/09/21.
//

import UIKit

class ViewOrderDetailsViewController: UIViewController {
    @IBOutlet weak var cancelLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var orderSummeryPriceStackView: UIStackView!
    @IBOutlet weak var orderSummeryStackView: UIStackView!
    @IBOutlet weak var returnItemsTableViewHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var returnedItemsTableView: UITableView!
    @IBOutlet weak var cancelledAmountCreditLabel: UILabel!
    @IBOutlet weak var refundSummeryView: UIView!
    @IBOutlet weak var returnedContainerView: UIView!
    @IBOutlet weak var refundTypeLabel: UILabel!
    @IBOutlet weak var refundAmountValueLabel: UILabel!
    @IBOutlet weak var amountCreditedDisclaimerLabel: UILabel!
    @IBOutlet weak var refundStatusLabel: UILabel!
    @IBOutlet weak var refundAmountLabel: UILabel!
    @IBOutlet weak var refundSummeryLabel: UILabel!
    @IBOutlet weak var resonForReturnValueLabel: UILabel!
    @IBOutlet weak var reasonForReturnLabel: UILabel!
    @IBOutlet weak var returnedItemLabel: UILabel!
    @IBOutlet weak var downloadSummeryButton: UIButton!
    @IBOutlet weak var downloadSummeryContainer: UIView!
    @IBOutlet weak var returnDisclaimerLabel: UILabel!
    @IBOutlet weak var returnDisclaimerIcon: UIImageView!
    @IBOutlet weak var returnDisclaimerView: UIView!
    @IBOutlet weak var returnItemContainerView: UIView!
    @IBOutlet weak var returnItemButton: UIButton!
    @IBOutlet weak var exclamationImageView: UIImageView!
    @IBOutlet weak var totalItemLabel: UILabel!
    @IBOutlet weak var orderSummeryTitleLabel: UILabel!
    @IBOutlet weak var paymentStatusIDLabel: UILabel!
    @IBOutlet weak var paymentStatusLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var totalTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var orderProgressView: UIView!
    @IBOutlet weak var actionButtonsView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var trackButton: UIButton!
    @IBOutlet weak var disclaimerView: UIView!
    @IBOutlet weak var cancelDisclaimerLabel: UILabel!
    @IBOutlet weak var orderSummaryContainer: UIView!
    @IBOutlet weak var shippingAddressContainer: UIView!
    @IBOutlet weak var statusContainer: UIView!
    @IBOutlet var progressDots: [UIView]!
    @IBOutlet var progressLines: [UIView]!
    @IBOutlet weak var orderPlacedTitleLabel: UILabel!
    @IBOutlet weak var orderPlacedDateLabel: UILabel!
    @IBOutlet weak var orderPreparedTitleLabel: UILabel!
    @IBOutlet weak var orderPreparedDateLabel: UILabel!
    @IBOutlet weak var orderCollectedTitleLabel: UILabel!
    @IBOutlet weak var orderCollectedDateLabel: UILabel!
    @IBOutlet weak var orderDeliveredTitleLabel: UILabel!
    @IBOutlet weak var orderDeliveredDateLabel: UILabel!
    @IBOutlet weak var shippingAddressTitleLabel: UILabel!
    @IBOutlet weak var addressContainerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var updateContainerView: UIView!
    @IBOutlet weak var updateTitleLabel: UILabel!
    @IBOutlet weak var emailTitleLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var mobileTitleLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var orderItemTableView: CustomIntrinsicTableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var presenter = ViewOrderDetailsPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        applyStyling()
        applyFont()
        configureTableView()
        self.view.showLoader()
        presenter.getOrderDetails()
    }
    override func viewWillLayoutSubviews() {
        self.tableViewHeightConstraint.constant = self.orderItemTableView.intrinsicContentSize.height
        self.returnItemsTableViewHeightContraint.constant = self.returnedItemsTableView.intrinsicContentSize.height
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func returnItemButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "ReturnItem", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(withIdentifier: "ReturnItemViewController") as?
                ReturnItemViewController else { return }
        nextvc.presenter.orderDetails = presenter.orderDetails
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    func configureStatus(status: String) {
        switch status {
        case "CREATED":
            applyOnGoingOrderUI()
        case "CANCELLED":
            applyCancelledOrderUI()
        case "DELIVERED":
            applyDeliveredOrderUI()
        case "RETURNED":
            applyReturnedOrderUI()
        default:
            print("Not under any Category")
        }
    }
    func configureTableView() {
        orderItemTableView.register(UINib(nibName: "OrderDetailCustomCell", bundle: nil),
                                    forCellReuseIdentifier: "OrderDetailCustomCell")
        returnedItemsTableView.register(UINib(nibName: "OrderDetailCustomCell", bundle: nil),
                                        forCellReuseIdentifier: "OrderDetailCustomCell")
    }
    @IBAction func downloadSummaryButtonAction(_ sender: UIButton) {
        self.view.showLoader()
        presenter.getOrderSummary()
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func trackButtonAction(_ sender: UIButton) {
        if let url = URL(string: presenter.orderDetails.orderTrackingURL) {
            UIApplication.shared.open(url,
                                      options: [:],
                                      completionHandler: nil)
        }
    }
    @IBAction func cancelOrderButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "CancelOrder", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "CancelOrderViewController") as? CancelOrderViewController else {
            return
        }
        nextvc.presenter.orderDetails = presenter.orderDetails
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
extension ViewOrderDetailsViewController {
    fileprivate func fontStylingExtensionMethod() {
        paymentStatusLabel.font = UIFont(name: FontConstants.regular, size: 12)
        paymentStatusIDLabel.font = UIFont(name: FontConstants.regular, size: 12)
        returnItemButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        returnDisclaimerLabel.font = UIFont(name: FontConstants.regular, size: 12)
        returnedItemLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
        reasonForReturnLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        resonForReturnValueLabel.font = UIFont(name: FontConstants.regular, size: 16)
        refundSummeryLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        refundAmountLabel.font = UIFont(name: FontConstants.regular, size: 15)
        refundStatusLabel.font = UIFont(name: FontConstants.medium, size: 12)
        amountCreditedDisclaimerLabel.font = UIFont(name: FontConstants.regular, size: 12)
        refundAmountValueLabel.font = UIFont(name: FontConstants.regular, size: 15)
        refundTypeLabel.font = UIFont(name: FontConstants.regular, size: 15)
        cancelledAmountCreditLabel.font = UIFont(name: FontConstants.regular, size: 11)
    }
    func applyFont() {
        titleLabel.font = UIFont(name: FontConstants.medium, size: 14)
        nameLabel.font = UIFont(name: FontConstants.medium, size: 18)
        addressLabel.font = UIFont(name: FontConstants.light, size: 16)
        phoneNumberLabel.font = UIFont(name: FontConstants.regular, size: 16)
        shippingAddressTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        updateTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        emailTitleLabel.font = UIFont(name: FontConstants.medium, size: 14)
        mobileTitleLabel.font = UIFont(name: FontConstants.medium, size: 14)
        emailLabel.font = UIFont(name: FontConstants.regular, size: 14)
        mobileLabel.font = UIFont(name: FontConstants.regular, size: 14)
        orderSummeryTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        totalItemLabel.font = UIFont(name: FontConstants.regular, size: 14)
        cancelButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        trackButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        cancelDisclaimerLabel.font = UIFont(name: FontConstants.regular, size: 12)
        orderPlacedTitleLabel.font = UIFont(name: FontConstants.medium, size: 14)
        orderPreparedTitleLabel.font = UIFont(name: FontConstants.medium, size: 14)
        orderCollectedTitleLabel.font = UIFont(name: FontConstants.medium, size: 14)
        orderDeliveredTitleLabel.font = UIFont(name: FontConstants.medium, size: 14)
        orderPlacedDateLabel.font = UIFont(name: FontConstants.light, size: 12)
        orderPreparedDateLabel.font = UIFont(name: FontConstants.light, size: 12)
        orderCollectedDateLabel.font = UIFont(name: FontConstants.light, size: 12)
        orderDeliveredDateLabel.font = UIFont(name: FontConstants.light, size: 12)
        statusLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        dateLabel.font = UIFont(name: FontConstants.regular, size: 14)
        totalTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
        totalPriceLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
        fontStylingExtensionMethod()
    }
    func applyStyling() {
        applyTextColour()
        titleLabel.applyLetterSpacing(spacing: 0.7)
        orderSummeryTitleLabel.applyLetterSpacing(spacing: 0.7)
        cancelButton.layer.borderWidth = 1
        cancelButton.layer.borderColor = ColourConstants.hex9B9B9B.cgColor
        cancelButton.setTitleColor(ColourConstants.hex191919, for: .normal)
        trackButton.backgroundColor = ColourConstants.hex191919
        trackButton.setTitleColor(.white, for: .normal)
        totalTitleLabel.text = "TOTAL"
        paymentStatusLabel.text = "PAYMENT STATUS"
        shippingAddressTitleLabel.text = "SHIPPING ADDRESS"
        updateTitleLabel.text = "UPDATE SENT TO"
        for dot in progressDots {
            dot.layer.cornerRadius = 5.5
            dot.backgroundColor = ColourConstants.hexCECECE
        }
        for line in progressLines {
            line.backgroundColor = ColourConstants.hexCECECE
        }
        addressContainerView.layer.borderWidth = 1
        addressContainerView.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
        updateContainerView.backgroundColor = ColourConstants.hexF8F8F8
        returnItemButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        returnItemButton.applyButtonLetterSpacing(spacing: 2.52)
        returnItemButton.backgroundColor = ColourConstants.hex191919
        returnDisclaimerView.backgroundColor = ColourConstants.hexFFEBD6
        returnDisclaimerLabel.textColor = ColourConstants.hex3C3C3C
        trackButton.applyButtonLetterSpacing(spacing: 2.52)
        downloadSummeryButton.backgroundColor = ColourConstants.hex191919
        downloadSummeryButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        downloadSummeryButton.applyButtonLetterSpacing(spacing: 2.52)
        refundSummeryView.layer.borderWidth = 1
        refundSummeryView.layer.borderColor = ColourConstants.hexEAEAEA.cgColor
        cancelledAmountCreditLabel.textColor = ColourConstants.hexEF5353
        updateContainerView.addBorders(edges: [.bottom, .left, .right],
                                       color: ColourConstants.hexCECECE, inset: 0, thickness: 1.0)
    }
    func applyTextColour() {
        nameLabel.textColor = ColourConstants.hex191919
        addressLabel.textColor = ColourConstants.hex191919
        phoneNumberLabel.textColor = ColourConstants.hex191919
        updateTitleLabel.textColor = ColourConstants.hex191919
        emailTitleLabel.textColor = ColourConstants.hex191919
        emailLabel.textColor = ColourConstants.hex191919
        mobileLabel.textColor = ColourConstants.hex191919
        mobileTitleLabel.textColor = ColourConstants.hex191919
        shippingAddressTitleLabel.textColor = ColourConstants.hex191919
        orderPlacedTitleLabel.textColor = ColourConstants.hex191919
        orderPreparedTitleLabel.textColor = ColourConstants.hex191919
        orderCollectedTitleLabel.textColor = ColourConstants.hex191919
        orderDeliveredTitleLabel.textColor = ColourConstants.hex191919
        orderPlacedDateLabel.textColor = ColourConstants.hex3C3C3C
        orderPreparedDateLabel.textColor = ColourConstants.hex3C3C3C
        orderCollectedDateLabel.textColor = ColourConstants.hex3C3C3C
        orderDeliveredDateLabel.textColor = ColourConstants.hex3C3C3C
        orderSummeryTitleLabel.textColor = ColourConstants.hex191919
        returnedItemLabel.textColor = ColourConstants.hexDE650D
        reasonForReturnLabel.textColor = ColourConstants.hex191919
        resonForReturnValueLabel.textColor = ColourConstants.hex1A1A1A
        refundSummeryLabel.textColor = ColourConstants.hex191919
        refundAmountLabel.textColor = ColourConstants.hex191919
        refundStatusLabel.textColor = ColourConstants.hex191919
        amountCreditedDisclaimerLabel.textColor = ColourConstants.hexDE650D
        refundAmountValueLabel.textColor = ColourConstants.hex191919
        refundTypeLabel.textColor = ColourConstants.hex191919
    }
    func applyOnGoingOrderUI() {
        let deliverydateFrom = Utilities.sharedInstance.convertServerDate(
            value: presenter.orderDetails.deliveryDateFrom, delivered: false)
        let deliverydateTo = Utilities.sharedInstance.convertServerDate(
            value: presenter.orderDetails.deliveryDateTo, delivered: false)
        statusLabel.text = "DELIVERY STATUS"
        dateLabel.text = "\(deliverydateFrom) - \(deliverydateTo)"
        statusLabel.textColor = ColourConstants.hex1B8CD3
        statusContainer.backgroundColor = ColourConstants.hexEBF7FD
        totalItemLabel.isHidden = true
        returnItemContainerView.isHidden = true
        downloadSummeryContainer.isHidden = true
        returnedContainerView.isHidden = true
        cancelledAmountCreditLabel.isHidden = true
        cancelLabelHeightConstraint.constant = 0
        if presenter.orderDetails.isCancellable {
            cancelDisclaimerLabel.textColor = ColourConstants.hex1B8CD3
            cancelDisclaimerLabel.text = "Cancel available till 12 hours after order placed."
            exclamationImageView.setImageColour(color: ColourConstants.hex1B8CD3)
        } else {
            cancelDisclaimerLabel.textColor = ColourConstants.hex717171
            cancelDisclaimerLabel.text = "Item cancellation window has been closed"
            exclamationImageView.setImageColour(color: ColourConstants.hex717171)
            cancelButton.isHidden = true
        }
    }
    func applyDeliveredOrderUI() {
        let deliveredDate = Utilities.sharedInstance.convertServerDate(
            value: presenter.orderDetails.deliveredAt, delivered: true)
        statusLabel.text = "DELIVERED"
        statusContainer.backgroundColor = ColourConstants.hexECFCF2
        statusLabel.textColor = ColourConstants.hex399861
        dateLabel.text = "On \(deliveredDate)"
        totalItemLabel.text = "Total Items: \(presenter.orderDetails.lineItems.count)"
        disclaimerView.isHidden = true
        orderProgressView.isHidden = true
        actionButtonsView.isHidden = true
        cancelledAmountCreditLabel.isHidden = true
        cancelLabelHeightConstraint.constant = 0
        var returnedCount: Int = 0
        for item in presenter.orderDetails.lineItems where item.status == "RETURNED" {
            returnedCount += 1
        }
        if presenter.orderDetails.lineItems.count == returnedCount {
            statusLabel.text = "RETURNED"
            statusContainer.backgroundColor = ColourConstants.hexFFEBD6
            statusLabel.textColor = ColourConstants.hexDE650D
        }
        if returnedCount > 0 {
            returnedContainerView.isHidden = false
        } else {
            returnedContainerView.isHidden = true
        }
        if presenter.orderDetails.isReturnable {
            let returnTillDate = Utilities.sharedInstance.convertServerDate(
                value: presenter.orderDetails.orderReturnTime, delivered: true)
            returnDisclaimerIcon.setImageColour(color: ColourConstants.hex3C3C3C)
            returnDisclaimerLabel.text = "Return available till \(returnTillDate)"
            if presenter.orderDetails.lineItems.count == returnedCount {
                returnItemContainerView.isHidden = true
            } else {
                returnItemContainerView.isHidden = false
                resonForReturnValueLabel.text = presenter.orderDetails.returnReason
                refundAmountValueLabel.text = "₹ \(presenter.orderDetails.refundAmmount)"
                refundTypeLabel.text = "XXXX XX56"
            }
        } else {
            returnItemContainerView.isHidden = true
        }
        returnedItemsTableView.reloadData()
    }
    func applyCancelledOrderUI() {
        let cancelledDate = Utilities.sharedInstance.convertServerDate(
            value: presenter.orderDetails.cancelledAt, delivered: true)
        statusLabel.text = "CANCELLED"
        statusContainer.backgroundColor = ColourConstants.hexFFF2F2
        statusLabel.textColor = ColourConstants.hexEF5353
        dateLabel.text = "On \(cancelledDate)"
        totalItemLabel.text = "Total Items: \(presenter.orderDetails.lineItems.count)"
        orderSummeryTitleLabel.text = "REFUND SUMMARY"
        orderProgressView.isHidden = true
        actionButtonsView.isHidden = true
        disclaimerView.isHidden = true
        returnItemContainerView.isHidden = true
        downloadSummeryContainer.isHidden = true
        returnedContainerView.isHidden = true
        cancelledAmountCreditLabel.isHidden = false
        if presenter.orderDetails.isCompleted {
            cancelledAmountCreditLabel.text = "Amount credited to source account"
        } else {
            cancelledAmountCreditLabel.text = "Refund has been initiated."
        }
    }
    func applyReturnedOrderUI() {
        let deliveredDate = Utilities.sharedInstance.convertServerDate(
            value: presenter.orderDetails.deliveredAt, delivered: true)
        statusLabel.text = "RETURNED"
        statusContainer.backgroundColor = ColourConstants.hexFFEBD6
        statusLabel.textColor = ColourConstants.hexDE650D
        dateLabel.text = "On \(deliveredDate)"
        totalItemLabel.text = "Total Items: \(presenter.orderDetails.lineItems.count)"
        disclaimerView.isHidden = true
        orderProgressView.isHidden = true
        actionButtonsView.isHidden = true
        cancelledAmountCreditLabel.isHidden = true
        cancelLabelHeightConstraint.constant = 0
        let returnTillDate = Utilities.sharedInstance.convertServerDate(
            value: presenter.orderDetails.orderReturnTime, delivered: true)
        returnDisclaimerIcon.setImageColour(color: ColourConstants.hex3C3C3C)
        returnDisclaimerLabel.text = "Return available till \(returnTillDate)"
        resonForReturnValueLabel.text = presenter.orderDetails.returnReason
        refundAmountValueLabel.text = "₹ \(presenter.orderDetails.refundAmmount)"
        refundTypeLabel.text = "XXXX XX56"
        returnItemContainerView.isHidden = true
        returnedItemsTableView.reloadData()
    }
}
