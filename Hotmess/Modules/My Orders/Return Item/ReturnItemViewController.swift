//
//  ReturnItemViewController.swift
//  Hotmess
//
//  Created by Cedan Misquith on 25/08/21.
//

import UIKit

class ReturnItemViewController: UIViewController {
    @IBOutlet weak var upiTransferCheckImageView: UIImageView!
    @IBOutlet weak var bankTransferCheckImageView: UIImageView!
    @IBOutlet weak var upiTransferBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var upiDetailTextFieldView: UIView!
    @IBOutlet weak var bankdetailTextFieldView: UIView!
    @IBOutlet weak var bankTransferBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var upiTextField: UITextField!
    @IBOutlet weak var upiTransferLabel: UILabel!
    @IBOutlet weak var upitransferView: UIView!
    @IBOutlet weak var ifscTextField: UITextField!
    @IBOutlet weak var accountNumberTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var bankTransferLabel: UILabel!
    @IBOutlet weak var bankTransferView: UIView!
    @IBOutlet weak var refundDiscliamerLabel: UILabel!
    @IBOutlet weak var waringImageView: UIImageView!
    @IBOutlet weak var refundDetailTitleLabel: UILabel!
    @IBOutlet weak var refundView: UIView!
    @IBOutlet weak var returnDisclaimerLabel: UILabel!
    @IBOutlet weak var pickUpAddressTitleLabel: UILabel!
    @IBOutlet weak var chooseItemTitleLabel: UILabel!
    @IBOutlet weak var productTableView: UITableView!
    @IBOutlet weak var addressContainerView: UIView!
    @IBOutlet weak var productsTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var reasonForCancelTitleLabel: UILabel!
    @IBOutlet weak var returnReasonsTableView: CustomIntrinsicTableView!
    @IBOutlet weak var returnReasonsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var exclamationImageView: UIImageView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mobileNoLabel: UILabel!
    @IBOutlet weak var viewPolicyButton: UIButton!
    var presenter = ReturnItemPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        applyStyling()
        applyFont()
        bankdetailTextFieldView.isHidden = true
        upiDetailTextFieldView.isHidden = true
        applyAttributedText()
        setUpTableView()
        self.view.showLoader()
        presenter.requestAddressList()
        applyData()
    }
    override func viewWillLayoutSubviews() {
        self.returnReasonsHeightConstraint.constant = self.returnReasonsTableView.intrinsicContentSize.height
        self.productsTableViewHeightConstraint.constant = self.productTableView.intrinsicContentSize.height
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func refundOptionSelectionButtonActon(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            bankTransferCheckImageView.image = UIImage(named: "accountCreatedCheck")
            upiTransferCheckImageView.image = UIImage(named: "orderUncheck")
            bankdetailTextFieldView.isHidden = false
            presenter.selectedRefundOption = 0
            bankTransferBottomConstraint.constant = 20
            upiTransferBottomConstraint.constant = 0
            upiDetailTextFieldView.isHidden = true
            upiTextField.text = ""
        case 1:
            upiTransferCheckImageView.image = UIImage(named: "accountCreatedCheck")
            bankTransferCheckImageView.image = UIImage(named: "orderUncheck")
            presenter.selectedRefundOption = 1
            upiDetailTextFieldView.isHidden = false
            bankdetailTextFieldView.isHidden = true
            bankTransferBottomConstraint.constant = 0
            upiTransferBottomConstraint.constant = 20
            nameTextField.text = ""
            accountNumberTextField.text = ""
            ifscTextField.text = ""
        default:
            print("None of the option selected")
        }
    }
    @IBAction func checkBoxButtonAction(_ sender: UIButton) {
        if presenter.isTermsAgreed {
            presenter.isTermsAgreed = false
            checkBoxButton.setImage(UIImage(named: "uncheckBoxIcon"), for: .normal)
        } else {
            presenter.isTermsAgreed = true
            checkBoxButton.setImage(UIImage(named: "checkBoxIcon"), for: .normal)
        }
    }
    @IBAction func viewPolicyButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "WebView", bundle: nil)
        guard let webViewVC = storyBoard.instantiateViewController(
                withIdentifier: "WebViewController") as? WebViewController else {
            return
        }
        webViewVC.state = .RETURNPOLICY
        self.navigationController?.pushViewController(webViewVC, animated: true)
    }
    @objc func confirmReturnItemButtonAction(sender: UIButton) {
        self.view.showLoader()
        presenter.requestForReturnItem(upiID: upiTextField.text ?? "",
                                       accountHolderName: nameTextField.text ?? "",
                                       accountNumber: accountNumberTextField.text ?? "",
                                       ifscCode: ifscTextField.text ?? "",
                                       comment: commentTextView.text ?? "")
    }
    @IBAction func continueButtonAction(_ sender: UIButton) {
        if presenter.orderDetails.paymentType == "COD" {
            presenter.validateForCODReturn(upiID: upiTextField.text ?? "",
                                           accountHolderName: nameTextField.text ?? "",
                                           accountNumber: accountNumberTextField.text ?? "",
                                           ifscCode: ifscTextField.text ?? "")
        } else {
            presenter.validationForOnlineReturn()
        }
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ReturnItemViewController {
    // MARK: - TextField Styling
    func applyTextFieldStyling(textFields: [UITextField]) {
        for textField in textFields {
            textField.font = UIFont(name: FontConstants.regular, size: 13)
            textField.backgroundColor = ColourConstants.hexFFFFFF
            textField.layer.borderWidth = 1
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
            textField.textColor = ColourConstants.hex191919
            textField.setLeftPaddingPoints(10)
        }
    }
    // MARK: - Table View Set Up Method
    func setUpTableView() {
        productTableView.register(UINib(nibName: "ReturnItemCustomCell", bundle: nil),
                                  forCellReuseIdentifier: "ReturnItemCustomCell")
    }
    func applyStyling() {
        exclamationImageView.setImageColour(color: ColourConstants.hex3C3C3C)
        returnDisclaimerLabel.textColor = ColourConstants.hex3C3C3C
        commentTextView.backgroundColor = ColourConstants.hexF8F8F8
        commentTextView.placeholder = "If any additional comments"
        chooseItemTitleLabel.textColor = ColourConstants.hex191919
        reasonForCancelTitleLabel.textColor = ColourConstants.hex191919
        changeButton.setTitleColor(ColourConstants.hex191919, for: .normal)
        pickUpAddressTitleLabel.textColor = ColourConstants.hex191919
        addressContainerView.layer.borderWidth = 0.5
        addressContainerView.layer.borderColor = ColourConstants.hexCECECE.cgColor
        nameLabel.textColor = ColourConstants.hex191919
        addressLabel.textColor = ColourConstants.hex191919
        mobileNoLabel.textColor = ColourConstants.hex191919
        termsLabel.textColor = ColourConstants.hex717171
        continueButton.setTitleColor(.white, for: .normal)
        continueButton.backgroundColor = ColourConstants.hex191919
        refundDiscliamerLabel.textColor = ColourConstants.hex717171
        refundDetailTitleLabel.textColor = ColourConstants.hex191919
        bankTransferLabel.textColor = ColourConstants.hex191919
        upiTransferLabel.textColor = ColourConstants.hex191919
        waringImageView.setImageColour(color: ColourConstants.hex717171)
        continueButton.applyButtonLetterSpacing(spacing: 2.52)
        applyTextFieldStyling(textFields: [
            nameTextField,
            accountNumberTextField,
            ifscTextField,
            upiTextField
        ])
        bankTransferView.layer.borderWidth = 1
        bankTransferView.layer.borderColor = ColourConstants.hexEAEAEA.cgColor
        upitransferView.layer.borderWidth = 1
        upitransferView.layer.borderColor = ColourConstants.hexEAEAEA.cgColor
    }
    func applyFont() {
        returnDisclaimerLabel.font = UIFont(name: FontConstants.regular, size: 12)
        chooseItemTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        reasonForCancelTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        changeButton.titleLabel?.font = UIFont(name: FontConstants.medium, size: 14)
        pickUpAddressTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        nameLabel.font = UIFont(name: FontConstants.medium, size: 18)
        addressLabel.font = UIFont(name: FontConstants.light, size: 16)
        mobileNoLabel.font = UIFont(name: FontConstants.regular, size: 16)
        termsLabel.font = UIFont(name: FontConstants.regular, size: 11)
        continueButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        refundDiscliamerLabel.font = UIFont(name: FontConstants.regular, size: 13)
        refundDetailTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        bankTransferLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        upiTransferLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
    }
    func applyAttributedText() {
        let viewPolicyAttributeText = presenter.getUnderlineAttribute(
            font: UIFont(name: FontConstants.medium, size: 13) ?? UIFont.systemFont(ofSize: 14),
            color: ColourConstants.hex191919,
            text: "View Policy")
        viewPolicyButton.setAttributedTitle(viewPolicyAttributeText, for: .normal)
        termsLabel.attributedText = presenter.createTermsAttributedText(
            text: "Please accept our Terms and Conditions & Return Policy")
        let termsLabepTap = UITapGestureRecognizer(target: self, action: #selector(didTapTermsLabel(_:)))
        termsLabel.addGestureRecognizer(termsLabepTap)
    }
    func applyData() {
        let returnTillDate = Utilities.sharedInstance.convertServerDate(
            value: presenter.orderDetails.orderReturnTime, delivered: true)
        returnDisclaimerLabel.text = "Return available till \(returnTillDate)"
        if presenter.orderDetails.paymentType == "COD" {
            refundView.isHidden = false
        } else {
            refundView.isHidden = true
        }
    }
    func updateAddress(isDefault: Bool) {
        let addressToFill = isDefault ? presenter.getDefaultAddress() : presenter.getSelectedAddress()
        let displayAddress = "\(addressToFill.addressLine1),\n\(addressToFill.addressLine2)" +
            "\n\(addressToFill.city), " +
            "\(addressToFill.state) - \n\(addressToFill.areaPinCode)" +
            "\n\(addressToFill.country)" +
            "\nLandmark : \(addressToFill.landMark)"
        let displayMobileNumber = " \(addressToFill.mobileNumber)"
        let displayFullName = "\(addressToFill.firstName)" + " \(addressToFill.lastName)"
        nameLabel.text = displayFullName
        addressLabel.text = displayAddress
        mobileNoLabel.text = "Mobile No: +91 \(displayMobileNumber)"
    }
    @objc func didTapTermsLabel(_ sender: UITapGestureRecognizer) {
        let text = termsLabel.text ?? "Please accept our Terms and Conditions & Return Policy"
        let termsRange = (text as NSString).range(of: "Terms and Conditions")
        let privacyRange = (text as NSString).range(of: "Return Policy")
        let storyBoard = UIStoryboard(name: "WebView", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "WebViewController") as? WebViewController else {
            return
        }
        if sender.didTapAttributedTextInLabel(label: termsLabel, inRange: termsRange) {
            nextvc.state = .TERMS
        } else if sender.didTapAttributedTextInLabel(label: termsLabel,
                                                     inRange: privacyRange) {
            nextvc.state = .RETURNPOLICY
        }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    @IBAction func changeAddressButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "ShippingAddress", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "ShippingAddressViewController")
                as? ShippingAddressViewController else { return }
        nextvc.state = .FROMRETURNORDER
        nextvc.changeAddressDelegate = self
        nextvc.presenter.userAddressList = presenter.userAddressList
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
