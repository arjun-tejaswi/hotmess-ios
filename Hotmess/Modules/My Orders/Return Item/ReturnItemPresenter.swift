//
//  ReturnItemPresenter.swift
//  Hotmess
//
//  Created by Cedan Misquith on 08/09/21.
//

import UIKit
import SwiftyJSON

protocol ReturnItemPresenterProtocol: AnyObject {
    func listAddressSuccess(status: Bool, title: String, message: String)
    func validationState(status: Bool, title: String, message: String)
    func returnItemSuccess(status: Bool, title: String, message: String)
}

class ReturnItemPresenter: NSObject {
    weak var delegate: ReturnItemPresenterProtocol?
    var userAddressList = [ShippingAddressRealm]()
    var selectedAddress = ShippingAddressRealm()
    var selectedRefundOption: Int = -1
    var isTermsAgreed: Bool = false
    var selectedReason: Int = -1
    var returnReasons = [
        "Too big",
        "Too small",
        "Product quality",
        "Different from photograph",
        "Wrong item",
        "Product didn’t meet my expectations"
    ]
    var orderDetails: OrderRealm!
    func requestAddressList() {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.listAddress,
            prametres: [:],
            type: .withAuth,
            method: .POST,
            completion: self.handleAddressListResponse)
    }
    func validationForOnlineReturn() {
        var returnedCount: Int = 0
        if selectedReason > -1 {
            if isTermsAgreed {
                for item in orderDetails.lineItems where item.isSelected {
                    returnedCount += 1
                }
                if returnedCount != 0 {
                    delegate?.validationState(status: true, title: "", message: "")
                } else {
                    delegate?.validationState(status: false,
                                              title: "Select Item", message: "Please select the item for return.")
                }
            } else {
                delegate?.validationState(status: false,
                                          title: "Accept Terms", message: "Please select terms and condition.")
            }
        } else {
            delegate?.validationState(status: false,
                                      title: "Reason ?", message: "Please select a reason for your returned item.")
        }
    }
    func validateForCODReturn(upiID: String, accountHolderName: String, accountNumber: String, ifscCode: String) {
        var returnedCount: Int = 0
        for item in orderDetails.lineItems where item.isSelected {
            returnedCount += 1
        }
        if selectedReason > -1 {
            if isTermsAgreed {
                if selectedRefundOption == 0 {
                    if accountHolderName != "" && accountNumber != "" && ifscCode != ""{
                        if returnedCount != 0 {
                            delegate?.validationState(status: true, title: "", message: "")
                        } else {
                            delegate?.validationState(status: false,
                                                      title: "Select Item",
                                                      message: "Please select the item for return.")
                        }
                    } else {
                        delegate?.validationState(status: false,
                                                  title: "Bank Details", message: "Please enter Bank deatils.")
                    }
                } else if selectedRefundOption == 1 {
                    if upiID != "" {
                        if returnedCount != 0 {
                            delegate?.validationState(status: true, title: "", message: "")
                        } else {
                            delegate?.validationState(status: false,
                                                      title: "Select Item", message:
                                                        "Please select the item for return.")
                        }
                    } else {
                        delegate?.validationState(status: false,
                                                  title: "UPI Details", message: "Please enter UPI ID.")
                    }
                } else {
                    delegate?.validationState(status: false,
                                              title: "Refund Option", message: "Please select Refund option.")
                }
            } else {
                delegate?.validationState(status: false,
                                          title: "Accept Terms", message: "Please select terms and condition.")
            }
        } else {
            delegate?.validationState(status: false,
                                      title: "Reason ?", message: "Please select a reason for your returned item.")
        }
    }
    func requestForReturnItem(upiID: String, accountHolderName: String,
                              accountNumber: String, ifscCode: String, comment: String) {
        var lineItems = [[String: Any]]()
        for item in orderDetails.lineItems where item.isSelected {
            lineItems.append([
                "quantity": item.quantity,
                "product_id": item.productId,
                "sku_id": item.skuId
            ])
        }
        selectedAddress = getSelectedAddress()
        let parameters: [String: Any] = [
            "payment_type": orderDetails.paymentType,
            "state": selectedAddress.state,
            "city": selectedAddress.city,
            "country": selectedAddress.country,
            "address": selectedAddress.addressLine1,
            "locality": selectedAddress.addressLine2,
            "pincode": selectedAddress.areaPinCode,
            "status": orderDetails.status,
            "order_id": orderDetails.orderId,
            "upi_id": upiID,
            "comment": comment,
            "return_reason": returnReasons[selectedReason],
            "line_items": lineItems,
            "bank_information": [
                "account_holder_name": accountHolderName,
                "account_number": accountNumber,
                "ifsc_code": ifscCode
            ]
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.returnOrder,
                                                         prametres: parameters, type: .withAuth,
                                                         method: .POST) { response in
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.requestForReturnItem(upiID: upiID, accountHolderName: accountHolderName,
                                                          accountNumber: accountNumber,
                                                          ifscCode: ifscCode, comment: comment)
                            }
                        } else {
                            self.delegate?.returnItemSuccess(status: false, title: "Error", message: message)
                        }
                    }
                } else {
                    self.delegate?.returnItemSuccess(status: true, title: "Return Success", message: "")
                }
            }
        }
    }
    func getSelectedItemForReturn() -> [LineItemsRealm] {
        var returnItems = [LineItemsRealm]()
        for item in orderDetails.lineItems where item.isSelected {
            returnItems.append(item)
        }
        return returnItems
    }
    func handleAddressListResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.requestAddressList()
                        }
                    } else {
                        delegate?.listAddressSuccess(status: false, title: "Error", message: message)
                    }
                }
            } else {
                if let data = response["data"].array {
                    userAddressList.removeAll()
                    for aAddress in data {
                        if let aAddressDictionary = aAddress.dictionary {
                            let allAddresses = AddressParser.sharedInstance.parseAddressData(data: aAddressDictionary)
                            userAddressList.append(allAddresses)
                        }
                    }
                }
                delegate?.listAddressSuccess(status: true, title: "", message: "")
            }
        }
    }
    func getDefaultAddress() -> ShippingAddressRealm {
        var defaultAddress: ShippingAddressRealm!
        for address in userAddressList where address.isDefaultAddress {
            defaultAddress = address
        }
        return defaultAddress
    }
    func getSelectedAddress() -> ShippingAddressRealm {
        var selectedAddress: ShippingAddressRealm!
        for address in userAddressList where address.isSelected {
            selectedAddress = address
        }
        if selectedAddress == nil {
            selectedAddress = getDefaultAddress()
        }
        return selectedAddress
    }
    func getUnderlineAttribute(font: UIFont, color: UIColor, text: String) -> NSAttributedString {
        let policyButtonAttributes: [NSAttributedString.Key: Any] = [
            .font: font,
            .foregroundColor: color,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        let attributedString = NSMutableAttributedString(
            string: text,
            attributes: policyButtonAttributes
        )
        return attributedString
    }
    func createTermsAttributedText(text: String) -> NSMutableAttributedString {
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms and Conditions")
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle,
                                          value: NSUnderlineStyle.single.rawValue,
                                          range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor,
                                          value: ColourConstants.hex717171,
                                          range: range1)
        underlineAttriString.addAttribute(NSAttributedString.Key.font,
                                          value: UIFont(
                                            name: FontConstants.regular, size: 11) ?? UIFont.systemFont(ofSize: 11),
                                          range: range1)
        let range2 = (text as NSString).range(of: "Return Policy")
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle,
                                          value: NSUnderlineStyle.single.rawValue,
                                          range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.foregroundColor,
                                          value: ColourConstants.hex717171,
                                          range: range2)
        underlineAttriString.addAttribute(NSAttributedString.Key.font,
                                          value: UIFont(
                                            name: FontConstants.regular, size: 11) ?? UIFont.systemFont(ofSize: 11),
                                          range: range2)
        return underlineAttriString
    }
}
