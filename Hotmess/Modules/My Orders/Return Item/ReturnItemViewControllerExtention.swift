//
//  ReturnItemViewControllerExtention.swift
//  Hotmess
//
//  Created by Flaxon on 27/09/21.
//

import Foundation
import UIKit

extension ReturnItemViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == returnReasonsTableView {
            return presenter.returnReasons.count
        } else {
            return presenter.orderDetails.lineItems.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == returnReasonsTableView {
            guard let cell = tableView.dequeueReusableCell(
                    withIdentifier: "CancelOptionsCustomCell") as? CancelOptionsCustomCell else {
                return UITableViewCell()
            }
            if indexPath.row == presenter.selectedReason {
                cell.check()
            } else {
                cell.uncheck()
            }
            cell.titleLabel.text = presenter.returnReasons[indexPath.row]
            cell.selectionStyle = .none
            return cell
        } else {
            return getReturnItemCell(indexPath: indexPath)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == returnReasonsTableView {
            presenter.selectedReason = indexPath.row
            print(presenter.selectedReason)
            returnReasonsTableView.reloadData()
        } else {
            presenter.orderDetails.lineItems[indexPath.row].isSelected =
                !presenter.orderDetails.lineItems[indexPath.row].isSelected
            productTableView.reloadData()
        }
    }
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        if tableView == productTableView {
//            guard let cell = tableView.cellForRow(at: indexPath) as? ReturnItemCustomCell else {
//                return
//            }
//            cell.uncheck()
//        }
//    }
    func getReturnItemCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = productTableView.dequeueReusableCell(withIdentifier: "ReturnItemCustomCell",
                                                              for: indexPath) as? ReturnItemCustomCell
        else { return UITableViewCell() }
        let item = presenter.orderDetails.lineItems
        if item[indexPath.row].isSelected { cell.check() } else { cell.uncheck() }
        let url = item[indexPath.row].featuredImageURL
        NukeManager.sharedInstance.setImage(url: url, imageView: cell.productImageView, withPlaceholder: true)
        cell.productDesignerNameLabel.text = item[indexPath.row].designerName
        cell.productDescriptionLabel.text = item[indexPath.row].itemDescription
        cell.productPriceLabel.text = "\(item[indexPath.row].costPrice)"
        let attribute1: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font:
                                                            UIFont(name: FontConstants.regular, size: 14) ??
                                                            UIFont.systemFont(ofSize: 14),
                                                         NSAttributedString.Key.foregroundColor:
                                                            ColourConstants.hex3C3C3C]
        let attribute2: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font:
                                                            UIFont(name: FontConstants.medium, size: 14) ??
                                                            UIFont.boldSystemFont(ofSize: 14),
                                                         NSAttributedString.Key.foregroundColor:
                                                            ColourConstants.hex3C3C3C]
        let size = NSMutableAttributedString(string: "Size\t\t\t", attributes: attribute1)
        let sizeValue = NSMutableAttributedString(string: "\(item[indexPath.row].productSize)",
                                                  attributes: attribute2)
        size.append(sizeValue)
        let quantity = NSMutableAttributedString(string: "Qunatity\t", attributes: attribute1)
        let quantityValue = NSMutableAttributedString(string: "\(item[indexPath.row].quantity)",
                                                      attributes: attribute2)
        quantity.append(quantityValue)
        cell.productQuantityLabel.attributedText = quantity
        cell.productSizeLabel.attributedText = size
        if item[indexPath.row].status == "RETURNED" {
            cell.checkImageView.isHidden = true
        } else {
            cell.checkImageView.isHidden = false
        }
        cell.selectionStyle = .none
        if indexPath.row == item.count - 1 {
            cell.separaterView.isHidden = true
        } else {
            cell.separaterView.isHidden = false
        }
        return cell
    }
}
extension ReturnItemViewController: ChangeAddressProtocol {
    func changedAddress(address: [ShippingAddressRealm]) {
        presenter.userAddressList = address
        updateAddress(isDefault: false)
    }
    func clearAddresses() {
        presenter.userAddressList.removeAll()
    }
}
extension ReturnItemViewController: ReturnItemPresenterProtocol {
    func returnItemSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            let storyBoard = UIStoryboard(name: "MyOrders", bundle: nil)
            guard let nextvc = storyBoard.instantiateViewController(
                    withIdentifier: "OrderCancelledReturnedViewController") as?
                    OrderCancelledReturnedViewController else { return }
            nextvc.isCancel = false
            self.navigationController?.pushViewController(nextvc, animated: true)
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)        }
    }
    func validationState(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            let popUp = Utilities.sharedInstance.showReturnItemConfirmationPopUp(bounds: self.view.bounds)
            popUp.returnOrderInfo = presenter.getSelectedItemForReturn()
            print(popUp.returnOrderInfo)
            popUp.confirmButton.addTarget(self,
                                          action: #selector(confirmReturnItemButtonAction),
                                          for: .touchUpInside)
            self.view.addSubview(popUp)
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
    func listAddressSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        updateAddress(isDefault: true)
    }
}
