//
//  UpdatePasswordViewController.swift
//  Hotmess
//
//  Created by Akshatha on 10/08/21.
//

import UIKit

class UpdatePasswordViewController: UIViewController {
    @IBOutlet weak var changePasswordLabel: UILabel!
    @IBOutlet weak var oldPasswordTextfield: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextfield: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var oldPasswordTopLabel: UILabel!
    @IBOutlet weak var newPasswordTopLabel: UILabel!
    @IBOutlet weak var confirmPasswordTopLabel: UILabel!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var oldPasswordWarningImageView: UIImageView!
    @IBOutlet weak var newPasswordWarningImageView: UIImageView!
    @IBOutlet weak var confirmPasswordWarningImageView: UIImageView!
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var oldPasswordButton: UIButton!
    @IBOutlet weak var newPasswordButton: UIButton!
    @IBOutlet weak var confirmPasswordButton: UIButton!
    var presenter = UpdatePasswordPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        applyFont()
        applyStyling()
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - Font Styling Method
    func applyFont() {
        errorMessageLabel.font = UIFont(name: FontConstants.regular, size: 8)
        changePasswordLabel.font = UIFont(name: FontConstants.regular, size: 14)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        changePasswordLabel.applyLetterSpacing(spacing: 0.7)
        saveButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        saveButton.applyButtonLetterSpacing(spacing: 2.16)
        errorMessageLabel.textColor = UIColor.red
        applyTextFieldStyling(textFields: [
            oldPasswordTextfield,
            newPasswordTextField,
            confirmPasswordTextfield
        ])
        applyTopLabelStyling(labels: [
            oldPasswordTopLabel, newPasswordTopLabel, confirmPasswordTopLabel
        ])
        oldPasswordWarningImageView.isHidden = true
        newPasswordWarningImageView.isHidden = true
        confirmPasswordWarningImageView.isHidden = true
        oldPasswordTextfield.delegate = self
        newPasswordTextField.delegate = self
        confirmPasswordTextfield.delegate = self
    }
    func applyTextFieldStyling(textFields: [UITextField]) {
        for textField in textFields {
            textField.layer.borderWidth = 1
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
            textField.font = UIFont(name: FontConstants.regular, size: 13)
            textField.setLeftPaddingPoints(10)
            textField.setRightPaddingPoints(10)
        }
    }
    func applyTopLabelStyling(labels: [UILabel]) {
        for label in labels {
            label.font = UIFont(name: FontConstants.regular, size: 8)
            label.textColor = ColourConstants.hex9B9B9B
            errorMessageLabel.isHidden = true
            label.isHidden = true
        }
    }
    // MARK: - Displaying Warning Image Function
    func  showWarningImage() {
        if oldPasswordTextfield.text  == "" {
            oldPasswordButton.isHidden = true
            oldPasswordWarningImageView.isHidden = false
            oldPasswordTextfield.layer.borderColor = UIColor.red.cgColor
        }
        if newPasswordTextField.text == "" {
            newPasswordButton.isHidden = true
            newPasswordWarningImageView.isHidden = false
            newPasswordTextField.layer.borderColor = UIColor.red.cgColor
        }
        if confirmPasswordTextfield.text == "" {
            confirmPasswordButton.isHidden = true
            confirmPasswordWarningImageView.isHidden = false
            confirmPasswordTextfield.layer.borderColor = UIColor.red.cgColor
        }
    }
    // MARK: - Back Button Action
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Save Button Action
    @IBAction func saveButtonAction(_ sender: UIButton) {
        self.view.showLoader()
        presenter.requestUpdatePassword(oldPassword: oldPasswordTextfield.text ?? "",
                                 newPassword: newPasswordTextField.text ?? "",
                                 confirmPassword: confirmPasswordTextfield.text ?? "")
    }
    @IBAction func passwordButtonAction(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            if oldPasswordTextfield.isSecureTextEntry {
                oldPasswordButton.setImage(UIImage(named: "showPasswodIcon"), for: .normal)
                oldPasswordTextfield.isSecureTextEntry = false
            } else {
                oldPasswordButton.setImage(UIImage(named: "hidePasswordIcon"), for: .normal)
                oldPasswordTextfield.isSecureTextEntry = true
            }
        case 2:
            if newPasswordTextField.isSecureTextEntry {
                newPasswordButton.setImage(UIImage(named: "showPasswodIcon"), for: .normal)
                newPasswordTextField.isSecureTextEntry = false
            } else {
                newPasswordButton.setImage(UIImage(named: "hidePasswordIcon"), for: .normal)
                newPasswordTextField.isSecureTextEntry = true
            }
        case 3:
            if confirmPasswordTextfield.isSecureTextEntry {
                confirmPasswordButton.setImage(UIImage(named: "showPasswodIcon"), for: .normal)
                confirmPasswordTextfield.isSecureTextEntry = false
            } else {
                confirmPasswordButton.setImage(UIImage(named: "hidePasswordIcon"), for: .normal)
                confirmPasswordTextfield.isSecureTextEntry = true
            }
        default:
            print("Unknown option selected")
        }
    }
}
// MARK: - Protocol Methods
extension UpdatePasswordViewController: updatePasswordPresenterProtocol {
    func fillAllFields() {
        self.view.hideLoader()
        showWarningImage()
    }
    func updatePasswordSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if !status {
            oldPasswordButton.isHidden = true
            oldPasswordTextfield.layer.borderColor = UIColor.red.cgColor
            oldPasswordWarningImageView.isHidden = false
            errorMessageLabel.isHidden = false
        } else {
            Utilities.sharedInstance.logOutApp()
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                fatalError("Not of type 'AppDelegate'")
            }
            appDelegate.restartApp()
        }
        }
    func passwordMismatch() {
        self.view.hideLoader()
        newPasswordWarningImageView.isHidden = false
        newPasswordTextField.layer.borderColor = UIColor.red.cgColor
        confirmPasswordWarningImageView.isHidden = false
        confirmPasswordTextfield.layer.borderColor = UIColor.red.cgColor
    }
}
// MARK: - Text Delegate Methods
extension UpdatePasswordViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if oldPasswordTextfield.text  == "" {
            oldPasswordTopLabel.isHidden = true
        }
        if newPasswordTextField.text == "" {
            newPasswordTopLabel.isHidden = true
        }
        if confirmPasswordTextfield.text == "" {
            confirmPasswordTopLabel.isHidden = true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == oldPasswordTextfield {
            oldPasswordTopLabel.isHidden = false
            oldPasswordButton.isHidden = false
            oldPasswordWarningImageView.isHidden = true
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
        if textField == newPasswordTextField {
            newPasswordTopLabel.isHidden = false
            newPasswordWarningImageView.isHidden = true
            newPasswordButton.isHidden = false
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
        if textField == confirmPasswordTextfield {
            confirmPasswordTopLabel.isHidden = false
            confirmPasswordButton.isHidden = false
            confirmPasswordWarningImageView.isHidden = true
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
    }
}
