//
//  UpdatePasswordPresenter.swift
//  Hotmess
//
//  Created by Akshatha on 13/08/21.
//

import Foundation
import UIKit

protocol updatePasswordPresenterProtocol: AnyObject {
    func fillAllFields()
    func updatePasswordSuccess(status: Bool, title: String, message: String)
    func passwordMismatch()
}

class UpdatePasswordPresenter: NSObject {
    weak var delegate: updatePasswordPresenterProtocol?
    let userDetails = RealmDataManager.sharedInstance.getUserDetails()
    var updateParameters: [String: Any] = [
        "old_password": "",
        "new_password": "",
        "confirm_password": ""
    ]
    func requestUpdatePassword(oldPassword: String, newPassword: String, confirmPassword: String) {
        if oldPassword != "" &&
            newPassword != "" &&
            confirmPassword != "" {
            if newPassword == confirmPassword {
                updateParameters.updateValue(oldPassword, forKey: "old_password")
                updateParameters.updateValue(newPassword, forKey: "new_password")
                updateParameters.updateValue(confirmPassword, forKey: "confirm_password")
                updatePassword()
            } else {
                delegate?.passwordMismatch()
            }
        } else {
            delegate?.fillAllFields()
        }
    }
    func updatePassword() {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.passwordUpdate,
            prametres: updateParameters,
            type: .withAuth,
            method: .POST) { response in
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.updatePassword()
                            }
                        } else {
                            self.delegate?.updatePasswordSuccess(status: false, title: "Error", message: message)
                        }
                    }
                } else {
                    self.delegate?.updatePasswordSuccess(status: true, title: "", message: "")
                }
            }
        }
    }
}
