//
//  UpdateProfilePresenter.swift
//  Hotmess
//
//  Created by Akshatha on 12/08/21.
//

import Foundation
import UIKit

protocol updateProfilePresenterProtocol: AnyObject {
    func fillAllFields()
    func updateProfileSuccess(status: Bool, title: String, message: String)
}

class UpdateProfilePresenter: NSObject {
    weak var delegate: updateProfilePresenterProtocol?
    var userDetails: UserRealm!
    var imageurl: String = ""
    var updateParameters: [String: Any] = [
        "lastName": "",
        "firstName": "",
        "imageUrl": "",
        "phoneNumber": "",
        "countryCode": ""
    ]
    func requestUpdateProfile(firstName: String, lastName: String, phoneNumber: String) {
        if firstName == "" ||
            lastName == "" ||
            phoneNumber == "" {
            delegate?.fillAllFields()
        } else if phoneNumber.count < 10 || phoneNumber.count > 10 {
            delegate?.updateProfileSuccess(status: false,
                                           title: "MobileNumber Alert !",
                                           message: "Mobile Number must have 10 digits")
        } else {
            updateParameters.updateValue(lastName, forKey: "lastName")
            updateParameters.updateValue(firstName, forKey: "firstName")
            if imageurl == "" {
                updateParameters.updateValue(userDetails.userImageURL, forKey: "imageUrl")
            } else {
                updateParameters.updateValue(imageurl, forKey: "imageUrl")
            }
            updateParameters.updateValue(phoneNumber, forKey: "phoneNumber")
            updateParameters.updateValue("+91", forKey: "countryCode")
            updateProfile()
        }
    }
    func uploadImageToS3(image: UIImage) {
        let imageName = "\(userDetails.userId)/\(userDetails.userFirstName)"+".jpeg"
        AWSS3Manager.sharedInstance.uploadS3(
            image: image,
            name: imageName) { error in
            print("Error uploading: \(error)")
        } successCompletion: { url in
            print("Uploaded to URL: \(url)")
            self.imageurl = url
        }
    }
    func updateProfile() {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.updateProfile,
            prametres: updateParameters,
            type: .withAuth,
            method: .POST) { response in
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.updateProfile()
                            }
                        } else {
                            self.delegate?.updateProfileSuccess(status: false, title: "Error", message: message)
                        }
                    }
                } else {
                    if let data = response["data"].dictionary {
                        RealmDataManager.sharedInstance.updateUserProfileDetails(
                            firstName: data["firstName"]?.string ?? "",
                            lastName: data["lastName"]?.string ?? "",
                            imageUrl: data["imageUrl"]?.string ?? "",
                            mobileNumber: data["phoneNumber"]?.string ?? ""
                        )
                    }
                    self.delegate?.updateProfileSuccess(status: true, title: "", message: "")
                }
            }
        }
    }
}
