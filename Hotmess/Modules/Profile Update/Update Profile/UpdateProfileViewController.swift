//
//  UpdateProfileViewController.swift
//  Hotmess
//
//  Created by Akshatha on 10/08/21.
//

import UIKit

class UpdateProfileViewController: UIViewController {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var firstNameTopLabel: UILabel!
    @IBOutlet weak var lastNameTopLabel: UILabel!
    @IBOutlet weak var emailIdTopLabel: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailIDTextField: UITextField!
    @IBOutlet weak var myAccountLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var firstNameWarningImageView: UIImageView!
    @IBOutlet weak var lastNameWarningImageView: UIImageView!
    @IBOutlet weak var contactNumberTextField: UITextField!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var contactNumberTopLabel: UILabel!
    @IBOutlet weak var contactNumberWarningImageView: UIImageView!
    var presenter = UpdateProfilePresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        applyStyling()
        applyFont()
        applyData()
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - Assigning Data To TextFields
    func applyData() {
        firstNameTextField.text = presenter.userDetails.userFirstName
        lastNameTextField.text = presenter.userDetails.userLastName
        emailIDTextField.text = presenter.userDetails.userEmail
        contactNumberTextField.text = presenter.userDetails.userMobileNumber
        if presenter.userDetails.userImageURL == "" {
            profileImageView.image = Utilities.sharedInstance.imageFromInitials(
                initial01: presenter.userDetails.userFirstName,
                initial02: presenter.userDetails.userLastName,
                BGColor: ColourConstants.hexE0E0E0,
                fontName: FontConstants.semiBold,
                fontSize: 40,
                textColor: ColourConstants.hex1D1D1D)
        } else {
            NukeManager.sharedInstance.setImage(
                url: presenter.userDetails.userImageURL,
                imageView: profileImageView,
                withPlaceholder: true)
        }
    }
    // MARK: - Font Styling Method
    func applyFont() {
        saveButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
        myAccountLabel.font = UIFont(name: FontConstants.regular, size: 14)
        countryCodeLabel.font = UIFont(name: FontConstants.regular, size: 13)
        emailIDTextField.isUserInteractionEnabled = false
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        cameraButton.layer.cornerRadius = cameraButton.frame.size.height/2
        profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2
        myAccountLabel.applyLetterSpacing(spacing: 0.7)
        saveButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        saveButton.applyButtonLetterSpacing(spacing: 2.16)
        emailIDTextField.setLeftPaddingPoints(10)
        emailIDTextField.setRightPaddingPoints(10)
        emailIDTextField.layer.borderWidth = 0.5
        emailIDTextField.layer.borderColor = ColourConstants.hexCECECE.cgColor
        countryCodeLabel.font = UIFont(name: FontConstants.regular, size: 13)
        countryCodeLabel.textColor = ColourConstants.hex191919
        applyTextFieldStyling(textFields: [
            firstNameTextField,
            lastNameTextField, contactNumberTextField
        ])
        applyTopLabelStyling(labels: [
            firstNameTopLabel, lastNameTopLabel, emailIdTopLabel,
            contactNumberTopLabel
        ])
        contactNumberTextField.setLeftPaddingPoints(40)
        contactNumberTextField.setRightPaddingPoints(10)
        firstNameWarningImageView.isHidden = true
        lastNameWarningImageView.isHidden = true
        contactNumberWarningImageView.isHidden = true
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        contactNumberTextField.delegate = self
    }
    func applyTextFieldStyling(textFields: [UITextField]) {
        for textField in textFields {
            textField.layer.borderWidth = 0.5
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
            textField.font = UIFont(name: FontConstants.regular, size: 13)
            textField.setLeftPaddingPoints(16.29)
            textField.setRightPaddingPoints(10)
        }
    }
    func applyTopLabelStyling(labels: [UILabel]) {
        for label in labels {
            label.font = UIFont(name: FontConstants.regular, size: 8)
            label.textColor = ColourConstants.hex9B9B9B
            //            label.isHidden = true
        }
    }
    // MARK: - Back Button Action
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Camera Button Action
    @IBAction func photoSelectionButtonAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning",
                                           message: "You don't have permission to access gallery.",
                                           preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    // MARK: - Save Button Action
    @IBAction func saveButtonAction(_ sender: UIButton) {
        self.view.showLoader()
        firstNameWarningImageView.isHidden = true
        firstNameTextField.layer.borderColor = ColourConstants.hex717171.cgColor
        lastNameWarningImageView.isHidden = true
        lastNameTextField.layer.borderColor = ColourConstants.hex717171.cgColor
        contactNumberWarningImageView.isHidden = true
        contactNumberTextField.layer.borderColor = ColourConstants.hex717171.cgColor
        presenter.requestUpdateProfile(
            firstName: firstNameTextField.text ?? "",
            lastName: lastNameTextField.text ?? "",
            phoneNumber: contactNumberTextField.text ?? "")
    }
    // MARK: - Displaying Warning Image Function
    func showWarningImage() {
        if firstNameTextField.text  == "" {
            firstNameWarningImageView.isHidden = false
            firstNameTextField.layer.borderColor = UIColor.red.cgColor
        }
        if lastNameTextField.text == "" {
            lastNameWarningImageView.isHidden = false
            lastNameTextField.layer.borderColor = UIColor.red.cgColor
        }
        if contactNumberTextField.text == "" {
            contactNumberWarningImageView.isHidden = false
            contactNumberTextField.layer.borderColor = UIColor.red.cgColor
        }
    }
}
// MARK: - Image Picker Delegate Methods
extension UpdateProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        guard let image = info[.editedImage] as? UIImage else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                            bounds: self.view.bounds,
                            title: "Error",
                            subTitle: "Unable to find the Image",
                            buttonTitle: "OK") { return }
                        self.view.addSubview(popUp)
            return
        }
        profileImageView.image = image
        presenter.uploadImageToS3(image: image)
    }
}
// MARK: - Protocol Methods
extension UpdateProfileViewController: updateProfilePresenterProtocol {
    func fillAllFields() {
        self.view.hideLoader()
        showWarningImage()
    }
    func updateProfileSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if !status {
            contactNumberWarningImageView.isHidden = false
            contactNumberTextField.layer.borderColor = UIColor.red.cgColor
        } else {
        print(message)
        self.navigationController?.popViewController(animated: true)
        }
    }
}
// MARK: - TextField delegate Methods
extension UpdateProfileViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if firstNameTextField.text  == "" {
            firstNameTopLabel.isHidden = true
        }
        if lastNameTextField.text == "" {
            lastNameTopLabel.isHidden = true
        }
        if contactNumberTextField.text == "" {
            contactNumberTopLabel.isHidden = true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == firstNameTextField {
            firstNameTopLabel.isHidden = false
            firstNameWarningImageView.isHidden = true
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
        if textField == lastNameTextField {
            lastNameTopLabel.isHidden = false
            lastNameWarningImageView.isHidden = true
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
        if textField == contactNumberTextField {
            contactNumberTopLabel.isHidden = false
            contactNumberWarningImageView.isHidden = true
            contactNumberTextField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
    }
}
