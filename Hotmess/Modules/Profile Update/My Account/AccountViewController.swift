//
//  AccountViewController.swift
//  Hotmess
//
//  Created by Akshatha on 10/08/21.
//

import UIKit

class AccountViewController: UIViewController {
    @IBOutlet weak var firstNameTopLabel: UILabel!
    @IBOutlet weak var lastNameTopLabel: UILabel!
    @IBOutlet weak var emailTopLabel: UILabel!
    @IBOutlet weak var contactNumberTopLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var myAccountLabel: UILabel!
    @IBOutlet weak var countryCodeLabel: UILabel!
    var userDetails = UserRealm()
    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyling()
        applyFont()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        userDetails = RealmDataManager.sharedInstance.getUserDetails()
        applyData()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - Assigning Data To TextFields
    func applyData() {
        firstNameLabel.text = userDetails.userFirstName
        lastNameLabel.text = userDetails.userLastName
        emailLabel.text = userDetails.userEmail
        mobileNumberLabel.text = userDetails.userMobileNumber
        if userDetails.userImageURL == "" {
        profileImageView.image = Utilities.sharedInstance.imageFromInitials(
            initial01: userDetails.userFirstName,
            initial02: userDetails.userLastName,
            BGColor: ColourConstants.hexE0E0E0,
            fontName: FontConstants.semiBold,
            fontSize: 40,
            textColor: ColourConstants.hex1D1D1D)
        } else {
            let url = URL(string: userDetails.userImageURL)
            NukeManager.sharedInstance.setImage(url: url, imageView: profileImageView, withPlaceholder: true)
        }
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2
        editButton.applyButtonLetterSpacing(spacing: 2.16)
        changePasswordButton.applyButtonLetterSpacing(spacing: 2.16)
        myAccountLabel.applyLetterSpacing(spacing: 0.7)
        editButton.setTitleColor(ColourConstants.hex191919, for: .normal)
        changePasswordButton.setTitleColor(ColourConstants.hex191919, for: .normal)
    }
    func applyTopLabelStyling(labels: [UILabel]) {
        for label in labels {
            label.font = UIFont(name: FontConstants.regular, size: 11)
            label.textColor = ColourConstants.hex9B9B9B
        }
    }
    func applyLabelStyling(labels: [UILabel]) {
        for label in labels {
            label.font = UIFont(name: FontConstants.regular, size: 13)
            label.textColor = ColourConstants.hex191919
        }
    }
    // MARK: - Font Styling Method
    func applyFont() {
        editButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
        changePasswordButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 12)
        myAccountLabel.font = UIFont(name: FontConstants.regular, size: 14)
        applyTopLabelStyling(labels: [
            firstNameTopLabel, lastNameTopLabel, emailTopLabel,
            contactNumberTopLabel
        ])
        applyLabelStyling(labels: [
            firstNameLabel, lastNameLabel, emailLabel,
            mobileNumberLabel, passwordLabel, countryCodeLabel
        ])
    }
    // MARK: - Edit Button Action
    @IBAction func editButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "UpdateProfile", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(withIdentifier: "UpdateProfileViewController") as?
                UpdateProfileViewController else {
            return
        }
        nextvc.presenter.userDetails = userDetails
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    // MARK: - Change Password Button Action
    @IBAction func changePasswordButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "UpdatePassword", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(withIdentifier: "UpdatePasswordViewController") as?
                UpdatePasswordViewController else {
            return
        }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    // MARK: - Back Button Action
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
