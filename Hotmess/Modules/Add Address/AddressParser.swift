//
//  AddressParser.swift
//  Hotmess
//
//  Created by Flaxon on 27/07/21.
//

import Foundation
import SwiftyJSON

class AddressParser: NSObject {
    static let sharedInstance = AddressParser()
    func parseAddressData(data: [String: JSON]) -> ShippingAddressRealm {
        let addressData = ShippingAddressRealm()
        addressData.addressID = data["address_id"]?.string ?? ""
        addressData.firstName = data["first_name"]?.string ?? ""
        addressData.lastName = data["last_name"]?.string ?? ""
        addressData.mobileNumber = data["mobile_number"]?.string ?? ""
        addressData.countryCode = data["country_code"]?.string ?? ""
        addressData.addressLine1 = data["address_line1"]?.string ?? ""
        addressData.addressLine2 = data["address_line2"]?.string ?? ""
        addressData.areaPinCode = data["area_pin_code"]?.string ?? ""
        addressData.landMark = data["landmark"]?.string ?? ""
        addressData.city = data["city"]?.string ?? ""
        addressData.state = data["state"]?.string ?? ""
        addressData.country = data["country"]?.string ?? ""
        addressData.isDefaultAddress = data["default"]?.bool ?? false
        return addressData
    }
}
