//
//  AddAddressPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 27/07/21.
//

import Foundation
import SwiftyJSON

protocol AddAddressPresenterProtocol: AnyObject {
    func fillAllFields()
    func refresh(status: Bool, message: String)
    func addAddressSuccess(status: Bool, title: String, message: String)
}

class AddAddressPresenter: NSObject {
    weak var delegate: AddAddressPresenterProtocol?
    var addressData = ShippingAddressRealm()
    var setAddressDefault: Bool = false
    var viewMode: Int = 0
    var shoppingBagItemArray = [ShoppingBagRealm]()
    var email: String = ""
    func isAddressEmpty(addressData: ShippingAddressRealm) -> Bool {
        if viewMode == 2 {
            if email == "" || addressData.firstName == "" ||
                addressData.lastName == "" ||
                addressData.mobileNumber == "" ||
                addressData.addressLine1 == "" ||
                addressData.addressLine2 == "" ||
                addressData.areaPinCode == "" ||
                addressData.city == "" ||
                addressData.state == "" ||
                addressData.country == "" {
                return true
            } else {
                return false
            }
        } else {
            if addressData.firstName == "" ||
                addressData.lastName == "" ||
                addressData.mobileNumber == "" ||
                addressData.addressLine1 == "" ||
                addressData.addressLine2 == "" ||
                addressData.areaPinCode == "" ||
                addressData.city == "" ||
                addressData.state == "" ||
                addressData.country == "" {
                return true
            } else {
                return false
            }
        }
    }
    func requestAddAddress() {
        if isAddressEmpty(addressData: addressData) {
            delegate?.fillAllFields()
        } else if addressData.mobileNumber.count < 10 {
            delegate?.addAddressSuccess(status: false,
                                        title: "MobileNumberAlert",
                                        message: "Mobile Number must have 10 digits")
        } else {
            if viewMode == 1 {
                updateAddress()
            } else if viewMode == 0 {
                addAddress()
            }
        }
    }
    func addAddress() {
        print(setAddressDefault)
        let parameters: [String: Any] = [
            "address_type": "Home",
            "first_name": addressData.firstName ,
            "last_name": addressData.lastName,
            "mobile_number": addressData.mobileNumber,
            "country_code": "+91",
            "address_line1": addressData.addressLine1,
            "address_line2": addressData.addressLine2,
            "landmark": addressData.landMark,
            "area_pin_code": addressData.areaPinCode,
            "city": addressData.city,
            "state": addressData.state,
            "country": addressData.country,
            "default": setAddressDefault
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.addAddress,
            prametres: parameters,
            type: .withAuth,
            method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.requestAddAddress()
                            }
                        } else {
                            self.delegate?.addAddressSuccess(status: false, title: "Error", message: message)
                        }
                    }
                } else {
                    self.delegate?.addAddressSuccess(status: true, title: "", message: "")
                }
            }
        }
    }
    func updateAddress() {
        let parameters: [String: Any] = [
            "address_type": "Home",
            "first_name": addressData.firstName ,
            "last_name": addressData.lastName,
            "mobile_number": addressData.mobileNumber,
            "country_code": "+91",
            "address_line1": addressData.addressLine1,
            "address_line2": addressData.addressLine2,
            "landmark": addressData.landMark,
            "area_pin_code": addressData.areaPinCode,
            "city": addressData.city,
            "state": addressData.state,
            "country": addressData.country,
            "address_id": addressData.addressID,
            "default": setAddressDefault
        ]
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.updateAddress,
            prametres: parameters,
            type: .withAuth,
            method: .POST) { response in
            print(response)
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.requestAddAddress()
                            }
                        } else {
                            self.delegate?.addAddressSuccess(status: false, title: "Error", message: message)
                        }
                    }
                } else {
                    self.delegate?.addAddressSuccess(status: true, title: "", message: "")
                }
            }
        }
    }
    func requestAddressByPinCode(pinCode: String) {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.getLocationByPin,
            prametres: [
                "pincode": pinCode
            ],
            type: .withoutAuth,
            method: .POST,
            completion: self.handlePinCodeResponse)
    }
    func handlePinCodeResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                delegate?.refresh(status: false, message: "Please enter valid PIN Code")
            } else {
                if let data = response["data"].dictionary {
                    addressData.city = data["city"]?.string ?? ""
                    addressData.state = data["state"]?.string ?? ""
                    addressData.country = data["country"]?.string ?? ""
                    delegate?.refresh(status: true, message: "")
                }
            }
        }
    }
}
