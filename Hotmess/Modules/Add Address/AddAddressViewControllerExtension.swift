//
//  AddAddressViewControllerExtension.swift
//  Hotmess
//
//  Created by Flaxon on 18/10/21.
//

import Foundation
import UIKit

extension AddAddressViewController {
    func showAlertUI() {
        if emailTextField.text == "" {
            emailWarningImageView.isHidden = false
            emailTextField.layer.borderColor = UIColor.red.cgColor
        }
        if firstNameTextField.text  == "" {
            firstNameWarningImageView.isHidden = false
            firstNameTextField.layer.borderColor = UIColor.red.cgColor
        }
        if lastNametextField.text == "" {
            lastNameWarningImageView.isHidden = false
            lastNametextField.layer.borderColor = UIColor.red.cgColor
        }
        if mobileNumberTextField.text == "" {
            mobileWarningImageView.isHidden = false
            mobileNumberView.layer.borderColor = UIColor.red.cgColor
        }
        if addressLine1TextField.text == "" {
            addressWarningImageView.isHidden = false
            addressLine1TextField.layer.borderColor = UIColor.red.cgColor
        }
        if addressLine2TextField.text == "" {
            localityWarningImageView.isHidden = false
            addressLine2TextField.layer.borderColor = UIColor.red.cgColor
        }
        if landmarkTextField.text == "" {
            landmarkWarningImageView.isHidden = false
            landmarkTextField.layer.borderColor = UIColor.red.cgColor
        }
        if pinCodeTextField.text == "" {
            pincodeWarningImageView.isHidden = false
            pinCodeTextField.layer.borderColor = UIColor.red.cgColor
        }
    }
    func applyAddressTextFieldStyling(textFields: [UITextField]) {
        for textField in textFields {
            textField.layer.borderWidth = 1
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
            textField.font = UIFont(name: FontConstants.regular, size: 13)
            textField.setLeftPaddingPoints(10)
            textField.setRightPaddingPoints(10)
        }
    }
    func applyAddressTopLabelStyling(labels: [UILabel]) {
        for label in labels {
            label.font = UIFont(name: FontConstants.regular, size: 8)
            label.textColor = ColourConstants.hex9B9B9B
            label.isHidden = true
        }
    }
}
