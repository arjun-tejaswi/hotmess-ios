//
//  AddAddressViewController.swift
//  Hotmess
//
//  Created by Flaxon on 27/07/21.
//

import UIKit

class AddAddressViewController: UIViewController {
    @IBOutlet weak var shippingAddressLabel: UILabel!
    @IBOutlet weak var shippingAddressHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var defaultAddressImageView: UIImageView!
    @IBOutlet weak var setAddressDefaultLabel: UILabel!
    @IBOutlet weak var cityWarningImageView: UIImageView!
    @IBOutlet weak var pincodeWarningImageView: UIImageView!
    @IBOutlet weak var landmarkWarningImageView: UIImageView!
    @IBOutlet weak var localityWarningImageView: UIImageView!
    @IBOutlet weak var addressWarningImageView: UIImageView!
    @IBOutlet weak var mobileWarningImageView: UIImageView!
    @IBOutlet weak var lastNameWarningImageView: UIImageView!
    @IBOutlet weak var firstNameWarningImageView: UIImageView!
    @IBOutlet weak var emailHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var mobileNumberView: UIView!
    @IBOutlet weak var countryTopLabel: UILabel!
    @IBOutlet weak var stateTopLabel: UILabel!
    @IBOutlet weak var cityTopLabel: UILabel!
    @IBOutlet weak var pinCodeTopLabel: UILabel!
    @IBOutlet weak var landMarkTopLabel: UILabel!
    @IBOutlet weak var addressLine2TopLabel: UILabel!
    @IBOutlet weak var addressLine1TopLabel: UILabel!
    @IBOutlet weak var mobileNumberTopLabel: UILabel!
    @IBOutlet weak var lastNameTopLabel: UILabel!
    @IBOutlet weak var firstNameTopLabel: UILabel!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var lastNametextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var pinCodeTextField: UITextField!
    @IBOutlet weak var landmarkTextField: UITextField!
    @IBOutlet weak var addressLine2TextField: UITextField!
    @IBOutlet weak var addressLine1TextField: UITextField!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var addAddressLabel: UILabel!
    @IBOutlet weak var addAddressButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailTitleLabel: UILabel!
    @IBOutlet weak var emailWarningImageView: UIImageView!
    var presenter = AddAddressPresenter()
    weak var changeAddressDelegate: ChangeAddressProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        applyStyling()
        applyFont()
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @objc func maxValueReached(_ textField: UITextField) {
        if let textValue = textField.text {
            if textValue.count >= 10 {
                self.view.endEditing(true)
            }
        }
    }
    @objc func pincodeValueChanged(_ textField: UITextField) {
        if let textValue = textField.text {
            if textValue.count >= 6 {
                self.view.endEditing(true)
                self.view.showLoader()
                presenter.requestAddressByPinCode(pinCode: pinCodeTextField.text ?? "")
            }
        }
    }
    @IBAction func setDefaultAddressButtonAction(_ sender: UIButton) {
        if presenter.setAddressDefault {
            presenter.setAddressDefault = false
            defaultAddressImageView.image = UIImage(named: "checkbox")
        } else {
            presenter.setAddressDefault = true
            defaultAddressImageView.image = UIImage(named: "checkboxActive")
        }
    }
    @IBAction func addAddressButtonAction(_ sender: UIButton) {
        let addressData = ShippingAddressRealm()
        addressData.firstName = firstNameTextField.text ?? ""
        addressData.lastName = lastNametextField.text ?? ""
        addressData.mobileNumber = mobileNumberTextField.text ?? ""
        addressData.addressLine1 = addressLine1TextField.text ?? ""
        addressData.addressLine2 = addressLine2TextField.text ?? ""
        addressData.areaPinCode = pinCodeTextField.text ?? ""
        addressData.landMark = landmarkTextField.text ?? ""
        addressData.city = cityTextField.text ?? ""
        addressData.state = stateTextField.text ?? ""
        addressData.country = countryTextField.text ?? ""
        addressData.addressID = presenter.addressData.addressID
        presenter.email = emailTextField.text ?? ""
        if presenter.viewMode == 2 {
            if presenter.isAddressEmpty(addressData: addressData) {
                fillAllFields()
            } else if mobileNumberTextField.text?.count ?? 0 < 10 {
                addAddressSuccess(status: false,
                                  title: "MobileNumberAlert",
                                  message: "Mobile Number must have 10 digits")
            } else {
                let storyBoard = UIStoryboard(name: "OrderSummary", bundle: nil)
                guard let nextvc = storyBoard.instantiateViewController(
                        withIdentifier: "OrderInformationViewController") as? OrderInformationViewController else {
                    return
                }
                nextvc.presenter.receiveNews = presenter.setAddressDefault
                nextvc.presenter.isGuest = true
                nextvc.presenter.selectedAddress = addressData
                nextvc.presenter.guestEmail = presenter.email
                nextvc.presenter.bagItems = presenter.shoppingBagItemArray
                self.navigationController?.pushViewController(nextvc, animated: true)
            }
        } else {
            self.view.showLoader()
            presenter.addressData = addressData
            presenter.requestAddAddress()
        }
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        pinCodeTextField.addTarget(self, action: #selector(pincodeValueChanged), for: .editingChanged)
        mobileNumberTextField.addTarget(self, action: #selector(maxValueReached), for: .editingChanged)
        setAddressDefaultLabel.textColor = ColourConstants.hex717171
        addAddressButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        addAddressLabel.textColor = ColourConstants.hexFFFFFF
        addAddressButton.applyButtonLetterSpacing(spacing: 2.52)
        mobileNumberView.layer.borderWidth = 1
        mobileNumberView.layer.borderColor = ColourConstants.hex717171.cgColor
        mobileNumberTextField.font = UIFont(name: FontConstants.regular, size: 13)
        mobileNumberTextField.setLeftPaddingPoints(10)
        mobileNumberTextField.setRightPaddingPoints(10)
        countryCodeLabel.applyLetterSpacing(spacing: 3.9)
        countryCodeLabel.textColor = ColourConstants.hex191919
        applyAddressTextFieldStyling(textFields: [
            emailTextField, firstNameTextField,
            lastNametextField, addressLine1TextField, addressLine2TextField,
            pinCodeTextField, landmarkTextField, cityTextField,
            stateTextField, countryTextField
        ])
        applyAddressTopLabelStyling(labels: [emailTitleLabel,
                                      firstNameTopLabel, lastNameTopLabel, mobileNumberTopLabel,
                                      addressLine1TopLabel, addressLine2TopLabel,
                                      landMarkTopLabel, pinCodeTopLabel,
                                      cityTopLabel, stateTopLabel, countryTopLabel
        ])
        emailTextField.delegate = self
        firstNameTextField.delegate = self
        lastNametextField.delegate = self
        mobileNumberTextField.delegate = self
        addressLine1TextField.delegate = self
        addressLine2TextField.delegate = self
        pinCodeTextField.delegate = self
        landmarkTextField.delegate = self
        cityTextField.delegate = self
        stateTextField.delegate = self
        countryTextField.delegate = self
        changeMode()
    }
    // MARK: - Font Style Method
    func applyFont() {
        addAddressLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        countryCodeLabel.font = UIFont(name: FontConstants.regular, size: 13)
        addAddressButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        setAddressDefaultLabel.font = UIFont(name: FontConstants.light, size: 10)
        shippingAddressLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
    }
    func changeMode() {
        if presenter.viewMode == 1 {
            firstNameTextField.text = presenter.addressData.firstName
            lastNametextField.text = presenter.addressData.lastName
            mobileNumberTextField.text = "\(presenter.addressData.mobileNumber)"
            addressLine1TextField.text = presenter.addressData.addressLine1
            addressLine2TextField.text = presenter.addressData.addressLine2
            landmarkTextField.text = presenter.addressData.landMark
            pinCodeTextField.text = presenter.addressData.areaPinCode
            cityTextField.text = presenter.addressData.city
            stateTextField.text = presenter.addressData.state
            countryTextField.text = presenter.addressData.country
            presenter.setAddressDefault = presenter.addressData.isDefaultAddress
            if presenter.setAddressDefault {
                defaultAddressImageView.image = UIImage(named: "checkboxActive")
            } else {
                defaultAddressImageView.image = UIImage(named: "checkbox")
            }
            emailTextField.isHidden = true
            shippingAddressHeightConstraint.constant = 0
            addAddressButton.setTitle("UPDATE ADDRESS", for: .normal)
            addAddressLabel.text = "EDIT ADDRESS"
        } else if presenter.viewMode == 0 {
            shippingAddressHeightConstraint.constant = 0
            emailTextField.isHidden = true
            addAddressButton.setTitle("ADD ADDRESS", for: .normal)
            addAddressLabel.text = "ADD ADDRESS"
        } else {
            emailTextField.isHidden = false
            emailHeightConstraint.constant = 60
            shippingAddressHeightConstraint.constant = 50
            addAddressButton.setTitle("CONTINUE", for: .normal)
            addAddressLabel.text = "CHECKOUT"
            setAddressDefaultLabel.text = "I would like to receive style advice, news and promotional offers via email"
        }
    }
}
extension AddAddressViewController: AddAddressPresenterProtocol {
    func refresh(status: Bool, message: String) {
        self.view.hideLoader()
        if status {
            cityTextField.text = presenter.addressData.city
            stateTextField.text = presenter.addressData.state
            stateTextField.isUserInteractionEnabled = false
            countryTextField.text = presenter.addressData.country
            countryTextField.isUserInteractionEnabled = false
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: "Error",
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
            cityTextField.text = .none
            stateTextField.text = .none
            countryTextField.text = .none
        }
    }
    func fillAllFields() {
        self.view.hideLoader()
        showAlertUI()
    }
    func addAddressSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            self.navigationController?.popViewController(animated: true)
        } else {
            if title == "MobileNumberAlert" {
                mobileWarningImageView.isHidden = false
                mobileNumberView.layer.borderColor = UIColor.red.cgColor
                let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                    bounds: self.view.bounds,
                    title: "Error", subTitle: message, buttonTitle: "OK")
                self.view.addSubview(popUp)
            } else {
                let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                    bounds: self.view.bounds,
                    title: title, subTitle: message, buttonTitle: "OK")
                self.view.addSubview(popUp)
            }
        }
    }
}
extension AddAddressViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if textField == pinCodeTextField {
            let maxLength = 6
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        if textField == mobileNumberTextField {
            let maxLength = 10
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if emailTextField.text == "" {
            emailTitleLabel.isHidden = true
        }
        if firstNameTextField.text  == "" {
            firstNameTopLabel.isHidden = true
        }
        if lastNametextField.text == "" {
            lastNameTopLabel.isHidden = true
        }
        if mobileNumberTextField.text == "" {
            mobileNumberTopLabel.isHidden = true
        }
        if addressLine1TextField.text == "" {
            addressLine1TopLabel.isHidden = true
        }
        if addressLine2TextField.text == "" {
            addressLine2TopLabel.isHidden = true
        }
        if landmarkTextField.text == "" {
            landMarkTopLabel.isHidden = true
        }
        if pinCodeTextField.text == "" {
            pinCodeTopLabel.isHidden = true
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTextField {
            emailTitleLabel.isHidden = false
            emailWarningImageView.isHidden = true
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
        if textField == firstNameTextField {
            firstNameTopLabel.isHidden = false
            firstNameWarningImageView.isHidden = true
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
        if textField == lastNametextField {
            lastNameTopLabel.isHidden = false
            lastNameWarningImageView.isHidden = true
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
        if textField == mobileNumberTextField {
            mobileNumberTopLabel.isHidden = false
            mobileWarningImageView.isHidden = true
            mobileNumberView.layer.borderColor = ColourConstants.hex717171.cgColor
        }
        if textField == addressLine1TextField {
            addressLine1TopLabel.isHidden = false
            addressWarningImageView.isHidden = true
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
        if textField == addressLine2TextField {
            addressLine2TopLabel.isHidden = false
            localityWarningImageView.isHidden = true
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
        if textField == pinCodeTextField {
            pinCodeTopLabel.isHidden = false
            pincodeWarningImageView.isHidden = true
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
        if textField == landmarkTextField {
            landMarkTopLabel.isHidden = false
            landmarkWarningImageView.isHidden = true
            textField.layer.borderColor = ColourConstants.hex717171.cgColor
        }
    }
}
