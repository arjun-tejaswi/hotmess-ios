//
//  SlideMenuViewController.swift
//  Hotmess
//
//  Created by Flaxon on 02/06/21.
//

import UIKit
import Nuke

class SlideMenuViewController: UIViewController {
    @IBOutlet weak var profileTableView: UITableView!
    var presenter: SlideMenuPresenter!
    let userDetails = RealmDataManager.sharedInstance.getUserDetails()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = SlideMenuPresenter()
        presenter.delegate = self
        menuTabelViewCellConfiguration()
    }
    @IBAction func closeButtonActon(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: - Menu TableView Cell Configuration
    func menuTabelViewCellConfiguration() {
        profileTableView.delegate = self
        profileTableView.dataSource = self
        profileTableView.register(UINib(nibName: "ProfileMenuCustomCell", bundle: nil),
                                  forCellReuseIdentifier: "ProfileMenuCustomCell")
        profileTableView.register(UINib(nibName: "PersonalizeMenuCustomCell", bundle: nil),
                                  forCellReuseIdentifier: "PersonalizeMenuCustomCell")
        profileTableView.register(UINib(nibName: "LegalInfoCustomCell", bundle: nil),
                                  forCellReuseIdentifier: "LegalInfoCustomCell")
        profileTableView.register(UINib(nibName: "LogOutCustomCell", bundle: nil),
                                  forCellReuseIdentifier: "LogOutCustomCell")
        profileTableView.register(UINib(nibName: "AppVersionCustomCell", bundle: nil),
                                  forCellReuseIdentifier: "AppVersionCustomCell")
        profileTableView.register(UINib(nibName: "TouchIDCustomCell", bundle: nil),
                                  forCellReuseIdentifier: "TouchIDCustomCell")
    }
    // MARK: - Logout Button Action
    @objc func logOutButtonAction(sender: UIButton) {
        presenter.logoutRequest()
    }
    @objc func myAccountButtonAction(sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Account", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(withIdentifier: "AccountViewController") as?
                AccountViewController else {
            return
        }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
    // MARK: - Personalize Menu Cell Configuration
    func personalizeMenuImageAndText(index: Int) -> (UIImage?, String) {
        var personalizeImage: UIImage?
        var personalizeText: String!
        if index == 1 {
            personalizeImage = UIImage(named: "wishList_Icon")
            personalizeText = "MY WISHLIST"
        } else if index == 2 {
            personalizeImage = UIImage(named: "myOrderIcon")
            personalizeText = "MY ORDERS"
        } else if index == 3 {
            personalizeImage = UIImage(named: "myAddressIcon")
            personalizeText = "MY ADDRESS"
        } else if index == 4 {
            personalizeImage = UIImage(named: "buyRedeemGiftIcon")
            personalizeText = "BUY & REDEEM GIFT CARDS"
        } else if index == 5 {
            personalizeImage = UIImage(named: "newsLetter")
            personalizeText = "NEWSLETTER"
        } else if index == 6 {
            personalizeImage = UIImage(named: "InviteFriendIcon")
            personalizeText = "INVITE FRIEND"
        } else if index == 7 {
            personalizeImage = UIImage(named: "TouchIDIcon")
            personalizeText = "TOUCH/FACE ID"
        }
        return (personalizeImage, personalizeText)
    }
    // MARK: - Leagal Menu Cell Configuration
    func leagalMenuText(index: Int) -> String {
        var leagalText: String!
        if index == 9 {
            leagalText = "RATE US"
        } else if index == 10 {
            leagalText = "PRIVACY POLICY"
        } else if index == 11 {
            leagalText = "CONTACT US"
        } else if index == 12 {
            leagalText = "TERMS & CONDITIONS"
        } else if index == 13 {
            leagalText = "FAQS"
        }
        return leagalText
    }
    func loginRegister() {
        let storyboardLogin = UIStoryboard(name: "Login", bundle: nil)
        guard let loginView = storyboardLogin.instantiateViewController(
                withIdentifier: "LoginViewController") as? LoginViewController else {
            return
        }
        self.navigationController?.pushViewController(loginView, animated: true)
    }
    @objc func loginRegisterButtonAction(sender: UIButton) {
        loginRegister()
    }
    func getProfileMenuCustomCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = profileTableView.dequeueReusableCell(
                withIdentifier: "ProfileMenuCustomCell", for: indexPath) as? ProfileMenuCustomCell else {
            return UITableViewCell()
        }
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            cell.myAccountView.isHidden = false
            cell.loginRegistrationButton.isHidden = true
            cell.customerNameLabel.text = "\(userDetails.userFirstName)" + " \(userDetails.userLastName)"
            if userDetails.userImageURL == "" {
                cell.customerImageView.image = Utilities.sharedInstance.imageFromInitials(
                    initial01: userDetails.userFirstName,
                    initial02: userDetails.userFirstName,
                    BGColor: ColourConstants.hexFFFFFF)
                cell.customerImageView.image = Utilities.sharedInstance.imageFromInitials(
                    initial01: userDetails.userFirstName,
                    initial02: userDetails.userLastName,
                    BGColor: ColourConstants.hexFFFFFF,
                    fontName: FontConstants.semiBold,
                    fontSize: 40,
                    textColor: ColourConstants.hex1D1D1D)
            } else {
                NukeManager.sharedInstance.setImage(url: userDetails.userImageURL,
                                                    imageView: cell.customerImageView,
                                                    withPlaceholder: true)
            }
            cell.myAccountButton.addTarget(self, action: #selector(myAccountButtonAction), for: .touchUpInside)
        } else {
            cell.myAccountView.isHidden = true
            //                cell.customerNameLabel.isHidden = true
            cell.loginRegistrationButton.isHidden = false
            cell.loginRegistrationButton.addTarget(self,
                                                   action: #selector(loginRegisterButtonAction),
                                                   for: .touchUpInside)
        }
        return cell
    }
    func getPersonalizeMenuCustomCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = profileTableView.dequeueReusableCell(
                withIdentifier: "PersonalizeMenuCustomCell", for: indexPath) as? PersonalizeMenuCustomCell else {
            return UITableViewCell()
        }
        let (personalizeImage, persoanlizeText) = personalizeMenuImageAndText(index: indexPath.item)
        cell.personalizeImageView.image = personalizeImage
        cell.personalizeLabel.text = persoanlizeText
        return cell
    }
    func getTouchIDCustomCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = profileTableView.dequeueReusableCell(
                withIdentifier: "TouchIDCustomCell", for: indexPath) as? TouchIDCustomCell else {
            return UITableViewCell()
        }
        let (personalizeImage, persoanlizeText) = personalizeMenuImageAndText(index: indexPath.item)
        cell.setSwitchState()
        cell.personalizeImageView.image = personalizeImage
        cell.personalizeLabel.text = persoanlizeText
        return cell
    }
    func getLegalInfoCustomCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = profileTableView.dequeueReusableCell(
                withIdentifier: "LegalInfoCustomCell", for: indexPath) as? LegalInfoCustomCell else {
            return UITableViewCell()
        }
        if indexPath.item == 8 {
            cell.separationViewTopConstraint.constant = 27.32
            cell.separationView.isHidden = false
            cell.leagalInfoLabel.text = "RETURNS & EXCHANGES"
        } else {
            let leagalText = leagalMenuText(index: indexPath.item)
            cell.leagalInfoLabel.text = leagalText
        }
        return cell
    }
    func getLogOutCustomCell(indexPath: IndexPath) -> UITableViewCell {
        guard let cell = profileTableView.dequeueReusableCell(
                withIdentifier: "LogOutCustomCell", for: indexPath) as? LogOutCustomCell else {
            return UITableViewCell()
        }
        if UserDefaults.standard.bool(forKey: "isLoggedIn") == true {
            cell.logOutButton.addTarget(self, action: #selector(logOutButtonAction), for: .touchUpInside)
        } else {
            cell.logOutButton.isHidden = true
        }
        return cell
    }
}
// MARK: - Tabelview Methods
extension SlideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 16
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case 0:
            return getProfileMenuCustomCell(indexPath: indexPath)
        case 1, 2, 3, 4, 5, 6:
            return getPersonalizeMenuCustomCell(indexPath: indexPath)
        case 7:
            return getTouchIDCustomCell(indexPath: indexPath)
        case 8, 9, 10, 11, 12, 13:
            return getLegalInfoCustomCell(indexPath: indexPath)
        case 14:
           return getLogOutCustomCell(indexPath: indexPath)
        default:
            guard let cell = tableView.dequeueReusableCell(
                    withIdentifier: "AppVersionCustomCell") as? AppVersionCustomCell else {
                return UITableViewCell()
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1, 2, 3, 4, 5, 11:
            goToView(index: indexPath.row)
        default:
            if indexPath.row == 6 {
                let shareMessage = "Hey, have you checked out Hotmess yet?\nI think you’d love it! \n"
                let textToShare = ["\(shareMessage)\(BranchStaticURL.inviteFriend)"]
                let activityViewController = UIActivityViewController(
                    activityItems: textToShare,
                    applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
            } else if indexPath.row == 9 {
                guard let appStoreURL = URL(string: ExternalURL.appStoreURL) else { return }
                var components = URLComponents(url: appStoreURL, resolvingAgainstBaseURL: false)
                components?.queryItems = [ URLQueryItem(name: "action", value: "write-review")]
                guard let writeReviewURL = components?.url else { return }
                UIApplication.shared.open(writeReviewURL)
            } else if indexPath.row == 10 {
                goToWebView(state: .PRIVACY)
            } else if indexPath.row == 12 {
                goToWebView()
            } else if indexPath.row == 13 {
                goToWebView(state: .FAQS)
            }
        }
    }
}
