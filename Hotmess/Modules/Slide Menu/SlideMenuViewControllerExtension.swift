//
//  SlideMenuViewControllerExtension.swift
//  Hotmess
//
//  Created by Cedan Misquith on 28/09/21.
//

import UIKit

extension SlideMenuViewController {
    func goToView(index: Int) {
        switch index {
        case 1:
            goToWishlist()
        case 2:
            goToMyOrders()
        case 3:
            goToShippingAddresses()
        case 4:
            goToGiftCard()
        case 5:
            goToNewsLetter()
        case 11:
            goToContactUs()
        default:
            print("Invalid Choice")
        }
    }
    func goToContactUs() {
        let storyBoard = UIStoryboard(name: "ContactUs", bundle: nil)
        guard let contactUsVC = storyBoard.instantiateViewController(
                withIdentifier: "ContactUsViewController") as? ContactUsViewController else {
            return
        }
        self.navigationController?.pushViewController(contactUsVC, animated: true)
    }
    func goToWebView(state: WebViewState = .TERMS) {
        let storyBoard = UIStoryboard(name: "WebView", bundle: nil)
        guard let webViewVC = storyBoard.instantiateViewController(
                withIdentifier: "WebViewController") as? WebViewController else {
            return
        }
        webViewVC.state = state
        self.navigationController?.pushViewController(webViewVC, animated: true)
    }
    func goToWishlist() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            let storyBoard = UIStoryboard(name: "Wishlist", bundle: nil)
            guard let wishListVC = storyBoard.instantiateViewController(
                    withIdentifier: "WishlistViewController") as? WishlistViewController else {
                return
            }
            self.navigationController?.pushViewController(wishListVC, animated: true)
        } else {
            loginRegister()
        }
    }
    func goToMyOrders() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            let storyBoard = UIStoryboard(name: "MyOrders", bundle: nil)
            guard let nextVC = storyBoard.instantiateViewController(
                    withIdentifier: "ListOrdersViewController") as? ListOrdersViewController else {
                return
            }
            self.navigationController?.pushViewController(nextVC, animated: true)
        } else {
            loginRegister()
        }
    }
    func goToShippingAddresses() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            let storyBoard = UIStoryboard(name: "ShippingAddress", bundle: nil)
            guard let shippingAddressVC = storyBoard.instantiateViewController(
                    withIdentifier: "ShippingAddressViewController") as? ShippingAddressViewController else {
                return
            }
            shippingAddressVC.state = .FROMSIDEMENU
            self.navigationController?.pushViewController(shippingAddressVC, animated: true)
        } else {
            loginRegister()
        }
    }
    func goToGiftCard() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") == true {
            let storyBoard = UIStoryboard(name: "GiftCardWallet", bundle: nil)
            guard let giftCardVC = storyBoard.instantiateViewController(
                    withIdentifier: "GiftCardWalletViewController") as? GiftCardWalletViewController else {
                return
            }
            self.navigationController?.pushViewController(giftCardVC, animated: true)
        } else {
            loginRegister()
        }
    }
    func goToNewsLetter() {
        let storyBoard = UIStoryboard(name: "NewsLetter", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(withIdentifier: "NewsLetterViewController") as?
                NewsLetterViewController else {
            return
        }
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}

extension SlideMenuViewController: SlideMenuProtocol {
    func success() {
        Utilities.sharedInstance.logOutApp()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError("Not of type 'AppDelegate'")
        }
        appDelegate.restartApp()
    }
}
