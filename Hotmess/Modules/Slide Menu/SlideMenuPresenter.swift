//
//  SlideMenuPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 16/06/21.
//

import Foundation

protocol SlideMenuProtocol: AnyObject {
    func success()
}

class SlideMenuPresenter: NSObject {
    weak var delegate: SlideMenuProtocol!
    func logoutRequest() {
        delegate?.success()
    }
}
