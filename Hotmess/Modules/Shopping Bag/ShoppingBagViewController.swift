//
//  ShoppingBagViewController.swift
//  Hotmess
//
//  Created by Flaxon on 02/06/21.
//

import UIKit

class ShoppingBagViewController: UIViewController {
    @IBOutlet weak var orderSummeryPriceStackView: UIStackView!
    @IBOutlet weak var orderSummeryStackView: UIStackView!
    @IBOutlet weak var orderSummeryTitleLabel: UILabel!
    @IBOutlet weak var totalTitleLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var shoppingBagTitleLabel: UILabel!
    @IBOutlet weak var removeAllFromWishlistButton: UIButton!
    @IBOutlet weak var removeAllFromBagButton: UIButton!
    @IBOutlet weak var bagItemsCountLabel: UILabel!
    @IBOutlet weak var taxDutiesLabel: UILabel!
    @IBOutlet weak var proceedToCheckoutButton: UIButton!
    @IBOutlet weak var taxesDutiesView: UIView!
    @IBOutlet weak var shoppingBagTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var shoppingBagTableView: UITableView!
    @IBOutlet weak var emptyBagView: UIView!
    @IBOutlet weak var shopBagScrollView: UIScrollView!
    @IBOutlet weak var checkOutOptionsContainerView: UIView!
    @IBOutlet weak var checkOutOptionContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var haveAnAccountImageView: UIImageView!
    @IBOutlet weak var guestCheckoutImageView: UIImageView!
    var presenter: ShoppingBagPresenter!
    var selectedPackagging = -1
    var checkOutOption: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ShoppingBagPresenter()
        presenter.delegate = self
        shoppinBagTableViewConfigure()
        shoppingBagTableView.reloadData()
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.shoppingBagTableViewHeightConstraint.constant = self.shoppingBagTableView.intrinsicContentSize.height
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.showLoader()
        presenter.getShoppingBagData()
        applyStyling()
        applyFont()
    }
    @IBAction func shopNowButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func removeAllButtonAction(_ sender: UIButton) {
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: "REMOVE ALL",
            subTitle: "Are you sure you want to remove all items from your shopping bag ?",
            buttonTitle: "REMOVE NOW") {
            self.view.showLoader()
            self.presenter.removeAllFromShoppingBag()
        }
        self.view.addSubview(popUp)
    }
    @IBAction func moveAllToWishlistButtonAction(_ sender: UIButton) {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            self.view.showLoader()
            presenter.moveAllToWishlist()
        } else {
            presenter.saveAllToWishList()
            let storyboardLogin = UIStoryboard(name: "Login", bundle: nil)
            guard let loginView = storyboardLogin.instantiateViewController(
                    withIdentifier: "LoginViewController") as? LoginViewController else {
                return
            }
            self.navigationController?.view.setNavigationTransitionLayer()
            self.navigationController?.pushViewController(loginView, animated: true)
        }
    }
    @IBAction func haveAnAccountButtonAction(_ sender: UIButton) {
        checkOutOption = 0
        selectCheckoutOption()
    }
    @IBAction func guestCheckOutButtonAction(_ sender: UIButton) {
        checkOutOption = 1
        selectCheckoutOption()
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        taxesDutiesView.backgroundColor = ColourConstants.hexE8F8FF
        taxesDutiesView.layer.borderWidth = 1
        taxesDutiesView.layer.borderColor = ColourConstants.hex9AB5BC.cgColor
        proceedToCheckoutButton.backgroundColor = ColourConstants.hex191919
        proceedToCheckoutButton.applyButtonLetterSpacing(spacing: 2.52)
        bagItemsCountLabel.applyLetterSpacing(spacing: 0.8)
        shoppingBagTitleLabel.applyLetterSpacing(spacing: 0.7)
        orderSummeryTitleLabel.applyLetterSpacing(spacing: 0.8)
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            checkOutOptionContainerHeightConstraint.constant = 0
            checkOutOptionsContainerView.isHidden = true
        }
        selectCheckoutOption()
    }
    func selectCheckoutOption() {
        if checkOutOption == 0 {
            haveAnAccountImageView.image = UIImage(named: "accountCreatedCheck")
            guestCheckoutImageView.image = UIImage(named: "orderUncheck")
        } else {
            haveAnAccountImageView.image = UIImage(named: "orderUncheck")
            guestCheckoutImageView.image = UIImage(named: "accountCreatedCheck")
        }
    }
    // MARK: - Font Styling Method
    func applyFont() {
        shoppingBagTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
        bagItemsCountLabel.font = UIFont(name: FontConstants.semiBold, size: 16)
        removeAllFromBagButton.titleLabel?.font = UIFont(name: FontConstants.medium, size: 14)
        removeAllFromWishlistButton.titleLabel?.font = UIFont(name: FontConstants.medium, size: 14)
        taxDutiesLabel.font = UIFont(name: FontConstants.light, size: 14)
        totalTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
        totalPriceLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
        orderSummeryTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
    }
    // MARK: - Shopping Tableview Configuration Method
    func shoppinBagTableViewConfigure() {
        shoppingBagTableView.delegate = self
        shoppingBagTableView.dataSource = self
        shoppingBagTableView.register(UINib(nibName: "ShoppingBagCustomCell", bundle: nil),
                                      forCellReuseIdentifier: "ShoppingBagCustomCell")
        shoppingBagTableView.register(UINib(nibName: "PromoCodeCustomCell", bundle: nil),
                                      forCellReuseIdentifier: "PromoCodeCustomCell")
    }
    @IBAction func proceedToCheckoutButtonAction(_ sender: UIButton) {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            let storyBoard = UIStoryboard(name: "OrderSummary", bundle: nil)
            guard let nextvc = storyBoard.instantiateViewController(
                    withIdentifier: "OrderInformationViewController") as? OrderInformationViewController else {
                return
            }
            nextvc.presenter.bagItems = presenter.shoppingBagItemArray
            self.navigationController?.pushViewController(nextvc, animated: true)
        } else {
            if checkOutOption == 0 {
                let storyboardLogin = UIStoryboard(name: "Login", bundle: nil)
                guard let loginView = storyboardLogin.instantiateViewController(
                        withIdentifier: "LoginViewController") as? LoginViewController else {
                    return
                }
                self.navigationController?.view.setNavigationTransitionLayer()
                self.navigationController?.pushViewController(loginView, animated: true)
            } else {
                let storyBoard = UIStoryboard(name: "AddAddress", bundle: nil)
                guard let nextvc = storyBoard.instantiateViewController(withIdentifier: "AddAddressViewController")
                        as? AddAddressViewController else {
                    return
                }
                nextvc.presenter.viewMode = 2
                nextvc.presenter.shoppingBagItemArray = presenter.shoppingBagItemArray
                self.navigationController?.pushViewController(nextvc, animated: true)
            }
        }
    }
    @objc func increaseQuantityButtonAction(sender: UIButton) {
        self.view.showLoader()
        presenter.increaseQuantity(for: sender.tag)
    }
    @objc func decreaseQuantityButtonAction(sender: UIButton) {
        self.view.showLoader()
        presenter.decreaseQuantity(for: sender.tag)
    }
}
// MARK: - Tableview Methods
extension ShoppingBagViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.shoppingBagItemArray.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == presenter.shoppingBagItemArray.count {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PromoCodeCustomCell",
                                                           for: indexPath) as? PromoCodeCustomCell else {
                return UITableViewCell()
            }
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShoppingBagCustomCell",
                                                           for: indexPath) as? ShoppingBagCustomCell else {
                return UITableViewCell()
            }
            let item = presenter.shoppingBagItemArray[indexPath.row]
            let bagCount = presenter.shoppingBagItemArray.count
            if item.productInStock {
                cell.applyData(item: item)
                cell.bottomSeparationView.isHidden = indexPath.row == bagCount - 1 ? true : false
                cell.colorIndicatorView.backgroundColor = Utilities.sharedInstance.hexStringToUIColor(
                    hex: item.productColorCode)
                cell.increaseQuantityButton.tag = indexPath.row
                cell.decreaseQuantityButton.tag = indexPath.row
                cell.increaseQuantityButton.addTarget(self,
                                                      action: #selector(increaseQuantityButtonAction),
                                                      for: .touchUpInside)
                cell.decreaseQuantityButton.addTarget(self,
                                                      action: #selector(decreaseQuantityButtonAction),
                                                      for: .touchUpInside)
            } else {
                cell.applyUnavailableData(item: item)
                cell.bottomSeparationView.isHidden = indexPath.row == bagCount - 1 ? true : false
                cell.colorIndicatorView.backgroundColor = Utilities.sharedInstance.hexStringToUIColor(
                    hex: item.productColorCode)
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(
            style: .destructive,
            title: "Remove") {(_: UITableViewRowAction, indexPath: IndexPath) in
            let item = self.presenter.shoppingBagItemArray[indexPath.row]
            self.view.showLoader()
            self.presenter.removeFromShoppingBag(item: item)
        }
        delete.backgroundColor = .red
        let moveToWishlist = UITableViewRowAction(
            style: .normal,
            title: "Move to wishlist") {(_: UITableViewRowAction, indexPath: IndexPath) in
            if UserDefaults.standard.bool(forKey: "isLoggedIn") {
                self.view.showLoader()
                self.presenter.moveToWishlist(index: indexPath.row)
            } else {
                self.presenter.moveToWishlist(index: indexPath.row)
                let storyboardLogin = UIStoryboard(name: "Login", bundle: nil)
                guard let loginView = storyboardLogin.instantiateViewController(
                        withIdentifier: "LoginViewController") as? LoginViewController else {
                    return
                }
                self.navigationController?.view.setNavigationTransitionLayer()
                self.navigationController?.pushViewController(loginView, animated: true)
            }
        }
        moveToWishlist.backgroundColor = .orange
        return [delete, moveToWishlist]
    }
}
extension ShoppingBagViewController: ShoppingBagPresenterProtocol {
    func alertMessage(title: String, message: String) {
        self.view.hideLoader()
        let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
            bounds: self.view.bounds,
            title: title,
            subTitle: message,
            buttonTitle: "OK") { return }
        self.view.addSubview(popUp)
    }
    func refresh() {
        self.view.hideLoader()
        orderSummeryStackView.subviews.forEach({ $0.removeFromSuperview() })
        orderSummeryPriceStackView.subviews.forEach({ $0.removeFromSuperview() })
        if presenter.shoppingBagItemArray.count == 0 {
            emptyBagView.isHidden = false
            shopBagScrollView.isHidden = true
        } else {
            let itemString = presenter.shoppingBagItemArray.count == 1 ? "ITEM" : "ITEMS"
            bagItemsCountLabel.text = "\(presenter.shoppingBagItemArray.count) \(itemString)"
            emptyBagView.isHidden = true
            shopBagScrollView.isHidden = false
            let totalAmount = String(format: "%.2f", presenter.totalPrice)
            totalPriceLabel.text = "₹ \(totalAmount)"
            for orderTitles in presenter.priceBreakup {
                let orderPriceTitleLabel = UILabel()
                orderPriceTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
                orderPriceTitleLabel.textColor = ColourConstants.hex191919
                orderPriceTitleLabel.text = orderTitles.title
                orderSummeryStackView.addArrangedSubview(orderPriceTitleLabel)
            }
            for orderPrice in presenter.priceBreakup {
                let orderPriceLabel = UILabel()
                orderPriceLabel.font = UIFont(name: FontConstants.regular, size: 14)
                orderPriceLabel.textColor = ColourConstants.hex191919
                if orderPrice.title == "PromoCode Discount" {
                    orderPriceLabel.text = "- ₹ \(orderPrice.price ?? "0.00")"
                } else {
                    orderPriceLabel.text = "₹ \(orderPrice.price ?? "0.00")"
                }
                orderSummeryPriceStackView.addArrangedSubview(orderPriceLabel)
            }
        }
        shoppingBagTableView.reloadData()
    }
}
