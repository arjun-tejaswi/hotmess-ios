//
//  ShoppingBagPresenterExtension.swift
//  Hotmess
//
//  Created by Flaxon on 28/09/21.
//

import Foundation
import SwiftyJSON

extension ShoppingBagPresenter {
    func moveToWishlist(index: Int) {
        selectedIndex = index
        let item = shoppingBagItemArray[index]
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.addToWishlist,
                                                             prametres: [
                                                                [
                                                                    "product_id": item.productID,
                                                                    "sku_id": item.stockKeepUnitID
                                                                ]
                                                             ],
                                                             type: .withAuth,
                                                             method: .POST,
                                                             completion: handleMoveToWishlistResponse)
        } else {
            let productItem = WishlishRealm()
            productItem.productID = item.productID
            productItem.stockKeepUnitID = item.stockKeepUnitID
            productItem.productColour = item.productColour
            productItem.productImageURL = item.productImageURL
            productItem.productPrice = item.productPrice
            productItem.productName = item.productName
            productItem.productSize = item.productSize
            productItem.productColourCode = item.productDesignerName
            productItem.productDescription = item.productDescription
            RealmDataManager.sharedInstance.saveProductToWishlist(productInfo: productItem)
        }
    }
    func saveAllToWishList() {
        var wishlistItems = [WishlishRealm]()
        for item in shoppingBagItemArray {
            let productItem = WishlishRealm()
            productItem.productID = item.productID
            productItem.stockKeepUnitID = item.stockKeepUnitID
            productItem.productColour = item.productColour
            productItem.productImageURL = item.productImageURL
            productItem.productPrice = item.productPrice
            productItem.productName = item.productName
            productItem.productSize = item.productSize
            productItem.productColourCode = item.productDesignerName
            productItem.productDescription = item.productDescription
            wishlistItems.append(productItem)
        }
        RealmDataManager.sharedInstance.saveMultipleToWishList(items: wishlistItems)
    }
    func increaseQuantity(for item: Int) {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            shoppingBagItemArray[item].productQuantity += 1
            updateQuantity(item: shoppingBagItemArray[item])
            getShoppingBagData()
        } else {
            RealmDataManager.sharedInstance.increaseProductQuantity(
                productId: shoppingBagItemArray[item].productID,
                skuId: shoppingBagItemArray[item].stockKeepUnitID)
            getShoppingBagData()
        }
    }
    func decreaseQuantity(for item: Int) {
        if shoppingBagItemArray[item].productQuantity > 1 {
            if UserDefaults.standard.bool(forKey: "isLoggedIn") {
                shoppingBagItemArray[item].productQuantity -= 1
                updateQuantity(item: shoppingBagItemArray[item])
                getShoppingBagData()
            } else {
                RealmDataManager.sharedInstance.decreaseProductQuantity(
                    productId: shoppingBagItemArray[item].productID,
                    skuId: shoppingBagItemArray[item].stockKeepUnitID)
                getShoppingBagData()
            }
        } else {
            delegate?.refresh()
        }
    }
    func handleCalculateOrderGuestPriceResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                delegate?.alertMessage(title: "Error", message: response["message"].string ?? "")
            } else {
                if let data = response["data"].dictionary {
                    priceBreakup.removeAll()
                    totalPrice = data["total"]?.double ?? 0.0
                    let shippingCharges = PriceListingModel.init(
                        title: "Shipping",
                        price: String(format: "%0.2f", data["shipping_charges"]?.double ?? 0.00))
                    let tax = PriceListingModel.init(
                        title: "Taxes",
                        price: String(format: "%0.2f", data["tax"]?.double ?? 0.00))
                    let subTotal = PriceListingModel.init(
                        title: "Item Subtotal",
                        price: String(format: "%0.2f", data["sub_total"]?.double ?? 0.00))
                    let expressDeliveryCharge = PriceListingModel.init(
                        title: "Express Delivery",
                        price: String(format: "%0.2f", data["express_delivery_charge"]?.double ?? 0.00))
                    let giftPackingAmount = PriceListingModel.init(
                        title: "Premium Gift Packing",
                        price: String(format: "%0.2f", data["gift_packing_amount"]?.double ?? 0.00))
                    let promoCodeDiscount = PriceListingModel.init(
                        title: "PromoCode Discount",
                        price: String(format: "%0.2f", data["promocode_discount"]?.double ?? 0.00))
                    print(expressDeliveryCharge)
                    if subTotal.price != "0.00" {
                        priceBreakup.append(subTotal)
                    }
                    if shippingCharges.price != "0.00" {
                        priceBreakup.append(shippingCharges)
                    }
                    if tax.price != "0.00" {
                        priceBreakup.append(tax)
                    }
                    if expressDeliveryCharge.price != "0.00" {
                        priceBreakup.append(expressDeliveryCharge)
                    }
                    if giftPackingAmount.price != "0.00" {
                        priceBreakup.append(giftPackingAmount)
                    }
                    if promoCodeDiscount.price != "0.00" {
                        priceBreakup.append(promoCodeDiscount)
                    }
                    delegate?.refresh()
                }
            }
        }
    }
    func shoppingBagResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.getShoppingBagData()
                        }
                    } else {
                        delegate?.alertMessage(title: "Error", message: message)
                    }
                }
            } else {
                priceBreakup.removeAll()
                let tax = PriceListingModel.init( title: "Taxes",
                    price: String(format: "%0.2f", response["tax"].double ?? 0.00))
                let subTotal = PriceListingModel.init( title: "Item Subtotal",
                    price: String(format: "%0.2f", response["sub_total"].double ?? 0.00))
                totalPrice = response["total"].double ?? 0.0
                if let items = response["data"].array {
                    shoppingBagItemArray.removeAll()
                    for item in items {
                        let shoppingBagItem = ShoppingBagRealm()
                        shoppingBagItem.productID = item["product_id"].string ?? ""
                        shoppingBagItem.stockKeepUnitID = item["sku_id"].string ?? ""
                        shoppingBagItem.productImageURL = ApiBaseURL.imageBaseURL +
                            "\(item["featured_image_url"].string ?? "")"
                        shoppingBagItem.productName = item["name"].string ?? ""
                        shoppingBagItem.productOfferPrice = item["offer_price"].int64 ?? 0
                        shoppingBagItem.productPrice = item["cost_price"].int64 ?? 0
                        shoppingBagItem.productDesignerName = item["designer_name"].string ?? ""
                        shoppingBagItem.productColour = item["colour"].string ?? ""
                        shoppingBagItem.productSize = item["size"].string ?? ""
                        shoppingBagItem.productQuantity = item["quantity"].int64 ?? 0
                        shoppingBagItem.productColorCode = item["colour_code"].string ?? ""
                        shoppingBagItem.productDescription = item["description"].string ?? ""
                        shoppingBagItem.productInStock = item["in_stock"].bool ?? false
                        shoppingBagItemArray.append(shoppingBagItem)
                    }
                }
                if subTotal.price != "0.00" {
                    priceBreakup.append(subTotal)
                }
                if tax.price != "0.00" {
                    priceBreakup.append(tax)
                }
                delegate?.refresh()
            }
        }
    }
}
