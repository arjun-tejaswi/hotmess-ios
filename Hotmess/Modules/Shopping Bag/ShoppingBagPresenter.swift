//
//  ShoppingBagPresenter.swift
//  Hotmess
//
//  Created by Akshatha on 15/07/21.
//

import Foundation
import SwiftyJSON
import RealmSwift

protocol ShoppingBagPresenterProtocol: AnyObject {
    func refresh()
    func alertMessage(title: String, message: String)
}

class ShoppingBagPresenter: NSObject {
    weak var delegate: ShoppingBagPresenterProtocol?
    var shoppingBagItemArray = [ShoppingBagRealm]()
    var priceBreakup = [PriceListingModel]()
    var selectedIndex: Int = -1
    var totalPrice: Double = 0.0
    func moveAllToWishlist() {
        var items = [[String: Any]]()
        for item in shoppingBagItemArray {
            items.append([
                "product_id": item.productID,
                "sku_id": item.stockKeepUnitID
            ])
        }
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.addToWishlist,
                                                         prametres: items,
                                                         type: .withAuth,
                                                         method: .POST,
                                                         completion: handleMoveAllToWishlistResponse)
    }
    func handleMoveAllToWishlistResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.getShoppingBagData()
                        }
                    } else {
                        self.delegate?.alertMessage(title: "Error", message: message)
                    }
                }
            } else {
                removeAllFromShoppingBag()
            }
        }
    }
    func handleMoveToWishlistResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.getShoppingBagData()
                        }
                    } else {
                        self.delegate?.alertMessage(title: "Error", message: message)
                    }
                }
            } else {
                let item = shoppingBagItemArray[selectedIndex]
                removeFromShoppingBag(item: item)
            }
        }
    }
    func updateQuantity(item: ShoppingBagRealm) {
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.updateBag,
                                                         prametres: [
                                                            "sku_id": item.stockKeepUnitID,
                                                            "quantity": item.productQuantity
                                                         ],
                                                         type: .withAuth,
                                                         method: .POST,
                                                         completion: handleUpdateBagResponse)
    }
    func handleUpdateBagResponse(response: JSON) {
        print(response)
        if let status = response["error_status"].bool {
            if status {
                if let message = response["message"].string {
                    if message == "requestForRefresh" {
                        NetworkManager.sharedInstance.refreshBearerToken {
                            self.getShoppingBagData()
                        }
                    } else {
                        self.delegate?.alertMessage(title: "Error", message: message)
                    }
                }
            } else {
                getShoppingBagData()
            }
        }
    }
    func removeAllFromShoppingBag() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            NetworkManager.sharedInstance.makeNetworkRequest(
                urlString: ApiConstants.removeAllFromBag,
                prametres: [:],
                type: .withAuth,
                method: .POST) { response in
                print(response)
                if let status = response["error_status"].bool {
                    if status {
                        if let message = response["message"].string {
                            if message == "requestForRefresh" {
                                NetworkManager.sharedInstance.refreshBearerToken {
                                    self.removeAllFromShoppingBag()
                                }
                            } else {
                                self.delegate?.alertMessage(title: "Error", message: message)
                            }
                        }
                    } else {
                        self.getShoppingBagData()
                    }
                }
            }
        } else {
            RealmDataManager.sharedInstance.deleteAllFromBag()
            getShoppingBagData()
        }
    }
    func removeFromShoppingBag(item: ShoppingBagRealm) {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            let parameters: Any = [
                [
                    "sku_id": item.stockKeepUnitID
                ]
            ]
            NetworkManager.sharedInstance.makeNetworkRequest(
                urlString: ApiConstants.removeFromBag,
                prametres: parameters,
                type: .withAuth,
                method: .POST) { response in
                print(response)
                if let status = response["error_status"].bool {
                    if status {
                        if let message = response["message"].string {
                            if message == "requestForRefresh" {
                                NetworkManager.sharedInstance.refreshBearerToken {
                                    self.removeFromShoppingBag(item: item)
                                }
                            } else {
                                self.delegate?.alertMessage(title: "Error", message: message)
                            }
                        }
                    } else {
                        self.getShoppingBagData()
                    }
                }
            }
        } else {
            RealmDataManager.sharedInstance.deleteFromBag(productId: item.productID, skuID: item.stockKeepUnitID)
            getShoppingBagData()
        }
    }
    func getShoppingBagData() {
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.shoppingBag,
                                                             prametres: [:],
                                                             type: .withAuth,
                                                             method: .POST,
                                                             completion: shoppingBagResponse)
        } else {
            // Get data from local DB
            shoppingBagItemArray.removeAll()
            shoppingBagItemArray = RealmDataManager.sharedInstance.getAllProductsFromBag()
            var lineItems = [[String: Any]]()
            for item in shoppingBagItemArray {
                lineItems.append([
                    "quantity": item.productQuantity,
                    "product_id": item.productID,
                    "sku_id": item.stockKeepUnitID
                ])
            }
            NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.calculateGuestPrice,
                                                             prametres: [
                                                             "is_gift": false,
                                                             "promocode": "",
                                                             "delivery_type": "FREE",
                                                             "line_items": lineItems
                                                             ],
                                                             type: .withoutAuth,
                                                             method: .POST,
                                                             completion: self.handleCalculateOrderGuestPriceResponse)
        }
    }
}
