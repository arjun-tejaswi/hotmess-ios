//
//  WebViewController.swift
//  Hotmess
//
//  Created by Flaxon on 04/08/21.
//

import UIKit
import WebKit

enum WebViewState {
    case TERMS
    case PRIVACY
    case CANCELPOLICY
    case RETURNPOLICY
    case FAQS
}

class WebViewController: UIViewController, WKNavigationDelegate {
    @IBOutlet weak var hotmessLogoImageView: UIImageView!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var navigationView: UIView!
    var orderTrackingURL: String = ""
    var state: WebViewState = .TERMS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.showLoader()
        webView.navigationDelegate = self
        let request = URLRequest(url: getURL())
        webView.load(request)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func getURL() -> URL {
        var url: URL!
        switch state {
        case .TERMS:
            url = URL(string: LeagalTermsURL.termsAndConditions)
        case .PRIVACY:
            url = URL(string: LeagalTermsURL.privacyAndPolicies)
        case .CANCELPOLICY:
            url = URL(string: LeagalTermsURL.cancelPolicy)
        case .RETURNPOLICY:
            url = URL(string: LeagalTermsURL.returnPolicy)
        case .FAQS:
            url = URL(string: LeagalTermsURL.faqs)
        }
        return url
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.view.hideLoader()
    }
}
