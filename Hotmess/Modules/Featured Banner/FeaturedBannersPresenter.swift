//
//  FeaturedBannersPresenter.swift
//  Hotmess
//
//  Created by Akshatha on 11/06/21.
//

import Foundation
import SwiftyJSON
import RealmSwift

class FeaturedBannersRealmModel: Object {
    @objc dynamic var featuredBannerImageUrl: String = ""
    @objc dynamic var featuredBannerTitle: String = ""
    @objc dynamic var featuredBannerSubTitle: String = ""
    @objc dynamic var featuredBannerDescription: String = ""
}
protocol FeaturedBannersPresenterProtocol: AnyObject {
    func dataNotFound(status: Bool, title: String, message: String)
    func dataFound(status: Bool, message: String)
}
class FeaturedBannersPresenter: NSObject {
    weak var delegate: FeaturedBannersPresenterProtocol?
    var featuredBannerRealms = [FeaturedBannersRealmModel]()
    func getFeaturedBannerData() {
        NetworkManager.sharedInstance.makeNetworkRequest(urlString: ApiConstants.quickLinks,
                                                         prametres: [:],
                                                         type: .withoutAuth,
                                                         method: .POST,
                                                         completion: featuredBannersResponse)
    }
    func featuredBannersResponse(response: JSON) {
        if let status = response["error_status"].bool {
            if !status {
                if let data = response["data"].array {
                    for featuredBanner in data {
                        let featuredBannerRealm = FeaturedBannersRealmModel()
                        featuredBannerRealm.featuredBannerImageUrl = ApiBaseURL.imageBaseURL +
                            "\( featuredBanner["banner_image_url"].string ?? "")"
                        featuredBannerRealm.featuredBannerTitle = featuredBanner["title"].string ?? ""
                        featuredBannerRealm.featuredBannerSubTitle = featuredBanner["sub_title"].string ?? ""
                        featuredBannerRealm.featuredBannerDescription = featuredBanner["description"].string ?? ""
                        featuredBannerRealms.append(featuredBannerRealm)
                    }
                    delegate?.dataFound(status: true, message: response[""].string ?? "")
                } else {
                    delegate?.dataNotFound(status: false, title: "Error", message: response[""].string ?? "")
                }
            }
        }
    }
}
