//
//  FeaturedBannersCollectionViewCell.swift
//  Hotmess
//
//  Created by Akshatha on 17/05/21.
//

import UIKit
class FeaturedBannersCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var featureCategoryDescriptionLabel: UILabel!
    @IBOutlet weak var hereSoonLabel: UILabel!
    @IBOutlet weak var featureCategorySubLabel: UILabel!
    @IBOutlet weak var featuredBannerImageView: UIImageView!
    @IBOutlet weak var rightLabel: UILabel!
    @IBOutlet weak var backwardImageView: UIImageView!
    @IBOutlet weak var backwardButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var forwardImageView: UIImageView!
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var featuredBannerNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
        hereSoonLabel.applyLetterSpacing(spacing: 1.8)
    }
    // MARK: - Font Style Method
    func applyFont() {
        leftLabel.font = UIFont(name: FontConstants.regular, size: 10)
        rightLabel.font = UIFont(name: FontConstants.regular, size: 10)
        featuredBannerNameLabel.font = UIFont(name: FontConstants.semiBold, size: 28)
        featureCategorySubLabel.font = UIFont(name: FontConstants.medium, size: 12)
        featureCategoryDescriptionLabel.font = UIFont(name: FontConstants.regular, size: 12)
        hereSoonLabel.font = UIFont(name: FontConstants.bold, size: 36)
    }
}
