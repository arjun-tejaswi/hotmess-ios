//
//  FeaturedBannersViewController.swift
//  Hotmess
//
//  Created by Akshatha on 17/05/21.
//

import UIKit
import LocalAuthentication

struct FeaturedbannerImage {
    var image: String?
    var name: String?
}
class FeaturedBannersViewController: UIViewController {
    @IBOutlet weak var hotmessLogoImageView: UIImageView!
    @IBOutlet weak var featuredBannerCollectionView: UICollectionView!
    var featuredBannerImageList = [FeaturedbannerImage]()
    var presenter: FeaturedBannersPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = FeaturedBannersPresenter()
        presenter.delegate = self
        if UserDefaults.standard.bool(forKey: "isLoggedIn") && UserDefaults.standard.bool(forKey: "isTouchIDEnabled") {
            initializeAppAuthentication()
        } else {
            renderUI()
        }
    }
    func renderUI() {
        applyStyling()
        configureCollectionView()
        let featuredBannerImageOne = FeaturedbannerImage(image: "homeImage", name: "HOME")
        featuredBannerImageList.append(featuredBannerImageOne)
        let featuredBannerImageTwo = FeaturedbannerImage(image: "womenImage", name: "WOMEN")
        featuredBannerImageList.append(featuredBannerImageTwo)
        let featuredBannerImageThree = FeaturedbannerImage(image: "kidsImage", name: "KID")
        featuredBannerImageList.append(featuredBannerImageThree)
        featuredBannerCollectionView.reloadData()
        self.view.showLoader()
        presenter.getFeaturedBannerData()
    }
    func initializeAppAuthentication() {
        let context: LAContext = LAContext()
        var error: NSError?
        let reason: String = "To continue using the app, please authorize."
        if context.canEvaluatePolicy(
            LAPolicy.deviceOwnerAuthenticationWithBiometrics,
            error: &error) {
            if error != nil {
                return
            }
            context.evaluatePolicy(
                LAPolicy.deviceOwnerAuthenticationWithBiometrics,
                localizedReason: reason,
                reply: {(success, error) -> Void in
                    if success {
                        DispatchQueue.main.async {
                            self.renderUI()
                        }
                    } else {
                        print("Error: \(String(describing: error))")
                    }
                })
        } else {
            renderUI()
        }
    }
    // MARK: - Statusbar Style Method
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        hotmessLogoImageView.setImageColour(color: .black)
    }
    // MARK: Collection view configuration
    func configureCollectionView() {
        let cellSize = CGSize(width: self.view.frame.width, height: featuredBannerCollectionView.frame.height)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        featuredBannerCollectionView.backgroundColor = .clear
        featuredBannerCollectionView.isPagingEnabled = true
        featuredBannerCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    @objc func backwardButtonAction(sender: UIButton) {
        // Go to previous screen
        if sender.tag > 0 {
            let page: CGFloat = CGFloat(sender.tag - 1)
            featuredBannerCollectionView.setContentOffset(CGPoint(x: page * self.view.frame.width, y: 0),
                animated: true)
        }
    }
    @objc func forwardButtonAction(sender: UIButton) {
        // Go to next screen
        if sender.tag < featuredBannerImageList.count - 1 {
            let page: CGFloat = CGFloat(sender.tag + 1)
            featuredBannerCollectionView.setContentOffset(CGPoint(x: page * self.view.frame.width, y: 0),
                animated: true)
        }
    }
}
// MARK: - Collection view methods
extension FeaturedBannersViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.featuredBannerRealms.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "FeaturedBannersCollectionViewCell",
                for: indexPath) as? FeaturedBannersCollectionViewCell else {
            return UICollectionViewCell()
        }
        let url = URL(string: presenter.featuredBannerRealms [indexPath.row].featuredBannerImageUrl)
        NukeManager.sharedInstance.setImage(url: url, imageView: cell.featuredBannerImageView, withPlaceholder: true)
        cell.featuredBannerNameLabel.text = presenter.featuredBannerRealms[indexPath.row].featuredBannerTitle
        cell.featureCategorySubLabel.text = presenter.featuredBannerRealms[indexPath.row].featuredBannerSubTitle
        cell.featureCategoryDescriptionLabel.text = presenter.featuredBannerRealms[indexPath.row]
            .featuredBannerDescription
        if indexPath.row == 0 {
            cell.rightLabel.text = presenter.featuredBannerRealms[indexPath.row + 1].featuredBannerTitle
            cell.leftLabel.isHidden = true
            cell.backwardImageView.isHidden = true
        } else if indexPath.row == 1 {
            cell.leftLabel.text = presenter.featuredBannerRealms[indexPath.row - 1].featuredBannerTitle
            cell.rightLabel.text = presenter.featuredBannerRealms[indexPath.row + 1].featuredBannerTitle
        } else {
            cell.leftLabel.text = presenter.featuredBannerRealms[indexPath.row - 1].featuredBannerTitle
            cell.rightLabel.isHidden = true
            cell.forwardImageView.isHidden = true
        }
        if cell.featuredBannerNameLabel.text == "WOMEN" {
            cell.hereSoonLabel.isHidden = true
        } else {
            cell.hereSoonLabel.isHidden = false
        }
        cell.backwardButton.tag = indexPath.row
        cell.forwardButton.tag = indexPath.row
        cell.backwardButton.addTarget(self, action: #selector(backwardButtonAction), for: .touchUpInside)
        cell.forwardButton.addTarget(self, action: #selector(forwardButtonAction), for: .touchUpInside)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard.init(name: "TabBar", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "TabBarViewController")
                as? TabBarViewController else { return }
        self.navigationController?.pushViewController(nextvc, animated: false)
    }
}
extension FeaturedBannersViewController: FeaturedBannersPresenterProtocol {
    func dataNotFound(status: Bool, title: String, message: String) {
        print("data not found")
        self.view.hideLoader()
    }
    func dataFound(status: Bool, message: String) {
        print("data  found")
        self.view.hideLoader()
        featuredBannerCollectionView.reloadData()
    }
}
