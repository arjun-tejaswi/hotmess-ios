//
//  giftCardWalletViewController.swift
//  Hotmess
//
//  Created by Akshatha on 30/08/21.
//

import UIKit

class GiftCardWalletViewController: UIViewController {
    @IBOutlet weak var giftCardsTopLabel: UILabel!
    @IBOutlet weak var giftCardWalletView: UIView!
    @IBOutlet weak var walletTitleLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var sendGiftCardView: UIView!
    @IBOutlet weak var sendGiftCardTitle: UILabel!
    @IBOutlet weak var sendGiftCardSubtitle: UILabel!
    @IBOutlet weak var giftNowButton: UIButton!
    @IBOutlet weak var receivedGiftCardView: UIView!
    @IBOutlet weak var receivedGiftCardTitle: UILabel!
    @IBOutlet weak var receivedGiftCardSubtitle: UILabel!
    @IBOutlet weak var addNowButton: UIButton!
    var presenter = GiftCardWalletPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.delegate = self
        applyFont()
        applyStyling()
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.showLoader()
        presenter.requestWalletBalance()
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        giftCardsTopLabel.textColor = ColourConstants.hexFFFFFF
        giftCardsTopLabel.applyLetterSpacing(spacing: 0.7)
        giftCardWalletView.layer.borderWidth = 1
        giftCardWalletView.layer.borderColor = ColourConstants.hexCECECE.cgColor
        walletTitleLabel.textColor = ColourConstants.hex191919
        currencyLabel.textColor = ColourConstants.hex191919
        balanceLabel.textColor = ColourConstants.hex191919
        sendGiftCardView.layer.borderWidth = 1
        sendGiftCardView.layer.borderColor = ColourConstants.hexEAEAEA.cgColor
        sendGiftCardView.backgroundColor = ColourConstants.hexF8F8F8
        sendGiftCardTitle.textColor = ColourConstants.hex191919
        sendGiftCardTitle.applyLetterSpacing(spacing: 0.8)
        sendGiftCardSubtitle.textColor = ColourConstants.hex191919
        giftNowButton.backgroundColor = ColourConstants.hex191919
        giftNowButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        giftNowButton.applyButtonLetterSpacing(spacing: 2.52)
        receivedGiftCardView.layer.borderWidth = 1
        receivedGiftCardView.layer.borderColor = ColourConstants.hexEAEAEA.cgColor
        receivedGiftCardView.backgroundColor = ColourConstants.hexF8F8F8
        receivedGiftCardTitle.textColor = ColourConstants.hex191919
        receivedGiftCardTitle.applyLetterSpacing(spacing: 0.8)
        receivedGiftCardSubtitle.textColor = ColourConstants.hex191919
        addNowButton.backgroundColor = ColourConstants.hex191919
        addNowButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        addNowButton.applyButtonLetterSpacing(spacing: 2.52)
    }
    // MARK: - Font Styling Method
    func applyFont() {
        giftCardsTopLabel.font = UIFont(name: FontConstants.medium, size: 14)
        walletTitleLabel.font = UIFont(name: FontConstants.regular, size: 16)
        currencyLabel.font = UIFont(name: FontConstants.semiBold, size: 24)
        balanceLabel.font = UIFont(name: FontConstants.semiBold, size: 24)
        sendGiftCardTitle.font = UIFont(name: FontConstants.semiBold, size: 16)
        sendGiftCardSubtitle.font = UIFont(name: FontConstants.light, size: 13)
        giftNowButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        receivedGiftCardTitle.font = UIFont(name: FontConstants.semiBold, size: 16)
        receivedGiftCardSubtitle.font = UIFont(name: FontConstants.light, size: 13)
        addNowButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
    }
    // MARK: - Back Button Action Method
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Gift Now Button Action Method
    @IBAction func giftNowButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "SendGiftCard", bundle: nil)
        guard let nextVC = storyBoard.instantiateViewController(
                withIdentifier: "SendGiftCardViewController") as? SendGiftCardViewController else {
            return
        }
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    // MARK: - Add Now Button Action Method
    @IBAction func addNowButtonAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "RedeemGiftCard", bundle: nil)
        guard let nextVC = storyBoard.instantiateViewController(
                withIdentifier: "RedeemGiftCardViewController") as? RedeemGiftCardViewController else {
            return
        }
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
}
// MARK: - Protocol Methods
extension GiftCardWalletViewController: GiftCardWalletProtocol {
    func walletResponseSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            balanceLabel.text = presenter.balanceAmount
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                            bounds: self.view.bounds,
                            title: title,
                            subTitle: message,
                            buttonTitle: "OK") { return }
                        self.view.addSubview(popUp)
        }
    }
}
