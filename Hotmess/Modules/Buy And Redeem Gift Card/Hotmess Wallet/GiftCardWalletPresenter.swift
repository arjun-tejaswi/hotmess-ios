//
//  GiftCardWalletPresenter.swift
//  Hotmess
//
//  Created by Akshatha on 06/09/21.
//

import Foundation
protocol GiftCardWalletProtocol: AnyObject {
    func walletResponseSuccess(status: Bool, title: String, message: String)
}

class GiftCardWalletPresenter: NSObject {
    weak var delegate: GiftCardWalletProtocol?
    var balanceAmount: String = ""
    func requestWalletBalance() {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.giftCardWallet,
            prametres: [:],
            type: .withAuth,
            method: .POST) { response in
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.requestWalletBalance()
                            }
                        } else {
                            self.delegate?.walletResponseSuccess(status: false, title: "Error", message: "")
                        }
                    }
                } else {
                    self.balanceAmount = response["data"].string ?? ""
                    self.delegate?.walletResponseSuccess(status: true, title: "", message: "")
                }
            }
        }
    }
}
