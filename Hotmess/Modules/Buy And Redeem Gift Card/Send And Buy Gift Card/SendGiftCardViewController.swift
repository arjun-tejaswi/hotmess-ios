//
//  SendGiftCardViewController.swift
//  Hotmess
//
//  Created by Akshatha on 31/08/21.
//

import UIKit

class PreselectedAmount {
    var value: Int?
    var isSelected: Bool?
    init(amount: Int, isSelected: Bool) {
        self.value = amount
        self.isSelected = isSelected
    }
}

class SendGiftCardViewController: UIViewController {
    @IBOutlet weak var sendGiftCardTopLabel: UILabel!
    @IBOutlet weak var hotmessCardImageview: UIImageView!
    @IBOutlet weak var giftCardCashCollectionView: UICollectionView!
    @IBOutlet weak var enterAmountTextfield: UITextField!
    @IBOutlet weak var sendgiftCardDescriptionLabel: UILabel!
    @IBOutlet weak var recipientEmailAddressTextfield: UITextField!
    @IBOutlet weak var recipientNameTextfield: UITextField!
    @IBOutlet weak var giftersNameTextfield: UITextField!
    @IBOutlet weak var enterAmountTopLabel: UILabel!
    @IBOutlet weak var recepientEmailTopLabel: UILabel!
    @IBOutlet weak var recepientNameTopLabel: UILabel!
    @IBOutlet weak var giftersNameTopLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var messageTopLabel: UILabel!
    var amountSelected: String = "1000"
    let numberFormatter = NumberFormatter()
    var amountValue = [
        PreselectedAmount.init(amount: 1000, isSelected: true),
        PreselectedAmount.init(amount: 2000, isSelected: false),
        PreselectedAmount.init(amount: 3000, isSelected: false),
        PreselectedAmount.init(amount: 5000, isSelected: false)
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        applyFont()
        applyStyling()
        setUpGiftCardCashCollectionView()
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        enterAmountTextfield.delegate = self
        sendGiftCardTopLabel.textColor = ColourConstants.hexFFFFFF
        sendGiftCardTopLabel.applyLetterSpacing(spacing: 0.7)
        applyTextFieldStyling(textFields: [
            recipientEmailAddressTextfield,
            recipientNameTextfield, giftersNameTextfield
        ])
        applyTopLabelStyling(labels: [
            recepientEmailTopLabel, recepientNameTopLabel,
            giftersNameTopLabel
        ])
        enterAmountTextfield.layer.borderWidth = 0.5
        enterAmountTextfield.layer.borderColor = ColourConstants.hexEAEAEA.cgColor
        enterAmountTextfield.textColor = ColourConstants.hex717171
        enterAmountTextfield.backgroundColor = ColourConstants.hexF8F8F8
        enterAmountTextfield.font = UIFont(name: FontConstants.regular, size: 12)
        enterAmountTextfield.setLeftPaddingPoints(14.33)
        enterAmountTextfield.setRightPaddingPoints(10)
        continueButton.backgroundColor = ColourConstants.hex1A1A1A
        continueButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        continueButton.applyButtonLetterSpacing(spacing: 2.52)
        hotmessCardImageview.image = UIImage(named: "Hotmess Card")
        enterAmountTopLabel.textColor = ColourConstants.hex9B9B9B
        enterAmountTopLabel.isHidden = true
        enterAmountTextfield.delegate = self
        messageTopLabel.textColor = ColourConstants.hex9B9B9B
        messageTextView.layer.borderWidth = 0.5
        messageTextView.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
        messageTextView.text = "Your message- Maximum 100 Character (Optional)"
        messageTextView.textColor = ColourConstants.hexCECECE
        messageTextView.font = UIFont(name: FontConstants.regular, size: 12)
        messageTextView.delegate = self
        messageTextView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 0)
        messageTopLabel.isHidden = true
    }
    func applyTextFieldStyling(textFields: [UITextField]) {
        for textField in textFields {
            textField.delegate = self
            textField.layer.borderWidth = 0.5
            textField.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
            textField.textColor = ColourConstants.hex717171
            textField.font = UIFont(name: FontConstants.regular, size: 12)
            textField.setLeftPaddingPoints(14.33)
            textField.setRightPaddingPoints(10)
        }
    }
    func applyTopLabelStyling(labels: [UILabel]) {
        for label in labels {
            label.font = UIFont(name: FontConstants.regular, size: 12)
            label.textColor = ColourConstants.hex9B9B9B
            label.isHidden = true
        }
    }
    // MARK: - Font Styling Method
    func applyFont() {
        sendGiftCardTopLabel.font = UIFont(name: FontConstants.medium, size: 14)
        sendgiftCardDescriptionLabel.font = UIFont(name: FontConstants.regular, size: 14)
        continueButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        enterAmountTopLabel.font = UIFont(name: FontConstants.regular, size: 12)
        messageTopLabel.font = UIFont(name: FontConstants.regular, size: 12)
    }
    // MARK: - Gift Card Cash CollectionView Cell SetUp
    func configureGiftCardCashCollectionView() {
        let numberOfItemsPerRow: CGFloat = 4
        let spacingBetweenCells: CGFloat = 10
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        layout.minimumLineSpacing = 10
        let totalSpacing = (2 * spacingBetweenCells) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)
        let itemWidth = (giftCardCashCollectionView.frame.width - totalSpacing)/numberOfItemsPerRow
        let cellSize = CGSize(width: itemWidth,
                              height: giftCardCashCollectionView.frame.height)
        layout.itemSize = cellSize
        layout.scrollDirection = .horizontal
        giftCardCashCollectionView.backgroundColor = .clear
        giftCardCashCollectionView.bounces = false
        giftCardCashCollectionView.isPagingEnabled = false
        giftCardCashCollectionView.setCollectionViewLayout(layout, animated: true)
    }
    // MARK: - Gift Card Cash CollectionView SetUp Method
    func setUpGiftCardCashCollectionView() {
        configureGiftCardCashCollectionView()
        giftCardCashCollectionView.delegate = self
        giftCardCashCollectionView.dataSource = self
        giftCardCashCollectionView.register(UINib(nibName: "GiftCardMoneyCustomCell", bundle: nil),
                                            forCellWithReuseIdentifier: "GiftCardMoneyCustomCell")
    }
    func showWarning() {
        if recipientEmailAddressTextfield.text == "" {
            recipientEmailAddressTextfield.layer.borderColor = ColourConstants.hexEF5353.cgColor
        }
        if recipientNameTextfield.text == "" {
            recipientNameTextfield.layer.borderColor = ColourConstants.hexEF5353.cgColor
        }
        if giftersNameTextfield.text == "" {
            giftersNameTextfield.layer.borderColor = ColourConstants.hexEF5353.cgColor
        }
    }
    // MARK: - Continue Button Action
    @IBAction func continueButtonAction(_ sender: UIButton) {
        if recipientEmailAddressTextfield.text == "" || recipientNameTextfield.text == "" ||
            giftersNameTextfield.text == "" {
            showWarning()
        } else if amountSelected == "" && enterAmountTextfield.text == "" {
            enterAmountTextfield.layer.borderColor = ColourConstants.hexEF5353.cgColor
        } else {
            let storyBoard = UIStoryboard(name: "BuyGiftCard", bundle: nil)
            guard let nextVC = storyBoard.instantiateViewController(
                    withIdentifier: "BuyGiftCardViewController") as? BuyGiftCardViewController else {
                return
            }
            if amountSelected == "" {
                nextVC.amountValue = enterAmountTextfield.text ?? ""
            } else {
                nextVC.amountValue = amountSelected
            }
            nextVC.recepientName = recipientNameTextfield.text ?? ""
            nextVC.giftersName = giftersNameTextfield.text ?? ""
            nextVC.message = messageTextView.text ?? ""
            nextVC.email = recipientEmailAddressTextfield.text ?? ""
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
    // MARK: - Back Button Action
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - CollectionView Methods
extension SendGiftCardViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return amountValue.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "GiftCardMoneyCustomCell",
                for: indexPath) as? GiftCardMoneyCustomCell else {
            return UICollectionViewCell()
        }
        numberFormatter.numberStyle = .decimal
        guard let formattedAmountValue = numberFormatter.string(
                from: NSNumber(value: amountValue[indexPath.row].value ?? 0))
        else { return UICollectionViewCell() }
        cell.amountLabel.text = formattedAmountValue
        if amountValue[indexPath.row].isSelected ?? false {
            cell.select()
        } else {
            cell.deSelect()
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var index: Int = 0
        for _ in amountValue {
            if index == indexPath.row {
                if amountValue[index].isSelected == true {
                    amountValue[index].isSelected = false
                    amountSelected = enterAmountTextfield.text ?? ""
                } else {
                    amountValue[index].isSelected = true
                    amountSelected = "\(amountValue[index].value ?? 0)"
                }
            } else {
                amountValue[index].isSelected = false
            }
            index += 1
        }
        giftCardCashCollectionView.reloadData()
    }
}
// MARK: - Textfield Delegate Method
extension SendGiftCardViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == enterAmountTextfield {
            amountSelected = ""
            var index: Int = 0
            for _ in amountValue {
                amountValue[index].isSelected = false
                index += 1
            }
        }
        giftCardCashCollectionView.reloadData()
        if textField == enterAmountTextfield {
            enterAmountTextfield.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
            enterAmountTopLabel.isHidden = false
        }
        if textField == recipientEmailAddressTextfield {
            textField.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
            recepientEmailTopLabel.isHidden = false
        }
        if textField == recipientNameTextfield {
            textField.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
            recepientNameTopLabel.isHidden = false
        }
        if textField == giftersNameTextfield {
            textField.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
            giftersNameTopLabel.isHidden = false
        }
    }
}
// MARK: - Textview Delegate Methods
extension SendGiftCardViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == messageTextView {
            messageTopLabel.isHidden = false
            textView.text = nil
            textView.textColor = ColourConstants.hex717171
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = messageTextView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        return updatedText.count <= 100
    }
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == enterAmountTextfield {
            guard CharacterSet(charactersIn: "0123456789.").isSuperset(of: CharacterSet(charactersIn: string)) else {
                return false
            }
        }
        return true
    }
}
