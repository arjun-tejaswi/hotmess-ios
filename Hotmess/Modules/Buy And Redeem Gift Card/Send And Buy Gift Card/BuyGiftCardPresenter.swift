//
//  BuyGiftCardPresenter.swift
//  Hotmess
//
//  Created by Akshatha on 01/09/21.
//

import Foundation
import SwiftyJSON
import RealmSwift

protocol BuyGiftCardPresenterProtocol: AnyObject {
    func alertMessage(status: Bool, title: String, message: String)
    func orderCreated()
    func orderPlacedSuccessfully()
    func orderPlacedFailed()
}

class BuyGiftCardPresenter: NSObject {
    weak var delegate: BuyGiftCardPresenterProtocol?
    var razorPayOrderId: String = ""
    var orderId: String = ""
    var giftCardNumber: String = ""
    func getRazorpayPayload() -> [String: Any] {
        let options: [String: Any] = [
            "modal": [
                "animation": true
            ],
            "currency": "INR",
            "description": "Gift Card",
            "order_id": razorPayOrderId,
            "image": RazorPayConstants.logoUrl,
            "name": "Hotmess Fashion",
            "theme": [
                "backdrop_color": "#123456",
                "color": "#191919"
            ]
        ]
        return options
    }
    func verifyPayment(paymentId: String) {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.verifyGiftCardPayment,
            prametres: [
                "payment_id": paymentId,
                "gift_card_number": giftCardNumber
            ], type: .withAuth,
            method: .POST) { response in
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.verifyPayment(paymentId: paymentId)
                            }
                        } else {
                            self.delegate?.orderPlacedFailed()
                        }
                    }
                } else {
                    self.delegate?.orderPlacedSuccessfully()
                }
            }
        }
    }
    func giftCardOrderCreation(amount: String, recepientName: String,
                               email: String, senderName: String, message: String) {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.createGiftCard,
            prametres: [
                "amount": amount,
                "recipient_name": recepientName,
                "recipient_email": email,
                "gifter_name": senderName,
                "message": message
            ],
            type: .withAuth,
            method: .POST) { response in
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.giftCardOrderCreation(amount: amount, recepientName: recepientName,
                                                           email: email, senderName: senderName, message: message)
                            }
                        } else {
                            self.delegate?.alertMessage(status: false, title: "Error", message: message)
                        }
                    }
                } else {
                    self.orderId =  response["data"]["order_id"].string ?? ""
                    self.giftCardNumber = response["data"]["gift_card_number"].string ?? ""
                    if let razorPayId = response["data"]["payment_info"]["id"].string {
                        self.razorPayOrderId = razorPayId
                        self.delegate?.orderCreated()
                    } else {
                        print("Could not find order id to trigger payment.")
                    }
                }
            }
        }
    }
}
