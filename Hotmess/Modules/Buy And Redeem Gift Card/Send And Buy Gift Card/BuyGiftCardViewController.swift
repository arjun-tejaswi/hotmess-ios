//
//  BuyGiftCardViewController.swift
//  Hotmess
//
//  Created by Akshatha on 31/08/21.
//

import UIKit
import Razorpay

class BuyGiftCardViewController: UIViewController {
    @IBOutlet weak var sendGiftCardTopLabel: UILabel!
    @IBOutlet weak var recepientNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var hotmessCardImageView: UIImageView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var expiryTimeLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var giftersNameLabel: UILabel!
    @IBOutlet weak var warningView: UIView!
    @IBOutlet weak var warningImageview: UIImageView!
    @IBOutlet weak var warningMessageLabel: UILabel!
    @IBOutlet weak var buyGiftCardButton: UIButton!
    var razorpay: RazorpayCheckout!
    var recepientName: String = ""
    var giftersName: String = ""
    var amountValue: String = ""
    var message: String = ""
    var email: String = ""
    var presenter = BuyGiftCardPresenter()
    let numberFormatter = NumberFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        applyFont()
        applyStyling()
        applyData()
        presenter.delegate = self
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    internal func showPaymentForm() {
        let options = presenter.getRazorpayPayload()
        razorpay.open(options, displayController: self)
    }
    func applyData() {
        if message == "Your message- Maximum 100 Character (Optional)" || message == "" {
            messageLabel.text = "HERE IS YOUR GIFT CARD. HAPPY SHOPPING!"
        } else {
            messageLabel.text = message
        }
        recepientNameLabel.text = "Hi \(recepientName)" + ","
        giftersNameLabel.text = giftersName
        numberFormatter.numberStyle = .decimal
        let convertedValue = numberFormatter.number(from: amountValue)?.floatValue
        guard let formattedAmountValue = numberFormatter.string(
                from: NSNumber(value: convertedValue ?? 0.00)) else { return }
        amountLabel.text = "₹ \(formattedAmountValue)"
        buyGiftCardButton.setTitle("BUY GIFT CARD OF ₹\(formattedAmountValue)", for: .normal)
        buyGiftCardButton.applyButtonLetterSpacing(spacing: 2.52)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        sendGiftCardTopLabel.textColor = ColourConstants.hexFFFFFF
        sendGiftCardTopLabel.applyLetterSpacing(spacing: 0.7)
        recepientNameLabel.textColor = ColourConstants.hex191919
        messageLabel.applyLetterSpacing(spacing: 1)
        messageLabel.textColor = ColourConstants.hex191919
        hotmessCardImageView.image = UIImage(named: "Hotmess Card")
        amountLabel.textColor = ColourConstants.hex191919
        expiryDateLabel.textColor = ColourConstants.hex191919
        expiryTimeLabel.textColor = ColourConstants.hex191919
        fromLabel.textColor = ColourConstants.hex191919
        giftersNameLabel.textColor = ColourConstants.hex191919
        warningMessageLabel.textColor = ColourConstants.hex191919
        warningView.layer.borderWidth = 1
        warningView.layer.borderColor = ColourConstants.hexEAEAEA.cgColor
        warningView.backgroundColor = ColourConstants.hexF8F8F8
        buyGiftCardButton.backgroundColor = ColourConstants.hex191919
        buyGiftCardButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        warningImageview.image = UIImage(named: "warning")
    }
    // MARK: - Font Styling Method
    func applyFont() {
        sendGiftCardTopLabel.font = UIFont(name: FontConstants.medium, size: 14)
        recepientNameLabel.font = UIFont(name: FontConstants.regular, size: 16)
        messageLabel.font = UIFont(name: FontConstants.semiBold, size: 20)
        amountLabel.font = UIFont(name: FontConstants.semiBold, size: 24)
        expiryDateLabel.font = UIFont(name: FontConstants.regular, size: 14)
        expiryTimeLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        fromLabel.font = UIFont(name: FontConstants.regular, size: 14)
        giftersNameLabel.font = UIFont(name: FontConstants.semiBold, size: 14)
        warningMessageLabel.font = UIFont(name: FontConstants.light, size: 12)
        buyGiftCardButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
    }
    // MARK: - Back Button Action Method
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Buy And Gift Button Action Method
    @IBAction func buyAndGiftButtonAction(_ sender: UIButton) {
        self.view.showLoader()
        presenter.giftCardOrderCreation(amount: amountValue,
                                        recepientName: recepientName,
                                        email: email,
                                        senderName: giftersName,
                                        message: messageLabel.text ?? "")
    }
    func goToOrderStatus(status: OrderStatus) {
        let storyBoard = UIStoryboard.init(name: "GiftCardStatus", bundle: nil)
        guard let nextvc = storyBoard.instantiateViewController(
                withIdentifier: "GiftCardStatusViewController")
                as? GiftCardStatusViewController else { return }
        nextvc.orderStatus = status
        nextvc.orderID = presenter.orderId
        self.navigationController?.pushViewController(nextvc, animated: true)
    }
}
// MARK: - Protocol Methods
extension BuyGiftCardViewController: BuyGiftCardPresenterProtocol {
    func alertMessage(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if !status {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                            bounds: self.view.bounds,
                            title: title,
                            subTitle: message,
                            buttonTitle: "OK") { return }
                        self.view.addSubview(popUp)
        }
    }
    func orderCreated() {
        self.view.hideLoader()
        razorpay = RazorpayCheckout.initWithKey(RazorPayConstants.apiKey, andDelegate: self)
        showPaymentForm()
    }
    func orderPlacedSuccessfully() {
        self.view.hideLoader()
        goToOrderStatus(status: .SUCCESS)
    }
    func orderPlacedFailed() {
        self.view.hideLoader()
        goToOrderStatus(status: .FAILURE)
    }
}
// MARK: - Protocol Methods
extension BuyGiftCardViewController: RazorpayPaymentCompletionProtocol {
    func onPaymentError(_ code: Int32, description str: String) {
        self.view.hideLoader()
        goToOrderStatus(status: .FAILURE)
        print("Payment failed: \(code) \(str)")
    }
    func onPaymentSuccess(_ paymentId: String) {
        self.view.showLoader()
        print("Payment successful: \(paymentId)")
        presenter.verifyPayment(paymentId: paymentId)
    }
}
