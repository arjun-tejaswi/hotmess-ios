//
//  GiftCardStatusViewController.swift
//  Hotmess
//
//  Created by Akshatha on 02/09/21.
//

import UIKit

class GiftCardStatusViewController: UIViewController {
    @IBOutlet weak var giftCardOrderSuccessView: UIView!
    @IBOutlet weak var orderSuccessTitleLabel: UILabel!
    @IBOutlet weak var orderIDLabel: UILabel!
    @IBOutlet weak var orderSuccessSubtitleOne: UILabel!
    @IBOutlet weak var orderSuccessSubtitleTwo: UILabel!
    @IBOutlet weak var goToGiftCardButton: UIButton!
    @IBOutlet weak var giftCardOrderFailureView: UIView!
    @IBOutlet weak var orderFailureTitle: UILabel!
    @IBOutlet weak var orderFailureSubtitle: UILabel!
    @IBOutlet weak var tryAgainButton: UIButton!
    var orderStatus: OrderStatus = .FAILURE
    var orderID: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        applyFont()
        applyStyling()
        switch orderStatus {
        case .SUCCESS:
            giftCardOrderFailureView.isHidden = true
            giftCardOrderSuccessView.isHidden = false
            orderIDLabel.text = "Order ID \(orderID)"
        case .FAILURE:
            giftCardOrderSuccessView.isHidden = true
            giftCardOrderFailureView.isHidden = false
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - Try Again Button Action
    @IBAction func tryAgainButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Go To Gift Card Button Action
    @IBAction func goToGiftCardButtonAction(_ sender: UIButton) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as
            [UIViewController]
        self.navigationController!.popToViewController(
            viewControllers[viewControllers.count - 4],
            animated: true)
    }
    // MARK: - Font Styling Method
    func applyFont() {
        orderSuccessTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        orderIDLabel.font = UIFont(name: FontConstants.regular, size: 18)
        orderSuccessSubtitleOne.font = UIFont(name: FontConstants.light, size: 14)
        orderSuccessSubtitleTwo.font = UIFont(name: FontConstants.light, size: 14)
        goToGiftCardButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
        orderFailureTitle.font = UIFont(name: FontConstants.semiBold, size: 18)
        orderFailureSubtitle.font = UIFont(name: FontConstants.light, size: 14)
        tryAgainButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        orderSuccessTitleLabel.textColor = ColourConstants.hex191919
        orderIDLabel.textColor = ColourConstants.hex191919
        orderSuccessSubtitleOne.textColor = ColourConstants.hex191919
        orderSuccessSubtitleTwo.textColor = ColourConstants.hex191919
        goToGiftCardButton.backgroundColor = ColourConstants.hex191919
        goToGiftCardButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        goToGiftCardButton.applyButtonLetterSpacing(spacing: 2.52)
        goToGiftCardButton.setTitle("BACK TO GIFT CARD", for: .normal)
        orderFailureTitle.textColor = ColourConstants.hex191919
        orderFailureSubtitle.textColor = ColourConstants.hex191919
        tryAgainButton.backgroundColor = ColourConstants.hex191919
        tryAgainButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        tryAgainButton.applyButtonLetterSpacing(spacing: 2.52)
        tryAgainButton.setTitle("TRY AGAIN", for: .normal)
    }
}
