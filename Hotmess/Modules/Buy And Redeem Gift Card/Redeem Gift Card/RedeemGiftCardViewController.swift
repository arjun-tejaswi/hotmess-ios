//
//  RedeemGiftCardViewController.swift
//  Hotmess
//
//  Created by Akshatha on 31/08/21.
//

import UIKit

class RedeemGiftCardViewController: UIViewController {
    @IBOutlet weak var receivedGiftCardTopLabel: UILabel!
    @IBOutlet weak var receivedGiftCardTitleLabel: UILabel!
    @IBOutlet weak var giftCardNumberTextfield: UITextField!
    @IBOutlet weak var giftCardPinTextfield: UITextField!
    @IBOutlet weak var warningMessageview: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var warningMessageLabel: UILabel!
    @IBOutlet weak var enterPinTopLabel: UILabel!
    @IBOutlet weak var giftCardNumberTopLabel: UILabel!
    var presenter = RedeemGiftCardPresenter()
    override func viewDidLoad() {
        super.viewDidLoad()
        warningMessageview.isHidden = true
        applyStyling()
        applyFont()
        presenter.delegate = self
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        giftCardNumberTextfield.delegate = self
        giftCardPinTextfield.delegate = self
        receivedGiftCardTopLabel.textColor = ColourConstants.hexFFFFFF
        receivedGiftCardTopLabel.applyLetterSpacing(spacing: 0.7)
        receivedGiftCardTitleLabel.textColor = ColourConstants.hex191919
        warningMessageview.layer.backgroundColor = ColourConstants.hexFFE8E8.cgColor
        warningMessageLabel.textColor = ColourConstants.hexEF5353
        continueButton.backgroundColor = ColourConstants.hex1A1A1A
        continueButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        continueButton.applyButtonLetterSpacing(spacing: 2.52)
        applyTextFieldStyling(textFields: [
            giftCardNumberTextfield,
            giftCardPinTextfield
        ])
        applyTopLabelStyling(labels: [
            enterPinTopLabel, giftCardNumberTopLabel
        ])
    }
    func applyTextFieldStyling(textFields: [UITextField]) {
        for textField in textFields {
            textField.layer.borderWidth = 0.5
            textField.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
            textField.font = UIFont(name: FontConstants.regular, size: 12)
            textField.setLeftPaddingPoints(14.33)
            textField.setRightPaddingPoints(10)
        }
    }
    func applyTopLabelStyling(labels: [UILabel]) {
        for label in labels {
            label.font = UIFont(name: FontConstants.regular, size: 12)
            label.textColor = ColourConstants.hex9B9B9B
            label.isHidden = true
        }
    }
    // MARK: - Font Styling Method
    func applyFont() {
        receivedGiftCardTopLabel.font = UIFont(name: FontConstants.medium, size: 14)
        receivedGiftCardTitleLabel.font = UIFont(name: FontConstants.regular, size: 14)
        warningMessageLabel.font = UIFont(name: FontConstants.regular, size: 13)
        continueButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 14)
    }
    // MARK: - Back Button Action Method
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Continue Button Action Method
    @IBAction func continueButtonAction(_ sender: UIButton) {
        if giftCardNumberTextfield.text == "" || giftCardPinTextfield.text == "" {
            showWarning()
        } else {
            self.view.showLoader()
            presenter.redeemGiftCard(giftCardNumber: giftCardNumberTextfield.text ?? "",
                                     giftCardPin: giftCardPinTextfield.text ?? "" )
        }
    }
    func showWarning() {
        if giftCardNumberTextfield.text == "" {
            giftCardNumberTextfield.layer.borderColor = ColourConstants.hexEF5353.cgColor
        }
        if giftCardPinTextfield.text == "" {
            giftCardPinTextfield.layer.borderColor = ColourConstants.hexEF5353.cgColor
        }
    }
}
// MARK: - Protocol Methods
extension RedeemGiftCardViewController: RedeemGiftCardPresenterProtocol {
    func redeemSuccess(status: Bool, title: String, message: String) {
        if !status {
            self.view.hideLoader()
            warningMessageview.isHidden = false
            warningMessageLabel.text = message
        } else {
            self.view.hideLoader()
            let storyBoard = UIStoryboard(name: "RedeemGiftCardSuccess", bundle: nil)
            guard let nextVC = storyBoard.instantiateViewController(
                    withIdentifier: "RedeemGiftCardSuccessViewController")
                    as? RedeemGiftCardSuccessViewController else {
                return
            }
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
}
extension RedeemGiftCardViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == giftCardNumberTextfield {
            giftCardNumberTextfield.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
            giftCardNumberTopLabel.isHidden = false
        }
        if textField == giftCardPinTextfield {
            giftCardPinTextfield.layer.borderColor = ColourConstants.hex8B8B8B.cgColor
            enterPinTopLabel.isHidden = false
        }
    }
}
