//
//  RedeemGiftCardPresenter.swift
//  Hotmess
//
//  Created by Akshatha on 06/09/21.
//

import Foundation

protocol RedeemGiftCardPresenterProtocol: AnyObject {
    func redeemSuccess(status: Bool, title: String, message: String)
}

class RedeemGiftCardPresenter: NSObject {
    weak var delegate: RedeemGiftCardPresenterProtocol?
    func redeemGiftCard(giftCardNumber: String, giftCardPin: String) {
        NetworkManager.sharedInstance.makeNetworkRequest(
            urlString: ApiConstants.redeemGiftCard,
            prametres: [
                "gift_card_number": giftCardNumber,
                "pin": giftCardPin
            ],
            type: .withAuth,
            method: .POST) { response in
            if let status = response["error_status"].bool {
                if status {
                    if let message = response["message"].string {
                        if message == "requestForRefresh" {
                            NetworkManager.sharedInstance.refreshBearerToken {
                                self.redeemGiftCard(giftCardNumber: giftCardNumber, giftCardPin: giftCardPin)
                            }
                        } else {
                            self.delegate?.redeemSuccess(status: false, title: "Error", message: message)
                        }
                    }
                } else {
                    self.delegate?.redeemSuccess(status: true, title: "", message: "")
                }
            }
        }
    }
}
