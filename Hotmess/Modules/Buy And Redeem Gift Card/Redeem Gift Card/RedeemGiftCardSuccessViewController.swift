//
//  RedeemGiftCardSuccessViewController.swift
//  Hotmess
//
//  Created by Akshatha on 02/09/21.
//

import UIKit

class RedeemGiftCardSuccessViewController: UIViewController {
    @IBOutlet weak var giftCardAddedImageview: UIImageView!
    @IBOutlet weak var giftCardAddedTitleLabel: UILabel!
    @IBOutlet weak var balanceCheckButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        applyFont()
        applyStyling()
        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    // MARK: - Font Styling Method
    func applyFont() {
        giftCardAddedTitleLabel.font = UIFont(name: FontConstants.semiBold, size: 18)
        balanceCheckButton.titleLabel?.font = UIFont(name: FontConstants.regular, size: 13)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        giftCardAddedImageview.image = UIImage(named: "Order Confirmed")
        giftCardAddedTitleLabel.textColor = ColourConstants.hex191919
        balanceCheckButton.backgroundColor = ColourConstants.hex191919
        balanceCheckButton.setTitleColor(ColourConstants.hexFFFFFF, for: .normal)
        balanceCheckButton.applyButtonLetterSpacing(spacing: 2.52)
    }
    // MARK: - Back Button Action Method
    @IBAction func balanceCheckButtonAction(_ sender: UIButton) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as
            [UIViewController]
        self.navigationController!.popToViewController(
            viewControllers[viewControllers.count - 3],
            animated: true)
    }
}
