//
//  SearchCustomCell.swift
//  Hotmess
//
//  Created by Flaxon on 18/05/21.
//

import UIKit

class SearchCustomCell: UITableViewCell {
    @IBOutlet weak var searchSubTitleLabel: UILabel!
    @IBOutlet weak var searchTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        applyFont()
    }
    // MARK: - Font Style Method
    func applyFont() {
        searchTitleLabel.font = UIFont(name: FontConstants.regular, size: 13)
        searchSubTitleLabel.font = UIFont(name: FontConstants.light, size: 10)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
