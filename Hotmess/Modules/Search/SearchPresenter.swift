//
//  SearchPresenter.swift
//  Hotmess
//
//  Created by Flaxon on 16/09/21.
//

import Foundation
import SwiftyJSON

protocol SearchPresenterProtocol: AnyObject {
    func productSearchSuccess(status: Bool, title: String, message: String)
}

class SearchPresenter: NSObject {
    weak var delegate: SearchPresenterProtocol?
    var searchProductList = [ProductListRealm]()
    var searchItemList = [SearchTextRealm]()
    var didReachPageEnd: Bool = false
    func requestForSearch(searchText: String, isFromRecent: Bool = false) {
        if searchText != "" && !searchText.trimmingCharacters(in: .whitespaces).isEmpty {
            let searchItem = SearchTextRealm()
            searchItem.searchText = searchText
            if !isFromRecent {
                RealmDataManager.sharedInstance.saveSearchText(searchText: searchItem)
            }
//            print(RealmDataManager.sharedInstance.getAllProductsFromTextSearch())
            delegate?.productSearchSuccess(status: true, title: "", message: searchText)
        } else {
            delegate?.productSearchSuccess(status: false, title: "Error", message: "Please Refine your Search!")
        }
    }
}
