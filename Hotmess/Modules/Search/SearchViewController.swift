//
//  SearchViewController.swift
//  Hotmess
//
//  Created by Flaxon on 18/05/21.
//

import UIKit
import Speech
import AVKit

class SearchViewController: UIViewController {
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var speechButton: UIButton!
    @IBOutlet weak var uploadPhotoLabel: UILabel!
    @IBOutlet weak var takePhotoLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchHistoryTableView: UITableView!
    let speechRecognizer = SFSpeechRecognizer()
    var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    var recognitionTask: SFSpeechRecognitionTask?
    let audioEngine = AVAudioEngine()
    let searchItemList = RealmDataManager.sharedInstance.getAllProductsFromTextSearch()
    var presenter: SearchPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = SearchPresenter()
        presenter.delegate = self
        applyStyling()
        applyFont()
        searchButton.isHidden = false
        searchButton.isEnabled = true
        searchHistoryTableView.reloadData()
    }
    // MARK: - Statusbar Style Method
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        searchHistoryTableView.reloadData()
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - UI Styling Method
    func applyStyling() {
        searchTextField.setLeftPaddingPoints(10)
    }
    // MARK: - Font Style Method
    func applyFont() {
        searchTextField.font = UIFont(name: FontConstants.regular, size: 14)
        takePhotoLabel.font = UIFont(name: FontConstants.regular, size: 14)
        uploadPhotoLabel.font = UIFont(name: FontConstants.regular, size: 14)
    }
    @IBAction func searchButtonAction(_ sender: UIButton) {
        presenter.requestForSearch(searchText: searchTextField.text ?? "")
    }
    @IBAction func speechButtonAction(_ sender: UIButton) {
        searchButton.isHidden = true
        searchButton.isEnabled = false
        var startSearch: Bool = false
        setupSpeech()
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            speechButton.isEnabled = false
            speechButton.setImage(UIImage(named: "microphoneLine_Icon"), for: .normal)
            startSearch = true
            searchButton.isHidden = false
            searchButton.isEnabled = true
            if startSearch && searchTextField.text != "Say something, I'm listening!" {
                presenter.requestForSearch(searchText: searchTextField.text ?? "")
            } else {
                let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                    bounds: self.view.bounds,
                    title: "Error",
                    subTitle: "Please Refine your Search!",
                    buttonTitle: "OK") {
                    self.searchTextField.text = ""
                }
                self.view.addSubview(popUp)
            }
        } else {
            startSearch = false
            speechButton.setImage(UIImage(named: "search_Black_Icon"), for: .normal)
            startRecording()
        }
    }
    // MARK: - Take Photo Button Action
    @IBAction func takePhotoButtonAction(_ sender: UIButton) {
        let photo = UIImagePickerController()
        photo.sourceType = .camera
        photo.delegate = self
        present(photo, animated: true)
    }
    // MARK: - Upload Photo Button Action
    @IBAction func uploadPhotoButtonAction(_ sender: UIButton) {
        let photo = UIImagePickerController()
        photo.sourceType = .savedPhotosAlbum
        photo.delegate = self
        present(photo, animated: true)
    }
    func startRecording() {
        // Clear all previous session data and cancel task
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        // Create instance of audio session to record voice
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.record,
                                         mode: AVAudioSession.Mode.measurement,
                                         options: AVAudioSession.CategoryOptions.defaultToSpeaker)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        self.recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        let inputNode = audioEngine.inputNode
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        recognitionRequest.shouldReportPartialResults = true
        self.recognitionTask = speechRecognizer?.recognitionTask(
            with: recognitionRequest, resultHandler: { (result, error) in
                var isFinal = false
                if result != nil {
                    self.searchTextField.text = result?.bestTranscription.formattedString
                    isFinal = (result?.isFinal)!
                }
                if error != nil || isFinal {
                    self.audioEngine.stop()
                    inputNode.removeTap(onBus: 0)
                    self.recognitionRequest = nil
                    self.recognitionTask = nil
                    self.speechButton.isEnabled = true
                }
            })
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0,
                             bufferSize: 1024,
                             format: recordingFormat) { (buffer, _) in
            self.recognitionRequest?.append(buffer)
        }
        self.audioEngine.prepare()
        do {
            try self.audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        self.searchTextField.text = "Say something, I'm listening!"
    }
    func setupSpeech() {
        self.speechButton.isEnabled = false
        self.speechRecognizer?.delegate = self
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            var isButtonEnabled = false
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            @unknown default:
                print("Not Type of Speech")
                fatalError()
            }
            OperationQueue.main.addOperation {
                self.speechButton.isEnabled = isButtonEnabled
            }
        }
    }
}
// MARK: - TableView Methods
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchItemList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
                withIdentifier: "SearchCustomCell",
                for: indexPath) as? SearchCustomCell else {
            return UITableViewCell()
        }
        cell.searchTitleLabel.text = searchItemList.reversed()[indexPath.row].searchText
        cell.searchSubTitleLabel.isHidden = true
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.requestForSearch(searchText: searchItemList.reversed()[indexPath.row].searchText, isFromRecent: true)
    }
}
extension SearchViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ photo: UIImagePickerController) {
        photo.dismiss(animated: true, completion: nil)
    }
}
extension SearchViewController: SFSpeechRecognizerDelegate {
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            self.speechButton.isEnabled = true
        } else {
            self.speechButton.isEnabled = false
        }
    }
}

extension SearchViewController: SearchPresenterProtocol {
    func productSearchSuccess(status: Bool, title: String, message: String) {
        self.view.hideLoader()
        if status {
            searchHistoryTableView.reloadData()
            let storyBoard = UIStoryboard(name: "ProductListing", bundle: nil)
            guard let nextvc = storyBoard.instantiateViewController(
                    withIdentifier: "ProductListViewController")
                    as? ProductListViewController else { return }
            nextvc.presenter.searchText = message
            nextvc.presenter.isFromSearch = true
            self.navigationController?.pushViewController(nextvc, animated: true)
        } else {
            let popUp = AlertManager.sharedInstance.showCustomAlertPopUp(
                bounds: self.view.bounds,
                title: title,
                subTitle: message,
                buttonTitle: "OK") { return }
            self.view.addSubview(popUp)
        }
    }
}
