//
//  AppDelegate.swift
//  Hotmess
//
//  Created by Cedan Misquith on 12/05/21.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import RealmSwift
import Firebase
import GoogleSignIn
import Branch

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var navController: UINavigationController?
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions
                        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if UserDefaults.standard.bool(forKey: "notFirstInApp") == false {
            let storyBoard = UIStoryboard(name: "OnBoarding", bundle: nil)
            let launchViewController = storyBoard.instantiateViewController(
                withIdentifier: "OnBoardingViewController")
            let navigationController = UINavigationController(rootViewController: launchViewController)
            navController = navigationController
            self.window?.rootViewController = navController
        } else {
            let storyBoard = UIStoryboard(name: "FeaturedBanners", bundle: nil)
            let launchViewController = storyBoard.instantiateViewController(
                withIdentifier: "FeaturedBannersViewController")
            let navigationController = UINavigationController(rootViewController: launchViewController)
            navController = navigationController
            navController?.isNavigationBarHidden = true
            self.window?.rootViewController = navController
        }
        // MARK: Initializing Realm DB
        let config = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { _, oldSchemaVersion in
                // We haven't migrated anything yet, so oldSchemaVersion == 0
                if oldSchemaVersion < 1 {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
            })
        let freshchatConfig: FreshchatConfig = FreshchatConfig.init(
            appID: "c0528b45-afb5-4d5a-96e7-1e4b94ba7931",
            andAppKey: "7d4ab117-7912-4a67-ac43-46a425a7f1da")
        freshchatConfig.domain = "msdk.in.freshchat.com"
        Freshchat.sharedInstance().initWith(freshchatConfig)
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        IQKeyboardManager.shared.enable = true
        NewRelic.start(withApplicationToken: "AAb5fdfdebfe050238f3d2927093c8904332dc7440-NRMA")
        GIDSignIn.sharedInstance().clientID = "255777101877-mnt2qrbpp8vqveivm13oiq3gj498tkuf.apps.googleusercontent.com"
        FirebaseApp.configure()
        NetworkManager.sharedInstance.updateUserIdToken()
        initialzeBranch(launchOptions)
        return true
    }
    fileprivate func handleNavigationFor(_ navigationType: DeepLinkNavigationType, _ data: [String: Any]) {
        switch navigationType {
        case .SHAREDWISHLIST:
            let storyBoard = UIStoryboard(name: "Wishlist", bundle: nil)
            guard let launchViewController = storyBoard.instantiateViewController(
                    withIdentifier: "SharedWishlistViewController") as? SharedWishlistViewController else {
                return
            }
            launchViewController.presenter.wishlistID = data["wishlist_id"] as? String ?? ""
            launchViewController.presenter.isFromDeepLink = true
            let navigationController = UINavigationController(rootViewController: launchViewController)
            self.navController = navigationController
            self.navController?.isNavigationBarHidden = true
            self.window?.rootViewController = self.navController
        case .VIEWPRODUCT:
            let storyBoard = UIStoryboard(name: "ProductOrderDetail", bundle: nil)
            guard let launchViewController = storyBoard.instantiateViewController(
                    withIdentifier: "ProductOrderDetailViewController")
                    as? ProductOrderDetailViewController else {
                return
            }
            launchViewController.presenter.productSKUID = data["sku_id"] as? String ?? ""
            launchViewController.presenter.productID = data["product_id"] as? String ?? ""
            launchViewController.presenter.isFromDeepLink = true
            let navigationController = UINavigationController(rootViewController: launchViewController)
            self.navController = navigationController
            self.navController?.isNavigationBarHidden = true
            self.window?.rootViewController = self.navController
        case .VIEWEDITORIAL:
            let storyBoard = UIStoryboard(name: "EditorialDetail", bundle: nil)
            guard let launchViewController = storyBoard.instantiateViewController(
                    withIdentifier: "EditorialDetailViewController")
                    as? EditorialDetailViewController else {
                return
            }
            launchViewController.presenter.categoryID = data["category_id"] as? String ?? ""
            launchViewController.presenter.editorialID = data["editorial_id"] as? String ?? ""
            launchViewController.presenter.isFromDeepLink = true
            let navigationController = UINavigationController(rootViewController: launchViewController)
            self.navController = navigationController
            self.navController?.isNavigationBarHidden = true
            self.window?.rootViewController = self.navController
        }
    }
    fileprivate func initialzeBranch(_ launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        Branch.setUseTestBranchKey(true)
        Branch.getInstance().enableLogging()
        Branch.getInstance().initSession(launchOptions: launchOptions) { (params, error) in
            // do stuff with deep link data (nav to page, display content, etc)
            if error != nil {
                print(error ?? "")
            } else {
                print(params ?? "")
                DeeplinkManager.sharedInstance.handleNavigationForBranchDeeplink(
                    result: params) { (navigationType, data) in
                    self.handleNavigationFor(navigationType, data)
                }
            }
        }
    }
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        return Branch.getInstance().continue(userActivity)
    }
    // MARK: - Facebook SDK Initialzation
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        return Branch.getInstance().application(app, open: url, options: options)
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Freshchat.sharedInstance().setPushRegistrationToken(deviceToken)
    }
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        Branch.getInstance().handlePushNotification(userInfo)
        if Freshchat.sharedInstance().isFreshchatNotification(userInfo) {
            Freshchat.sharedInstance().handleRemoteNotification(userInfo, andAppstate: application.applicationState)
        }
    }
    // MARK: Start App
    func restartApp() {
        let storyBoard = UIStoryboard(name: "FeaturedBanners", bundle: nil)
        let launchViewController = storyBoard.instantiateViewController(
            withIdentifier: "FeaturedBannersViewController")
        let navigationController = UINavigationController(rootViewController: launchViewController)
        navController = navigationController
        navController?.isNavigationBarHidden = true
        self.window?.rootViewController = navController
    }
}
