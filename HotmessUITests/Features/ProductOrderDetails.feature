Feature: Add to Bag Button Action

Scenario: User added items to the shopping cart
    Given the user on the Product Detail Screen
    When the user tap on ADD TO BAG button
    Then the shoping cart count should be increment by 1
    And the message should be displayed on the screen that item has been added successfully

Feature: Share the item

Scenario: Display Share Popup
    Given the user on the Product Detail Screen
    When the user tap on Share button
    Then the share popup should be displayed on the screen

Feature: More Colours to select

Scenario: Display colour selection Popup
    Given the user on the Product Detail Screen
    When the user tap on more colour arrow button
    Then the colour selection popup should be displayed on the screen

Feature: More Size to select

Scenario: Display size selection Popup
    Given the user on the Product Detail Screen
    When the user tap on select your size button
    Then the size selection popup should be displayed on the screen

Feature: Size Guide Info Popup

Scenario: Display Size Guide Popup
    Given the user on the Product Detail Screen
    When the user tap on size guide button
    Then the size guide popup should be displayed on the screen
    
Feature: Search Button Action

Scenario: User wants to search the item
    Given the user on the Product Detail Screen
    When the user tap on search button
    Then the Search screen should be displayed successfully
