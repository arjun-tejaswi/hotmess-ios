Feature: Selecting The Featured banner

Scenario: Featured Banner Selection
    Given that a user lands on the home screen/featured Banners
    When the user selects from one of the categories from the featured banners
    Then the user should be navigated to the particular category of list of items for shopping.

Feature : Swipe Through The Featured Banner

Scenario : Featured Banner
    Given that a user is on the featured banners section
    When the user swipes right/left
    Then user should be view Next/ Previous category.
