Feature: Skip On-Boarding Screen

Scenario: Skip On-Boarding Screen
    Given that the user lands on On-Boarding screen
    When the user is on first/second screen user should have the skip button
    Then user can procced on to the home page of the app.
    
Scenario: Continue On-Boarding Screen
    Given that the user lands on On-Boarding screen
    When the user on the third screen user should have the continue button
    Then user can procced on to the home page of the app.

Scenario: On-Boarding Screen Navigation
    Given that the user lands on On-Boarding screen
    When the user swipes left user should be able to see previous screen
    Then the user swipes right user should be able to see the next screen
