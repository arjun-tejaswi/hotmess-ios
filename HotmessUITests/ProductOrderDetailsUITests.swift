//
//  ProductOrderDetailsUITests.swift
//  HotmessUITests
//
//  Created by Flaxon on 19/05/21.
//

import XCTest
import XCTest_Gherkin

class ProductOrderDetailsUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        XCUIApplication().launch()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
}
