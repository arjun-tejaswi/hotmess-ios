//
//  OnBoardingUITests.swift
//  HotmessUITests
//
//  Created by Akshatha on 24/05/21.
//

import XCTest
import XCTest_Gherkin

class OnBoardingUITests: XCTestCase {
    override func setUpWithError() throws {
    }
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    func testSkipOnBoardingScreen() {
        NativeRunner.runScenario(featureFile: "Features/OnBoarding.feature",
                                 scenario: "Skip On-Boarding Screen",
                                 testCase: self)
    }
    func testContinueOnBoardingScreen() {
        NativeRunner.runScenario(featureFile: "Features/OnBoarding.feature",
                                 scenario: "Continue On-Boarding Screen",
                                 testCase: self)
    }
    func testNavigateOnBoardingScreen() {
        NativeRunner.runScenario(featureFile: "Features/OnBoarding.feature",
                                 scenario: "On-Boarding Screen Navigation",
                                 testCase: self)
    }
}

class UIStepDefinerOnBoarding: StepDefiner {
    override func defineSteps() {
        let app = XCUIApplication()
        step("that the user lands on On-Boarding screen") {
            app.launch()
        }
        step("the user is on first/second screen user should have the skip button") {
            let skip = app.collectionViews.buttons["NavigateButton"]
            skip.tap()
        }
        step("the user on the third screen user should have the continue button") {
            let collectionViewsQuery = app.collectionViews.children(
                matching: .cell).element(
                    boundBy: 0).otherElements["CollectionViewCell"]
            collectionViewsQuery.swipeLeft()
            collectionViewsQuery.swipeLeft()
            let contnue = app.collectionViews.buttons["NavigateButton"]
            contnue.tap()
        }
        step("user can procced on to the home page of the app.") {
            let hotmess = app.staticTexts["# H O T M E S S"]
            if hotmess.exists {
                app.terminate()
            } else {
                XCTFail("Failed")
            }
        }
        let collectionViewsQuery = app.collectionViews.children(matching: .cell).element(boundBy: 0)
            .otherElements["CollectionViewCell"]
        step("the user swipes left user should be able to see previous screen") {
            collectionViewsQuery.swipeLeft()
            collectionViewsQuery.swipeLeft()
        }
        step("the user swipes right user should be able to see the next screen") {
            collectionViewsQuery.swipeRight()
            collectionViewsQuery.swipeRight()
        }
    }
}
